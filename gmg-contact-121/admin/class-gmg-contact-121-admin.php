<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://goodmarketinggroup.com/
 * @since      1.0.0
 *
 * @package    Gmg_Contact_121
 * @subpackage Gmg_Contact_121/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gmg_Contact_121
 * @subpackage Gmg_Contact_121/admin
 * @author     Good Group LLC <info@goodmarketinggroup.com>
 */
class GMG_Contact_121_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Contact_121_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Contact_121_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gmg-contact-121-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Contact_121_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Contact_121_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gmg-contact-121-admin.js', array( 'jquery' ), $this->version, false );
        wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	}  
    
    
    /**
     * Registers and Defines the necessary fields we need.
     *
     */
    public function register_gmg_contact_121_menu_pages(){
        
        add_menu_page(
            __( 'GMG Contact 1:1', $this->plugin_name ),              // page title
            __( 'Contact 1:1', $this->plugin_name ),                  // menu title
            'publish_posts',                                               // capability
            $this->plugin_name,                                             // menu_slug
            array( $this, 'display_gmg_custom_121_intro_page' ),       // callable function
            'dashicons-email-alt',
            3       // Menu Position
        );
        
/*        add_submenu_page(
            $this->plugin_name,                                     // parent slug
            __( 'Contact 1:1 Programs', $this->plugin_name ),              // page title
            __( 'Programs', $this->plugin_name ),                  // menu title
            'publish_posts',                                               // capability
            $this->plugin_name .'-programs',                                             // menu_slug
            array( $this, 'display_gmg_contact_121_plugin_intro_page' )       // callable function
        );*/
        
/*        acf_add_options_sub_page(array(
            'parent_slug' 	=> $this->plugin_name,
            'page_title' 	=> 'Plugin Settings ',
            'menu_title' 	=> 'Plugin Settings ',
            'post_id'            => 'c121_settings',
            'capability' 	=> 'manage_options'
        ));*/
        
    }
    
    public function display_gmg_custom_121_intro_page(){
        
        echo '<h2>GMG Contact 1:1 Plugin</h2>';
        echo '<p>Copyright ' . date("Y") . ' Good Marketing Group</p>';
        
    }
}