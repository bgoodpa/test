<?php

use Twilio\Rest\Client;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://goodmarketinggroup.com/
 * @since             1.0.0
 * @package           Gmg_Contact_121
 *
 * @wordpress-plugin
 * Plugin Name:       GMG Contact 1:1
 * Plugin URI:        https://goodmarketinggroup.com/contact-121
 * Description:       Contact 1:1 is a collection of platforms built to better utilize your website to explode your business.
 * Version:           1.4.0
 * Author:            Good Group LLC
 * Author URI:        https://goodmarketinggroup.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gmg-contact-121
 * Domain Path:       /languages
 * 
 *  All Rights Reserved
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gmg-contact-121-activator.php
 */
function activate_gmg_contact_121() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gmg-contact-121-activator.php';
	Gmg_Contact_121_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gmg-contact-121-deactivator.php
 */
function deactivate_gmg_contact_121() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gmg-contact-121-deactivator.php';
	Gmg_Contact_121_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gmg_contact_121' );
register_deactivation_hook( __FILE__, 'deactivate_gmg_contact_121' );

$path = plugin_dir_path( __FILE__ );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require $path . 'includes/class-gmg-contact-121.php';

/**
 * These are the core elements that most of the Contact 1:1 parts run on.
 * 
 * Customer is at the core of these.
 * 
 */

require $path . 'platforms/gmg-class-api-talker.php';
//require $path . 'platforms/sharpspring/gmg-sharpspring.php';
//require $path . 'platforms/sharpspring/gmg-class-sharpspring.php';
//require $path . 'platforms/sharpspring/gmg-sharpspring-models.php';

//require $path . 'platforms/campaign-monitor/class-campaign-monitor.php';
//require $path . 'platforms/twilio-php-master/Twilio/autoload.php';

//require $path . 'parts/gmg-customers/gmg-customers.php';
//require $path . 'parts/gmg-capture/gmg-capture.php';
//require $path . 'parts/gmg-reviews/gmg-reviews.php';
//require $path . 'parts/gmg-reminders/gmg-reminders.php';
require $path . 'parts/gmg-phone-tester/gmg-phone-tester.php';
//require $path . 'parts/gmg-field-locator/gmg-field-locator.php';
//require $path . 'parts/gmg-leads/gmg-leads.php';
//require $path . 'parts/gmg-boomerang/gmg-boomerang.php';

/**
 * These are the custom roles for the plugin.
 *  
 */
add_role( 'contact-admin', 'Contact Admin' );
add_role( 'sr-messenger', 'Text Messenger' );

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gmg_contact_121() {

	$plugin = new Gmg_Contact_121();
	$plugin->run();

}
run_gmg_contact_121();
