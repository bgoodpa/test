<?php

/*
*
* This is the entry into Phone Tester
*    
*/

//set the variable for the plugin path
$pros = plugin_dir_path( __FILE__ );

//error_log( 'Phone Tester Called');

//Let's also bring in the Add Phone Tester Dashboard widget
require_once($pros . '/includes/gmg-phone-tester-add-dashboard.php');

//Let's also bring in codes of short
require_once( $pros . '/includes/gmg-phone-tester-shortcode.php' );