<?php

/*
*
* Register Reputation Management Settings page in the menu
*
* Status: SOLID
*
*/

function register_gmg_reviews_menu_page(){
    
    $plugin = new Gmg_Contact_121();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Reputation Management Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Reviews Settings',
            'capability'  => 'manage_options',
            'post_id'       => 'rm_options',
	));
    
}

/*
*
* Load CSS
*
* Status: ASSESS (IF NEEDED)
*
*/

add_action( 'wp_enqueue_scripts', 'register_reviews_scripts' );
function register_reviews_scripts(){
    
    wp_enqueue_style( 'review-css' , plugin_dir_url( __FILE__ ) . 'css/gmg-reviews.css' );
}

/*
*
* Load Admin CSS
*
* Status: ASSESS (IF NEEDED)
*
*/

add_action( 'admin_enqueue_scripts', 'register_reviews_admin_scripts' );
function register_reviews_admin_scripts() {
    
	wp_enqueue_style( 'review-admin-css' , plugin_dir_url( __FILE__ ) . 'css/gmg-reviews-admin.css' );
    
}

/*
*
* Load Copyright information
*
* Status: SOLID
*
*/

add_filter('acf/load_field/key=field_5b87ffa895a6e', 'gmg_rm_update_copyright');
function gmg_rm_update_copyright( $field ){
    
//    error_log( 'Update Copyright!');
    
    $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
    
    return $field;
}

/*
*
* Load Campaign Monitor List ID
*
* Status: ASSESS (IF NEEDED)
*
*/

add_filter('acf/load_field/key=field_5b80176e2246b', 'gmg_cm_reviews_lists_load');
function gmg_cm_reviews_lists_load( $field ){
    
//    error_log( 'Call Lists Load' );
    
    if( class_exists( 'ReviewCM' ) ){
    
        $bm = new ReviewCM();
        $list_array = $bm->get_clients_list();
        if( is_array( $list_array ) AND count( $list_array ) != 0) {
            foreach( $list_array as $list ) {

                $field['choices'][ $list['ListID'] ] = $list['Name'];
            }
        }
    }
    
    return $field;
}

/*
*
* Load Categories into Fields for choosing in the meta
*
* Status: ASSESS (IF NEEDED)
*
*/

add_filter('acf/load_field/key=field_5a56559425ed5', 'gmg_loader_backend_review_categories' );
function gmg_loader_backend_review_categories( $field ){
     
    // Where our new values will live
    $field['choices'] = array();
    
    // Get the ID of the parent category, which is reviews.
    $category = get_category_by_slug( 'reviews' );
    
    // Then get it's children.
    $gmg_categories = get_term_children( $category->term_id, 'category' );

    // If there are categories (which there always should be), then do something.
    if( $gmg_categories ){
        
        foreach( $gmg_categories as $gmg_category ){
            
            $choice = get_term( $gmg_category, 'category' )->name;            
            $field['choices'][ $choice ] = $choice;
            
        }
    }
    
    return $field;
}