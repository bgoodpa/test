<?php

//add action that only fires when draft is published
add_action( "draft_to_publish", 'gmg_review_approved', 10, 1 );
function gmg_review_approved( $post ) {
    
    if( $post->post_type == 'reviews' ){
        
        //Instantiate the Review Class
        $review = new Review( $post->ID );
        
        //Instantiate the Reviews Class
        $reviews = new Reviews();
        
        //check to see if the reviewer has been sent down already.
        if( $review->get_review_submitted() == 'no' || $review->get_review_submitted() == '' || $review->get_review_submitted() == null ){

            //If not, do so.

            //First, we need to figure out where we're sending it.
            if( $reviews->get_email_platform() == 'sharpspring' ){

                //If SharpSpring, then do this.
                $base = get_field( 'sharpspring_base', 'rm_options' );
                $end = get_field( 'sharpspring_sm_nudge_form_endpoint', 'rm_options' );

                //Instantiate the Review SharpSpring Class
                $sharpspring = new ReviewSS( $base, $end );                    

                //Get an array full of data
                $info_array = $review->get_review_info();
                
                error_log( 'Draft to Publish this review to SharpSpring for ' . $info_array['fname'] );

                //send it down
                $sharpspring->gmg_review_manual_enter_sharpspring( $info_array );

            } else {

                //Then Campaign Monitor
                $campaign = new ReviewCM();

                //Send Nudge
                $campaign->send_nudge( $post_id, 'Yes' );

            }

            //update to say that it was submitted
            $review->set_review_submitted('yes');

        }
    }
}

//add action that only fires when draft is trashed
add_action( "draft_to_trash", 'gmg_review_trashed', 10, 1 );
function gmg_review_trashed( $post ) {
    
    error_log( 'The review was trashed!' );
    
    if( $post->post_type == 'reviews' ){

        //Instantiate the Review Class
        $review = new Review( $post->ID );

        //First, we need to figure out where we're sending it.
        if( $reviews->get_email_platform() != 'sharpspring' ){
            
            $campaign = new ReviewCM();
            $result = $campaign->stop_emails( $post->ID );
            
            if($result->was_successful()) {
                
                error_log( "Changed with code ".$result->http_status_code);
            
            } else {
                
                error_log( 'Change failed with code '.$result->http_status_code );
            }
        }
    }
}