<?php

//This function was created to autocreate specific pages for the platform if they do not exist

add_action( 'wp_loaded', 'gmg_reviews_check_if_pages_exist' );
function gmg_reviews_check_if_pages_exist(){
    if ( !gmg_reviews_page_exists_by_slug('leave-a-review') && get_page_by_title( 'Leave Us A Review') == NULL ) {

        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Leave Us A Review',
            'post_name'     => 'leave-a-review',
            'post_content'      => '',
            'post_status'        => 'publish'
        );
        
        wp_insert_post( $args );        

    } else {
//        error_log('Review Page already exists!' );
    }

    if ( !gmg_reviews_page_exists_by_slug('page-testimonial') && get_page_by_title( 'Testimonials') == NULL ) {

        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Testimonials',
            'post_name'     => 'testimonials',
            'post_content'      => '',
            'post_status'        => 'publish'
        );
        
        wp_insert_post( $args ); 

    } else {
//        error_log('Scheduler Page already exists!' );
    }
    
    if ( !gmg_reviews_page_exists_by_slug('thank-you') && get_page_by_title( 'Thank You') == NULL ) {
        
        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Thank You',
            'post_name'     => 'thank-you',
            'post_content'      => "<h2>Thanks for reaching out to us</h2><p>We'll reach out to you soon.</p>",
            'post_status'        => 'publish'
        );
        
        wp_insert_post( $args ); 

    } else {
//        error_log('Scheduler Page already exists!' );
    }
    
    if ( !gmg_reviews_page_exists_by_slug('thank-you-review') && get_page_by_title( 'Thank You For Review') == NULL ) {

        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Thank You For Review',
            'post_name'     => 'thank-you-review',
            'post_content'      => "<h2>Thanks for leaving a review</h2>",
            'post_status'        => 'publish'
        );
        
        wp_insert_post( $args );        

    } else {
//        error_log('Review Page already exists!' );
    }
    

}

function gmg_reviews_page_exists_by_slug( $post_slug ) {
    $args_posts = array(
        'post_type'      => array ( 'page' ),
        'post_status'    => array ( 'publish' ),
        'name'           => $post_slug,
        'posts_per_page' => 1,
    );
    
    if( get_posts( $args_posts ) ){
        return true;
    } else {
        return false;
    }
}
