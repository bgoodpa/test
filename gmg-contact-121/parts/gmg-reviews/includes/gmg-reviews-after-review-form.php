<?php

/*
*
* Creates a review after Gravity Forms Leave A Review Submission
*
* Then sends them down to SharpSpring of Campaign Monitor
*
* Status: SOLID
*
*/

add_action( 'gform_after_submission_10', 'gmg_review_page_autosave', 10, 2 );
function gmg_review_page_autosave( $entry, $form ) { //SOLID
    
    //As before, throw it all in an array to send.
    $info_array = array(
        'fname'    => rgar( $entry, '1.3' ),
        'lname'    => rgar( $entry, '1.6' ),
        'email'    => rgar( $entry, '4' ),
        'reviewer_town'     => rgar( $entry, '5' ),
        'review'            => rgar( $entry, '8' ),
        'stars'             => rgar( $entry, '9' ),
        'subject'           => rgar( $entry, '10' ),
        );
        
    //Initialize the Reviews class.
    $reviews = new Reviews();
    
    error_log('Review submitted by ' . $info_array['fname']  );

    //Create the review.
    $r_id = $reviews->create_new_review( $info_array );
    
    if( $info_array['stars'] < 5 ){
        
        //see which place we're sending things to
        if( $reviews->get_email_platform() != 'sharpspring' ){
        
            //If Campaing Monitor, then do this.
            $campaign = new ReviewCM();

            //Send Nudge
            $campaign->send_nudge( $post_id, 'Yes' );
            
        } else {
            
            //It was SharpSpring and there was nothing else to do.
        
        }        
    }    
}