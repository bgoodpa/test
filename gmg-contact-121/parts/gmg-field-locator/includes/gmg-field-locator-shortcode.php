<?php

add_shortcode( 'field-locator', 'gmg_field_locator_get_customers' );
function gmg_field_locator_get_customers(){
    
    $field_locator = new Field_Locator( 'd831d3dc047948be828e6b1d93bc265d' );
    
//    $response_json = $field_locator->get_customers();
    $all_items = $field_locator->get_customers();
    
//    echo var_dump( $all_items );
    
//    $response = json_decode( $response_json );
    
//    if( $response->count > 0 ){
    if( is_array( $all_items) && count( $all_items ) > 0 ){
        
        error_log( 'This many items in this array ' . count( $all_items ) );
        
//        echo '<p>Count is ' . $response->count . '</p>';
        //echo var_dump( $response->items );
                    
        $return_filename = 'get-all-customers';

        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="' . $return_filename . '.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');

        fputcsv( $file, array('UUID', 'First', 'Last') );

//        foreach ( $response->items as $this_item ) {
        foreach ( $all_items as $this_item ) {
            
//            error_log( 'This item is ' . $this_item->uuid );
            
//            if( is_object( $this_item ) && 
//               property_exists( $this_item , 'firstName') && 
//               property_exists( $this_item , 'lastName') && 
//               property_exists( $this_item , 'uuid') ) {
                
                error_log( 'This item has uuid ' . $this_item->uuid );
                        
                fputcsv( $file, array(
                    $this_item->uuid,
                    $this_item->firstName,
                    $this_item->lastName
                    )
                   );
//            }
        }
        
        exit();
    }
}

add_shortcode( 'field-locators', 'gmg_field_locator_gets_customers' );
function gmg_field_locator_gets_customers(){
    
    $field_locator = new Field_Locator( 'd831d3dc047948be828e6b1d93bc265d' );
    
    $startIndex = 9299;
    
//    $response_json = $field_locator->get_customers();
    $all_items = $field_locator->get_customers( $startIndex );
    $all_customers = $field_locator->get_customer_info( $all_items, $startIndex );
    
//    echo var_dump( $all_customers );
    
    if( is_array( $all_customers ) && count( $all_customers ) > 0 ){
        
//        error_log( 'This many items in this array ' . count( $all_customers ) );
        
//        echo '<p>Count is ' . $response->count . '</p>';
        //echo var_dump( $response->items );
                    
        $return_filename = 'get-all-customers-detailed';

        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="' . $return_filename . '.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');

        fputcsv( $file, array( 'Index', 'UUID', 'First', 'Last', 'Name',
                              'Email 1', 'Email 2', 'Email 3',
                              'Phone 1', 'Phone 2', 'Phone 3',
                              'Work 1', 'Work 2', 'Work 3',
                              'Mobile 1', 'Mobile 2', 'Mobile 3' ) );

//        foreach ( $response->items as $this_item ) {
        foreach ( $all_customers as $this_item ) {
            
//            error_log( 'This item is ' . $this_item['UUID'] );
            
//            if( is_object( $this_item ) && 
//               property_exists( $this_item , 'firstName') && 
//               property_exists( $this_item , 'lastName') && 
//               property_exists( $this_item , 'uuid') ) {
                        
                fputcsv( $file, array(
                    $this_item['index'],
                    $this_item['UUID'],
                    $this_item['firstName'],
                    $this_item['lastName'],
                    $this_item['name'],
                    isset( $this_item['email_1'] ) ? $this_item['email_1'] : '',
                    isset( $this_item['email_2'] ) ? $this_item['email_2'] : '',
                    isset( $this_item['email_3'] ) ? $this_item['email_3'] : '',
                    isset( $this_item['phone_1'] ) ? $this_item['phone_1'] : '',
                    isset( $this_item['phone_2'] ) ? $this_item['phone_2'] : '',
                    isset( $this_item['phone_3'] ) ? $this_item['phone_3'] : '',
                    isset( $this_item['work_1'] ) ? $this_item['work_1'] : '',
                    isset( $this_item['work_2'] ) ? $this_item['work_2'] : '',
                    isset( $this_item['work_3'] ) ? $this_item['work_3'] : '',
                    isset( $this_item['mobile_1'] ) ? $this_item['mobile_1'] : '',
                    isset( $this_item['mobile_2'] ) ? $this_item['mobile_2'] : '',
                    isset( $this_item['mobile_3'] ) ? $this_item['mobile_3'] : '', 
                    )
                   );
//            }
        }
        
        exit();
    }
}
/*

add_shortcode( 'field-locator', 'gmg_field_locator_get_specific_customer' );
function gmg_field_locator_get_specific_customer(){
    
    $field_locator = new Field_Locator( 'd831d3dc047948be828e6b1d93bc265d' );
    
    $field_locator->get_customer_info*/