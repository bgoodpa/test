<?php

class Field_Locator {
    
    private $api_key;
    
    public function __construct( $api_key ) {
        $this->api_key = $api_key;
    }
    
    public function curl_field_locate_customers( $startIndex, $pageSize ){

        $curl = curl_init();
        
        $curl_url = 'https://app.fieldlocate.com:443/wsx/customer/?pageSize=' . $pageSize . '&pageStartIndex=' . $startIndex . '&api_key=' . $this->api_key;

        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Cookie: AWSALB=PfPkR5jHlQRAUKdKrJNtE8URhq+IpEGMihajP9rSYKePfIiooQzFTHYJmmZirD5UaXuRXjtRIVEXjMhS5t3JDL7ThIDatmox9HObjdrqcphwt95POCN2KR9Te3DS; AWSALBCORS=PfPkR5jHlQRAUKdKrJNtE8URhq+IpEGMihajP9rSYKePfIiooQzFTHYJmmZirD5UaXuRXjtRIVEXjMhS5t3JDL7ThIDatmox9HObjdrqcphwt95POCN2KR9Te3DS; JSESSIONID=7E202E990160793F962723209782601F'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        return $response;

    }
    
    public function curl_field_locate_get_customer( $uuid ){

        $curl = curl_init();
        
        https://app.fieldlocate.com:443/wsx/customer/6bb60389187d414caffa4568d8f454a1?api_key=d831d3dc047948be828e6b1d93bc265d
        $curl_url = 'https://app.fieldlocate.com:443/wsx/customer/' . $uuid . '?api_key=' . $this->api_key;

        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Cookie: AWSALB=PfPkR5jHlQRAUKdKrJNtE8URhq+IpEGMihajP9rSYKePfIiooQzFTHYJmmZirD5UaXuRXjtRIVEXjMhS5t3JDL7ThIDatmox9HObjdrqcphwt95POCN2KR9Te3DS; AWSALBCORS=PfPkR5jHlQRAUKdKrJNtE8URhq+IpEGMihajP9rSYKePfIiooQzFTHYJmmZirD5UaXuRXjtRIVEXjMhS5t3JDL7ThIDatmox9HObjdrqcphwt95POCN2KR9Te3DS; JSESSIONID=7E202E990160793F962723209782601F'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        return $response;

    }
    
    public function get_customers( $startIndex ){
        
        //this is the array that all items will go into
        $all_items = array();
        
        //this is the startIndex in the beginning
//        $startIndex = 0;
        
        //this is the amount of files to get with each call
        $pageSize = 300;
        
        //First starters, let's call the first time.
        $response_json = $this->curl_field_locate_customers( $startIndex, $pageSize );
        
        //Then decode it
        $response = json_decode( $response_json );
        
        //this is the overall count of all items
        $overall_count = $response->count;
        
        //add these items into all_items
        $all_items = array_merge_recursive( $all_items , $response->items );
        
        //set the number of loops by dividing overall count by page size.
//        $loops = intdiv( $overall_count, $pageSize );
//        
//        //Also see if there's any remaining so that you can add one more loop for it to do.
//        $remainder = $overall_count % $pageSize;
//        
//        if( $remainder > 0 ){
//            
//            $loops = $loops + 1;
//        }
//        
//        error_log( 'This many loops ' . $loops );
//        
//        error_log( 'This is the remainder left over ' . $remainder );
        
        $loops = 3;
        
        //set the current index
        $current_index = 1;
        
        //if the current index is less than or equal to the number of loops that it needs to do
        while( $current_index <= $loops ){
            
//            error_log( 'This loop is ' . $current_index . ' of ' . $loops );
            
            //set the place to start
            $new_startIndex = $startIndex + ( $current_index * $pageSize );
            
//            error_log( 'This current loop has startIndex of ' . $new_startIndex );
            
            //Let's call for each loop
            $response_json = $this->curl_field_locate_customers( $new_startIndex, $pageSize );
        
            //Then decode it
            $response = json_decode( $response_json );
            
            if( is_object( $response ) && 
               property_exists( $response , 'items' ) && 
               is_array( $response->items ) ){
        
                //add these items into all_items
                $all_items = array_merge_recursive( $all_items , $response->items );
                
            }
            
            //iterate the index so that we can make sure we're keeping track.
            $current_index++;
        }
        
        //return all the items when done.
        return $all_items;
        
//        $all_items = array_merge_recursive( $all_items , $response->items );
        
//        $count = $response[count];
        
    }
    
    public function get_customer_info( $customers_array, $startIndex ){
        
        //set new array
        $new_array = array();
        
        //iterate through each of the customers
        foreach( $customers_array as $this_item ){
            
//            error_log( 'Index is ' . $startIndex );
            
            //Let's call for each loop
            $customer = json_decode( $this->curl_field_locate_get_customer( $this_item->uuid ) );
            
            if( is_object( $customer ) ){
                
//                error_log( 'Got customer: ' . $customer->name );
            
                $this_array = array(
                    'firstName' => $this_item->firstName,
                    'lastName' => $this_item->lastName,
                    'UUID'  => $this_item->uuid,
                    'index' => $startIndex
                );
                
                $this_array['name' ] = $customer->name;

                if( property_exists( $customer , 'emails' ) ){

                    $email_index = 1;

                    foreach( $customer->emails as $this_email ){
                        
//                        error_log( 'Got email: ' . $this_email->email );

                        $this_array['email_' . $email_index ] = $this_email->email;

                        $email_index++;
                    }
                }

                if( property_exists( $customer , 'phones' ) ){
                    
                    $phone_index = 1;
                    $mobile_index = 1;
                    $work_index = 1;

                    foreach( $customer->phones as $this_phone ){
                        
                        $description = property_exists( $this_phone, 'description' ) ? $this_phone->description : ' ';
                        
//                        error_log( 'Got phone: ' . $this_phone->number. ' (' . $this_phone->type . ' ' . $this_phone->description . ')' );
                        
                        if( $this_phone->type == "CELL" ){
                            
                            $this_array['mobile_' . $mobile_index ] = $this_phone->number. ' (' . $this_phone->type . ' ' . $description . ')';
                            
                            $mobile_index++;
                            
                        } elseif( $this_phone->type == "WORK" ){
                            
                            $this_array['work_' . $work_index ] = $this_phone->number. ' (' . $this_phone->type . ' ' . $description . ')';
                            
                            $work_index++;
                            
                        } else {
                            
                            $this_array['phone_' . $phone_index ] = $this_phone->number . ' (' . $this_phone->type . ' ' . $description . ')' ;
                            
                            $phone_index++;                            
                            
                        }
                    }
                }

                array_push( $new_array, $this_array );
            }
            
            $startIndex++;
        }
        
//        error_log( 'Array to be returned is this large: ' . count( $new_array ) );
        
        return $new_array;
    }    
}