<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
add_action( 'wp_dashboard_setup', 'gmg_field_locator_add_dashboard_widgets' );
function gmg_field_locator_add_dashboard_widgets() {
    
//    error_log( 'Widget Called');

	wp_add_dashboard_widget(
        'gmg_field_locator_dashboard_widget',         // Widget slug.
        'Upload UUIDs',         // Title.
        'gmg_field_locator_dashboard_widget_function' // Display function.   
        );	
}

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_field_locator_dashboard_widget_function() {

    ?>

    <form name="gmg_field_locator_form" method="post" action="" id="gmg_field_locator_form" enctype="multipart/form-data">
        
        <input name="update_sharpspring_contacts_csv" type="file">      
        <input type="submit" name="update_sharpspring_contacts" id="update_sharpspring_contacts" class="button import button-primary" value="Import">

    </form>

    <?php
}


/*
*
* This function is going to check SharpSpring for every email 
* in a CSV file to see if there is a contact.
*
* When it finds one and only one, it will update three custom fields
* to store:
*
* FieldLocate UUID //fieldlocate_uuid_5fe4be4d47751
* Email 2 //email_2_604157b1df824
* Email 3 //email_3_6041588eb6ebf
*
*/

add_action( 'init', 'func_gmg_field_locator' );
function func_gmg_field_locator() {
    
    //make we're calling from the right form submit
    if( isset( $_POST['update_sharpspring_contacts'] ) ) {
        
        error_log( 'Inside update UUIDs!' );
        
        error_log( 'Files are ' . $_FILES['update_sharpspring_contacts_csv']['name'] );
        
        //make sure we've got the right CSV
        if( $_FILES['update_sharpspring_contacts_csv'] ) {
            
            //get the name and break it into parts
            $filename = explode('.', $_FILES['update_sharpspring_contacts_csv']['name']);
            
            error_log( 'Filename is ' . $filename[1] . '!' );
            
            //make sure it's a CSV
            if( $filename[1]=='csv' ){
                
                //create an array to put things into
                $response_array = array();
                
                //instantiate the GMG API Sharpspring class to be able to talk to SharpSpring
                $sharp_talker = new GMG_API_Sharpspring();
                
                //dump all contents of the CSV into an array
                $csv_array = array_map('str_getcsv', file( $_FILES['update_sharpspring_contacts_csv']['tmp_name'] ));
//                echo var_dump( $csv_array );
                
                error_log( 'Array is ' . count( $csv_array ) . ' big!' );
                
                //set an index to be able to skip the header
                $index = 0;
                
                //iterate through the csv
                foreach( $csv_array as $csv_line ){
                    
                    //skip if there's a header
                    if( $index > 0 ){
                    
                        //make an array of neccessary data
                        $this_reply = array();

                        $this_reply['fname'] = $csv_line[1];
                        $this_reply['lname'] = $csv_line[0];
                        $this_reply['email'] = $csv_line[9];
                        
                        
                        $column_index = 3;
                        $phone_index = 1;
                        
                        while( $column_index <= 8 ){
                            
                            if( $csv_line[$column_index] != "" && $csv_line[$column_index] != " " ){
                                
                                $raw_to_phone = preg_replace( "/[^0-9]/", "", $csv_line[$column_index] );
                                                
                                if( strlen( $raw_to_phone ) >= 10 && strlen( $raw_to_phone ) <= 11 ){
                                
                                    $this_reply['phone_' . $phone_index ] = $api_talker->phone_formatter( $raw_to_phone );

                                    $response = $api_talker->send_text_verify( $this_reply['phone_' . $phone_index ] );

                                    if( is_array( $response ) ){

                                        $this_reply['response_' . $phone_index ] = $response['type'];

                                    } else {

                                        $this_reply['response_' . $phone_index ] = 'Error';

                                    }
                                    
                                } else {
                                    
                                    $this_reply['phone_' . $phone_index ] = $csv_line[ $column_index ];
                                    $this_reply['response_' . $phone_index ] = '';                                    
                                    
                                }

                            } else {

                                $this_reply['phone_' . $phone_index ] = $csv_line[ $column_index ];
                                $this_reply['response_' . $phone_index ] = '';

                            }
                            
                            
                            $phone_index++;
                            $column_index++;
                        }
                        
//                        if( $csv_line[3] != "" && $csv_line[3] != " " ){
//    //                    
//                            $this_reply['phone'] = $api_talker->phone_formatter( $csv_line[3] );
//        //                    
//                            $response = $api_talker->send_text_verify( $this_reply['phone'] );
//                            
//                            if( is_array( $response ) ){
//                            
//                                $this_reply['response'] = $response['type'];
//                                
//                            } else {
//                                
//                                $this_reply['response'] = 'Error';
//                                
//                            }
//                            
//                        } else {
//                            
//                            $this_reply['phone'] = $csv_line[3];
//                            $this_reply['response'] = 'Number was faulty';
//                            
//                        }

                        array_push( $response_array , $this_reply );
                        
                    }
                    
                    $index++;
                    
                }
                
//                echo var_dump( $response_array );
                
                if( count( $response_array ) > 0 ){
                    
                    $return_filename = $filename[0] . '-phone-verified';
                    
                    header('Content-type: text/csv');
                    header('Content-Disposition: attachment; filename="' . $return_filename . '.csv"');
                    header('Pragma: no-cache');
                    header('Expires: 0');
                    
                    $file = fopen('php://output', 'w');

        			fputcsv( $file, array('First', 'Last', 'Email',
                                          "Phone Number 1", "Type",
                                          "Phone Number 2", "Type",
                                          "Phone Number 3", "Type",
                                          "Phone Number 4", "Type",
                                          "Phone Number 5", "Type",
                                          "Phone Number 6", "Type" ) );
                    
                    foreach ($response_array as $this_response ) {
                        
                        fputcsv( $file, array( $this_response['fname'],
                                              $this_response['lname'],
                                              $this_response['email'],
                                              $this_response['phone_1'],
                                              $this_response['response_1'],
                                              $this_response['phone_2'],
                                              $this_response['response_2'],
                                              $this_response['phone_3'],
                                              $this_response['response_3'],
                                              $this_response['phone_4'],
                                              $this_response['response_4'],
                                              $this_response['phone_5'],
                                              $this_response['response_5'],
                                              $this_response['phone_6'],
                                              $this_response['response_6'] )
                           );
                    }
                    
                    exit();
                }
            }
        }
    }
}