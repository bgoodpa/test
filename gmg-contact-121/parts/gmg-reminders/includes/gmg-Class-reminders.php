<?php

class GMG_Appointment {
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }

    
    //Get Appointment Fields

    public function get_first_name(){
        return get_field( 'first_name', $this->id );
    }

    public function get_last_name(){
        return get_field( 'last_name', $this->id );
    }
    
    public function get_email(){
        return get_field( 'email', $this->id );
    }

    public function get_phone_number(){
        return get_field( 'phone_number', $this->id );
    }
    
    public function get_lead_id(){
        return get_field( 'lead_id', $this->id );
    }

    public function get_opportunity_id(){
        return get_field( 'opportunity_id', $this->id );
    }
    
    public function get_appointment_date(){
        return get_field( 'appointment_date', $this->id );
    }
    
    public function get_appointment_time(){
        return get_field( 'appointment_time', $this->id );
    }
    
    public function get_last_service_date(){
        return get_field( 'last_service_date', $this->id );
    }
    
    public function get_next_service_date(){
        return get_field( 'next_service_date', $this->id );
    }
    
    public function get_send_text_message(){
        return get_field( 'send_text_message', $this->id );
    }
    
    public function get_appointment_status(){
        return get_field( 'appointment_status', $this->id );
    }

    
    //Update Appointment Fields
    
    public function set_first_name( $value ){
        return update_field( 'first_name', $value , $this->id );
    }

    public function set_last_name( $value ){
        return update_field( 'last_name', $value, $this->id );
    }
    
    public function set_email( $value ){
        return update_field( 'email', $value , $this->id );
    }

    public function set_phone_number( $value ){
        return update_field( 'phone_number', $value, $this->id );
    }
    
    public function set_lead_id( $value ){
        return update_field( 'lead_id', $value, $this->id );
    }

    public function set_opportunity_id( $value ){
        return update_field( 'opportunity_id', $value, $this->id );
    }
    
    public function set_appointment_date( $value ){
        return update_field( 'appointment_date', $value, $this->id );
    }
    
    public function set_appointment_time( $value ){
        return update_field( 'appointment_time', $value, $this->id );
    }
    
    public function set_last_service_date( $value ){
        return update_field( 'last_service_date', $value, $this->id );
    }
    
    public function set_next_service_date( $value ){
        return update_field( 'next_service_date',  $value, $this->id );
    }
    
    public function set_send_text_message( $value ){
        return update_field( 'send_text_message',  $value, $this->id );
    }
    
    public function set_appointment_status( $value ){
        return update_field( 'appointment_status',  $value, $this->id );
    }
    
    //update all fields
    
    public function update_appointment( $info_array ){
        
        $this->set_first_name( $info_array['first_name'] );
        $this->set_last_name( $info_array['last_name'] );
        
        $this->set_email( $info_array['email'] );
        $this->set_phone_number( $info_array['phone_number'] );
        
        $this->set_appointment_date( $info_array['appointment_date'] );
        $this->set_appointment_time( $info_array['appointment_time'] );

        $this->set_last_service_date( $info_array['appointment_date']);
        
        $service_date = DateTime::createFromFormat( 'Y-m-d', $info_array['appointment_date'] );
        
        $future_date = $service_date->add( new DateInterval('P1Y') );
//        error_log('Future Date is ' . $future_date->format( 'Y-m-d' ) );
        $this->set_next_service_date( $future_date->format( 'Y-m-d' ) );
        
        $this->set_send_text_message( $info_array['send_text_message'] );
        $this->set_appointment_status( $info_array['appointment_status'] );
        
        $this->update_title();
        
    }
    
    public function update_title(){
        
        $update_post = array(
            'post_title'        => $this->get_last_name() . ' - ' . $this->get_appointment_date(),
            'post_name'        => $this->get_last_name() . ' - ' . $this->get_appointment_date(),
            'ID'                => $this->id
        );

        wp_update_post( $update_post );
        
    }
    
}

class GMG_Appointments {
    
    public function __construct() {}
    
    public function create_new_appointment( $info_array ){
        
        $my_post = array(
            'post_title'        => $info_array['last_name'] . ' - ' . $info_array['appointment_date'], 
            'post_type'         => 'gmg-reminders',
            'post_status'       => 'publish'
        );
        
//        error_log( 'Before insert post' );
        
        $new_id = wp_insert_post( $my_post );
        
        $appointment = new GMG_Appointment( $new_id );
        
        $appointment->set_first_name( $info_array['first_name'] );
        $appointment->set_last_name( $info_array['last_name'] );
        
        $appointment->set_email( $info_array['email'] );
        $appointment->set_phone_number( $info_array['phone_number'] );
        
        $appointment->set_appointment_date( $info_array['appointment_date'] );
        error_log('Appointment Date is ' . $info_array['appointment_date'] );

        $appointment->set_appointment_time( $info_array['appointment_time'] );

        $appointment->set_last_service_date( $info_array['appointment_date']);
        
        $service_date = DateTime::createFromFormat( 'Y-m-d', $info_array['appointment_date'] );
        
        $future_date = $service_date->add( new DateInterval('P1Y') );
        error_log('Future Date is ' . $future_date->format( 'Y-m-d' ) );
        $appointment->set_next_service_date( $future_date->format( 'Y-m-d' ) );
        
        $appointment->set_send_text_message( $info_array['send_text_message'] );
        $appointment->set_appointment_status( $info_array['appointment_status'] );
        
        return $new_id;
    }
    
    public function get_all_appointments(){
        
        // error_log('Get all service reminders!');
        $service = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg-reminders' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'post_date',
            'order'                  => 'DESC'
        );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                
                $query->the_post();
                
                $appointment = new GMG_Appointment( get_the_ID() );
                
                $raw_date = DateTime::createFromFormat( 'Y-m-d' , $appointment->get_appointment_date() );
                
                array_push( $service, 
                           array(
                               'name'  => get_the_title(),
                               'id'    => get_the_ID(),
                               'first_name' => $appointment->get_first_name(),
                               'last_name' => $appointment->get_last_name(),
                               'email' => $appointment->get_email(),
                               'phone_number' => $appointment->get_phone_number(),
                               'appointment_date' => $raw_date->format( 'm/d/Y'),
                               'appointment_time' => $appointment->get_appointment_time(),
                               'send_text_message' => $appointment->get_send_text_message()
                           )
                          );
            }
        
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $service;
        
    }
}

class GMG_Appointment_Talker {
    
    public function __construct() {}
    
    //Function to send Text Messages    
    public function gmg_reminders_sar_text_message( $info_array ){
        
        //get set phone number
        $phone_number = get_field('scheduler_phone', 'sr_options');
        
        $raw_to_phone = preg_replace("/[^0-9]/", "", $info_array['mobile_phone_number'] );

        $to_phone[0] = "+";

        if( substr( $raw_to_phone, 0, 1 ) != "1" ){
            array_push( $to_phone, "1" );
        }

        array_push( $to_phone, $raw_to_phone );

        //set the array that everything will go into
        $message_array = array();

        $raw_appt_date = DateTime::createFromFormat( 'Y-m-d' , $info_array['appointment_date'] );
//        error_log( 'New Date is '. $raw_appt_date->format('F j Y') );

        //Begin creating message
        array_push( $message_array , 'FROM ' . get_bloginfo( 'name' ) . '. ' );
        array_push( $message_array , 'Reminder: ' . $info_array['first_name'] . ', ');
        array_push( $message_array , ' appointment scheduled on ' . $raw_appt_date->format('F j Y') . ' at/between ' . $info_array['appointment_time'] . '. ' );
        array_push( $message_array , 'Any questions or you need to reschedule, please call ' . $phone_number . '.' );

        $api_talker = new API_Talker();
        $api_talker->send_text_message( implode( $to_phone ) , implode( $message_array ) );
        
    }
    
    //Function to handle SharpSpring Lead and Opportunity updates
    public function gmg_reminders_sar_updates( $info_array ){
        
        /*
        *
        * To accomplish this function, we need to:
        *
        * - Send data to SharpSpring Form
        * - Retrieve Lead ID
        * - Update the Opportunity 
        *
        */
        
        /*
        *
        * Send data to SharpSpring Form SR-SAR Updater
        *
        * 'baseURI', 'https://app-3QNNPSZK26.marketingautomation.services/webforms/receivePostback/MzawMLEwMbAwBAA/'
        * 'endpoint', 'fe606339-e382-485d-aeb1-47a5788be7f1'
        *
        */
        
        $baseURI = 'https://app-3QNNPSZK26.marketingautomation.services/webforms/receivePostback/MzawMLEwMbAwBAA/';
        $endpoint = 'fe606339-e382-485d-aeb1-47a5788be7f1';
            
        $class_sharpspring = new GMG_Reminders_Sharpspring( $baseURI, $endpoint );
        $params = $class_sharpspring->gmg_parse_reminders_sharpspring( $info_array );
        $result = $class_sharpspring->gmg_reminders_sharpspring_curl( $params );
        
        if( $result ){
                
            error_log( "Updated with code " . $result['http_code'] );

        } else {

            error_log( 'Updated Failed with code '. $result['http_code'] );
        }
        
        /*
        *
        * Retrieve Lead ID
        *
        */
        
        
        $sharpspring = new GMG_API_Sharpspring();
        $result = $sharpspring->get_lead_id( $info_array['email'] );

        if( is_object( $result) && property_exists( $result, 'lead') ){

            $lead_object = $result->lead[0];

            if( is_object( $lead_object) && property_exists( $lead_object, 'id') ){

                //set Lead ID
                error_log( 'The Lead ID is ' . $lead_object->id );
                
                $appointment = new GMG_Appointment( $info_array['id'] );
                $appointment->set_lead_id( $lead_object->id );
                $info_array['lead_id'] = $lead_object->id;
                
//                $fields = $sharpspring->get_fields();
//                echo var_dump( $fields );
                
                $models = new GMG_Reminder_Models();
                
                /*
                *
                * Update the Lead - NOT NEEDED
                *
                */
                
/*                //set the Lead Model
                $lead_models = array( $models->model_reminder_lead( $info_array ) );
//                echo var_dump( $lead_models );
                
                //update Lead
                $lead_result = $sharpspring->update_lead( $lead_models );
//                echo var_dump( $lead_result );*/
                
                /*
                *
                * Update the Opportunity
                *
                */                

                //See if there's an opportunity_id
                $opportunity_id = $appointment->get_opportunity_id();

                if( $opportunity_id != null ){
                    
                    /*
                    * 
                    * Update the Opportunity 
                    *
                    */
                    
                    error_log( 'The Opportunity ID is ' . $opportunity_id );


                    //get ready to update the opportunity
                    $info_array['opportunity_id'] = $opportunity_id;

                    //set the Opportunity Model
//                    $objects = array( $sharpspring->model_opportunity( $info_array ) );
                    $opp_objects = array( $models->model_reminder_opportunity( $info_array ) );
//                    echo var_dump( $opp_objects );


                    //update Opportunity
                    $opportunity_result = $sharpspring->update_opportunity( $opp_objects );
//                    echo var_dump( $opportunity_result );
                }
            }
        }     
    }
    
    //Function to handle SharpSpring Lead and Opportunity creation
    public function gmg_reminders_sar_creates( $info_array ){
        
        /*
        *
        * To accomplish this function, we need to:
        *
        * - Send data to SharpSpring Form
        * - Retrieve Lead ID
        * - Create the Opportunity
        *
        */
        
        
        /*
        *
        * Send to Form SR-SAR Dashboard
        *
        * 'baseURI', 'https://app-3QNNPSZK26.marketingautomation.services/webforms/receivePostback/MzawMLEwMbAwBAA/'
        * 'endpoint', '7d19a0bf-9063-4790-a2ae-d8c19c9e4e4f'
        *
        */
        
        $baseURI = 'https://app-3QNNPSZK26.marketingautomation.services/webforms/receivePostback/MzawMLEwMbAwBAA/';
        $endpoint = '7d19a0bf-9063-4790-a2ae-d8c19c9e4e4f';
            
        $class_sharpspring = new GMG_Reminders_Sharpspring( $baseURI, $endpoint );
        $params = $class_sharpspring->gmg_parse_reminders_sharpspring( $info_array );
        $result = $class_sharpspring->gmg_reminders_sharpspring_curl( $params );
        
        if( $result ){
                
            error_log( "Created with code " . $result['http_code'] );

        } else {

            error_log( 'Created Failed with code '. $result['http_code'] );
        }
        
        /*
        *
        * Retrieve Lead ID
        *
        */
        
        $sharpspring = new GMG_API_Sharpspring();
        $lead_result = $sharpspring->get_lead_id( $info_array['email'] );

        if( is_object( $lead_result) && property_exists( $lead_result, 'lead') ){

            //echo var_dump( $lead_result );           
            $lead_object = $lead_result->lead[0];

            if( is_object( $lead_object) && property_exists( $lead_object, 'id') ){

                $appointment = new GMG_Appointment( $info_array['post_id'] );
                
                $models = new GMG_Reminder_Models();

                //Grab the Lead ID
                error_log( 'The Lead ID is ' . $lead_object->id );
                $appointment->set_lead_id( $lead_object->id );
                $info_array['lead_id'] = $lead_object->id;
                
                /*
                *
                * Update the Lead - NOT NEEDED
                *
                */
                
/*                //set the Lead Model
                $lead_models = array( $models->model_reminder_lead( $info_array ) );
//                echo var_dump( $lead_models );

                
                //update Lead
                $lead_result = $sharpspring->update_lead( $lead_models );
//                echo var_dump( $lead_result );*/
                
                /*
                *
                * Create the Opportunity
                *
                */

                //First, model the Opportunity
//                $object = $sharpspring->model_opportunity( $info_array );
                $opp_objects = array( $models->model_reminder_opportunity( $info_array ) );
//                echo var_dump( $opp_objects );

                //Now create the Opportunity
                $opp_result = $sharpspring->create_opportunity( $opp_objects );
//                echo var_dump( $opp_result );

                //check if it's an object, has the "creates" property, and that property calls an array
                if( is_object( $opp_result ) && property_exists( $opp_result, 'creates') && is_array( $opp_result->creates ) ){

                    //gets me the opp object
                    $opp_object = $opp_result->creates[0];

                    //check if is object and has "id" property
                    if( is_object( $opp_object) && property_exists( $opp_object, 'id') ){

                        $appointment->set_opportunity_id( $opp_object->id );
                        error_log( 'The Opportunity ID is ' . $opp_object->id );

                    }
                }
            }
        }
    }
    
    public function gmg_reminders_sar_cancels( $info_array ){
    
        /*
        *
        * Send to Form SR-SAR Cancel
        *
        * 'baseURI', 'https://app-3QNNPSZK26.marketingautomation.services/webforms/receivePostback/MzawMLEwMbAwBAA/'
        * 'endpoint', '0f782d63-c822-40be-ac6a-53151bae4cba'
        *
        */
        
        $baseURI = 'https://app-3QNNPSZK26.marketingautomation.services/webforms/receivePostback/MzawMLEwMbAwBAA/';
        $endpoint = '0f782d63-c822-40be-ac6a-53151bae4cba';
            
        $class_sharpspring = new GMG_Reminders_Sharpspring( $baseURI, $endpoint );
        $params = $class_sharpspring->gmg_parse_reminders_sharpspring( $info_array );
        $result = $class_sharpspring->gmg_reminders_sharpspring_curl( $params );
        
        if( $result ){
                
            error_log( "Cancelled worked with code " . $result['http_code'] );

        } else {

            error_log( 'Cancelled Failed with code '. $result['http_code'] );
        }
//        echo var_dump( $result );
        
        //Mark the post to draft
        if( $info_array['id'] ){
            
            $appointment = new GMG_Appointment( $info_array['id'] );
            $appointment->set_appointment_status( 'cancelled' );
            
            $args = array(
                'ID'            => $info_array['id'],
                'post_status'   => array( 'draft' )
                );
            
            wp_update_post( $args );
        }
    }
    
}

class GMG_Reminder_Models extends GMG_Sharpspring_Model{
    
    //construct the class
    public function __construct() {}
    
    public function model_reminder_lead( $lead_info ){
    
        //Call the original class function to set up the lead with all the basic info
        $lead_array = $this->model_lead( $lead_info );
        
        //Add any custom fields specific to Service Reminders
//        $lead_array['appointmentdate_600f184eeca08'] = $lead_info['appointment_date'];
        
//        $lead_array['appointmenttime_600f187364c9e'] = $lead_info['appointment_time'];

        
        return $lead_array;
    }
    
    public function model_reminder_opportunity( $opp_info ){
        
        $opp_array = array();
    
        //Call the original class function to set up the Opportunity with all the basic info
//        $opp_array = $this->model_opportunity( $opp_info );
        
        //Add any custom fields specific to Service Reminders
        
        //id (Only if update)
        if( isset( $opp_info['opportunity_id'] ) ){
            
            $opp_array['id'] = $opp_info['opportunity_id'];
        }
        
        //Owner ID
        $owner_id = get_field( 'sharpspring_owner_id', 'sr_options');
        $opp_array['ownerID'] = $owner_id;

        //Deal State ID
        $deal_stage_id = get_field( 'sharpspring_deal_stage_id', 'sr_options');
        $opp_array['dealStageID'] = $deal_stage_id;

        //Opportunity Name
        $opp_array['opportunityName'] = 'Anniversary Service Appointment on ' . $opp_info['appointment_date'];

        //Amount
        $opp_array['amount'] = '500';

        //isClosed
        $opp_array['isClosed'] = '0';

        //isWon
        $opp_array['isWon'] = '0';

        //closeDate
        $days_until = get_field( 'days_until_close', 'sr_options');
        $service_date = DateTime::createFromFormat( 'Y-m-d', $opp_info['appointment_date'] );        
        $future_date = $service_date->add( new DateInterval('P' . $days_until . 'D') );
        error_log('Days until Close is ' . $future_date->format( 'Y-m-d H:i:s' ) );
        $opp_array['closeDate'] = $future_date->format( 'Y-m-d H:i:s' );

        //isActive
        $opp_array['isActive'] = '1';

        //primaryLeadID
        $opp_array['primaryLeadID'] = $opp_info['lead_id'];

        //custom field - appointment date
        $opp_array['appointment_date_6011aeba7bffa'] = $opp_info['appointment_date'];
        
        //custom field - appointment time
        $opp_array['appointment_time_6011af80c6eb6'] = $opp_info['appointment_time'];
        
        return $opp_array;
        
    }
}

class GMG_Reminders_Sharpspring{
    
    private $baseURL;
    private $endPoint;
    
    //construct the class
    public function __construct( $base, $end ) {
        $this->baseURL = $base;
        $this->endPoint = $end;
    }
    
    public function gmg_parse_reminders_sharpspring( $info ){
        
            /*'first_name',
            'last_name',
            'email',
            'phone_number',
            'appointment_date',
            'appointment_time',
            'send_text_message',
            'appointment_status'*/

        // Prepare parameters
        $parameters = array();
        
        array_push( $parameters, "FirstName=" . urlencode( $info['first_name'] ) );
        array_push( $parameters, "LastName=" . urlencode( $info['last_name'] ) );
        array_push( $parameters, "emailAddress=" . urlencode( $info['email'] ) );
        
/*        error_log( 'Sent down' . $info['appointment_date'] ) );
        error_log( 'Sent down' . $info['appointment_time'] ) );
        error_log( 'Sent down' . $info['send_text_message'] ) );*/
        
        if( isset( $info['mobile_phone_number'] ) ) {

            array_push( $parameters, "mobilePhoneNumber=" . urlencode( $info['mobile_phone_number'] ) );
        }
        
        
        if( isset( $info['phone_number'] ) ) {

            array_push( $parameters, "phoneNumber=" . urlencode( $info['phone_number'] ) );
        }
        
        if( isset( $info['appointment_date'] ) ) {

            array_push( $parameters, "appointmentdate_600f184eeca08=" . urlencode( $info['appointment_date'] ) );
        }
        
        if( isset( $info['appointment_time'] ) ) {
            
            array_push( $parameters, "appointmenttime_600f187364c9e=" . urlencode( $info['appointment_time'] ) );
        }
        
        if( isset( $info['send_text_message'] ) ) {
            
            array_push( $parameters, "agree_to_text_messages_600ef798e47f9=" . urlencode( $info['send_text_message'] ) );
        }       
        
        array_push( $parameters, "Subscribe=" . urlencode( 'No' ) );

        if( isset( $info['cookie'] ) ) {
            
            array_push( $parameters, "trackingid__sb=" . urlencode( $info['cookie'] ) );
        }
        
        error_log( 'Parameters are: ' . implode( "&" , $parameters ) );

        return implode( "&" , $parameters );
    }
    
    //send parameters through curl
    public function gmg_reminders_sharpspring_curl( $parameters ){
        
        $response = false;
        
        // Prepare URL
        $ssRequest = $this->baseURL . $this->endPoint . "/jsonp/?" . $parameters;
        
        error_log( 'Request is ' . $ssRequest );

        // set up curl
        $curl = curl_init();

        // the url to request
        curl_setopt( $curl, CURLOPT_URL, $ssRequest );           

        // return to variable
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true ); 

        // decompress using GZIP
        curl_setopt( $curl, CURLOPT_ENCODING, '');

        // don't verify peer ssl cert
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );

        // fetch remote contents, check for errors
        if ( false === ( $response = curl_exec( $curl ) ) ){
            
            $error = curl_error( $curl );
            
        } else {
            
            //if it worked, get the info
            $return = curl_getinfo( $curl );
        }

        // close the resource
        curl_close( $curl );

        if ( !$response ){

            return $error;

        } else {

            return $return;
        }
    }
    
}    