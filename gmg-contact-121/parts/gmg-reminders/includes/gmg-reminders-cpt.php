<?php

    //Add Custom Post Type For projects
    function reminders_init() {
        $proj_labels = array(
            'name'                  => _x( 'Service Appointments', 'Post Type General Name', 'gmg-contact-121' ),
            'singular_name'         => _x( 'Service Appointment', 'Post Type Singular Name', 'gmg-contact-121' ),
            'menu_name'             => __( 'Service Appointments', 'gmg-contact-121' ),
            'name_admin_bar'        => __( 'Service Appointments', 'gmg-contact-121' ),
            'archives'              => __( 'Service Appointment Archives', 'gmg-contact-121' ),
            'attributes'            => __( 'Service Appointment Attributes', 'gmg-contact-121' ),
            'parent_item_colon'     => __( 'Parent Service Appointment:', 'gmg-contact-121' ),
            'all_items'             => __( 'All Service Appointments', 'gmg-contact-121' ),
            'add_new_item'          => __( 'Add New Service Appointment', 'gmg-contact-121' ),
            'add_new'               => __( 'Add New', 'gmg-contact-121' ),
            'new_item'              => __( 'New Service Appointment', 'gmg-contact-121' ),
            'edit_item'             => __( 'Edit Service Appointment', 'gmg-contact-121' ),
            'update_item'           => __( 'Update Service Appointment', 'gmg-contact-121' ),
            'view_item'             => __( 'View Service Appointment', 'gmg-contact-121' ),
            'view_items'            => __( 'View Service Appointments', 'gmg-contact-121' ),
            'search_items'          => __( 'Search Service Appointment', 'gmg-contact-121' ),
            'not_found'             => __( 'Not found', 'gmg-contact-121' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'gmg-contact-121' ),
            'featured_image'        => __( 'Featured Image', 'gmg-contact-121' ),
            'set_featured_image'    => __( 'Set featured image', 'gmg-contact-121' ),
            'remove_featured_image' => __( 'Remove featured image', 'gmg-contact-121' ),
            'use_featured_image'    => __( 'Use as featured image', 'gmg-contact-121' ),
            'insert_into_item'      => __( 'Insert into Service Appointment', 'gmg-contact-121' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Service Appointment', 'gmg-contact-121' ),
            'items_list'            => __( 'Service Appointments list', 'gmg-contact-121' ),
            'items_list_navigation' => __( 'Service Appointments list navigation', 'gmg-contact-121' ),
            'filter_items_list'     => __( 'Filter Service Appointments list', 'gmg-contact-121' ),
        );

        $proj_args = array(
            'label'                 => __( 'Service Appointment', 'gmg-contact-121' ),
            'description'           => __( 'Custom Post Type for Service Appointments', 'gmg-contact-121' ),
            'labels'                => $proj_labels,
            'supports'              => array( 'title' ),
//            'taxonomies'            => array( 'category', 'post_tag' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => false,
            'has_archive'           => false,		
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'capability_type'       => 'page',
        );
        
        $current_user = wp_get_current_user();
        if ( user_can( $current_user, 'administrator' ) ) {
            
            $gmg_plugin = new Gmg_Contact_121();
            
            $proj_args['show_in_menu'] = $gmg_plugin->get_plugin_name();
            $proj_args['menu_position'] = 6;
        }
        
        register_post_type( 'gmg-reminders', $proj_args );
        
        $status_args = array(
            'label'                     => _x( 'Closed', 'post' ),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'show_in_metabox_dropdown'  => true,
            'show_in_inline_dropdown'   => true,
            'label_count'               => _n_noop( 'Closed <span class="count">(%s)</span>', 'Closed <span class="count">(%s)</span>' ), );
        
        register_post_status( 'closed', $status_args );
    }
add_action( 'init', 'reminders_init' );

// Display Custom Post Status Option in Post Edit
function gmg_reminders_custom_post_status(){
    
    global $post;
    $complete = '';
    $label = '';
    
    if( is_object( $post ) && $post->post_type == 'gmg-reminders' ){
        
        error_log( 'Custom Post Status for Reminders');
        
        if( $post->post_status == 'closed' ){
            
            $selected = 'selected';
            
        } else {
            
            $selected = '';
        }

        echo '<script>
            jQuery(document).ready( function(){
                jQuery("select#post_status").append("<option value=\"closed\" '.$selected.'>Closed</option>");
                jQuery(".misc-pub-section label").append("<span id=\"post-status-display\"> Closed</span>");
            });
        </script>';
    }
}

add_action('admin_footer-edit.php','gmg_reminders_custom_post_status');