<?php

/*
* 
* This file handles submission of Scheduled Appointment Reminder Entry & Updater form.
*
* It sends a notification to text the customer.
*    
*/

add_action( 'gform_after_submission_14', 'gmg_reminders_form_submit_appointment', 10, 2 );
function gmg_reminders_form_submit_appointment( $entry, $form ){
    
    error_log('Submit Appointment Entry & Update Form!');
    
    if( rgar( $entry, '2.3' ) != '' ){
        
        error_log('Submit Appointment Form Data Found!');
    
        $info_array = array(
            'first_name'            => rgar( $entry, '2.3' ),
            'last_name'             => rgar( $entry, '2.6' ),
            'email'                 => rgar( $entry, '7' ),
            'phone_number'          => rgar( $entry, '5' ),
            'appointment_date'      => rgar( $entry, '3' ),
            'appointment_time'      => rgar( $entry, '4' ),
            'id'                    => rgar( $entry, '8' ),
            'send_text_message'     => rgar( $entry, '6' ),
            'appointment_status'    => rgar( $entry, '9' )
        );
        
        $gmg_appointment_talker = new GMG_Appointment_Talker();

        //check if there was an id in the form
        if( $info_array['id'] != '' ){
            
            /*
            * 
            * There was, so we're either updating appointment or just cancelling it
            *    
            */
            
            
            if( strtolower( rgar( $entry, '9' )) != 'open' ){
                
                /*
                * 
                * Cancelling the appointment
                *    
                */
                
                error_log('Canceling Appointment!');

                
                $gmg_appointment_talker->gmg_reminders_sar_cancels( $info_array );
                
                
            } else {
                
                /*
                * 
                * Updating the appointment
                *    
                */
                
                error_log('Updating Appointment!');
                
                //First, let's seee if we need to send a text message.
                if( rgar( $entry, '6' ) == 'Yes' ){
                    
                    $info_array['mobile_phone_number'] = rgar( $entry, '5' );
                    
                    $gmg_appointment_talker->gmg_reminders_sar_text_message( $info_array );
                    
                }
                
                //Then, lets update the appointment.
                $appointment = new GMG_Appointment( $info_array['id'] );
                $appointment->update_appointment( $info_array );

                //Finally, send the updates to Sharpspring
                $gmg_appointment_talker->gmg_reminders_sar_updates( $info_array );
                
            }
        
        } else {
            
            //There wasn't, so create a new one
            
            error_log('Creating Appointment!');
            
            //First, let's seee if we need to send a text message.
            if( rgar( $entry, '6' ) == 'Yes' ){
                
                $info_array['mobile_phone_number'] = rgar( $entry, '5' );

                $gmg_appointment_talker->gmg_reminders_sar_text_message( $info_array );

            }

            //Next, create a new appointment.
            $appointments = new GMG_Appointments();
            $new_id = $appointments->create_new_appointment( $info_array );
            $info_array['post_id'] = $new_id;            
            
            //Finally, send everything to SharpSpring.
            $gmg_appointment_talker->gmg_reminders_sar_creates( $info_array );
            
        }
    }
}