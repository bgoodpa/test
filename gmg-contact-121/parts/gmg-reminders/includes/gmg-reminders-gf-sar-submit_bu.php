<?php

/*
* 
* This file handles submission of Scheduled Appointment Reminder Entry with Date form.
*
* It sends a notification to text the customer.
*    
*/

add_action( 'gform_after_submission_14', 'gmg_reminders_form_submit_appointment', 10, 2 );
function gmg_reminders_form_submit_appointment( $entry, $form ){
    
    error_log('Submit Appointment Form!');
    
    if( rgar( $entry, '2.3' ) != '' ){
        
        error_log('Submit Appointment Form Data Found!');
    
        $info_array = array(
            'first_name'            => rgar( $entry, '2.3' ),
            'last_name'             => rgar( $entry, '2.6' ),
            'email'                 => rgar( $entry, '7' ),
            'phone_number'          => rgar( $entry, '5' ),
            'appointment_date'      => rgar( $entry, '3' ),
            'appointment_time'      => rgar( $entry, '4' ),
            'id'                    => rgar( $entry, '8' ),
            'send_text_message'     => rgar( $entry, '6' ),
            'appointment_status'    => rgar( $entry, '9' )
        );
        
        $gmg_appointment_talker = new GMG_Appointment_Talker();

        //check if there was an id in the form
        if( $info_array['id'] != '' ){

            //There was, so we're either updating appointment or just cancelling it
            
            if( strtolower( rgar( $entry, '9' )) != 'open' ){
                
                
            } else {

                $appointment = new GMG_Appointment( $info_array['id'] );
                $appointment->update_appointment( $info_array );

                $gmg_appointment_talker->gmg_reminders_sar_updates( $info_array );
                
            }
        
        } else {

            //There wasn't, so create a new one

            $appointments = new GMG_Appointments();
            $new_id = $appointments->create_new_appointment( $info_array );
            $info_array['post_id'] = $new_id;            
            
            $gmg_appointment_talker->gmg_reminders_sar_creates( $info_array );
            
        }
    }
}

//add_action( 'gform_pre_submission_3', 'gmg_reminders_check_next_service_date' );
/*function gmg_reminders_check_next_service_date( $form ){
    
    error_log('Setting Next Service Date!' );
    
}*/

add_action( 'gform_after_submission_1', 'gmg_reminders_form_submit_sms', 10, 2 );
function gmg_reminders_form_submit_sms( $entry, $form ){
    
//    error_log('See to Send SMS!' );
    
    // Get the Send Text Message.
//    $sms_field_id = '12';
//    $sms_field_raw = GFAPI::get_field( $form, $sms_field_id );
//    $sms_field = $sms_field_raw->get_value_submission( array() );
    
//    error_log('SMS Value is ' . rgar( $entry, '12' ) );
    
    //If Yes...
    if( rgar( $entry, '12' ) == 'Yes' ){
        
        //get set phone number
        $phone_number = get_field('scheduler_phone', 'sr_options');
        
        // Get the Send Text Message.
//        $cell_field_id = '5';
//        $cell_field_raw = GFAPI::get_field( $form, $cell_field_id );
        $raw_to_phone = preg_replace("/[^0-9]/", "", rgar( $entry, '5' ) );

        $to_phone[0] = "+";

        if( substr( $raw_to_phone, 0, 1 ) != "1" ){
            array_push( $to_phone, "1" );
        }

        array_push( $to_phone, $raw_to_phone );
        
//        error_log( 'New Phone is '. implode( $to_phone ) );

        //set the array that everything will go into
        $message_array = array();
        
        // Get the Appointment Date
//        $date_field_id = '6';
//        $date_field_raw = GFAPI::get_field( $form, $date_field_id );
        
//        error_log( 'Raw Date is '. rgar( $entry, '6' ) );

        $raw_appt_date = DateTime::createFromFormat( 'Y-m-d' , rgar( $entry, '6' ) );
//        error_log( 'New Date is '. $raw_appt_date->format('F j Y') );
        
        // Get the Appointment Time
//        $time_field_id = '8';
//        $time_field_raw = GFAPI::get_field( $form, $time_field_id );
//        $time_field = $time_field_raw->get_value_submission( array() ); 

        //Begin creating message
        array_push( $message_array , 'FROM ' . get_bloginfo( 'name' ) . '. ' );
        array_push( $message_array , 'Reminder: ' . rgar( $entry, '3.3' ) . ', ');
        array_push( $message_array , ' appointment scheduled on ' . $raw_appt_date->format('F j Y') . ' at/between ' . rgar( $entry, '8' ) . '. ' );
        array_push( $message_array , 'Any questions or you need to reschedule, please call ' . $phone_number . '.' );

        $api_talker = new API_Talker();
        $api_talker->send_text_message( implode( $to_phone ) , implode( $message_array ) );
        
    }
}