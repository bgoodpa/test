<?php

add_shortcode( 'gmg-select-appt', 'gmg_reminders_show_new_input' );
function gmg_reminders_show_input(){
    
    $return_html = array();
    
    //<--label   
    array_push( $return_html, '<label class="gfield_label" for="gmg-appointment-list">Find Appointment</label>');
    
    //<--div    
    array_push( $return_html, '<div class="wp-block-columns">');
    
    //<--div    
    array_push( $return_html, '<div class="wp-block-column larger">');
    
    //<--select
    array_push( $return_html, '<select id="gmg-appointment-list" class="large" >');
    
    //Instantiate GMG_Appointments to call all appointments
    $appointments = new GMG_Appointments();
    
    //<--base option
    array_push( $return_html, '<option value="none">None</option>');
    
    //Iterate through each appointment
    foreach( $appointments->get_all_appointments() as $each_appt ){
        
        //<--option
        array_push( $return_html, '<option value="' . $each_appt['id'] . '"
        data-first_name="' . $each_appt['first_name'] . '" 
        data-last_name="' . $each_appt['last_name'] . '" 
        data-email="' . $each_appt['email'] . '" 
        data-phone_number="' . $each_appt['phone_number'] . '" 
        data-appointment_date="' . $each_appt['appointment_date'] . '" 
        data-appointment_time="' . $each_appt['appointment_time'] . '"
        data-send_text_message="' . $each_appt['send_text_message'] . '"
        >' . $each_appt['name'] . '</option>');
    
    }
    
    //<--/select    
    array_push( $return_html, '</select>' );
    
    //<--/div     
    array_push( $return_html, '</div>' );
    
    //<--div     
    array_push( $return_html, '<div class="wp-block-column smaller">');
    
    //<--input
    array_push( $return_html, '<input id="gmg-set-appointment" type="button" value="Submit">');
    
    //<--/div     
    array_push( $return_html, '</div>' );
    
    //<--/div     
    array_push( $return_html, '</div>' );
    
    return implode( '' , $return_html );
}

function gmg_reminders_show_new_input(){
    
    $return_html = array();
    
    //<--div
    array_push( $return_html, '<div class="gf_browser_gecko gform_wrapper">');
    
    //<--label   
    array_push( $return_html, '<label class="gfield_label" for="gmg-appointment-list">Find Appointment</label>');
    
    //<--description  
    array_push( $return_html, '<div class="gfield_description">Find the appointment that you want to upate or just enter a new one below.</div>');
    
    //<--div
    array_push( $return_html, '<div class="ginput_container ginput_container_select">');        
    
    //<--select
    array_push( $return_html, '<select id="gmg-appointment-list" class="large" >');
    
    //Instantiate GMG_Appointments to call all appointments
    $appointments = new GMG_Appointments();
    
    //<--base option
    array_push( $return_html, '<option value="none">None</option>');
    
    //Iterate through each appointment
    foreach( $appointments->get_all_appointments() as $each_appt ){
        
        //<--option
        array_push( $return_html, '<option value="' . $each_appt['id'] . '"
        data-first_name="' . $each_appt['first_name'] . '" 
        data-last_name="' . $each_appt['last_name'] . '" 
        data-email="' . $each_appt['email'] . '" 
        data-phone_number="' . $each_appt['phone_number'] . '" 
        data-appointment_date="' . $each_appt['appointment_date'] . '" 
        data-appointment_time="' . $each_appt['appointment_time'] . '"
        data-send_text_message="' . $each_appt['send_text_message'] . '"
        >' . $each_appt['name'] . '</option>');
    
    }
    
    //<--/select    
    array_push( $return_html, '</select>' );
    
    array_push( $return_html, '<a href="/sr-sar-entry"> Reset</a>' );
    
    //<--/div
    array_push( $return_html, '</div>');
    
    //<--/div    
    array_push( $return_html, '</div>' );
    
    return implode( '' , $return_html );
}

add_shortcode( 'twilio-verify', 'gmg_reminders_twilio_verify' );
function gmg_reminders_twilio_verify(){
    
    $api_talker = new API_Talker();
    $api_talker->send_text_verify( "+14843667096" );
}