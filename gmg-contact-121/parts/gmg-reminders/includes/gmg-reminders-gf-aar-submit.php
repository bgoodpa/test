<?php

/*
* 
* This file handles submission of Anniversary Appointment Reminder Entry with Date form.
*
* It adds a value to a hidden field called Next Service Date. 
* It creates that date from Last Service Date and Anniversary Length
*    
*/ 

add_action( 'gform_pre_submission_3', 'gmg_reminders_check_next_service_date' );
function gmg_reminders_check_next_service_date( $form ){
    
    error_log('Setting Next Service Date!' );
    
    // Get the Anniversary Length field.
    $anv_field_id = '18';
    $anv_field_raw = GFAPI::get_field( $form, $anv_field_id );
    $anv_field = $anv_field_raw->get_value_submission( array() );
    
    error_log('Anniversary Value is ' . $anv_field );
    
    //If it's anything except 0, then continue
    if( $anv_field !== 0 ){
        
        // Get the Last Service Date field.
        $date_field_id = '16';
        $date_field    = GFAPI::get_field( $form, $date_field_id );

        // Get the date field value.
        $value      = $date_field->get_value_submission( array() );
        $date_value = GFFormsModel::prepare_date( $date_field->dateFormat, $value );
        
        $service_date = new DateTime( $date_value );
        
        //set empty variable
        $future_date = "";
        
        //Get the reminder frequency and see how many days in the future we should check.
        switch( $anv_field ){
                
            case "1y":
                error_log('Time is 1 year!' );
                $future_date = $service_date->add( new DateInterval('P1Y') );
                break;
                
            case "3m":
                error_log('Time is 3 months!' );
                $future_date = $service_date->add( new DateInterval('P3M') );
                break;
                
            case "6m":
                error_log('Time is 6 months!' );
                $future_date = $service_date->add( new DateInterval('P3M') );
                break;
                
        }
        
//        error_log('Future Date is ' . $future_date );
        error_log('Future Date is ' . $future_date->format( 'Y-m-d' ) );
        
        if( $future_date != "" ){
            
            // Populate Next Service Date with the new date.
//            $_POST['input_17'] = $future_date;
            $_POST['input_17'] = $future_date->format( 'Y-m-d' );            
            
        }
    }
}