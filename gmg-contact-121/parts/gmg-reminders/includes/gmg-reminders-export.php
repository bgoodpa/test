<?php

add_action( 'restrict_manage_posts', 'gmg_reminders_add_export_button' );
function gmg_reminders_add_export_button() {
    
//    error_log( 'Inside Export Reminders');
    
    $screen = get_current_screen();
//    error_log( 'The screen parent file is ' . $screen->parent_file );
    
    if ( isset( $screen->parent_file ) && ('gmg-reminders' == $screen->post_type )) {
            
//        error_log( 'The post type is ' . $screen->post_type );
            ?>
    <input type="submit" name="export_reminders_posts" id="export_reminders_posts" class="button button-primary" value="Export All Reminders">
    <script type="text/javascript">
        jQuery(function($) {
            $('#export_reminders_posts').insertAfter('#post-query-submit');
        });

    </script>

    <?php
    }
}

add_action( 'init', 'func_gmg_customers_export_all_reminders' );
function func_gmg_customers_export_all_reminders() {
    
	if(isset($_GET['export_reminders_posts'])) {
        
		$arg = array(
				'post_type' => 'gmg-reminders',
				'post_status' => 'publish',
				'posts_per_page' => -1,
			);

		global $post;
		$arr_post = get_posts($arg);
		if ($arr_post) {

			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="reminders.csv"');
			header('Pragma: no-cache');
			header('Expires: 0');

			$file = fopen('php://output', 'w');

			fputcsv($file, array('First', 'Last', 'Email', "Phone Number", "Agree to Text", "Appointment Date", "Appointment Time", "Opp ID", "Lead ID"));

			foreach ($arr_post as $post) {
				setup_postdata($post);
                $appointment = new GMG_Appointment( $post->ID );
				fputcsv( $file, array( $appointment->get_first_name(),
                                      $appointment->get_last_name(),
                                      $appointment->get_email(),
                                      $appointment->get_phone_number(),
                                      $appointment->get_send_text_message(),
                                      $appointment->get_appointment_date(),
                                      $appointment->get_appointment_time(),
                                      $appointment->get_opportunity_id(),
                                      $appointment->get_lead_id() )
                       );
			}

			exit();
		}
	}
}
