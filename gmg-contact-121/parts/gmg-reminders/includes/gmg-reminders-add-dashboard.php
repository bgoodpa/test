<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */

function gmg_reminders_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_reminders_dashboard_widget',         // Widget slug.
        'Add Service Reminder Appointments',         // Title.
        'gmg_reminders_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_reminders_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_reminders_dashboard_widget_function() {
    
    ?>
    <style>
        #gmg_reminders_dashboard_widget li {
            width: 50%;
            float: left;
            margin-bottom: 10px;
        }
        #gmg_reminders_dashboard_widget .main {
            padding: 0 12px 11px;
        }
        #gmg_reminders_dashboard_widget ul {
            margin: 0;
            display: inline-block;
            width: 100%;
        }
        #gmg_reminders_dashboard_widget a {
            margin-top: 10px;
        }
    </style>
    <div class="main">
        <ul>
            <li class="sr-sar">
                <strong>Enter Scheduled Appointment</strong>
                <a href="/sr-sar-entry" class="button" target="_blank">go to page</a>
            </li>
            <li class="sr-aar">
                <strong>Enter Anniversary Appointment</strong>
                <a href="/sr-aar-entry" class="button" target="_blank">go to page</a>
            </li>
        </ul>        
    </div>
	
<?php
    
}