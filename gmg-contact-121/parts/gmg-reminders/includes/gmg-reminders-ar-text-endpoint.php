<?php

/*
* 
* This file has functions to add a custom Endpoint to the API
* as well as handle the call to the endpoint.
*
* These are to handle request for text message from SharpSpring
*    
*/ 

/*
* 
* Set a custom endpoint
*
* url: https://fcpros.com/wp-json/gmg-121/sar-text
*
* This function handles text messages for Scheduled Appointment Reminders
*    
*/ 

add_action( 'rest_api_init', 'gmg_reminders_register_sar_texter_api' );
function gmg_reminders_register_sar_texter_api(){
    
    register_rest_route( 'gmg-121',
                      '/sar-text',
                      array(
                          'methods' => 'POST',
                          'callback' => 'gmg_reminders_sar_send_text',
                          'permission_callback' => '__return_true'
                      ) 
                     );
}

function gmg_reminders_sar_send_text( $request ){
    
    //get the parameters
    $parameters = $request->get_params();
    
    error_log( 'Name is '. $parameters['firstName'] );
    error_log( 'Date is '. $parameters['appointmentdate_600f184eeca08'] ); //appointmentdate_600f184eeca08
    error_log( 'Time is '. $parameters['appointmenttime_600f187364c9e'] ); //appointmenttime_600f187364c9e
    
    //It's from SharpSpring so continue
    error_log( 'Inside SAR Send Text!' );

    //set the array that everything will go into
    $message_array = array();
    
    //get the appointment date
    $raw_appt_date = DateTime::createFromFormat( 'Y-m-d' , $parameters['appointmentdate_600f184eeca08'] );
    error_log( 'New Date is '. $raw_appt_date->format('F j Y') );
    
    //Begin creating message
    array_push( $message_array , 'FROM ' . get_bloginfo( 'name' ) . '. ' );
    
    array_push( $message_array , 'Reminder: ' . $parameters['firstName'] . ', ');
    
    array_push( $message_array , ' appointment scheduled on ' . $raw_appt_date->format('F j Y') . ' at/between ' . $parameters['appointmenttime_600f187364c9e'] . '. ' );
    
    array_push( $message_array , 'Any questions or you need to reschedule, please call ' . get_field('scheduler_phone', 'sr_options') . '.' );
    
    gmg_reminders_send_text( $parameters, implode( $message_array ) );
    
}

/*
* 
* This function handles text messages for Anniversary Appointment Reminders
*
* url: https://fcpros.com/wp-json/gmg-121/aar-text
*
*/ 

add_action( 'rest_api_init', 'gmg_reminders_register_aar_texter_api' );
function gmg_reminders_register_aar_texter_api(){
    
    register_rest_route( 'gmg-121',
                      '/aar-text',
                      array(
                          'methods' => 'POST',
                          'callback' => 'gmg_reminders_aar_send_text',
                          'permission_callback' => '__return_true'
                      ) 
                     );
}

function gmg_reminders_aar_send_text( $request ){
    
    //get the parameters
    $parameters = $request->get_params();
    
    if( isset( $parameters['mobilePhoneNumber'] ) ){
    
        error_log( 'Name is '. $parameters['firstName'] );

        //It's from SharpSpring so continue
        error_log( 'Inside AAR Send Text!' );

        $raw_to_phone = preg_replace("/[^0-9]/", "", $parameters['mobilePhoneNumber'] );

        $to_phone[0] = "+";

        if( substr( $raw_to_phone, 0, 1 ) != "1" ){

            array_push( $to_phone, "1" );

        }

        array_push( $to_phone, $raw_to_phone );

        //set the array that everything will go into
        $message_array = array();

        //Begin creating message
        array_push( $message_array , 'FROM ' . htmlspecialchars_decode( get_bloginfo( 'name' ) ) . ': ' );

        array_push( $message_array , $parameters['firstName'] . ' , ');

        array_push( $message_array , "it's time to schedule your annual service appointment. " );

        array_push( $message_array , 'Go to http://bit.ly/3iS6EGz to request the best times to fit your schedule.' );

        gmg_reminders_send_text( $parameters , implode( $message_array ) );
        
    }    
}


function gmg_reminders_send_text( $parameters, $message ){
    
    //It's from SharpSpring so continue
    error_log( 'Inside New Send Text!' );
    
    $raw_to_phone = preg_replace("/[^0-9]/", "", $parameters['mobilePhoneNumber'] );
    
    $to_phone[0] = "+";
    
    if( substr( $raw_to_phone, 0, 1 ) != "1" ){
        
        array_push( $to_phone, "1" );
        
    }
    
    array_push( $to_phone, $raw_to_phone );

    $api_talker = new API_Talker();
    $api_talker->send_text_message( implode( $to_phone ) , $message );
    
}



//add_shortcode('text-appt', 'gmg_reminders_initiate');
function gmg_reminders_initiate(){
    
    $name = 'Raymond';
    $date = '2010-01-20';
    $time = '12 PM';
    
    //It's from SharpSpring so continue
    error_log( 'Inside AR Send Text Shortcode!' );

    //set phone number
    $phone_number = '+14843667096';

    //set the array that everything will go into
    $message_array = array();
    
    $raw_appt_date = DateTime::createFromFormat( 'Y-m-d' , $date );
    error_log( 'New Date is '. $raw_appt_date->format('F j Y') );

    //Set message parts
    array_push( $message_array , 'FROM ' . htmlspecialchars_decode( get_bloginfo( 'name' ) ) . '. ' );
    array_push( $message_array , 'Reminder: ' . $name . ', appointment scheduled');
    array_push( $message_array , ' on ' . $raw_appt_date->format('F j Y') . ' at/between ' . $time . '. ' );
    array_push( $message_array , 'Any questions or you need to reschedule, please call ' . $phone_number . '.' );

    $api_talker = new API_Talker();
    $api_talker->send_text_message($phone_number , implode( $message_array ) );
}

function gmg_reminders_get_days_until_appt( $appt_date ){

//    $appt_date = date_create( $appt_day );
    $appt_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
    $appt_date = $appt_date->add( new DateInterval('P1D') );

    $today_date = date_create( );
    $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );

    return date_diff( $appt_date, $today_date );     
}