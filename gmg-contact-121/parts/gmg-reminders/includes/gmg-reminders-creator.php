<?php

/*
*
* Autocreate specific pages for the platform if they do not exist
*    
*/

add_action( 'wp_loaded', 'gmg_service_check_if_pages_exist' );
function gmg_service_check_if_pages_exist(){
    
    //Check to see if Anniversary Scheduler Form page already exists.
    
    if ( !gmg_service_page_exists_by_slug('scheduler-form') && get_page_by_title( 'Scheduler Form') == NULL ) {

        $args = array(

            'post_type'     => 'page',
            'post_title'    => 'Scheduler Form',
            'post_name'     => 'sr-aar-scheduler-form',
            'post_content'      => '',
            'post_status'        => 'publish'
        );

        wp_insert_post( $args ); 


    //Check to see if Scheduled Appointment Entry Form page already exists.
    
    } elseif ( !gmg_service_page_exists_by_slug('sr-sar-entry') && get_page_by_title( 'Scheduled Appointment Entry Form') == NULL ) {

        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Scheduled Appointment Entry Form',
            'post_name'     => 'sr-sar-entry',
            'post_content'      => '',
            'post_status'        => 'publish'
        );

        wp_insert_post( $args );
        

    //Check to see if Anniversary Rerminder Entry Form page already exists.
    
    } elseif ( !gmg_service_page_exists_by_slug('sr-aar-entry') && get_page_by_title( 'Anniversary Reminder Entry Form') == NULL ) {

        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Anniversary Reminder Entry Form',
            'post_name'     => 'sr-aar-entry',
            'post_content'      => '',
            'post_status'        => 'publish'
        );

        wp_insert_post( $args );
    }
}

//Function that checks if page exists by slug.
function gmg_service_page_exists_by_slug( $post_slug ) {
    
    $args_posts = array(
        'post_type'      => array ( 'page' ),
        'post_status'    => array ( 'publish' ),
        'name'           => $post_slug,
        'posts_per_page' => 1,
    );
    
    if( get_posts( $args_posts ) ){
        return true;
    } else {
        return false;
    }
}
