<?php

add_action( 'admin_menu', 'register_gmg_reminders_menu_page', 11 );

function register_gmg_reminders_menu_page(){
    
//    error_log( 'Inside Boomer menu page ' );
    
    $plugin = new Gmg_Contact_121();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Service Reminders Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Service Reminders Settings',
            'post_id'       => 'sr_options',
            'capability'    => 'manage_options',
	));
}

add_filter('acf/load_field/key=field_600ae8a4d714d', 'gmg_sr_update_copyright', 10, 3);
function gmg_sr_update_copyright( $field ){
    
//    var_dump( $field );
    $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
    
    return $field;
}