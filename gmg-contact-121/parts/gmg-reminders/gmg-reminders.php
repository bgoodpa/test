<?php

/*
*
* This is the entry into Service Reminders. Entry is based on whether
* "Service Reminders" is checked in Settings page.
*    
*/

//$fields = get_fields('overall_settings');

//if( $fields['service_reminders'] != '' ){
//    if( $fields['service_reminders'][0] == 'yes' ){

//set the variable for the plugin path
$pros = plugin_dir_path( __FILE__ );

//Let's also bring in the class GMG_Appointment and GMG_Appointments
require_once( $pros . '/includes/gmg-class-reminders.php' );

//Let's bring in code to handle custom endpoint for API
require_once( $pros . '/includes/gmg-reminders-ar-text-endpoint.php' );

//Let's also bring in the Add Reminders Dashboard widget
require_once($pros . '/includes/gmg-reminders-add-dashboard.php');

//Let's also bring in code to create the right pages if they don't exist.
require_once( $pros . '/includes/gmg-reminders-creator.php');

//Let's also bring in code to add settings page to menu
require_once( $pros . '/includes/gmg-reminders-add-menu.php' );

//Let's also bring in the code to load settings ACF fields.
require_once( $pros . '/includes/gmg-reminders-acf.php' );

//Let's also bring in code to handle Next Service Date if last Service Date is set
require_once( $pros . '/includes/gmg-reminders-gf-aar-submit.php' );

//Let's also bring in code to handle if staff wants to text customer when entering 
require_once( $pros . '/includes/gmg-reminders-gf-sar-submit.php' );

//Let's bring in the custom post type Reminders
require_once( $pros . '/includes/gmg-reminders-cpt.php' );

//Let's also bring in codes of short
require_once( $pros . '/includes/gmg-reminders-shortcode.php' );

//Let's also bring in code to export
require_once( $pros . '/includes/gmg-reminders-export.php' );

//Let's also bring in the code to save the ACF fields.
//require_once( $pros . '/includes/gmg-reminders-acf.php' );

//Let's also bring in code to load the right page content.
//require_once( $pros . '/includes/gmg-reminders-loader.php');





//Let's also bring in the Import Reminders Dashboard widget
//require_once($pros . '/includes/gmg-reminders-import-dashboard.php');

//add_action( 'admin_menu', 'register_gmg_reminders_menu_page', 11 );
        
//    }
//}