jQuery(document).ready(function ($) {
    "use strict";
    
    $('#gmg-appointment-list').select2();
    
    $( "#gmg-set-appointment" ).click(function() {
        
//        alert( "Handler for .click() called." );
        
        var optionSel = $('#gmg-appointment-list option:selected');
        
        var username = optionSel.text();
        var apptID = $('#gmg-appointment-list').val();
        var dataFN = optionSel.attr('data-first_name');        
        
//        alert( "Handler for .click() called with " + username + " " + apptID + " " + dataFN );
        
        //change Post ID input
        $('#input_14_8').val( apptID );
        
        //Change first and last name inputs
        $('#input_14_2_3').val( optionSel.attr('data-first_name') );
        $('#input_14_2_6').val( optionSel.attr('data-last_name') );
        
        //Change email and phone number inputs
        $('#input_14_7').val( optionSel.attr('data-email') );
        $('#input_14_5').val( optionSel.attr('data-phone_number') );
        
        //Change appointment date and time inputs
        $('#input_14_3').val( optionSel.attr('data-appointment_date') );
        $('#input_14_4').val( optionSel.attr('data-appointment_time') );
    
    });
    
    $('#gmg-appointment-list').on('select2:select', function (e) {
        
//        alert( "Handler for .click() called." );
        
        var apptID = $('#gmg-appointment-list').val();
        
//        alert( "This Handler for .select() called with value " + apptID );
        
        if( apptID == "none" ){
            
            //Everything needs to be set to empty
            
            var none = "";
            
            //change Post ID input
            $('#input_14_8').val( none );

            //Change first and last name inputs
            $('#input_14_2_3').val( none );
            $('#input_14_2_6').val( none );

            //Change email and phone number inputs
            $('#input_14_7').val( none );
            $('#input_14_5').val( none );

            //Change appointment date and time inputs
            $('#input_14_3').val( none );
            $('#input_14_4').val( none );
            
            //Change send text message to no.
            $('input:radio[name="input_6"][value="Yes"]').prop('checked', false);
            $('input:radio[name="input_6"][value="No"]').prop('checked', true);
//            $('#choice_4_6_0').prop('checked', false);
//            $('#choice_4_6_1').prop('checked', true);
            
        } else {
                        
            var optionSel = $('#gmg-appointment-list option:selected');
            
//            alert( "Send Text Message is " + optionSel.attr('data-send_text_message') );
            
            //change Post ID input
            $('#input_14_8').val( apptID );

            //Change first and last name inputs
            $('#input_14_2_3').val( optionSel.attr('data-first_name') );
            $('#input_14_2_6').val( optionSel.attr('data-last_name') );

            //Change email and phone number inputs
            $('#input_14_7').val( optionSel.attr('data-email') );
            $('#input_14_5').val( optionSel.attr('data-phone_number') );

            //Change appointment date and time inputs
            $('#input_14_3').val( optionSel.attr('data-appointment_date') );
            $('#input_14_4').val( optionSel.attr('data-appointment_time') );
            
            //Change send text message radio button if need be
            if( optionSel.attr('data-send_text_message') == 'Yes' ){
                
                $('input:radio[name="input_6"][value="No"]').prop('checked', false);
                
            } else {
                
                $('input:radio[name="input_6"][value="Yes"]').prop('checked', false);
                
            }
            
            $('input:radio[name="input_6"][value="' + optionSel.attr('data-send_text_message') + '"]').prop('checked', true);
            
        }
        
/*        var username = optionSel.text();        
        var dataFN = optionSel.attr('data-first_name');        
        
//        alert( "Handler for .click() called with " + username + " " + apptID + " " + dataFN );
        
        //change Post ID input
        $('#input_14_8').val( apptID );
        
        //Change first and last name inputs
        $('#input_14_2_3').val( optionSel.attr('data-first_name') );
        $('#input_14_2_6').val( optionSel.attr('data-last_name') );
        
        //Change email and phone number inputs
        $('#input_14_7').val( optionSel.attr('data-email') );
        $('#input_14_5').val( optionSel.attr('data-phone_number') );
        
        //Change appointment date and time inputs
        $('#input_14_3').val( optionSel.attr('data-appointment_date') );
        $('#input_14_4').val( optionSel.attr('data-appointment_time') );*/
    
    });
    
});
