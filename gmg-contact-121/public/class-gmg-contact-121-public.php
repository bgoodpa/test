<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://goodmarketinggroup.com/
 * @since      1.0.0
 *
 * @package    Gmg_Contact_121
 * @subpackage Gmg_Contact_121/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gmg_Contact_121
 * @subpackage Gmg_Contact_121/public
 * @author     Good Group LLC <info@goodmarketinggroup.com>
 */
class Gmg_Contact_121_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Contact_121_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Contact_121_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gmg-contact-121-public.css', array(), $this->version, 'all' );
        wp_enqueue_style( 'selct2', "https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Contact_121_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Contact_121_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gmg-contact-121-public.js', array( 'jquery' ), $this->version, false );
        wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
        
        wp_enqueue_script( 'select2', "https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js", array( 'jquery' ) );

	}
    
        public function email_staff( $email_from, $message_array ) {
        
            //gets our api details from the database.
            $part_details = get_option( 'gmg-service-reminders-pval' );
            $email_to = $part_details['sr_scheduler_email'];

            error_log( 'Ready to send Mail' );

            $site_name = get_bloginfo( 'name' );
            $site_email = get_bloginfo( 'admin_email' );

            $subject = "New Scheduler Request from $site_name";

            $headers   = array();
            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-type:text/html;charset=UTF-8";
            $headers[] = "From: $site_name <$site_email>";
            $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
            $headers[] = "Reply-To: {$email_from}";
            $headers[] = "Subject: {$subject}";
            $headers[] = "X-Mailer: PHP/".phpversion();

            $answer = wp_mail($email_to, $subject, implode("\n\n" , $message_array), implode("\r\n", $headers));

            error_log( 'Answer is ' . $answer );
            return $answer;        
        
    }

}
