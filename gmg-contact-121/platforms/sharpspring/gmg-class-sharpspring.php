<?php

//error_log( 'Calling SharpSpring file');

class GMG_SharpSpring {
    
    private $baseURL;
    private $endPoint;
    
    //construct the class
    public function __construct( $base, $end ) {
        $this->baseURL = $base;
        $this->endPoint = $end;
    }
    
    //send parameters through curl
    public function gmg_sharpspring_curl( $parameters ){
        
        $response = false;
        
        // Prepare URL
        $ssRequest = $this->baseURL . $this->endPoint . "/jsonp/?" . $parameters;
        
//        error_log( 'Request is ' . $ssRequest );

        // set up curl
        $curl = curl_init();

        // the url to request
        curl_setopt( $curl, CURLOPT_URL, $ssRequest );           

        // return to variable
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true ); 

        // decompress using GZIP
        curl_setopt( $curl, CURLOPT_ENCODING, '');

        // don't verify peer ssl cert
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );

        // fetch remote contents, check for errors
        if ( false === ( $response = curl_exec( $curl ) ) ){
            
            $error = curl_error( $curl );
            
        } else {
            
            //if it worked, get the info
            $return = curl_getinfo( $curl );
        }

        // close the resource
        curl_close( $curl );

        if ( !$response ){

            return $error;

        } else {

            return $return;
        }
    }
    
    public function gmg_parse_core_sharpspring( $info ){

        // Prepare parameters
        $parameters = array();

        if( isset( $info['fname'] ) ) {

            array_push( $parameters, "FirstName=" . urlencode( $info['fname'] ) );
        }

        if( isset( $info['lname'] ) ) {

            array_push( $parameters, "LastName=" . urlencode( $info['lname'] ) );
        }

        if( isset( $info['email'] ) ) {

            array_push( $parameters, "emailAddress=" . urlencode( $info['email'] ) );
        }

        array_push( $parameters, "Subscribe=" . urlencode( 'No' ) );

        if( isset( $info['cookie'] ) ) {
            
            array_push( $parameters, "trackingid__sb=" . urlencode( $info['cookie'] ) );
        }
        
//        error_log( 'Parameters are: ' . implode( "&" , $parameters ) );

        return implode( "&" , $parameters );
    }
}

class GMG_API_Sharpspring {

    /**
     * @var string
     */
    
    protected $accountID;
    
    /**
     * @var string
     */
    
    protected $secretKey;

    /**
     * @param string $accountID 
     * @param string $secretKey
     */
    
    public function __construct($accountID = '', $secretKey = ''){
        
        $act_id = get_field('sharpspring_account_id', 'sr_options');

        
        if( get_field('sharpspring_account_id', 'sr_options') != null && 
           get_field('sharpspring_secret_key', 'sr_options') != null ){
            
            $this->accountID = get_field('sharpspring_account_id', 'sr_options');
            $this->secretKey = get_field('sharpspring_secret_key', 'sr_options');
            
        } else {
            
            $this->accountID = $accountID;
            $this->secretKey = $secretKey;
            
        }
    }

    /**
    * @param array $data
    */
    private function runQuery( $data ){
        
        $queryString = http_build_query(array('accountID' => $this->accountID, 'secretKey' => $this->secretKey));

        $url = "http://api.sharpspring.com/pubapi/v1/?$queryString";                             

        $data = json_encode($data);                                                                  
        $ch = curl_init($url);                                                                       
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                             
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                              
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                  
         'Content-Type: application/json',                                                        
         'Content-Length: ' . strlen($data)                                                       
        ));                                                                                                                                                                                       
        $query = json_decode( curl_exec($ch) );
        $return = curl_getinfo( $ch );
//        echo var_dump( $return );
        curl_close($ch); 

        return $query;
    }
    
    /**
    * Get Lead ID based on email
    */
    public function get_lead_id($email, $limit = 20, $offset = 0 ){
        
        $method = 'getLeads';
        $where = array( 'emailAddress'  => $email );
        $params = array('where' => $where, 'limit' => $limit, 'offset' => $offset);
        $requestID = session_id();                                                       

        $data = array(                                                                              
           'method' => $method,                                                               
           'params' => $params,                                                                  
           'id' => $requestID                                                                     
        );
        
        $getLeads = $this->runQuery($data);
        
//        echo var_dump( $data );

        if ( $getLeads->error ){ //CONTINUE FROM HERE
            
            error_log('There was an error in getting Lead ID!');
            
        } else {
            
            return $getLeads->result;
            
        }
    }
    
    /**
    * Get Fields
    */
    public function get_fields( $limit = 100, $offset = 0 ){
        
        $method = 'getFields';
        $where = array( 'isCustom' => '1' );
        $params = array('where' => $where, 'limit' => $limit, 'offset' => $offset);
        $requestID = session_id();                                                       

        $data = array(                                                                              
           'method' => $method,                                                               
           'params' => $params,                                                                  
           'id' => $requestID                                                                     
        );
        
        $getFields = $this->runQuery($data);
        
//        echo var_dump( $data );

        if ( $getFields->error ){ //CONTINUE FROM HERE
            
            error_log('There was an error in getting Fields!');
            
        } else {
            
            return $getFields->result;
            
        }
    }
    
    /**
    * Create Lead in SharpSpring - Only 1 at this time.
    */
    public function create_lead( $objects ){
        
        $method = 'createLeads';
        $params = array( 'objects' => $objects );
        $requestID = session_id();
        
        $data = array(                                                                              
           'method' => $method,                                                               
           'params' => $params,                                                           
           'id' => $requestID                                                                     
        );
        
//        echo var_dump( $data );
        
        $createLeads = $this->runQuery( $data );

        if ( $createLeads->error ){
            
            error_log('There was an error in Creating Lead!');
            
        } else {
            
            return $createLeads->result;
            
        }
    }
    
    /**
    * Update Lead in SharpSpring - Only 1 at this time.
    */
    public function update_lead( $objects ){
        
        $method = 'updateLeads';
        $params = array( 'objects' => $objects );
        $requestID = session_id();
        
        $data = array(                                                                              
           'method' => $method,                                                               
           'params' => $params,                                                           
           'id' => $requestID                                                                     
        );
        
//        echo var_dump( $data );
        
        $updateLeads = $this->runQuery( $data );

        if ( $updateLeads->error ){
            
            error_log('There was an error in Update Lead!');
            
        } else {
            
            return $updateLeads->result;
            
        }
    }
    
    /**
    * Create New Opportunity in SharpSpring - Only 1 at this time.
    */
    public function create_opportunity( $objects ){
        
        $method = 'createOpportunities';
        $params = array( 'objects' => $objects );
        $requestID = session_id();
        
        $data = array(                                                                              
           'method' => $method,                                                               
           'params' => $params,                                                           
           'id' => $requestID                                                                     
        );
        
//        echo var_dump( $data );
        
        $createOpportunities = $this->runQuery( $data );

        if ( $createOpportunities->error ){
            
            error_log('There was an error in creating opportunity!');
            
        } else {
            
            return $createOpportunities->result;
            
        }

        /*        
          CURLOPT_POSTFIELDS =>'{
            "method":"createOpportunities",
            "params":{
                "objects": [
                    {"ownerID":"313499702","dealStageID": "430300162", "opportunityName": "Catalog Website Project", "amount": "2500", "isClosed": "0", "isWon": "0", "closeDate": "2021-04-17 00:00:00", "isActive": "1", "primaryLeadID": "707997069314", "website_start_date_3968564226": "02-17-2021"}
                    ]
            },
            "id":1234
        }',
        */

    }
    
    /**
    * Update Opportunity in SharpSpring - Only 1 at this time.
    */
    public function update_opportunity( $objects ){
        
        $method = 'updateOpportunities';
        $params = array( 'objects' => $objects );
        $requestID = session_id();
        
        $data = array(                                                                              
           'method' => $method,                                                               
           'params' => $params,                                                           
           'id' => $requestID                                                                     
        );
        
//        echo var_dump( $data );
        
        $updateOpportunities = $this->runQuery( $data );

        if ( $updateOpportunities->error ){
            
            error_log('There was an error in updating opportunity!');
            
        } else {
            
            return $updateOpportunities->result;
            
        }

        /*        
        CURLOPT_POSTFIELDS =>'{
            "method":"updateOpportunities",
            "params":{
                "objects": [
                    { "id" : "8105528322", "ownerID": "313499702", "dealStageID": "430300162", "opportunityName": "Rose Anniversary Service Appointment on 2021-04-15", "amount": "600", "isClosed": "0", "isWon": "0", "closeDate": "2021-05-15 16:54:50", "isActive": "1", "primaryLeadID": "707898697730", "website_start_date_3968564226": "2021-04-15" }
                    ]
            },
            "id":1234
        }',
        */

    }

}