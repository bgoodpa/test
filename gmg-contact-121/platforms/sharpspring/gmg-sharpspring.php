<?php

add_action('gform_after_submission', 'post_to_third_party', 10, 2);

function post_to_third_party($entry, $form) {
	$body = [];

	function dupeCheck($fieldName, $bodyData) {
		$cleanLabel = substr(preg_replace("/[^a-zA-Z0-9]+/", "", $fieldName), 0, 24);
		for ($x = 0; $x <= 20; $x++) {
			if (array_key_exists($cleanLabel, $bodyData)) {
				$cleanLabel = $cleanLabel.$x;
			} else {
				break;
			}
		}
		return $cleanLabel;
	}

	$formFields = $form['fields'];
	foreach($formFields as $formField):
		if ($formField['label'] == 'sharpspring_base_uri') {
			$base_uri = rgar($entry, $formField['id']);
			$sendToSharpSpring = true;
		}
	elseif($formField['label'] == 'sharpspring_endpoint') {
		$post_endpoint = rgar($entry, $formField['id']);
	}
	elseif($formField['type'] == 'multiselect') {

		$fieldNumber = $formField['id'];
		$fieldLabel = dupeCheck($formField['label'], $body);
		$tempValue = rgar($entry, strval($fieldNumber));
		$trimmedValue = str_replace('[', '', $tempValue);
		$trimmedValue = str_replace(']', '', $trimmedValue);
		$trimmedValue = str_replace('"', '', $trimmedValue);
		$body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = $trimmedValue;

	}
	elseif($formField['inputs']) {
		if ($formField['type'] == 'checkbox') {
			$fieldNumber = $formField['id'];
			$fieldLabel = dupeCheck($formField['label'], $body);
			$checkBoxField = GFFormsModel::get_field($form, strval($fieldNumber));
			$tempValue = is_object($checkBoxField) ? $checkBoxField->get_value_export($entry) : '';
			$trimmedValue = str_replace(', ', ',', $tempValue);
			$body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = $trimmedValue;

		}
		elseif($formField['inputs']) {
			foreach($formField['inputs'] as $subField):
				$fieldLabel = dupeCheck($subField['label'], $body);
			$fieldNumber = $subField['id'];
			$body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = rgar($entry, strval($fieldNumber));
			endforeach;
		}
	} else {
		$fieldNumber = $formField['id'];
		$fieldLabel = dupeCheck($formField['label'], $body);
		$body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = rgar($entry, strval($fieldNumber));
	};
	endforeach;

	$body['form_source_url'] = $entry['source_url'];
	$body['trackingid__sb'] = $_COOKIE['__ss_tk']; //DO NOT CHANGE THIS LINE... it collects the tracking cookie to establish tracking
	$post_url = $base_uri.$post_endpoint;
	if ($sendToSharpSpring) {
		$request = new WP_Http();
		$response = $request->post($post_url, array('body' => $body));
	}
}

add_filter( 'gform_field_value_product_parent_cat', 'gmg_sharpspring_product_parent_cat' );
function gmg_sharpspring_product_parent_cat( $value ) {
    
    global $post;
    
    $terms = get_the_terms( $post, 'product_cat' );
    
//    error_log( 'Terms is ' . $terms );
    
    if( !is_wp_error( $terms ) ){
        
        if( is_array( $terms ) ){
            
            foreach( $terms as $term ){
                
                if( $term->parent ){
                    
                    $parent = get_term_by( 'id', $term->parent, 'product_cat', OBJECT );
                    
//                    error_log( $term->name . ' and parent ' . $parent->name );
                    
                    return $parent->name;
                }
                
            }
            
            return $terms[0]->name;
            
        } else {
            
            if( $terms->parent ){
                    
                $parent = get_term_by( 'id', $terms->parent, 'product_cat', OBJECT );

                return $parent->name;

            }
            
            return $terms->name;
        }
        
    }
    
    return '';
    
}

add_filter( 'gform_field_value_product_sub_cat', 'gmg_sharpspring_product_sub_cat' );
function gmg_sharpspring_product_sub_cat( $value ) {
    
    global $post;
    
    $terms = get_the_terms( $post, 'product_cat' );
    
    if( !is_wp_error( $terms ) ){
        
        if( is_array( $terms ) ){
            
            foreach( $terms as $term ){
                
                if( $term->parent ){
                    
                    return $term->name;
                }                
            }            
        } else {
            
            if( $terms->parent ){
                
                return $terms->name;
                
            }
        }        
    }
    
    return '';
    
}

add_filter( 'gform_field_value_consult_current_category', 'gmg_sharpspring_consult_current_category' );
function gmg_sharpspring_consult_current_category( $value ) {
    
    return get_queried_object()->name;
    
}

add_filter( 'gform_pre_render_1', 'gmg_populate_technicians_checkbox' );
add_filter( 'gform_pre_validation_1', 'gmg_populate_technicians_checkbox' );
add_filter( 'gform_pre_submission_filter_1', 'gmg_populate_technicians_checkbox' );
add_filter( 'gform_admin_pre_render_1', 'gmg_populate_technicians_checkbox' );
function gmg_populate_technicians_checkbox( $form ) {
    
//    error_log( 'Inside Pre Render with Form');
 
    foreach( $form['fields'] as &$field )  {
 
        //NOTE: replace 3 with your checkbox field id
        $field_id = 14;
        if ( $field->id != $field_id ) {
            continue;
        }
        
        //Instantiate Reviews Class
        $reviews = new Reviews();

        //grab all technicians
        $techs_raw = $reviews->get_technicians();
        
//        echo var_dump( $techs_raw );

        // Generate nice arrays that Gravity Forms can understand
        $input_id = 1;

        foreach( $techs_raw as $tech_raw ){
            
            //skipping index that are multiples of 10 (multiples of 10 create problems as the input IDs)
            if ( $input_id % 10 == 0 ) {
                $input_id++;
            }
            
            $choices[] = array( 'text' => $tech_raw['staff_name'], 'value' => $tech_raw['staff_name'] );
            $inputs[] = array( 'label' => $tech_raw['staff_name'], 'id' => "{$field_id}.{$input_id}" );
            
            $input_id++;
        }
        
        $field->choices = $choices;
        $field->inputs = $inputs;
    }
 
    return $form;
}