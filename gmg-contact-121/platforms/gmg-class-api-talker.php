<?php

//require __DIR__ . '/vendor/autoload.php';
// /home/fcpros/public_html/wp-content/plugins/gmg-contact-121/platforms/vendor/autoload.php
require '/home/gmgwebs/vendor/autoload.php';
use Twilio\Rest\Client;

class API_Talker {
    
    public function __construct() {}
    
    public function send_text_message( $phone, $message ){
        
        if( $phone ){
            
            error_log( "Text customer at phone number $phone with this message: $message" );
            
            //Twilio Sender Phone ID
            $sender_id = get_field('twilio_sender_sid', 'sr_options');
             
            //Twilio SID
            $TWILIO_SID = get_field('twilio_sid', 'sr_options');

            //Twilio Token
            $TWILIO_TOKEN = get_field('twilio_token', 'sr_options');
            
/*            $sender_id = '+16105699958';            
            
            $TWILIO_SID = 'AC1e8af62c4db80a4eeb68e6d60104ee86';
            $TWILIO_TOKEN = '036ea2a19c743d1af5d80de5d8a9cb0d';*/

            try{
                
//                $to = explode(',', $phone);
                $client = new Client( $TWILIO_SID, $TWILIO_TOKEN );

                $response = $client->messages->create(
                    $phone ,
                    array(
                      'from' => $sender_id,
                     'body' => $message
                    )
                );

//                       self::DisplaySuccess();
                
                error_log( 'Success!' );
                
                return true;

                } catch (Exception $e) {

//                    self::DisplayError( $e->getMessage() );

                error_log( 'Failure!' . $e->getMessage() );
                
                return false;
                
                }
        }
        
    }
    
    public function send_text_verify( $phone ){
        
        if( $phone ){
            
            error_log( "Verify phone number $phone" );
            
            //Twilio SID
            $TWILIO_SID = get_field('twilio_sid', 'sr_options');

            //Twilio Token
            $TWILIO_TOKEN = get_field('twilio_token', 'sr_options');
            
            $client = new Client( $TWILIO_SID, $TWILIO_TOKEN );
            
            $phone_number = $client->lookups->v1->phoneNumbers($phone)
                                    ->fetch(["type" => ["carrier"]]);
            
            return $phone_number->carrier;
            
            /*
            $verification = $client->verify->v2->services("VAc84512fd212b16347a6b68177fef76fa")
                ->verifications
                ->create( $phone, "sms" );
            
            echo var_dump( $verification->sid );*/
        }
    }
    
    public function phone_formatter( $phone ){
        
        $raw_to_phone = preg_replace( "/[^0-9]/", "", $phone );
        
//        error_log( $raw_to_phone );

        $to_phone[0] = "+";

        if( substr( $raw_to_phone, 0, 1 ) != "1" ){
            
            array_push( $to_phone, "1" );
        }

        array_push( $to_phone, $raw_to_phone );
        
//        error_log( 'Returning ' . implode( '' , $to_phone ) );
        
        return implode( '' , $to_phone );
    }
}