<?php

class GMG_Sharpspring_Model{
    
    //construct the class
    public function __construct() {}

    /**
    * Opportunity model
    */
    public function model_opportunity( $opp_info ){

        /*
        * {
        * "id": //NOT SET IF NEW
        * "ownerID":"313499702",
        * "dealStageID": "430300162",
        * "opportunityName": "Catalog Website Project",
        * "amount": "2500",
        * "isClosed": "0",
        * "isWon": "0",
        * "closeDate": "2021-04-17 00:00:00",
        * "isActive": "1",
        * "primaryLeadID": "707997069314"
        * }
        */

        $opp_array = array();

        //id (Only if update)
        if( isset( $opp_info['opportunity_id'] ) ){
            
            $opp_array['id'] = $opp_info['opportunity_id'];
        }

        //Owner ID (REQUIRED)
        $opp_array['ownerID'] = $opp_info['owner_id'];
        
        //primaryLeadID (REQUIRED)
        $opp_array['primaryLeadID'] = $opp_info['lead_id'];

        //Deal State ID (REQUIRED)
        $opp_array['dealStageID'] = $opp_info['deal_stage_id'];

        //Opportunity Name (REQUIRED)
        if( isset( $opp_info['opportunity_name'] ) ){

            $opp_array['opportunityName'] = $opp_info['opportunity_name'];
        } else {
            
            $opp_array['opportunityName'] = 'New Opportunity';            
        }
        //<-- end 
        
        //closeDate (REQUIRED)
        $today_date = new DateTime('now', new DateTimeZone('America/New_York') );
        
        if( isset( $opp_info['days_until_close'] ) ){
            
            $future_date = $today_date->add( new DateInterval('P' . $opp_info['days_until_close'] . 'D') );
        } else {
            
            $future_date = $today_date->add( new DateInterval('P30D') );            
        }
        
        error_log('Days until Close is ' . $future_date->format( 'Y-m-d H:i:s' ) );
        $opp_array['closeDate'] = $future_date->format( 'Y-m-d H:i:s' );
        //<-- end closeDate

        //Amount
        if( isset( $opp_info['opportunity_amount'] ) ){
            
            $opp_array['amount'] = $opp_info['opportunity_amount'];            
        }

        //isClosed
        if( isset( $opp_info['opportunity_closed'] ) ){

            $opp_array['isClosed'] = '1';
        } else {
            
            $opp_array['isClosed'] = '0';
        }

        //isWon
        if( isset( $opp_info['opportunity_won'] ) ){
            
            $opp_array['isWon'] = '1';
        } else {
            
            $opp_array['isWon'] = '0';
        }

        //isActive
        if( isset( $opp_info['opportunity_active'] ) ){
            
            $opp_array['isActive'] = '0';
        } else {
            
            $opp_array['isActive'] = '1';
        }

        return $opp_array;        

    }
    
    /**
    * Lead model
    */
    public function model_lead( $lead_info ){

        /*
        * {
        * "id": ""
        * "firstName": "",
        * "lastName": "",
        * "companyName": "",
        * "street": "",
        * "city": "",        
        * "state": "",
        * "zipcode": "",
        * "phoneNumber": "",
        * "mobilePhoneNumber": "",
        * "emailAddress":"",
        * }
        */

        $lead_array = array();
        
        //Email Address (REQUIRED)
        $lead_array['emailAddress'] = $lead_info['email']; 

        //id
        if( isset( $lead_info['lead_id'] ) ){
            
            $lead_array['id'] = $lead_info['lead_id'];
        }
        
        //First Name
        if( isset( $lead_info['first_name'] ) ){

            $lead_array['firstName'] = $lead_info['first_name'];
        }
        
        //Last Name
        if( isset( $lead_info['last_name'] ) ){
            
            $lead_array['lastName'] = $lead_info['last_name'];
        }
        
        //Company Name
        if( isset( $lead_info['company_name'] ) ){

            $lead_array['companyName'] = $lead_info['company_name'];
        }
        
        //Street
        if( isset( $lead_info['street'] ) ){
            
            $lead_array['street'] = $lead_info['street'];
        }
        
        //City
        if( isset( $lead_info['city'] ) ){
            
            $lead_array['city'] = $lead_info['city'];
        }
        
        //State
        if( isset( $lead_info['state'] ) ){
            
            $lead_array['state'] = $lead_info['state'];
        }
        
        //Zip Code
        if( isset( $lead_info['zip_code'] ) ){
            
            $lead_array['zipcode'] = $lead_info['zip_code'];
        }
        
        //Phone Number
        if( isset( $lead_info['phone_number'] ) ){
            
            $lead_array['phoneNumber'] = $lead_info['phone_number'];            
        }
        
        //Mobile Phone Number
        if( isset( $lead_info['mobile_phone_number'] ) ){
            
            $lead_array['mobilePhoneNumber'] = $lead_info['mobile_phone_number'];
        }
        
        return $lead_array;
    }    
}