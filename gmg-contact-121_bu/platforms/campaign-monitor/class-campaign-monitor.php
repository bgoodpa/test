<?php

class CampaignMonitor {
        
    private $api_key;

    private $id;

    private $list_id;
    
//      Construct
    
    public function __construct() {
        
        //gets our api details from the database.
        $api_details = get_option( 'gmg-contact-121-pval' );
        
        if(is_array($api_details) AND count($api_details) != 0) {
            
            $this->api_key = $api_details['cm_client_api'];
//            $this->id = $api_details['cm_client_id'];
        }        
    }
    
    public function read_subscriber_file(){
        
        require plugin_dir_path( __FILE__ ) . 'campaignmonitor/csrest_subscribers.php';
        
    }
    
    public function read_client_file(){
        
        require plugin_dir_path( __FILE__ ) . 'campaignmonitor/csrest_clients.php';
        
    }
    
    public function read_transactional_file(){
        
        require plugin_dir_path( __FILE__ ) . 'campaignmonitor/csrest_transactional_smartemail.php';
        
    }
    
//      Set Settings
    public function set_api_key( $api ){
        $this->api_key = $api;
    }
    
    public function set_id( $id ){
        $this->id = $id;
    }
    
    public function set_list_id( $list_id ){
        $this->list_id = $list_id;
    }
    
    
    
//      Get settings
    
    public function get_api_key(){
        return $this->api_key;
    }
    
    public function get_id(){
        return $this->id;
    }
    
    public function get_list_id(){
        return $this->list_id;
    }
    
    
    //Unsubscribe Subscriber    
    public function unsubscribe_subscriber( $list_id , $email ){
        
        require plugin_dir_path( __FILE__ ) . 'campaignmonitor/csrest_subscribers.php';    
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
        return $wrap->unsubscribe( $email );
    }
    

    
//      Add Subscriber
    
    public function add_subscriber( $cust_array, $list_id ){
        
        require plugin_dir_path( __FILE__ ) . 'campaignmonitor/csrest_subscribers.php';
        
        $email = $cust_array['email'];
        $name = $cust_array['fname'] . ' ' . $cust_array['lname'];
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
//        $result = $wrap->get( $email );
//        
//        echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
//        if($result->was_successful()) {
//            echo "Got subscriber <pre>";
//            var_dump($result->response);
//        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
//        }
        
        if( $wrap->get( $email )->was_successful() ){
            
            error_log('Update Subscriber');
        
            $result = $wrap->update(  $email ,
                array(
                    'EmailAddress' => $email,
                    'Name' => $name,
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        } else {
            
            error_log('Add Subscriber');
            
            $result = $wrap->add(
                array(
                    'EmailAddress'      => $email,
                    'Name'              => $name,
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;         
    }    
    
}
