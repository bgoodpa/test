<?php

$field = get_field('boomerang', 'overall_settings');
//echo var_dump( $field );

if( !empty( $field ) ){
    if( $field[0] == 'yes' ){
    
        $revs = plugin_dir_path( __FILE__ );
        
        //Let's bring in Boomerang CPT.
        require_once($revs . '/includes/gmg-boomerang-cpt.php');

        //Let's bring in updates to Customer Class.
        require_once($revs . '/includes/gmg-class-boomerang.php');

        //Let's also bring in the dashboard widget
        require_once($revs . '/includes/gmg-boomerang-dashboard.php');

        //Let's also bring in options to be show in admin
        require_once($revs . '/includes/gmg-boomerang-admin.php');

        add_action( 'admin_menu', 'register_gmg_boomerang_menu_page' );
//    add_action( 'admin_init', 'gmg_boomerang_admin_save' );
    }
}
