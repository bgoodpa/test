<?php

//Add Custom Post Type For Boomerangs
function boomerang_init() {
	$quo_labels = array(
		'name'                  => _x( 'Boomerangs', 'Post Type General Name', 'wellness_pro' ),
		'singular_name'         => _x( 'Boomerang', 'Post Type Singular Name', 'wellness_pro' ),
		'menu_name'             => __( 'Boomerangs', 'wellness_pro' ),
		'name_admin_bar'        => __( 'Boomerangs', 'wellness_pro' ),
		'archives'              => __( 'Boomerang Archives', 'wellness_pro' ),
		'attributes'            => __( 'Boomerang Attributes', 'wellness_pro' ),
		'parent_boomerang_colon'     => __( 'Parent Boomerang:', 'wellness_pro' ),
		'all_boomerangs'             => __( 'All Boomerangs', 'wellness_pro' ),
		'add_new_boomerang'          => __( 'Add New Boomerang', 'wellness_pro' ),
		'add_new'               => __( 'Add New', 'wellness_pro' ),
		'new_boomerang'              => __( 'New Boomerang', 'wellness_pro' ),
		'edit_boomerang'             => __( 'Edit Boomerang', 'wellness_pro' ),
		'update_boomerang'           => __( 'Update Boomerang', 'wellness_pro' ),
		'view_boomerang'             => __( 'View Boomerang', 'wellness_pro' ),
		'view_boomerangs'            => __( 'View Boomerangs', 'wellness_pro' ),
		'search_boomerangs'          => __( 'Search Boomerang', 'wellness_pro' ),
		'not_found'             => __( 'Not found', 'wellness_pro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wellness_pro' ),
		'featured_image'        => __( 'Featured Image', 'wellness_pro' ),
		'set_featured_image'    => __( 'Set featured image', 'wellness_pro' ),
		'remove_featured_image' => __( 'Remove featured image', 'wellness_pro' ),
		'use_featured_image'    => __( 'Use as featured image', 'wellness_pro' ),
		'insert_into_boomerang'      => __( 'Insert into Boomerang', 'wellness_pro' ),
		'uploaded_to_this_boomerang' => __( 'Uploaded to this Boomerang', 'wellness_pro' ),
		'boomerangs_list'            => __( 'Boomerangs list', 'wellness_pro' ),
		'boomerangs_list_navigation' => __( 'Boomerangs list navigation', 'wellness_pro' ),
		'filter_boomerangs_list'     => __( 'Filter Boomerangs list', 'wellness_pro' ),
	);
    
	$quo_args = array(
		'label'                 => __( 'Boomerang', 'wellness_pro' ),
		'description'           => __( 'Custom Post Type for Boomerangs', 'wellness_pro' ),
		'labels'                => $quo_labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'post',
	);
    
    $current_user = wp_get_current_user();
    if ( user_can( $current_user, 'administrator' ) ) {
        $quo_args['show_ui'] = true;
        $quo_args['show_in_menu'] = 'gmg-contact-121';
        $quo_args['menu_position'] = 7;        
    }
    
	register_post_type( 'boomerang', $quo_args );

}
add_action( 'init', 'boomerang_init' );