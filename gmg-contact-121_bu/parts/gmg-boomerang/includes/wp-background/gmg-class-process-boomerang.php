<?php

//error_log('Class Boomerang!');

class WP_Boomerang_Request extends WP_Async_Request {
    
//    error_log('Background Request!');

	/**
	 * @var string
	 */
	protected $action = 'request_boomerang';

	/**
	 * Handle
	 *
	 * Override this method to perform any actions required
	 * during the async request.
	 */
	protected function handle() {
		// Actions to perform
        
        $cust_array = $POST['customers'];
        error_log( 'Array count is ' . $cust_array );
	}

}


class WP_Boomerang_Process extends WP_Background_Process {
    
//    error_log('Background Process!');
    
    /**
	 * @var string
	 */
	protected $action = 'process_boomerang';

	/**
	 * Task
	 *
	 * Override this method to perform any actions required on each
	 * queue item. Return the modified item for further processing
	 * in the next pass through. Or, return false to remove the
	 * item from the queue.
	 *
	 * @param mixed $item Queue item to iterate over
	 *
	 * @return mixed
	 */
	protected function task( $item ) {
        
		// Actions to perform
//        error_log('Doing task for first name: ' . $item['fname'] );
        
        $bcs = new BoomerangCustomers();
        
        $post = get_post( intval( $item['job'] ) );
        $list_id = get_post_meta( $post->ID, 'boomerang_list', true);        
//        error_log('List ID is: ' . $list_id );
        
        $bm = new BoomerangCM();
//        $result = $bm->add_subscriber( $item , $list_id );
        $bm->add_subscriber( $item , $list_id );
        error_log( 'Index: ' . $item['index'] . ', ' . $item['email'] . " was sent." );
//        error_log( "Processing: " . $item['index'] );
        
        $bcs->really_long_running_task();

		return false;
	}
    
    /**
	 * Complete
	 *
	 * Override if applicable, but ensure that the below actions are
	 * performed, or, call parent::complete().
	 */
	protected function complete() {
		parent::complete();
        
        error_log('Complete!');

		// Show notice to user or perform some other arbitrary task...
	}
    
}

