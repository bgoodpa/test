<?php

class Boomerang{
    
    private $id;
//    private $list_id;
    
    public function __construct( $id ) {
        $this->id = $id;
//        $list_id = get_field( 'boomerang_list', $id );
    }
    
    /*
    Update ____________________________________
    */
    
    public function update_title(){
        
        $campaign_name = $this->get_boomerang_list();        
        $tag_names = array();        
        $tags = $this->get_boomerang_tags();
        
        if( count( $tags ) > 0 ){            
            $post_title = $campaign_name . ' for ' . implode( ' & ' , $tags );            
        } else {            
            $post_title = $campaign_name;            
        }

        $update_post = array(
            'ID'     => $this->id,
            'post_title'  => $post_title,
            'post_name' => $post_title
        );

        wp_update_post( $update_post );        
    }
    
    /*
    Get _______________________________________
    */
    
    //Get Boomerang ID
    public function get_boomerang_id(){        
        return $this->id;        
    }
    
    //Get Boomerang Post Tags
    public function get_boomerang_tags(){        
        return wp_get_post_tags( $this->id );        
    }
    
    //Get Boomerang Selected Campaign
    public function get_boomerang_list(){
//        echo var_dump( $list );
        return get_field( 'boomerang_list', $this->id );
    }
    
    /*
    Set ______________________________________
    */
    
    //Set Boomerang Post Tags
    public function set_boomerang_tags( $tags ){      
        wp_set_post_tags( $this->id, $tags, true );      
    }
    
}

class Boomerangs{
    
    public function __construct() {
        
    }
    
    public function get_all_boomerangs(){
        
        $booms = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'boomerang' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'publish_date',
            'post_status'            => 'publish',
            'order'                  => 'ASC' 
        );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                array_push( $booms, array( get_the_ID() => get_the_title() ) );
            }
                
        }  else {
            // no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( count( $booms ) > 0 ){
            
            return $booms;
            
        } else{
            
            return false;
        }
    }
    
    
    
}

class BoomerangCustomer extends Customer {
    
//    private $category;
    
//    private $list;
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
/*    
*
*    GET BoomerangCustomer
*
*        */
    
    public function get_customer_interests(){
        return get_field( 'interests', $this->id );
    }
    
    //Get Customer Post Tags
    public function get_customer_tags(){        
        return wp_get_post_tags( $this->id );        
    }   
    
/*    
*
*    SET BoomerangCustomer
*
*        */
    
//    Set Customer Interests

    public function set_customer_interests( $interests ){
        $field = get_field_object( 'field_5aa82444d36f2' );
//        error_log( 'Field updated!');
        
        if( $field ) {
//            error_log( 'Field has this many choices: ' . count( $field['choices'] ) );
            $match = false;
            
            if( is_array( $field['choices' ] ) && count( $field['choices'] > 0 ) ){
//                echo var_dump( $field );
                $choices_array = $field['choices' ];
                
//                error_log( 'Choices: ' . count( $choices_array ) );
            
                foreach( $choices_array as $k => $v ){
//                    echo var_dump( $choice );
                    if( $interests != $v ){
//                        error_log( 'No match with ' . $interests . ' and ' . $v );                    
                    } else {
//                        error_log( 'Match with ' . $interests . ' and ' . $v );
                        $match = true;
                    }
                }
//                error_log( 'Done with foreach' );
            }
            
            if( !$match ){
//                error_log( 'Inside match false');
                $field['choices'][ $interests ] = $interests;
//                error_log( 'Field choice updated');
                update_field( 'field_5aa82444d36f2', $field['choices'], $this->id );
//                error_log( 'Field now has this many choices: ' . count( $field['choices'] ) );
            }            
        }
//        update_field( 'interests', $interests, $this->id );
    }
    
    
    public function set_customer_tags( $tags ){      
        wp_set_post_tags( $this->id, $tags, true );      
    }
    
    
//    This function takes the List ID attached to a Category and returns the name of the Category to be set as a customer's interests.
    
    public function process_categories( $passed_cat ){
        
        $fields = get_fields('boom_options');
        $categories = $fields['new_categories'];
        if( is_array( $categories ) && count( $categories ) > 0 ){
            foreach( $categories as $cat){
                if ( $passed_cat == $cat['cat_list_id']['value'] ){
                    $this->set_customer_interests( $cat['cat_name'] );
                }
            }
        } else {
            if( $categories ){
                if ( $passed_cat == $categories['cat_list_id']['value'] ){
                    $this->set_customer_interests( $cat['cat_name'] );
                }                
            }
        }        
    }    
}

class BoomerangCustomers extends Customers{
        
    public function process_customer( $cust_info ){
                            
//      Check if Customer exists.

        if( !$this->check_if_customer_exists_by_email( $cust_info['email'] ) ){
                                
//       If not, create one.
            
//            error_log( 'Customer does not exist so let us create one!' );         
            $c_id = $this->create_new_customer( $cust_info );
        
        } else {            
//            If so, get Customer.
//            error_log( 'Customer exists!' );
            
            $c_id = $this->get_customer_by_email( $cust_info['email'] );
        }        
        return $c_id;
    }
    
    
    public function get_all_interests(){
        
        $cust_array = $this->get_all_customers();
//        error_log( 'Got all Customers with a count of ' . count( $cust_array ) );
        $interests_array = array();
        
        if( is_array( $cust_array ) && count( $cust_array ) > 0 ){
            foreach( $cust_array as $cust ){
//                error_log( 'Customer has ID of ' . key( $cust) );
                $customer = new BoomerangCustomer( key( $cust) );
                if( $customer->get_customer_interests() ){
//                    error_log( 'Interersts might be ' . $customer->get_customer_interests() );
                    array_push( $interests_array , $customer->get_customer_interests() );
                }
            }
        }
        
        if( count( $interest_array) > 0 ){
            return $interests_array;
        } else {
            return false;
        }        
    }
    
    public function really_long_running_task() {
		return sleep( 5 );
	}
}

class BoomerangCM extends CampaignMonitor{
        
    private $api_key;
    
    private $client_id;
    
    function __construct() {
        parent::__construct();
        
        $fields = get_fields('overall_settings');
//        var_dump( $options );
        
        if( is_array( $fields ) AND count( $fields ) != 0) {
            $this->api_key = $fields['cm_client_api_key'];
//            error_log( 'API key is ' . $api_details['cm_client_api'] );
            $this->client_id = $fields['cm_client_id'];
        }
    }
    
    public function get_clients_list(){        
        
        $this->read_client_file();
        
//        error_log( 'API key is ' . $this->api_key );
//        error_log( 'Cliend ID is ' . $this->client_id );
        
        $wrap = new CS_REST_Clients(
            $this->client_id,
            array( 'api_key' => $this->api_key )
        );
        $result = $wrap->get_lists();
//        echo "Result of /api/v3.1/clients/{id}/lists\n<br />";
        if($result->was_successful()) {
            
            $list_array = array();
            $result_array = $result->response;
            foreach( $result_array as $result ){
                if( is_object( $result ) ){
                    array_push( $list_array, json_decode(json_encode($result), True) );
                }
                
            }
            
//            var_dump( $list_array );
            return $list_array;
            
//            echo "Got lists\n<br /><pre>";
//            var_dump($result->response);
            
        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
        }
//        echo '</pre>';
//        var_dump( $result );
//        return $result;
        }
    
    //Check Subscriber
    
    public function check_subscriber( $email, $list_id ){
        
        $this->read_subscriber_file();
        
        $wrap = new CS_REST_Subscribers( 
            $list_id,
            array( 'api_key' => $this->api_key )
        );
        
        $result = $wrap->get( $email );
        error_log( 'Result: ' . $result->was_successful() );
        
        if( $result->was_successful() ){
            return true;
        } else {
            return false;
        }
    }
    
    //      Add Subscriber
    
    public function add_subscriber( $cust_array, $list_id ){
        
        $this->read_subscriber_file();
        
        $email = $cust_array['email'];
        $name = $cust_array['fname'] . ' ' . $cust_array['lname'];
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
        $result = $wrap->get( $email, true );
        
//        error_log( 'Email? ' . $result->http_status_code );
        
        if( $result->was_successful() ){
            
//            error_log( 'Got email with code ' . $result->http_status_code );
            $result = $wrap->update( $email,
                                    array(
                                        'EmailAddress' => $email,
                                        'Name' => $name,
                                        'ConsentToTrack' => 'yes',
                                        'Resubscribe' => true,
                                        'RestartSubscriptionBasedAutoresponders' => true,
                                        )
                                   );
//        
        } else {
            
//            error_log( 'No email so add!' );
            
            //Add them if they aren't.        
            $result = $wrap->add(
                array(
                    'EmailAddress' => $email,
                    'Name' => $name,
                    'ConsentToTrack' => 'yes',
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;
    }
}

class Boomerang_Background_Processing {
	/**
	 * @var WP_Example_Request
	 */
	protected $request_boomerang;
	/**
	 * @var WP_Example_Process
	 */
	protected $process_boomerang;
	/**
	 * Boomerang_Background_Processing constructor.
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
//		add_action( 'admin_bar_menu', array( $this, 'admin_bar' ), 100 );
//		add_action( 'init', array( $this, 'process_handler' ) );
	}
	/**
	 * Init
	 */
	public function init() {
//        error_log('In init!');
        
        require_once plugin_dir_path( __FILE__ ) . '/wp-background/wp-background-processing.php';
        require_once plugin_dir_path( __FILE__ ) . '/wp-background/gmg-class-process-boomerang.php';
        $this->request_boomerang = new WP_Boomerang_Request();
        $this->process_boomerang = new WP_Boomerang_Process();
        
//        echo var_dump( $this->process_boomerang );
	}
    
	/**
	 * Handle single
	 */
	public function handle( $customers_array ) {
//        $this->request_boomerang array( 'customer' => $customers_array );
//		$names = $this->get_names();
//		$rand  = array_rand( $names, 1 );
//		$name  = $names[ $rand ];
//		$this->process_single->data( array( 'name' => $name ) )->dispatch();
	}
	/**
	 * Handle all
	 */
	public function handle_all( $cust_array ) {
        
        foreach( $cust_array as $cust ){
            
            $bcs = new BoomerangCustomers();
            $c_id = $bcs->process_customer( $cust );
            $this->process_boomerang->push_to_queue( $cust );
        }
        $this->process_boomerang->save()->dispatch();
    
    }
    
    public function take_a_nap(){
        
            return sleep( 5 );
        }
}

//new Boomerang_Background_Processing();
