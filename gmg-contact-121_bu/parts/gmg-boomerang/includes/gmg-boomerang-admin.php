<?php

function register_gmg_boomerang_menu_page(){
    
//    error_log( 'Inside Boomer menu page ' );
    
    $plugin = new Gmg_Contact_121();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Boomerang Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Boomerang Settings',
            'post_id'       => 'boom_options',
	));   
    
}

//add_filter('acf/load_field/key=field_5aa82444d36f2', 'gmg_customer_interests_load', 10, 3);
function gmg_customer_interests_load( $field ){
    
    error_log( 'Let us load interests!' );
    
    $bcs = new BoomerangCustomers();
    $interests = $bcs->get_all_interests();
    
    error_log( 'Interests are many numbers ' . count( $interests ) );
    
    if( !interests ) {
        foreach( $interests as $interest ) {            
            $field['choices'][ $interest ] = $interest;
            error_log( 'Interest!');
        }        
    }
    
    error_log( 'Lets return!');
    return $field;   

}

add_filter('acf/load_field/key=field_5cc344a2119ab', 'gmg_cm_lists_load', 10, 3);
function gmg_cm_lists_load( $field ){
    
//    error_log( 'Load New Boomerang Lists!' );
    
    $bm = new BoomerangCM();
    $list_array = $bm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}

add_filter('acf/load_field/key=field_5c2fd4c6a3407', 'gmg_cm_boom_lists_load', 10, 3);
function gmg_cm_boom_lists_load( $field ){
    
//    error_log( 'Load Boomerang Lists!' );
    
    $bm = new BoomerangCM();
    $list_array = $bm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}

add_filter( 'template_include', 'gmg_boomerang_handle_customers' ); 
function gmg_boomerang_handle_customers( $original_template ) {
    
//    http://test.goodgroupllc.com/fireplaces/?bcid=484
//    http://test.goodgroupllc.com/stoves/?bcid=484
    
//    error_log( 'Inside checking for customer!');
    
    if( isset( $_GET['bcid'] ) ){
        
//        error_log( 'Get is ' . $_GET['bcid'] );
        $customer = new Customer( $_GET['bcid'] );
        error_log( 'Customer name is ' . $customer->get_name() );
        
        global $post;
        error_log( 'Page is ' . $post->post_title );
        
        $tags = get_field( 'overall_tags', $post->ID );
        
        if( isset( $tags ) ){
            $customer->process_overall_tags( $tags );           
            
        }
        
    }
    
    return $original_template;
}