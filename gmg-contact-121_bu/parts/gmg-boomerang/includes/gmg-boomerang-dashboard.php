<?php

function gmg_boomer_loaded(){
    error_log( 'Class Processing Loaded!');
    global $class_processing;
    $class_processing = new Boomerang_Background_Processing();
}
//add_action( 'plugins_loaded', 'gmg_boomer_loaded' );

global $classy_processing;
$classy_processing = new Boomerang_Background_Processing();

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_boomerang_add_dashboard_widget() {

	wp_add_dashboard_widget(
        'gmg_boomerang_dashboard_widget',         // Widget slug.
        'Add Bomerang Customer',         // Title.
        'gmg_boomerang_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_boomerang_add_dashboard_widget' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_boomerang_dashboard_widget_function() {
    
    ?>

    <form name="gmg_boomerang_form" action="" id="gmg_boomerang_form" method="POST" enctype="multipart/form-data">
        
        <div class="boomerang-add-box">
            
            <?php $booms = new Boomerangs(); ?>
            <?php if( $booms->get_all_boomerangs() != false ): ?>
            
                <p><strong>First, pick your Boomerang Job.</strong></p>            

                <p>
                    <label for="fieldJob">Boomerang Job</label><br />
                    <select id="fieldJob" name="fieldJob" >

                        <?php $booms_array = $booms->get_all_boomerangs(); ?>
                            <?php foreach( $booms_array as $boom ): ?>
                                <option value="<?php echo key( $boom ) ?> "><?php echo $boom[ key( $boom ) ] ?></option>';
                            <?php endforeach; ?>
                    </select>
                </p>
            
            <?php endif; ?>
            
           <p><strong>Now, either add one at a time...</strong></p>

            <p>
                <label for="fieldName">Name</label><br />
                <input id="fieldFName" name="fieldFName" placeholder="First" type="text" /> <input id="fieldLName" name="fieldLName" placeholder="Last" type="text" />
            </p>
            <p>
                <label for="fieldEmail">Email</label><br />
                <input id="fieldEmail" name="fieldEmail" type="email" placeholder="Email" />
            </p>


        
        </div>
        
        <div class="boomerang-import-box">
            <p><strong>...or import many.</strong></p>
            <p>Use only a .csv. Choose which column the information is in. Also, please delete any heading rows.</p>

            <table>
                <tr>
                    <td>
                        <label for="CustFName">First Name *</label>
                        <input id="CustFName" name="CustFName" type="number" value="1" style="width: 100%;" />
                    </td>
                    <td>
                        <label for="CustLName">Last Name *</label>
                        <input id="CustLName" name="CustLName" type="number" value="2" style="width: 100%;" />
                    </td>
                    <td>
                        <label for="CustEmail">Email *</label><br />
                        <input id="CustEmail" name="CustEmail" type="number" value="3" style="width: 100%;"/>
                    </td>
                </tr>
            </table>
            
            <label for="import_boomerang_csv">Choose file to upload</label>
            <input type="file" name="import_boomerang_csv" id="import_boomerang_csv">
        </div>
        
        <?php if( file_exists ( plugin_dir_path( __FILE__ ) . 'boom_error_log.txt' ) ) : ?>
            <a href="<?php echo plugins_url( 'boom_error_log.txt', __FILE__ ); ?>" target="_blank">Error Log</a>
        <?php endif; ?>
        
        <input type="hidden" name="gmg_boomerang_action" value="gmg_boomerang_submit" />
        <p>
            <input type="submit" name="gmg_boomerang_dashboard_button" class="button button-primary" value="Submit">
        </p>
    </form>
    <?php
}

add_action( 'init', 'func_gmg_boomerang_send_contact' );
function func_gmg_boomerang_send_contact() {
    
    //First, check that we're calling the right action.    
	if( isset($_POST['gmg_boomerang_action']) ) {
        
        //Now let's check if we're using the import and if there's a file.        
        if( $_FILES['import_boomerang_csv'] && $_FILES['import_boomerang_csv']['name'] != '' ) {
            
            $filename = explode('.', $_FILES['import_boomerang_csv']['name']);
            
            //Is the form a .csv because that's all we're accepting right now.
            if( $filename[1]=='csv' ){
                
                $csv_array = array_map('str_getcsv', file( $_FILES['import_boomerang_csv']['tmp_name'] ));
                
                $fname_column = intval($_POST['CustFName']) - 1;
                $lname_column = intval($_POST['CustLName']) - 1;
                $email_column = intval($_POST['CustEmail']) - 1;
                
                //$boomerang_background = new Boomerang_Background_Processing();
                //global $classy_processing;
                //echo var_dump( $classy_processing );
                
//                echo var_dump( $csv_array );
                
                $error_array = array();
                $customers_array = array();
                $index = 1;
                
                foreach( $csv_array as $csv_line ){
                    
//                    error_log( 'Index is ' . $index );
                    
                    if( $csv_line[$fname_column] != '' && $csv_line[$fname_column] != ' ' && $csv_line[$lname_column] != '' && $csv_line[$lname_column] != ' ' && $csv_line[$email_column] != '' && $csv_line[$email_column] != ' ' ){
                        
//                        error_log( 'Inside CSV with Field Job ' . $_POST['fieldJob'] );
                        
                        $cust_info = array(
                            'fname'          => ltrim( rtrim( $csv_line[$fname_column] ) ),
                            'lname'          => ltrim( rtrim( $csv_line[$lname_column] ) ),
                            'email'         => ltrim( rtrim( $csv_line[$email_column] ) ),
                            'job'           => $_POST['fieldJob'],
                            'index'         => $index
                        );
                        
//                        error_log( 'Customer has job ' . $cust_info['job'] );
                        array_push( $customers_array, $cust_info );

                    } else {

//                        error_log( 'Problem data' );

                        if ( $csv_line[$fname_column] != '' && $csv_line[$fname_column] != ' ' && $csv_line[$lname_column] != '' && $csv_line[$lname_column] != ' ' ){
                            
                            $error_cust = ltrim( rtrim( $csv_line[$fname_column] ) ) . ' ' . ltrim( rtrim( $csv_line[$lname_column] ) );

                        } elseif(  $csv_line[$lname_column] != '' && $csv_line[$lname_column] != ' ' ){
                            $error_cust = ltrim( rtrim( $csv_line[$lname_column] ) );
                        } else {
                            $error_cust = ltrim( rtrim( $csv_line[$fname_column] ) );
                        }
//                        error_log( 'Customer data' . $error_cust );
                        array_push( $error_array, $error_cust . ' was not imported.' );
//                        error_log( 'Error Array count is ' . count( $error_array ) );                    
                    }
                    $index++;
                }
                
                if( count( $customers_array ) > 0  ){
                    
//                    error_log('Handle All with count ' . count( $customers_array ) );                    
                    global $classy_processing;
                    $classy_processing->handle_all( $customers_array );
                    
                }
            }
            
            //Otherwise, we're just entering one file at a time.
            
        } else {
            
            if( $_POST['fieldFName'] && $_POST['fieldLName'] && $_POST['fieldEmail'] ){
                
                $cust_info = array(
                    'fname'          => $_POST['fieldFName'],
                    'lname'          => $_POST['fieldLName'],
                    'email'         => $_POST['fieldEmail'],
                    'job'           => $_POST['fieldJob']
                );
            
                $bcs = new BoomerangCustomers();          
                $c_id = $bcs->process_customer( $cust_info );
                
//                if( $_POST['fieldCat'] ){
////                    echo var_dump( get_fields( $c_id ) );
//                    $bc = new BoomerangCustomer( $c_id );
//                    $bc->process_categories( $_POST['fieldCat'] );
//                }
                
//                error_log( 'Inside One at time with Field Job ' . $_POST['fieldJob'] );
                
//                error_log('Categories processed' );
                $post = get_post( intval( $_POST['fieldJob']) );
                $boomer = new Boomerang( $post->ID );
                $list_id = get_post_meta( $post->ID, 'boomerang_list', true);
                
//                error_log('Now List ID is: ' . $list_id );
                
                $bm = new BoomerangCM();
                $result = $bm->add_subscriber( $cust_info , $list_id );

                if($result->was_successful()) {
                    error_log( "Subscribed with code ". $result->http_status_code);
                } else {
                    error_log( 'Failed with code '. $result->http_status_code );
                    $file = fopen( plugin_dir_path( __FILE__ ) . 'captureErrors.txt', "w" );
                    fwrite( $file, json_encode($result, JSON_PRETTY_PRINT) );
                    fclose($file);
                }
            }
        }
        
        wp_redirect( $_SERVER['HTTP_REFERER'] );
        exit();
    }

}