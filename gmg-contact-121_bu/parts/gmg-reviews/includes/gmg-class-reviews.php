<?php

/*
* 
* Class that handles a Review
*     
*/

class Review {
    
    private $fname;
    
    private $lname;
    
    private $email;
    
    private $review;
    
    private $id;
    
    private $subject;
    
    private $stars;
    
    public function __construct( $id ) {
        $this->id = $id;
        $this->fname = get_field( 'reviewer_fname' , $this->id );
        $this->lname = get_field( 'reviewer_lname' , $this->id );
        $this->email = get_field( 'reviewer_email' , $this->id );
    }
    
    
//    Create Review
    
    public function update_review( $info_array ){
        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $this->id);
        }
    }
    
    public function update_title(){
        
        $name = $this->fname . ' ' . $this->lname;

        $new_post = array(
            'ID'     => $this->id,
            'post_title'  => $name . ' Review',
            'post_name' => $name . ' Review'
        );

        wp_update_post( $new_post );        
    }
    
//    Get Review
    
    public function get_id(){
        return $this->id;
    }
    
    public function get_name(){
        return $this->name;
    }
    
    public function get_email(){
        return $this->email;
    }
    
    public function get_review(){
        return get_field( 'review' , $this->id );
    }
    
    public function get_town(){
        return get_field( 'reviewer_town' , $this->id );
    }
    
    public function get_subject(){
        return get_field( 'subject' , $this->id );
    }

    public function get_stars(){
        return get_field( 'review_rating' , $this->id );
    }
    
    
    //    Set Review
    
    public function set_fname( $fname ){
        update_field( 'reviewer_fname', $fname, $this->id );
    }
    
    public function set_lname( $lname ){
        update_field( 'reviewer_lname', $lname, $this->id );
    }
    
    public function set_email( $email ){
        update_field( 'reviewer_email', $email, $this->id );
    }
    
    public function set_town( $town ){
        update_field( 'reviewer_town', $town, $this->id );
    }
    
    public function set_review( $review ){
        update_field( 'review', $review, $this->id );
    }
    
    public function set_subject( $subject ){
        $cat = get_term( $subject, 'category' );
        update_field( 'subject', array( $cat->name ), $this->id );
        wp_set_post_categories( $this->id, array( $subject ), true );
    }

    public function set_stars( $stars ){
        update_field( 'review_rating', array( $stars ), $this->id );
    }
    
}

/*
* 
* Class that handles many Reviews
*     
*/

class Reviews {
    
        public function __construct( ) {
            
        }
    
    //    Check Reviews    
    public function check_if_review_exists( $review ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'review',
                    'value'   => $review,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            return true;            
        } else{ return false; }
        
    }
    
    public function get_review( $review ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'review',
                    'value'   => $review,
                    'compare' => '=',
                ),
            ),
        );
        
        return get_posts( $args )[0];
        
    }
    
    public function create_new_review( $info_array ){
        
        $my_post = array(
            'post_title'        => $info_array['reviewer_fname'] . ' ' . $info_array['reviewer_lname'] . ' Review',
            'post_type'         => 'reviews',
            'post_status'       => 'pending'
        );
        
        $new_id = wp_insert_post( $my_post );
        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $new_id );
        }
        
        return $new_id;
        
    }
    
    public function get_all_reviews(){
         
        $reviews = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $reviews, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $reviews;
        
    }
    
}

/*
* 
* Extend Customer functionality for Reviews
*     
*/

class ReviewCustomer extends Customer{
    
}

/*
* 
* Extend Customers functionality for Reviews
*     
*/

class ReviewCustomers extends Customers{
    
    public function get_last_used(){        
        $fields = get_fields('rm_options');
        if( $fields['last_used'] ){
            return $fields['last_used'];
        } else {
            return false;
        }
    }
    
    public function set_last_used(){
        
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        update_field( 'last_used', $today_date->format('F j, Y'), 'rm_options' );
    }
}

/*
* 
* Extend CampaignMonitor functionality for Reviews
*     
*/

class ReviewCM extends CampaignMonitor{
    
    private $api_key;
    private $client_id;
//    private $list_id;
    
    function __construct() {
        parent::__construct();
        
        $fields = get_fields('overall_settings');
        
        if( is_array( $fields ) AND count( $fields ) != 0) {
            $this->client_id = $fields['cm_client_id'];
            $this->api_key = $fields['cm_client_api_key'];
            
//            $rm_fields = get_fields('rm_options');
//            $this->list_id = get_field( 'rm_list_id' , 'rm_options' );
        }
    }
    
    public function get_clients_list(){
        
        $this->read_client_file();
        
//        error_log( 'API key is ' . $this->api_key );
        
        $wrap = new CS_REST_Clients(
            $this->client_id,
            array( 'api_key' => $this->api_key )
        );
        $result = $wrap->get_lists();
//        echo "Result of /api/v3.1/clients/{id}/lists\n<br />";
        if($result->was_successful()) {
            
            $list_array = array();
            $result_array = $result->response;
            foreach( $result_array as $result ){
                if( is_object( $result ) ){
                    array_push( $list_array, json_decode(json_encode($result), True) );
                }
                
            }
            
//            var_dump( $list_array );
            return $list_array;
            
//            echo "Got lists\n<br /><pre>";
//            var_dump($result->response);
            
        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
        }
//        echo '</pre>';
//        var_dump( $result );
//        return $result;
        }
    
    
    //      Add Subscriber
    public function add_reviewer( $cust, $c_id ){
        
        $this->read_subscriber_file();
        
        $customer = new Customer( $c_id );
        
        $email = $customer->get_email();
        $name = $customer->get_name();
        
        $list_id = get_field( 'rm_list_id' , 'rm_options' );
        
//        error_log('Name is ' . $name. ', email is ' . $email . ', List ID is ' . $list_id );
//        error_log('Client API Key is ' . $this->api_key );
        
        $wrap = new CS_REST_Subscribers( $list_id , 
                                        array( 'api_key' => $this->api_key )
                                       );
        
//        $result = $wrap->get( $email );
//        
//        echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
//        if($result->was_successful()) {
//            echo "Got subscriber <pre>";
//            var_dump($result->response);
//        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
//        }
        
        if( $wrap->get( $email )->was_successful() ){
            
//            error_log('Update Subscriber');
        
            $result = $wrap->update(  $email ,
                array(
                    'EmailAddress' => $email,
                    'Name' => $name,
                    'CustomFields'      => array(
                        array(
                            'Key' => 'CustomerID',
                            'Value' => $c_id
                        ),
                    ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        } else {
            
//            error_log('Add Subscriber');
            
            $result = $wrap->add(
                array(
                    'EmailAddress'      => $email,
                    'Name'              => $name,
                    'CustomFields'      => array(
                        array(
                            'Key' => 'CustomerID',
                            'Value' => $c_id
                        ),
                    ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;         
    }
    
//      Add Reviewed Customer
    public function update_reviewer( $info ){
        
        $this->read_subscriber_file();
        
        $name = $info['name'];
        $email = $info['email'];
        $subject = $info['subject'];
        $status = $info['status'];
        
        $list_id = get_field( 'rm_list_id' , 'rm_options' );
        
//        error_log('Name is ' . $name . ', email is ' . $email . ', List ID is ' . $list_id . ' and the status is ' . $status . 'and the subject is ' . $subject );
//        error_log('Client API Key is ' . $this->api_key );
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
        if( $wrap->get( $email )->was_successful() ){
            
            error_log( 'Update!' );
            
            $result = $wrap->update(  $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'ReviewedWebsite',
                        'Value' => $status
                    ),
                    array(
                        'Key' => 'CustomerType',
                        'Value' => $subject
                    ),
                ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true
                )
            );
            
        } else {
            
            error_log( 'Add!' );
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'ReviewedWebsite',
                        'Value' => 'Yes'
                    ),
                    array(
                        'Key' => 'CustomerType',
                        'Value' => $subject
                    ),
                ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;
    }

}