<?php

add_filter('acf/load_field/key=field_5a56559425ed5', 'gmg_loader_backend_review_categories' );
function gmg_loader_backend_review_categories( $field ){
     
    // where our new values will live
    $field['choices'] = array();
    
//    Get the ID of the parent category, which is reviews.
    $category = get_category_by_slug( 'reviews' );
    
//    Then get it's children.
    $gmg_categories = get_term_children( $category->term_id, 'category' );

//    If there are categories (which there always should be), then do something.
    if( $gmg_categories ){
        
//        $proofs = $project['client_concepts'];
        $choice_array = array();
        
        foreach( $gmg_categories as $gmg_category ){
            $choice_name = get_term( $gmg_category, 'category' )->name;
//            error_log( 'The category is called ' . $choice_name );
            array_push( $choice_array , $choice_name );
        }
        
        // loop through array and add to field 'choices'
        if( is_array($choice_array) ) {

            foreach( $choice_array as $choice ) {
//                error_log( 'The choice is called ' . $choice );
                $field['choices'][ $choice ] = $choice;

            }

        }

    }
//     
//    // All done!
    return $field;

}

add_filter('acf/save_post' , 'gmg_review_backend_save', 10, 1 );
function gmg_review_backend_save( $post_id ) {
    
//    error_log( 'This post is ' . get_post_type( $post_id ) );
    
    if( !is_page() ){
    
        if( get_post_type( $post_id ) == 'Reviews'){
            
//            error_log( 'Inside Review Page Save ACF!' );

            $fields = get_fields( $post_id );
            $category = $fields['subject'];
    //        error_log( 'The category is called ' . $category );

            $cat_id = get_term_by( 'name', $category, 'category' )->term_id;

            wp_set_post_categories( $post_id, array( $cat_id ), true );
        }

        $review = isset( $fields[ 'review' ] ) ? sanitize_text_field( $fields[ 'review' ] ) : '';
        $reviewer_first = isset( $fields[ 'reviewer_fname' ] ) ? sanitize_text_field( $fields[ 'reviewer_fname' ] ) : '';'';
        $reviewer_last = isset( $fields[ 'reviewer_lname' ] ) ? sanitize_text_field( $fields[ 'reviewer_lname' ] ) : '';'';
        $reviewer_email = isset( $fields[ 'reviewer_email' ] ) ? sanitize_text_field( $fields[ 'reviewer_email' ] ) : '';

        $customers = new Customers();    

        if( $reviewer_email ){
            
            $cust_info = array(
                    'fname'          => $reviewer_first,
                    'lname'          => $reviewer_last,
                    'email'         => $reviewer_email
                );

            if( !$customers->check_if_customer_exists( $reviewer_email ) ){

                $customers->create_new_customer( $cust_info );

            } else {

                $c_id = $customers->get_customer( $reviewer_email );            
                $customer = new Customer( $c_id );

                $customer->update_customer( $cust_info );

                //If calling wp_update_post, unhook this function so it doesn't loop infinitely
                remove_action('save_post', 'gmg_review_backend_save');

                $customer->update_title();

                // re-hook this function
                add_action('save_post', 'gmg_review_backend_save');

            }
        }

        $review = new Review( $post_id );

        //If calling wp_update_post, unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'gmg_review_backend_save');

        $review->update_title();

        // re-hook this function
        add_action('save_post', 'gmg_review_backend_save');
    }
    
}
