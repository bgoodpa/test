<?php

function register_gmg_reviews_menu_page(){
    
//    error_log( 'Inside Reviews menu page ' );
    
    $plugin = new Gmg_Contact_121();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Reputation Management Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Reviews Settings',
            'capability'  => 'manage_options',
            'post_id'       => 'rm_options',
	));
    
}

add_action( 'wp_enqueue_scripts', 'register_reviews_scripts' );
function register_reviews_scripts(){
    
    wp_enqueue_script( 'review' , plugin_dir_url( __FILE__ ) . 'js/gmg-rm-leave-a-review.js', array( 'jquery' ), null, true );
    wp_enqueue_script('review');
    wp_localize_script( 'review', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action('wp_ajax_gmg_review_page_save', 'gmg_review_page_save');
add_action('wp_ajax_nopriv_gmg_review_page_save', 'gmg_review_page_save');
function gmg_review_page_save() {
    
    error_log( 'Inside Page Reviewer!' );
    
    $info_array = array(
        'reviewer_fname'    => $_POST['fname'],
        'reviewer_lname'    => $_POST['lname'],
        'reviewer_email'    => $_POST['email'],
        'reviewer_town'     => $_POST['town'],
        'review'            => $_POST['review']
        );
    
    $reviews = new Reviews();    
    $r_id = $reviews->create_new_review( $info_array );
    $review = new Review( $r_id );
    
    $review->set_stars( $_POST['stars'] );
    $review->set_subject( $_POST['subject'] );
    $review->update_title();
    
    $reviewer = $_POST['fname'] . ' ' . $_POST['lname'];
    $review = $_POST['review'];
    $reviewer_email = $_POST['email'];
    $link = get_edit_post_link( $r_id, '' );
    error_log( 'The edit post link is ' . $link );    
    
    $customers = new Customers();
    
    $cust_info = array(
            'fname'          => $_POST['fname'],
            'lname'          => $_POST['lname'],
            'email'         => $reviewer_email
        );
    
    if( isset( $_POST['c_id'] ) ){
        
        $customer = new Customer( $_POST['c_id'] );
        $customer->update_customer( $cust_info );
        
    } else {
        
        if( !$customers->check_if_customer_exists( $reviewer_email ) ){
            
            $customers->create_new_customer( $cust_info );
        
        } else {
            
            $c_id = $customers->get_customer( $reviewer_email );
            $customer = new Customer( $c_id );
            $customer->update_customer( $cust_info );
        }
    }
    
    $recipient = get_field( 'reviewer_email_address' , 'rm_options' );
    
    $email_message = "You got a new review from " . $customer->get_name() . ".\n\n" .
        "Their review was:" . "\n" .
        '"' . $review . '"' . "\n\n" .
        "Please log in at " .
        $link .
        " to review and approve it.";

    $message = "FROM: " . $reviewer . "\n" .
        "EMAIL: " . $reviewer_email . "\n" .
        "MESSAGE: " . $email_message;

//        error_log( 'Ready to send Mail' );

    $site_name = get_bloginfo( 'name' );
    $site_email = get_bloginfo( 'admin_email' );

    $subject = "New Review on $site_name";
    $headers   = array();
    $headers[] = "MIME-Version: 1.0";
    $headers[] = "Content-type: text/plain; charset=iso-8859-1";
    $headers[] = "From: $site_name <$site_email>";
//        $headers[] = "CC: Ray <ray@goodmarketinggroup.com>";
    $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
    $headers[] = "Reply-To: { $reviewer_email }";
    $headers[] = "Subject: { $subject }";
    $headers[] = "X-Mailer: PHP/".phpversion();

    $answer = wp_mail($recipient, $subject, $message, implode("\r\n", $headers));
//        error_log( 'Answer in Page Review is ' . $answer );
    
    if ( $answer ){
        $return = array ( 'foo' => 'Review submitted!' );
        error_log( 'Success' );
    } else {
        $return = array ( 'foo' => 'There was an error sending the message.' );
        error_log( 'Failure' );
    }
    
    echo wp_send_json( $return );
    exit();  
    
}

add_filter('acf/load_field/key=field_5b87ffa895a6e', 'gmg_rm_update_copyright', 10, 3);
function gmg_rm_update_copyright( $field ){
    
//    var_dump( $field );
    $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
    
    return $field;
    
//    return $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
}

add_filter('acf/load_field/key=field_5b80176e2246b', 'gmg_cm_reviews_lists_load', 10, 3);
function gmg_cm_reviews_lists_load( $field ){
    
    $bm = new ReviewCM();
    $list_array = $bm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}