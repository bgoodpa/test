<?php

add_action('genesis_entry_content', 'gmg_reviews_loader');
function gmg_reviews_loader() {
//    error_log( 'Page Loader!' );
    
    if ( is_page( 'leave-a-review' ) ) {
        
//        error_log( 'Review Page Loaded!' );
            echo '<h1 class="headline">Leave A Review</h1>';

//    http://test.goodgroupllc.com/scheduler-form/?cid=[ProjectID,fallback=]
//    http://test.goodgroupllc.com/scheduler-form/?cid=8590
    
    if( isset( $_GET['cid'] ) ){
        
//        error_log( 'Get is ' . $_GET['cid'] );        
        $customer = new Customer( $_GET['cid'] );
    
    }
        
        ?>
        
        <div class="review-form">
        <form id="gmg-review-form" action="" method="post">

            <?php if( isset( $customer) ): ?>

            <label for="webdev_form_fname">*First Name: </label>
            <input class="form-control" type="text" name="webdev_form_fname" placeholder="First Name" id="webdev_form_fname" <?php if ( null !== $customer->get_fname() ){ echo 'value="', strip_tags( $customer->get_fname() ), '"'; } ?>>

            <label for="webdev_form_lname">*Last Name: </label>
            <input class="form-control" type="text" name="webdev_form_lname" placeholder="Last Name" id="webdev_form_lname" <?php if ( null !== $customer->get_lname() ){ echo 'value="', strip_tags( $customer->get_lname() ), '"'; } ?>>

            <?php else: ?>

            <label for="webdev_form_fname">*First Name: </label>
            <input class="form-control" type="text" name="webdev_form_fname" placeholder="First Name" id="webdev_form_fname">

            <label for="webdev_form_lname">*Last Name: </label>
            <input class="form-control" type="text" name="webdev_form_lname" placeholder="Last Name" id="webdev_form_lname">

            <?php endif; ?>

            <br />

            <?php if( isset( $customer ) ): ?>

            <label for="webdev_form_email">*Email: </label>
            <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address" id="webdev_form_email" <?php if ( null !== $customer->get_email() ){ echo 'value="', strip_tags( $customer->get_email() ), '"'; } ?>>
            
            <?php else: ?>

            <label for="webdev_form_email">*Email: </label>
            <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address" id="webdev_form_email">

            <?php endif; ?>

            <br />

            <label for="webdev_form_town">Town: </label>
            <input class="form-control" type="tel" name="webdev_form_town" placeholder="Town" id="webdev_form_town">

            <br />

            <label for="webdev_form_category">Subject: </label>
            <select name="webdev_form_category" id="webdev_form_category">

            <?php
    
            $category = get_category_by_slug( 'reviews' );
    
            $gmg_categories = get_term_children( $category->term_id, 'category' );
    
            if( $gmg_categories ){
                foreach( $gmg_categories as $gmg_category ){
                    
                    echo '<option value="' . get_term( $gmg_category, 'category' )->term_id .'">' . get_term( $gmg_category, 'category' )->name . '</option>';
                    
                }                    
            }
    
            ?>
            
            </select>
            
            <br />

            <label for="webdev_form_stars">How Many Stars Would You Rate Us? </label><br />
            <i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_1" value="1"><br />
            <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_2" value="2"><br />
            <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_3" value="3"><br />
            <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_4" value="4"><br />
            <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_5" value="5"><br />
            <br />

            <label for="webdev_form_review">Leave a Review </label>
            <textarea class="form-control" name="webdev_form_review" id="webdev_form_review" required></textarea>
            <br />

            <?php if( isset( $customer ) ): ?>

            <input class="form-control" type="text" name="webdev_form_c_id" style="visibility: hidden" id="webdev_form_c_id" value="<?php echo $customer->get_id(); ?>">

            <?php endif; ?>

            <p class="form-control" name="webdev_form_response" id="webdev_form_response" style="visibility: hidden"></p>

            <button id="webdev_form_submit" class="btn btn-primary" type="submit" value="submit">Submit</button>

        </form>
    </div>

<?php
        
    }
    
    if ( is_page( 'testimonials' ) ) {
        
        error_log( 'Testimonials Loaded!' );
        
        echo '<h1 class="headline">Testimonials</h1>';

    // WP_Query arguments
    $args = array(
    	'post_type'              => array( 'reviews' ),
    	'posts_per_page'         => '10',
        'orderby'                => 'rand'
    );
    
    // The Query
    $query = new WP_Query( $args );
    
    // The Loop
    if ( $query->have_posts() ) {
    	while ( $query->have_posts() ) {
    		$query->the_post();
            
            $post_id = get_the_ID();
            
            $fields = get_fields( $post_id );
            
            // Sanitize user input for review contact name.
            $reviewer_first = isset( $fields[ 'reviewer_fname' ] ) ? sanitize_text_field( $fields[ 'reviewer_fname' ] ) : '';
            $reviewer_last = isset( $fields[ 'reviewer_lname' ] ) ? sanitize_text_field( $fields[ 'reviewer_lname' ] ) : '';
            $reviewer_town = isset( $fields[ 'reviewer_town' ] ) ? sanitize_text_field( $fields[ 'reviewer_town' ] ) : '';
            $reviewer = $reviewer_first . ' ' . substr( $reviewer_last, 0 , 1);
            $review_cat = $fields['subject'];
            $review = isset( $fields[ 'review' ] ) ? sanitize_text_field( $fields[ 'review' ] ) : '';
            
            ?>
            
            <div id="review-' . get_the_ID() . '" class="review-block">
            
            <?php $star_count = $fields[ 'review_rating' ]; ?>
            
            <div class="star-box">
                <?php for( $i = 1; $i <= $star_count; $i++ ): ?>
                <i class="fa fa-star" aria-hidden="true"></i>
                <?php endfor; ?>
            </div>
                
                <p class="customer-quote">"<?php echo $review; ?>"</p>
                <p class="customer-name">- <?php echo $reviewer; ?></p>
                <?php if( $reviewer_town != '' ): ?> 
                    <p class="review-category"><?php echo $review_cat . ' in ' . $reviewer_town; ?></p>
                <?php else :?>
                    <p class="review-category"><?php echo $review_cat; ?></p>
                <?php endif; ?>
            </div>
<?php

    		// do something
        }
    } else {
    	// no posts found
        
        echo '<p>There are no testimonials at this time. Check back later.</p>';
    }
    
    // Restore original Post Data
    wp_reset_postdata();
        
    }
    
    if ( is_page( 'thank-you' ) ) {
        
        echo "<h2>Thanks for reaching out to us</h2><p>We'll reach out to you soon.</p>";
    }
}