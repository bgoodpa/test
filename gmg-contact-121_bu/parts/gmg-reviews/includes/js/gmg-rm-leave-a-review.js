jQuery(document).ready(function ($) {
    "use strict";
    
    var former = $('#gmg-review-form');

    former.on('submit', function (c) {
        c.preventDefault();
        
//        alert('ALARM!');

        var form_fname = $("#webdev_form_fname").val();
        var form_lname = $("#webdev_form_lname").val();
        var form_email = $("#webdev_form_email").val();
        var form_town = $("#webdev_form_town").val();
        var form_subject = $("#webdev_form_category").val();
        var form_review = $("#webdev_form_review").val();
        var form_id = $("#webdev_form_p_id").val();

        var stars;

        $(".stars:checked").each(function () {
            stars = $(this).val();
        });

//        console.log('First Name is ' + form_fname);
//        console.log('Email is ' + form_email);
//        console.log('Stars is ' + stars);

        $.ajax({
            url: ajax_object.ajaxurl,
            type: 'post',
            data: {
                action: 'gmg_review_page_save',
                fname: form_fname,
                lname: form_lname,
                email: form_email,
                town: form_town,
                subject: form_subject,
                stars: stars,
                review: form_review,
                p_id: form_id
            },
            success: function (response) {
                if (response['foo']) {
                    //                    alert( 'Success' + response['foo']);
                    $('#webdev_form_response').html(response['foo']);
                    $('#webdev_form_response').css("visibility", "visible");
                    window.location.href = "/thank-you/";
                } else {
                    //                    alert( 'Problems' + response['errors']);
                    $('#webdev_form_response').html(response['errors']);
                    $('#webdev_form_response').css("visibility", "visible");
                }

            },
            error: function (errorThrown) {
                alert('Error' + errorThrown);
            }
        });
    });
});
