<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_reviewer_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_reviewer_dashboard_widget',         // Widget slug.
        'Add Reputation Management',         // Title.
        'gmg_reviewer_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_reviewer_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_reviewer_dashboard_widget_function() {
    
    ?>
	<p><strong>Enter the name and email of customer.</strong></p>    

    <form name="gmg_review_form" method="post" action="" id="gmg_review_form">
        
        <label for="fieldFName">Name</label><br />
        <table>
            <tr>
                <td><input id="fieldFName" name="fieldFName" placeholder="First" type="text" /> </td>
                <td><input id="fieldLName" name="fieldLName" placeholder="Last" type="text" /></td>
            </tr>        
        </table>
        <p>
            <label for="fieldEmail">Email</label><br />
            <input id="fieldEmail" name="fieldEmail" type="email" required />
        </p>
        <input type="hidden" name="gmg_action" value="gmg_review_sbumit" />
        <p>
            <input type="submit" name="gmg_reviewer_dashboard_button" value="Add Customer">
        </p>
        
        <?php $reviews = new ReviewCustomers(); ?>
        <?php if( $reviews->get_last_used() ): ?>
            <p>Last used: <?php echo $reviews->get_last_used(); ?></p>
        <?php endif; ?>
        
    </form>

    <?php
}

add_action( 'init', 'func_gmg_reviews_send_contact' );
function func_gmg_reviews_send_contact() {
    
//    error_log( 'Called gmg review send' );
    
	if(isset($_POST['gmg_action'])) {
        
        error_log( 'Inside!' );
        
        //gets List ID details from the database.
//        $part_details = get_option( 'gmg-contact-121-pval' );
//        $list_id = $part_details['rm_list_id'];
        
        $customers = new Customers();
        $cust_info = array(
            'fname'          => $_POST['fieldFName'],
            'lname'          => $_POST['fieldLName'],
            'email'         => $_POST['fieldEmail']
            );
        
//        error_log( 'Water is ' . $_POST['fieldWater'] );
        
        if( !$customers->check_if_customer_exists( $_POST['fieldEmail'] ) ){
            
            error_log( 'New Cust!' );

            $customers->create_new_customer( $cust_info );
            $c_id = $customers->get_customer( $_POST['fieldEmail'] );

        } else {
            
            error_log( 'Old Cust!' );

            $c_id = $customers->get_customer( $_POST['fieldEmail'] );            
            $customer = new Customer( $c_id );

//            $customer->update_customer( $cust_info );
//
//            $customer->update_title();

        }
        
        $campaign = new ReviewCM();
        $result = $campaign->add_reviewer( $cust_info , $c_id );
        
        if($result->was_successful()) {
            error_log( "Subscribed with code ". $result->http_status_code);
            $reviews = new ReviewCustomers();
            $reviews->set_last_used();
        } else {
            error_log( 'Failed with code '. $result->http_status_code );
        }
        
        wp_redirect( $_SERVER['HTTP_REFERER'] );
        
        exit();
        
    }

}
