jQuery(document).ready(function ($) {
    "use strict";

    $("#order_form_button").click(function () {
        $("#shop_contact").slideToggle( "slow" );
    });

    if ($('#gmg_capture_form')) {
        
        grecaptcha.ready(function() {
                grecaptcha.execute('6LeL9JsUAAAAAFfTJTIvV5nENNrTr3nPnj4Xopvu',
                                   { action: 'GMGCapture' }
                                  ).then( function ( token ) {
                    $("#CaptureRecaptcha").attr('value', token );
                }
                                        );
            }
                            );

        var form = $('#gmg_capture_form');

        form.on('submit', function (f) {
            //        alert( 'Selected!' );
            f.preventDefault();
            
            console.log( )

            var img = $("#sign-up-wait");
            img.css("display", "inherit");

            var form_contact_Fname = $("#CaptureFName").val();
            var form_contact_Lname = $("#CaptureLName").val();
            var form_email = $("#CaptureEmail").val();
            var form_phone = $("#CapturePhone").val();
            var form_message = $("#CaptureMessage").val();

            var form_id = $("#CaptureID").val();
            var form_state = $("#CaptureState option:selected").val();
            var form_street = $("#CaptureCompanyName").val();

            var form_company_name = $("#CaptureCompanyName").val();

            var form_list = $("#CaptureFormList").val();
            var form_subject,
                form_addressee;

            if ($("#CaptureRecipients").val()) {

                form_addressee = $("#CaptureRecipients").val();
                form_subject = $("#CaptureRecipients  option:selected").text();
            } else {
                form_addressee = $("#CaptureRecipient").val();
                form_subject = $("#CaptureHidden").val();
            }
            
            var form_token = $("#CaptureRecaptcha").val();

            $.ajax({
                url: ajax_object.ajaxurl,
                type: 'post',
                data: {
                    action: 'gmg_capture_form_process',
                    CaptureFName: form_contact_Fname,
                    CaptureLName: form_contact_Lname,
                    CaptureCompanyName: form_company_name,
                    CaptureFormList: form_list,
                    CaptureEmail: form_email,
                    CapturePhone: form_phone,
                    CaptureID: form_id,
                    CaptureState: form_state,
                    CaptureMessage: form_message,
                    CaptureRecipient: form_addressee,
                    CaptureSubject: form_subject,
                    CaptureToken: form_token,
                },
                success: function (response) {
                    if (response['foo']) {
                        //                    alert( 'Success' + response['foo']);
                        $('#CaptureResponse').html(response['foo']);
                        $('#CaptureResponse').css("visibility", "visible");
                        img.css("display", "none");
                        //                    form_contact_name.reset();
                        //                    formy[0].reset();
                        //                    window.location.href = "/thank-you/"

                        window.setTimeout(function () {
                            window.location.href = "/thank-you/"
                        }, 3000);

                    } else {
                        //                    alert( 'Problems' + response['errors']);
                        $('#webdev_form_response').html(response['errors']);
                        $('#webdev_form_response').css("visibility", "visible");
                    }

                },
                error: function (errorThrown) {
                    alert('Error' + errorThrown);
                }
            });
        });
    }


});
