<?php

// This file handles single entries, but only exists for the sake of child theme forward compatibility.

remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'gmg_form_loop', 10 , 2 ); // Add custom loop

function gmg_form_loop( $id ){
    
//    echo 'I am a form!';
    
    if ( have_posts() ){
        
        do_action( 'genesis_before_while' );
        while ( have_posts() ){
            
            the_post();

            do_action( 'genesis_before_entry' );
            
            printf( '<article %s>', genesis_attr( 'entry' ) );
            
//            do_action( 'genesis_entry_header' );
            printf( '<header %s>', genesis_attr( 'entry-header' ) );
            echo '<h1 class="entry-title" itemprop="headline">' . get_the_title( $id ) . '</h1>';
            echo '</header>';
            
            do_action( 'genesis_before_entry_content' );
            
            printf( '<div %s>', genesis_attr( 'entry-content' ) );
            
            do_action( 'genesis_entry_content' );
            
            $form = new GMG_Form( $id );
//    $form_name = str_replace( ' ' , '_', get_the_title( $id ) );
    
            echo $form->get_gmg_form_html_start();
//            echo '<h1>' . $form->get_gmg_form_name() . '</h1>';

            echo $form->show_gmg_form_recipients();

            echo $form->show_gmg_form_name();
            echo $form->show_gmg_form_company_name();
            echo $form->show_gmg_form_email();
            echo $form->show_gmg_form_phone();
            echo $form->show_gmg_form_birthdate();
            echo $form->show_gmg_form_question();

            echo $form->show_gmg_form_hidden();
            echo $form->show_gmg_form_list();
            echo $form->show_gmg_form_recaptcha();
            echo $form->show_gmg_form_response();
            echo $form->show_gmg_form_button();

            echo $form->get_gmg_form_html_close();

            echo '</div>';
            
            do_action( 'genesis_after_entry_content' );

            echo '</article>';

            do_action( 'genesis_after_entry' );
        
        } // End of one post.
        
        do_action( 'genesis_after_endwhile' );
    
    }
}

// Run the Genesis loop.
genesis();