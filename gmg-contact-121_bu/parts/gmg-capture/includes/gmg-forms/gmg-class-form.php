<?php

class GMG_Form{
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    /*
    Update ____________________________________
    */
    
    public function update_title(){
        
        $form_name_array = explode( ' ', $this->get_gmg_form_name() );
        
        if( in_array( 'form' , $form_name_array ) ){
            
            $post_title = implode( ' ' , $form_name_array );
            
        } else {
            
            $post_title = implode( ' ' , $form_name_array ) . ' Form';
            
        }

        $update_post = array(
            'ID'     => $this->id,
            'post_title'  => $post_title,
            'post_name' => $post_title
        );

        wp_update_post( $update_post );        
    }
    
    /*
    Get _______________________________________
    */
    
    //Get Form Post Tags
    public function get_gmg_form_tags(){        
        return wp_get_post_tags( $this->id );        
    }
    
    //Get Form Opening HTML
    public function get_gmg_form_html_start(){
        $form_name = str_replace( ' ' , '_', get_the_title( $this->id ) );
        return '<form name="' . $form_name . '" method="post" action="" id="gmg_capture_form" >';
    }
    
    //Get Form Closing HTML
    public function get_gmg_form_html_close(){
        return '</form>';
    }
    
    //Get Form Waiting gif
    public function get_gmg_form_wait_gif(){
        return '<img id="sign-up-wait" src="https://goodmarketinggroup.com/wp-content/uploads/2018/03/Loading_icon.gif" width="100" style="display: none;" />';
    }
    
    //Get Form Name
    public function get_gmg_form_name(){
        return get_field( 'form_name', $this->id );
    }
    
    //Check and Get Form List
    public function show_gmg_form_list(){
        
        if( get_field( 'form_campaign_monitor_list', $this->id ) ){
            
            return '<input type="text" name="CaptureFormList" id="CaptureFormList" value="' . get_field('form_campaign_monitor_list', $this->id ) . '" style="visibility: hidden" />';
            
        }
    }
    
    //Check and Show Recipients
    public function show_gmg_form_recipients(){
        
        if( get_field( 'show_recipients', $this->id ) ){
            
            $html_return = array();
            $recipient = new Recipients();
            $recipients = $recipient->get_all_recipients();
            
            array_push( $html_return, '<label for="CaptureRecipients">*Message Subject: </label>');
            array_push( $html_return, '<select name="CaptureRecipients" id="CaptureRecipients" required>');
            
            //loop over each post
            foreach( $recipients as $recipient_post ){
                
                array_push( $html_return, '<option value="' . $recipient_post['email'] . '">' . $recipient_post['name'] . '</option>');
                
            }
            
            array_push( $html_return, '</select>' );
            
            return implode( $html_return );
            
        } elseif( get_field( 'form_recipient', $this->id ) ){
            
            return '<input type="text" name="CaptureRecipient" id="CaptureRecipient" value="' . get_field('form_recipient', $this->id ) . '" style="visibility: hidden" />';
            
        }
    }
    
    //Check and Get First Name
    public function show_gmg_form_name(){
        
        if( get_field( 'show_name', $this->id ) ){
            
            $html_return = array();
            
            array_push( $html_return , '<div class="form-names">' );
            array_push( $html_return , '<label>Your Name *</label>' );
            array_push( $html_return , '<input id="CaptureFName" name="CaptureFName" placeholder="First" type="text" class="names"  required />');
            array_push( $html_return , '<input id="CaptureLName" name="CaptureLName" placeholder="Last" type="text" class="names"  required />' );
            array_push( $html_return , '</div>' );
            
            return implode( $html_return );
        }
    }
    
    //Check and Get Company Name
    public function show_gmg_form_company_name(){
        
        if( get_field( 'show_company_name', $this->id ) ){
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , '<label>Your Company *</label>' );
            array_push( $html_return , '<input id="CaptureCompany" name="CaptureCompany" placeholder="Company" type="text" required />');
            array_push( $html_return , '</div>' );
            
            return implode( $html_return );
        }
    }
    
    //Check and Get Email
    public function show_gmg_form_email(){
        
        if( get_field( 'show_email', $this->id ) ){
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , '<label>Your Email *</label>' );
            array_push( $html_return , '<input id="CaptureEmail" name="CaptureEmail" placeholder="Email" type="text" required />');
            array_push( $html_return , '</div>' );
            
            return implode( $html_return );
        }
    }
    
    //Check and Get Phone
    public function show_gmg_form_phone(){
        
        if( get_field( 'show_phone_number', $this->id ) ){
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , '<label>Your Phone *</label>' );
            array_push( $html_return , '<input id="CapturePhone" name="CapturePhone" placeholder="Phone" type="text" required />');
            array_push( $html_return , '</div>' );
            
            return implode( $html_return );
        }
    }
    
    //Check and Get Birthdate
    public function show_gmg_form_birthdate(){
        
        if( get_field( 'show_birthdate', $this->id ) ){
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , '<label>Your Birthday </label>' );
            array_push( $html_return , '<input type="date" id="CaptureBirth" name="CaptureBirth" value="04-08-2019"
            min="01-01-1918" max="04-08-2019" />');
            array_push( $html_return , '</div>' );
            
            return implode( $html_return );
        }
    }
    
    //Check and Get Messages
    public function show_gmg_form_question(){
        
        if( get_field( 'show_message_box', $this->id ) ){
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , '<label>Your Message / Question </label>' );
            array_push( $html_return , '<textarea name="CaptureMessage" id="CaptureMessage" placeholder="Leave a Message or Ask a question"></textarea>');
            array_push( $html_return , '</div>' );
            
            return implode( $html_return );
        }
    }
    
    //Show the Response Form
    public function show_gmg_form_response(){
        return '<p class="form-control" name="CaptureResponse" id="CaptureResponse" style="visibility: hidden"></p>';
    }
    
    //Show the Hidden Field
    public function show_gmg_form_hidden(){
        $form_name = str_replace( ' ' , '_', get_the_title( $this->id ) );
        return '<input id="CaptureHidden" name="CaptureHidden" type="hidden" style="height: 5px; padding: 0;" value="' . $form_name . '" />';
    }
    
    //Show the Hidden ID Field
    public function show_gmg_form_id( $id ){
        return '<input id="CaptureID" name="CaptureID" type="hidden" style="height: 5px; padding: 0;" value="' . $id . '" />';
    }
    
    //Show the Hidden ID Field
    public function show_gmg_form_recaptcha(){
        return '<input type="hidden" name="g-recaptcha-response" id="CaptureRecaptcha" value="">';
    }
    
    //Show the Submit Button
    public function show_gmg_form_button(){
        return '<div class="sign-up-submit"><img id="sign-up-wait" src="https://goodmarketinggroup.com/wp-content/uploads/2018/03/Loading_icon.gif" width="100" style="display: none;" /><button id="CaptureSubmit" class="btn btn-primary" type="submit" value="submit">Submit</button></div>';
    }
    
    //Check if Form is using shortcode
    public function get_gmg_form_shortcode(){
        $shortcode = get_field( 'form_shortcode', $this->id );
        return $shortcode;
    }
        
    
    /*
    Set ______________________________________
    */
    
    //Set Boomerang Post Tags
    public function set_gmg_form_tags( $tags ){      
        wp_set_post_tags( $this->id, $tags, true );    
    }
    
}

class GMG_Forms{
    
    public function __construct() {
        
    }
    
    public function find_form_with_shortcode( $shortcode ){
        
        $forms = array();

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg_form' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $form = new GMG_Form( $id );
                $shortcode = $form->get_gmg_form_shortcode();
                error_log( 'Shortcode is ' . $shortcode );
                if( $shortcode != 'none' ){
                    
                    array_push( $forms, $id );
                    
                }
            }
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( !empty( $forms ) ){
            
            return $forms;
            
        } else {
            
            return false;
        }

    }
    
}