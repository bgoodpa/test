jQuery(document).ready(function ($) {
    "use strict";
    var form = $('#gmg-contact-form');
    var service = $('#gmg_service_contact_form');
//    var button = $('#webdev_form_button');
//    form.change( function(e) {
//        alert( 'Selected!' );
//        e.preventDefault();
//    });
    form.on('submit', function (e) {
//        alert( 'Selected!' );
        e.preventDefault();
        
        var form_addressee = $("#webdev_form_addressee" ).val();
        var form_subject = $("#webdev_form_addressee  option:selected").text();
//        console.log( form_addressee );
        var form_first_name = $("#webdev_form_first_name").val();
        var form_last_name = $("#webdev_form_last_name").val();
        var form_email = $("#webdev_form_email").val();
        var form_zip = $("#webdev_form_zip").val();
        var form_number = $("#webdev_form_number").val();
        var form_message = $("#webdev_form_message").val();
        var form_title = $("#webdev_form_title").val();
        
        $.ajax({
            url : ajax_objecty.ajaxurl,
            type : 'post',
            data : {
                action : 'gmg_contact_mail',
                addressee : form_addressee,
                subject : form_subject,
                first : form_first_name,
                last : form_last_name,
                zip : form_zip,
                email : form_email,
                number : form_number,
                title: form_title,
                message : form_message,
                captcha : grecaptcha.getResponse()
            },
            success:function( response ){
                if(response['foo']){
//                    alert( 'Success' + response['foo']);
                    $('#webdev_form_response').html( response['foo'] );
                    $('#webdev_form_response').css("visibility","visible");
                    window.location.href = "/thank-you/";
                } else{
//                    alert( 'Problems' + response['errors']);
                    $('#webdev_form_response').html( response['errors'] );
                    $('#webdev_form_response').css("visibility","visible");
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            } 
//            success : function( response ) {
//                console.log(response);
//                $('.gmg_contents').html(response);
//            }
        });       
	});
    
    service.on('submit', function (j) {
//        alert( 'Selected!' );
        j.preventDefault();
        
        var form_addressee = $("#service_form_addressee" ).val();
        var form_first_name = $("#service_form_first_name").val();
        var form_last_name = $("#service_form_last_name").val();
        var form_phone = $("#service_form_phone").val();
        var form_email = $("#service_form_email").val();
        var form_street = $("#service_form_street").val();
        var form_city = $("#service_form_city").val();
        var form_state = $("#service_form_state").val();        
        var form_zip = $("#service_form_zip").val();
        var form_product = $("#service_form_product").val();
        var form_message = $("#service_form_message").val();
        var form_title = $("#service_form_title").val();
        
        var selecteddays = [];
        var pickeddays;
        
        $(".days:checked").each(function() {
            selecteddays.push($(this).val());
        });
        
        if ( selecteddays ){
            pickeddays = selecteddays.join(',') ;
        }
        
        var selectedtimes = [];
        var pickedtimes;
        
        $(".times:checked").each(function() {
            selectedtimes.push($(this).val());
        });
        
        if ( selectedtimes ){
            pickedtimes = selectedtimes.join(', ') ;
        }
        
        console.log( 'Selected Days are ' + pickeddays );
        console.log( 'Selected Times are ' + pickedtimes );
        
        $.ajax({
            url : ajax_objecty.ajaxurl,
            type : 'post',
            data : {
                action : 'gmg_contact_service_mail',
                addressee : form_addressee,
                first : form_first_name,
                last : form_last_name,
                email : form_email,
                phone : form_phone,
                street: form_street,
                city : form_city,
                state : form_state,
                zip : form_zip,
                days : pickeddays,
                times : pickedtimes,
                product : form_product,
                message : form_message,
                captcha : grecaptcha.getResponse()
            },
            success:function( response ){
                if(response['foo']){
//                    alert( 'Success' + response['foo']);
                    $('#service_form_response').html( response['foo'] );
                    $('#service_form_response').css("visibility","visible");
                    window.location.href = "/thank-you/";
                } else{
//                    alert( 'Problems' + response['errors']);
                    $('#service_form_response').html( response['errors'] );
                    $('#service_form_response').css("visibility","visible");
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            } 
//            success : function( response ) {
//                console.log(response);
//                $('.gmg_contents').html(response);
//            }
        });       
	});
});
