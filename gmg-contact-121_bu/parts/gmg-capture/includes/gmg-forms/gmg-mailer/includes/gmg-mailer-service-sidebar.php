<?php


add_shortcode('service-form', 'gmg_contact_service_form' );

function gmg_contact_service_form(){    
    
    $days_array = array(
        'Mon',
        'Tues',
        'Wed',
        'Thurs',
        'Fri',
        'Sat'
    );
    
    $times_array = array(
        'Morning',
        'Lunch',
        'Early Afternoon',
        'Late Afternoon',
        'Evening'
    );
    
    $us_state_abbrevs = array('AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY', 'AE', 'AA', 'AP');
    
    $sidebar_array = array();
    
    //Start Div
    array_push( $sidebar_array, '<div class="service-contact-form">' );
    
    //Start Form
    array_push( $sidebar_array, '<form id="gmg_service_contact_form" action="" method="post">' );
    

    //Let's do Name Input
    array_push( $sidebar_array, '<div class="label-input"><label class="name-label" >*Full Name: </label>' );
    array_push( $sidebar_array, '<input class="form-names" type="text" name="service_form_first_name" id="service_form_first_name" placeholder="First" required>' );
    array_push( $sidebar_array, '<input class="form-names" type="text" name="service_form_last_name" id="service_form_last_name" placeholder="Last" required></div>');
    
    //Let's do Email
    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_email">*Email: </label>');
    array_push( $sidebar_array, '<input class="form-control" type="text" name="service_form_email" id="service_form_email" required></div>');
    
    //Let's do Phone
    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_phone">*Phone: </label>');
    array_push( $sidebar_array, '<input class="form-control" type="text" name="service_form_phone" id="service_form_phone" required></div>');
    
    //Let's do Street Address
    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_street">*Street Address: </label>');
    array_push( $sidebar_array, '<input class="form-control" name="service_form_street" type="text" placeholder="Street Address" id="service_form_street" required></div>');
    
    //Let's do the Town, State, and Zip
    array_push( $sidebar_array, '<div class="label-input address"><label for="service_form_city">*Town, State, & Zip</label>');
    array_push( $sidebar_array, '<input id="service_form_city" name="service_form_city" type="text" placeholder="City" required>');    
    
    //Let's do State
//    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_state">*State</label>');
    array_push( $sidebar_array, '<select name="service_form_state" id="service_form_state" required>');
    
    foreach( $us_state_abbrevs as $state ){
        array_push( $sidebar_array, '<option value="' . $state . '">' . $state . '</option>');
    }
    
    array_push( $sidebar_array, '</select>');
//    array_push( $sidebar_array, '</div>');
    
    //Let's do Zip
//    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_zip">*Zip Code: </label>');
    array_push( $sidebar_array, '<input class="form-control" type="text" name="service_form_zip" placeholder="i.e. 19904" id="service_form_zip" value="" required></div>');
    
    //Let's see what are good days for folks
    array_push( $sidebar_array, '<div class="day-input">' );
    array_push( $sidebar_array, '<div class="label-input day"><label for="sidebar_form_days">*Days Available: </label></div>' );
    
    array_push( $sidebar_array, '<div class="day-checkboxes">' );
    
    $index = 1;
    
    foreach( $days_array as $day ){
        
        array_push( $sidebar_array, '<div class="this-day-check"><input type="checkbox" class="days" name="sidebar_form_days" id="sidebar_form_day_' . $index . '" value="' . $day . '" /> ' . $day . '</div>' );
        
        $index++;
    }
    
    array_push( $sidebar_array, '</div>' );
    array_push( $sidebar_array, '</div>' );
    
    //Let's see what's a good time for folks
    array_push( $sidebar_array, '<div class="time-input">' );
    array_push( $sidebar_array, '<div class="label-input time"><label for="sidebar_form_times">*Times Available: </label></div>' );
    
    array_push( $sidebar_array, '<div class="time-checkboxes">' );
    
    $index = 1;
    
    foreach( $times_array as $time ){
        
        array_push( $sidebar_array, '<div class="this-time-check"><input type="checkbox" class="times" class="sidebar_form_times" id="sidebar_form_time_' . $index . '" value="' . $time . '" /> ' . $time . '</div>' );
        
        $index++;
    }
    
    array_push( $sidebar_array, '</div>' );
    array_push( $sidebar_array, '</div>' );
    
    //Let's do Product
    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_product">*Product to be Serviced: </label>' );
    array_push( $sidebar_array, '<input class="form-control" type="text" name="service_form_product" id="service_form_product"></div>' );
    
    //Let's do Message    
    array_push( $sidebar_array, '<div class="label-input"><label for="service_form_message">Questions / Concerns: </label>' );
    array_push( $sidebar_array, '<textarea class="form-control" name="service_form_message" id="service_form_message"></textarea></div>' );
    
    $service_email = get_field( 'service_email', get_the_ID() );
    
    //Hidden Form Title
    array_push( $sidebar_array, '<input name="service_form_addressee" id="service_form_addressee" value="' . $service_email . '" style="visibility: hidden">' );
    
    //Hidden Form Title
    array_push( $sidebar_array, '<input name="service_form_title" id="service_form_title" value="Service Request Form" style="visibility: hidden">' );
    
    //Response Box
    array_push( $sidebar_array, '<p class="form-control" name="service_form_response" id="service_form_response" style="visibility: hidden"></p>' );
    
    //Recaptcha
    array_push( $sidebar_array, '<div class="g-recaptcha" data-sitekey="6LeuLn0UAAAAAJpPwYy2KlQtIwwxupmAhk-yh5IO">');
    
    //End of div
    array_push( $sidebar_array, '</div>' );
    
    //Submit Button
    array_push( $sidebar_array, '<button id="service_form_submit" class="btn btn-primary" type="submit" value="submit">Submit</button>' );
    
    //End of Form
    array_push( $sidebar_array, '</form>' );
    
    //End of div
    array_push( $sidebar_array, '</div>' );
    
    return implode( '' , $sidebar_array );
    
}