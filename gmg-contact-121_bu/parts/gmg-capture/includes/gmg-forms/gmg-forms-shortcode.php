<?php

add_shortcode('request-quote', 'gmg_write_request_quote');

function gmg_write_request_quote( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '',
		),
		$atts,
		'request-quote'
	);
    
    $forms = new GMG_Forms();
    $return_html = array();
    
    $ids = $forms->find_form_with_shortcode( 'request-quote' );
    error_log( 'ID is ' . $ids[0] . ' for ' . get_the_title( $ids[0] ) );
    
    if( $ids != false ){
        
        $form = new GMG_Form( $ids[0] );
        
        array_push( $return_html, '<div class="contact-form" id="shop_contact" >' );
        
        array_push( $return_html, $form->get_gmg_form_html_start() );
        array_push( $return_html, $form->show_gmg_form_recipients() );
        array_push( $return_html, $form->show_gmg_form_id( $atts['id']) );
        array_push( $return_html, $form->show_gmg_form_name() );
        array_push( $return_html, $form->show_gmg_form_company_name() );
        array_push( $return_html, $form->show_gmg_form_email()  );
        array_push( $return_html, $form->show_gmg_form_phone()  );
        array_push( $return_html, $form->show_gmg_form_birthdate()  );
        array_push( $return_html, $form->show_gmg_form_question()  );

        array_push( $return_html, $form->show_gmg_form_hidden() );
        array_push( $return_html, $form->show_gmg_form_list() );
        array_push( $return_html, $form->show_gmg_form_response()  );
        array_push( $return_html, $form->show_gmg_form_recaptcha() );
        array_push( $return_html, $form->show_gmg_form_button()  );
        
        array_push( $return_html, $form->get_gmg_form_html_close() );
        
        array_push( $return_html, '</div>' );
        
    } else {
        
        array_push( $return_html, '<p>Form not created at this time.</p>'  );
    }
    
    
    
    return implode( $return_html );    
    
}
    
    