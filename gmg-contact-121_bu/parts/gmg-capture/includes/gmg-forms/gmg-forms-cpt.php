<?php

//Add Custom Post Type For GMG Forms
function gmg_form_init() {
	$quo_labels = array(
		'name'                  => _x( 'GMG Forms', 'Post Type General Name', 'gmg_c121' ),
		'singular_name'         => _x( 'GMG Form', 'Post Type Singular Name', 'gmg_c121' ),
		'menu_name'             => __( 'GMG Forms', 'gmg_c121' ),
		'name_admin_bar'        => __( 'GMG Forms', 'gmg_c121' ),
		'archives'              => __( 'GMG Form Archives', 'gmg_c121' ),
		'attributes'            => __( 'GMG Form Attributes', 'gmg_c121' ),
		'parent_form_colon'     => __( 'Parent GMG Form:', 'gmg_c121' ),
		'all_forms'             => __( 'All GMG Forms', 'gmg_c121' ),
		'add_new_form'          => __( 'Add New GMG Form', 'gmg_c121' ),
		'add_new'               => __( 'Add New', 'gmg_c121' ),
		'new_form'              => __( 'New GMG Form', 'gmg_c121' ),
		'edit_form'             => __( 'Edit GMG Form', 'gmg_c121' ),
		'update_form'           => __( 'Update GMG Form', 'gmg_c121' ),
		'view_form'             => __( 'View GMG Form', 'gmg_c121' ),
		'view_forms'            => __( 'View GMG Forms', 'gmg_c121' ),
		'search_forms'          => __( 'Search GMG Form', 'gmg_c121' ),
		'not_found'             => __( 'Not found', 'gmg_c121' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gmg_c121' ),
		'featured_image'        => __( 'Featured Image', 'gmg_c121' ),
		'set_featured_image'    => __( 'Set featured image', 'gmg_c121' ),
		'remove_featured_image' => __( 'Remove featured image', 'gmg_c121' ),
		'use_featured_image'    => __( 'Use as featured image', 'gmg_c121' ),
		'insert_into_form'      => __( 'Insert into GMG Form', 'gmg_c121' ),
		'uploaded_to_this_form' => __( 'Uploaded to this GMG Form', 'gmg_c121' ),
		'forms_list'            => __( 'GMG Forms list', 'gmg_c121' ),
		'forms_list_navigation' => __( 'GMG Forms list navigation', 'gmg_c121' ),
		'filter_forms_list'     => __( 'Filter GMG Forms list', 'gmg_c121' ),
	);
    
	$quo_args = array(
		'label'                 => __( 'GMG Form', 'gmg_c121' ),
		'description'           => __( 'Custom Post Type for GMG Forms', 'gmg_c121' ),
		'labels'                => $quo_labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'post_tag', 'product_tag' ),
        'rewrite'               => array( 'slug' => 'form' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
        'show_in_rest'          => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
    
    $current_user = wp_get_current_user();
    if ( user_can( $current_user, 'administrator' ) ) {
        $quo_args['show_ui'] = true;
        $quo_args['show_in_menu'] = 'gmg-contact-121';
        $quo_args['menu_position'] = 7;        
    }
    
	register_post_type( 'gmg_form', $quo_args );

}
add_action( 'init', 'gmg_form_init' );


add_filter( 'template_include', 'gmg_call_single_form_template' ); 
function gmg_call_single_form_template( $original_template ) {
    
    if ( is_single() && get_post_type() == 'gmg_form' ) {
        
        global $post;
        
        $form = new GMG_Form( $post->ID );
        if( $form->get_gmg_form_shortcode() === 'none' ){
           
            $pro = plugin_dir_path( __FILE__ );
            return plugin_dir_path( __FILE__ ) . 'templates/single-gmg_form.php';
            
        } else {
      
    return $original_template;
      
  }
      
  } else {
      
    return $original_template;
      
  }
}