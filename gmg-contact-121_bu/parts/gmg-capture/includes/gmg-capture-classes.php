<?php

/**
 * The Classes that Capture & Connect needs to work with.
 *
 * Some of the Classes extend other Classes found within Contact 1:1 core files like Customer, Campaign Monitor, and
 * HubSpot.
 * 
 */


class CaptureCustomer extends Customer{
    
    private $id;
    
    public function __construct( $id ) {
        parent::__construct( $id );
        $this->id = $id;
    }
    
    public function get_customer_name(){
        return get_field( 'fname', $this->id ) . ' ' . get_field( 'lname', $this->id );
    }
    
}

if( class_exists( 'Lead' ) ){

class CaptureLead extends Lead {
    
    private $id;
    private $customer_id;    
    private $name;    
    private $email;    
    private $date;
    
    public function __construct( $id ) {
        parent::__construct( $id );
        $this->id = $id;
        $this->customer_id = get_field( 'lead_customer' , $this->id );
        $this->name = get_field( 'lead_name' , $this->id );
        $this->email = get_field( 'lead_email' , $this->id );
        $this->date = get_field( 'lead_date' , $this->id );
    }
    
    public function get_lead_company_name(){
        return get_field( 'company_name' , $this->id );        
    }
    
    public function get_lead_state(){
        return get_field( 'customer_state' , $this->id );        
    }

    public function set_lead_company_name( $company_name ){
        update_field( 'company_name' , $company_name,  $this->id );        
    }
    
    public function set_lead_state( $state ){
        update_field( 'customer_state' , $state, $this->id );        
    }
}
}


class CaptureCM extends CampaignMonitor{
        
    private $api_key;
    private $client_id;
    
    function __construct() {
        parent::__construct();
        
        $fields = get_fields('overall_settings');
        
        if( is_array( $fields ) AND count( $fields ) != 0) {
            $this->client_id = $fields['cm_client_id'];
            $this->api_key = $fields['cm_client_api_key'];
        }
    }
    
    
        
    public function get_capture_clients_list(){
        
        $this->read_client_file();
        
//        error_log( 'API key is ' . $this->api_key );
        
        $wrap = new CS_REST_Clients(
            $this->client_id,
            array( 'api_key' => $this->api_key )
        );
        
        $result = $wrap->get_lists();
//        echo "Result of /api/v3.1/clients/{id}/lists\n<br />";
        if($result->was_successful()) {
            
            $list_array = array();
            $result_array = $result->response;
            foreach( $result_array as $result ){
                if( is_object( $result ) ){
                    array_push( $list_array, json_decode(json_encode($result), True) );
                }
                
            }
            
            return $list_array;
        }
    }
    
    /*
    * Let's add a Capture to Campaign Monitor and initiate specific Journey.
    *
    * Takes two argumets:
    *   1) The ID of the Lead post
    *   2) The ID of the page that it was sent from. To get class list.
    *
    */
    
    public function add_capture( $c_id, $list_id ){
        
        $this->read_subscriber_file();
        
        $customer = new CaptureCustomer( $c_id );
        $name = $customer->get_customer_name();
        $email = $customer->get_email();
        
        error_log('Name is ' . $name . ', email is ' . $email );
        error_log('Client API Key is ' . $this->api_key . ' and List ID is ' . $list_id );
        
        $wrap = new CS_REST_Subscribers( 
            $list_id,
            array( 'api_key' => $this->api_key )
        );
        
        $result = $wrap->get( $email, true );
        
        error_log( 'Email? ' . $result->http_status_code );
        
        if( $result->was_successful() ){
//            
            error_log( 'Got email with code ' . $result->http_status_code );
//            
            $result = $wrap->update( $email,
                                    array(
                                        'EmailAddress' => $email,
                                        'Name' => $name,
                                        'ConsentToTrack' => 'yes',
                                        'Resubscribe' => true,
                                        'RestartSubscriptionBasedAutoresponders' => true,
                                        )
                                   );
//        
        } else {
            
            error_log( 'No email so add!' );
            
            //Add them if they aren't.        
            $result = $wrap->add(
                array(
                    'EmailAddress' => $email,
                    'Name' => $name,
                    'ConsentToTrack' => 'yes',
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;
        
    }
}