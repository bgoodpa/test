<?php

/**
 * This is the start of the Capture and Connect part of Contact 1:1.
 *
 * If this has been checked in the overall settings, then this program will not load.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

$form = plugin_dir_path( __FILE__ );

require_once $form . 'gmg-forms/gmg-recipients-cpt.php';

require_once $form . 'gmg-forms/gmg-forms-cpt.php';
require_once $form . 'gmg-forms/gmg-forms-acf.php';
require_once $form . 'gmg-forms/gmg-class-form.php';
require_once $form . 'gmg-forms/gmg-forms-admin.php';
require_once $form . 'gmg-forms/gmg-forms-shortcode.php';
