<?php

/**
 * Functions for the administration of Contact 1:1 Capture & Connect part.
 * 
 */

add_filter('acf/load_field/key=field_5cae17f9c3aec', 'gmg_capture_update_lists', 10, 3);
function gmg_capture_update_lists( $field ){
    
//    error_log( 'Field is ' . $field );
    
    $captureCM = new CaptureCM();
    $list_array = $captureCM ->get_capture_clients_list();
    
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        
        $index = 0;
        
//        error_log( 'Count is ' . count( $list_array ) );
        
        foreach( $list_array as $list ) {
            
            if( $index > 0 ){
                
                $field['choices'][ $list['ListID'] ] = $list['Name'];
                
            } else {
                
                $field['choices'][ 'None' ] = 'None';
                
            }
            $index++;
        }
   }
    
    return $field;

}

add_action('wp_ajax_gmg_capture_class_form', 'gmg_capture_class_form');
add_action('wp_ajax_nopriv_gmg_capture_class_form', 'gmg_capture_class_form');
function gmg_capture_class_form() {
    
	if( isset($_POST['CaptureFormList']) ) {
        
//        error_log( 'Inside Class!' );
        
        $contact_Fname = (isset($_POST['CaptureFName']) ) ? $_POST['CaptureFName'] : '';
        $contact_Lname = (isset($_POST['CaptureLName']) ) ? $_POST['CaptureLName'] : '';
        $company_name = (isset($_POST['CaptureCompanyName']) ) ? $_POST['CaptureCompanyName'] : '';
        $email = (isset($_POST['CaptureEmail']) ) ? $_POST['CaptureEmail'] : '';
        $state = (isset($_POST['CaptureState']) ) ? $_POST['CaptureState'] : '';
        $answer = (isset($_POST['CaptureAnswer']) ) ? $_POST['CaptureAnswer'] : '';
        $mobile = (isset($_POST['CaptureMobileNumber']) ) ? $_POST['CaptureMobileNumber'] : '';
        $list_id = (isset($_POST['CaptureFormList']) ) ? $_POST['CaptureFormList'] : '';
        
        $other = (isset($_POST['CaptureOther']) ) ? $_POST['CaptureOther'] : '';
//        error_log( 'Other is ' . $other );
        $interests = (isset($_POST['CaptureInterests']) ) ? $_POST['CaptureInterests'] : '';
        
        if( !empty( $interests ) ){
            
            $interests_array = explode( ',' , $interests );
            if( isset($_POST['CaptureOther']) ){                
                array_push( $interests_array , $_POST['CaptureOther'] );
            }
        }
        
//        error_log('First Name is ' . $contact_Fname . ', last name is ' . $contact_Lname . ' and email is ' . $email);
        /*
        * 
        * First, create a Customer.
        * 
        */
        
        //Call Customers class
        $customers = new Customers();
        
        $cust_info = array(
                'fname'          => $contact_Fname,
                'lname'          => $contact_Lname,
                'email'         => $email );
        
        //1) Check to see if Customer exists.
        if( !$customers->check_if_customer_exists_by_email( $email ) ){           
            
            //It doesn't, so create new customer.
            $c_id = $customers->create_new_customer( $cust_info );
            
//            error_log( 'Customer is ' . $c_id );
        
        } else {
            
            //The customer must be older, so get it.
            $c_id = $customers->get_customer_by_email( $email );
            
            $customer = new Customer( $c_id );
            $customer->update_customer( $cust_info );
            $customer->update_title();
        }
        
        //Make a new CaptureCustomer Class
        $customer = new CaptureCustomer( $c_id );
        
        
        /*
        * 
        * Then, create a Lead post and enter this information.
        * 
        */
        
        //Call the Leads Class
        $leads = new Leads();
        
        //Save the Customer to a Lead Array.        
        $leads_array = array( 'lead_customer'  =>  $c_id );
        $leads_array['lead_email'] =  $email;
        
        //Set website as salesperson.
        $leads_array['salesperson'] =  'Website';
        
        //Make the Today's Date the Lead Date            
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        $leads_array['lead_date'] = $today_date->format('F j, Y');
        
        //Set the Product Name.
        $leads_array['lead_name'] = 'Class Participant';
        
        //Create new Lead
        $q_id = $leads->create_new_lead( $leads_array );
        
        //Call Capture Lead Class
        $c_lead = new CaptureLead( $q_id );
        
        //Set Lead Company Name and Salesperson
        $c_lead->set_lead_company_name( $company_name );
        $c_lead->set_lead_salesperson( 'Website' );
        $c_lead->set_lead_state( $state );
        
        
        //Let's go ahead and set Interets as tags.
        if( !empty( $interests_array) ){
            $customer->base_set_customer_tags( $interests_array );
            $c_lead->set_lead_tags( $interests_array );
        }        
        
        /*
        * 
        * Let's send this down to Campaign Monitor
        * 
        */
        
        //Call the CaptureCM Class
        $cm = new CaptureCM();
        
        $result = $cm->add_capture( $q_id , $list_id );
        
        $success_message = "<p>You've been added to the list!</p>";

        if( $result->was_successful() ) {
            error_log( "Subscribed with code " . $result->http_status_code);
            $return = array ( 'foo' => $success_message );
        } else {
            error_log( 'Failed with code ' . $result->http_status_code );
    //        error_log( 'Failed with code ' . $result->response );
            $return = array ( 'foo' => 'There was an error sending the message.' );
        }
        
        /*
        * 
        * If they have chosen mobile, send them to Twilio.
        * 
        */        
        error_log( 'Answer is ' . $answer );
        if( strtolower( $answer ) == 'yes' ){
            
            $ct = new CaptureTwilio();
        
            if( strstr( $mobile, '-', false ) ){
                
                error_log( 'Send a text message with fixed phone!');
                $mobile_parts = explode( '-', $mobile );
                $ct->send_class_message( implode('', $mobile_parts )  );
            } else {
                error_log( 'Send a text message!');
                $ct->send_class_message( $mobile );
            }
            
            $customer->set_mobile( $mobile );
        }
        
        
        
        /*
        * 
        * Lastly, let's throw it into HubSpot CRM.
        * 
        */
        
        $captureHub = new CaptureHubSpot();
        $captureHub->create_contact( $q_id );
        
        
    echo wp_send_json( $return );
    }
}

add_action('wp_ajax_gmg_capture_interest_form', 'gmg_capture_interest_form');
add_action('wp_ajax_nopriv_gmg_capture_interest_form', 'gmg_capture_interest_form');
function gmg_capture_interest_form() {
    
	if( isset($_POST['CaptureFormList']) ) {
        
        error_log( 'Inside Interest!' );
        
        $contact_Fname = (isset($_POST['CaptureFName']) ) ? $_POST['CaptureFName'] : '';
        $contact_Lname = (isset($_POST['CaptureLName']) ) ? $_POST['CaptureLName'] : '';
        $company_name = (isset($_POST['CaptureCompanyName']) ) ? $_POST['CaptureCompanyName'] : '';
        $email = (isset($_POST['CaptureEmail']) ) ? $_POST['CaptureEmail'] : '';
        $state = (isset($_POST['CaptureState']) ) ? $_POST['CaptureState'] : '';
        $answer = (isset($_POST['CaptureAnswer']) ) ? $_POST['CaptureAnswer'] : '';
        $list_id = (isset($_POST['CaptureFormList']) ) ? $_POST['CaptureFormList'] : '';
        $mobile = (isset($_POST['CaptureMobileNumber']) ) ? $_POST['CaptureMobileNumber'] : '';
        
        $other = (isset($_POST['CaptureOther']) ) ? $_POST['CaptureOther'] : '';
        $interests = (isset($_POST['CaptureInterests']) ) ? $_POST['CaptureInterests'] : '';
        
        if( !empty( $interests ) ){
            
            $interests_array = explode( ',' , $interests );
            if( isset($_POST['CaptureOther']) ){                
                array_push( $interests_array , $_POST['CaptureOther'] );
            }
        }
        
        /*
        * 
        * First, create a Customer.
        * 
        */
        
        //Call Customers class
        $customers = new Customers();
        
        $cust_info = array(
            'fname'          => $contact_Fname,
            'lname'          => $contact_Lname,
            'email'         => $email );
        
        //1) Check to see if Customer exists.
        if( !$customers->check_if_customer_exists_by_email( $email ) ){
            
            //It doesn't, so create new customer.
            $c_id = $customers->create_new_customer( $cust_info );
            
//            error_log( 'Customer is ' . $c_id );
        
        } else {
            
            //The customer must be older, so get it.
            $c_id = $customers->get_customer_by_email( $email );
            
            $customer = new Customer( $c_id );
            $customer->update_customer( $cust_info );
            $customer->update_title();
        }
        
        //Make a new CaptureCustomer Class
        $customer = new CaptureCustomer( $c_id );
        
        if( !empty( $mobile ) ){
            
            $customer->set_mobile( $mobile );
            
        }
        
        
        /*
        * 
        * Then, create a Lead post and enter this information.
        * 
        */
        
        //Call the Leads Class
        $leads = new Leads();
        
        //Save the Customer to a Lead Array.        
        $leads_array = array( 'lead_customer'  =>  $c_id );
        $leads_array['lead_email'] =  $email;
        
        //Set website as salesperson.
        $leads_array['salesperson'] =  'Website';
        
        //Make the Today's Date the Lead Date            
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        $leads_array['lead_date'] = $today_date->format('F j, Y');
        
        //Set the Product Name.
        $leads_array['lead_name'] = 'Trade Show Lead';
        
        //Create new Lead
        $q_id = $leads->create_new_lead( $leads_array );
        
        //Call Capture Lead Class
        $c_lead = new CaptureLead( $q_id );
        
        //Set Lead Company Name and Salesperson
        $c_lead->set_lead_company_name( $company_name );
        $c_lead->set_lead_salesperson( 'Website' );
        $c_lead->set_lead_state( $state );
        
        //Let's go ahead and set Interets as tags.
        if( !empty( $interests_array) ){
            $customer->base_set_customer_tags( $interests_array );
            $c_lead->set_lead_tags( $interests_array );
        }        
        
        /*
        * 
        * Let's send this down to Campaign Monitor
        * 
        */
        
        //Call the CaptureCM Class
        $cm = new CaptureCM();
        
        $result = $cm->add_capture( $q_id , $list_id );
        
        $success_message = "<p>You've been added to the list!</p>";

        if( $result->was_successful() ) {
            error_log( "Subscribed with code " . $result->http_status_code);
            $return = array ( 'foo' => $success_message );
        } else {
            error_log( 'Failed with code ' . $result->http_status_code );
    //        error_log( 'Failed with code ' . $result->response );
            $return = array ( 'foo' => 'There was an error sending the message.' );
        }        
        
        /*
        * 
        * Lastly, let's throw it into HubSpot CRM.
        * 
        */
        
        $captureHub = new CaptureHubSpot();
        $captureHub->create_contact( $q_id );        
        
    echo wp_send_json( $return );
        
    }
}

add_action('wp_ajax_gmg_capture_form_process', 'gmg_capture_form_process');
add_action('wp_ajax_nopriv_gmg_capture_form_process', 'gmg_capture_form_process');
function gmg_capture_form_process() {
        
//    error_log( 'Inside Capture Process!' );
        
    $contact_Fname = (isset($_POST['CaptureFName']) ) ? $_POST['CaptureFName'] : '';
    $contact_Lname = (isset($_POST['CaptureLName']) ) ? $_POST['CaptureLName'] : '';
    $company_name = (isset($_POST['CaptureCompanyName']) ) ? $_POST['CaptureCompanyName'] : '';
    $email = (isset($_POST['CaptureEmail']) ) ? $_POST['CaptureEmail'] : '';
    $state = (isset($_POST['CaptureState']) ) ? $_POST['CaptureState'] : '';
    $message = (isset($_POST['CaptureMessage']) ) ? $_POST['CaptureMessage'] : '';
    $phone = (isset($_POST['CapturePhone']) ) ? $_POST['CapturePhone'] : '';
    $p_id = (isset($_POST['CaptureID']) ) ? $_POST['CaptureID'] : '';
    $list_id = (isset($_POST['CaptureFormList']) ) ? $_POST['CaptureFormList'] : '';
    
    $addressee = (isset($_POST['CaptureRecipient']) ) ? $_POST['CaptureRecipient'] : '';
    $subject = (isset($_POST['CaptureSubject']) ) ? $_POST['CaptureSubject'] : '';
    
    // Build POST request:
//    $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    $recaptcha_secret = '6LeL9JsUAAAAAH7uWMH1jxGmumwtVryUVX5BU5Ij';
    $recaptcha_response = (isset($_POST['CaptureToken']) ) ? $_POST['CaptureToken'] : '';
//    error_log( 'Response is ' . $recaptcha_response );
    
    $data = array(
            'secret' => $recaptcha_secret,
            'response' => $recaptcha_response
        );
    
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    curl_close($verify);
    $response = json_decode($response, true );
    
    /*
    * 
    * First, create a Customer. And tag them with anything related to that Product, Page, or Selection
    * 
    */

    //Call Customers class
    $customers = new Customers();

    $cust_info = array(
            'fname'          => $contact_Fname,
            'lname'          => $contact_Lname,
            'email'         => $email,
            'phone'         => $phone);

    //1) Check to see if Customer exists.
    if( !$customers->check_if_customer_exists_by_email( $email ) ){           

        //It doesn't, so create new customer.
        $c_id = $customers->create_new_customer( $cust_info );

//            error_log( 'Customer is ' . $c_id );

    } else {

        //The customer must be older, so get it.
        $c_id = $customers->get_customer_by_email( $email );

        $customer = new Customer( $c_id );
        $customer->update_customer( $cust_info );
        $customer->update_title();
    }
        
    //Make a new CaptureCustomer Class
    $customer = new CaptureCustomer( $c_id );
    
    error_log( 'Product ID is ' . $p_id );
    if( $p_id != '' ){
        
        $get_tags = get_field( 'overall_tags', $p_id );
        error_log( 'Count of Prod Tags are ' . count( $get_tags ) );
        update_field( 'overall_tags', $get_tags, $c_id  );
        wp_set_post_tags( $c_id, $get_tags, true );
    }
    
    /*
    * 
    * Then, if Lead Chaser is installed, create a Lead post and enter this information.
    * 
    */
        
    if( class_exists( 'Lead' ) ){
        
        //Call the Leads Class
        $leads = new Leads();
        
        //Save the Customer to a Lead Array.        
        $leads_array = array( 'lead_customer'  =>  $c_id );
        $leads_array['lead_email'] =  $email;
        
        //Set website as salesperson.
        $leads_array['salesperson'] =  'Website';
        
        //Make the Today's Date the Lead Date            
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        $leads_array['lead_date'] = $today_date->format('F j, Y');
        
        //Set the Product Name.
        $leads_array['lead_name'] = 'Class Participant';
        
        //Create new Lead
        $q_id = $leads->create_new_lead( $leads_array );
        
        //Call Capture Lead Class
        $c_lead = new CaptureLead( $q_id );
        
        //Set Lead Company Name and Salesperson
        $c_lead->set_lead_company_name( $company_name );
        $c_lead->set_lead_salesperson( 'Website' );
        $c_lead->set_lead_state( $state );
        
        
        //Let's go ahead and set Interets as tags.
        if( !empty( $interests_array) ){
            $customer->base_set_customer_tags( $interests_array );
            $c_lead->set_lead_tags( $interests_array );
        }
    }
        
    /*
    * 
    * If there's a List ID, let's send this down to Campaign Monitor
    * 
    */
    
//    error_log( 'List ID is ' . $list_id );
    if( strcasecmp( $list_id, 'none' ) !== 0 ){
        
        //Call the CaptureCM Class
        $cm = new CaptureCM();
        
        $result = $cm->add_capture( $c_id , $list_id );
        
        $success_message = "<p>You've been added to the list!</p>";

        if( $result->was_successful() ) {
            error_log( "CM Subscribed with code " . $result->http_status_code);
            $return = array ( 'foo' => $success_message );
        } else {
            error_log( 'CM Failed with code ' . $result->http_status_code );
            $file = fopen( plugin_dir_path( __FILE__ ) . 'captureErrors.txt', "w" );
            fwrite( $file, json_encode($result, JSON_PRETTY_PRINT) );
            fclose($file);
    //        error_log( 'Failed with code ' . $result->response );
            $return = array ( 'foo' => 'There was an error.' );
        }
        
//    echo wp_send_json( $return );
    }
        
        /*
        * 
        * If they have chosen mobile, send them to Twilio.
        * 
        */        
//        error_log( 'Answer is ' . $answer );
    if( isset( $answer ) ){
        if( strtolower( $answer ) == 'yes' ){

            $ct = new CaptureTwilio();

            if( strstr( $mobile, '-', false ) ){

                error_log( 'Send a text message with fixed phone!');
                $mobile_parts = explode( '-', $mobile );
                $ct->send_class_message( implode('', $mobile_parts )  );
            } else {
                error_log( 'Send a text message!');
                $ct->send_class_message( $mobile );
            }

            $customer->set_mobile( $mobile );
        }
    }
        
    /*
    * 
    * Lastly, let's email folks
    * 
    */
    
//    error_log( 'Addressee is ' . $addressee );
    if( $addressee != '' ){
        
        // Take action based on the score returned:
        if ($response["score"] >= 0.5) {
        
            error_log( 'Score is good!' );    

            $message_html = array();

            array_push( $message_html, "FROM: " . $contact_Fname . " "  . $contact_Lname );
            array_push( $message_html, "EMAIL: " . $email );

            if( $phone != '' ){            
                array_push( $message_html, "PHONE NUMBER: " . $phone );
                $subject = str_replace( '_' , ' ', $subject );
            }

            if( $message != '' ){
                array_push( $message_html, "MESSAGE: " . $message );
            }

            if( get_post_type( $p_id ) == 'product' ){            
                array_push( $message_html, "PRODUCT: " . get_the_title( $p_id ) );

            }

            array_push( $message_html, "SUBJECT: " . $subject );

            $site_name = get_bloginfo( 'name' );
            $site_email = get_bloginfo( 'admin_email' );

            $success_message = '<p>Your message has been sent</p>';
            $email_subject = "New contact from $site_name for $subject";
            $headers   = array();
            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-type: text/plain; charset=iso-8859-1";
            $headers[] = "From: $site_name <$site_email>";
            $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
            $headers[] = "Reply-To: {$site_email}";
            $headers[] = "Subject: {$email_subject}";
            $headers[] = "X-Mailer: PHP/".phpversion();

            $answer = wp_mail($addressee, $email_subject, implode( "\n" , $message_html), implode("\r\n", $headers) );

            error_log( 'Answer is ' . $answer );

            if ($answer){
                $return = array ( 'foo' => $success_message );
                error_log( 'Success' );
            } else {
                $return = array ( 'foo' => 'There was an error sending the message.' );
                error_log( 'Failure' );
            }
            echo wp_send_json( $return );
            exit();
        }
        
        $return = array ( 'foo' => 'Thanks!' );
        echo wp_send_json( $return );
        exit();
    }
    
    $return = array ( 'foo' => 'Submitted!' );            
    echo wp_send_json( $return );
}

add_shortcode( 'transfer' , 'gmg_capture_tag_transfer');
function gmg_capture_tag_transfer(){
    
    $capture = new CaptureLeads();
    $lead = $capture->get_winner( 'Home Show Lead' );
    echo get_the_title( $lead );    
    $capture->email_staff( $lead );
}
