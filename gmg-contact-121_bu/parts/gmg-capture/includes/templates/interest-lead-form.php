<?php
/**
 * Altitude Pro.
 *
 * This file adds an Interest Sign Up form template
 *
 * Template Name: Interest Sign Up Form
 *
 * @package GMG Webs
 * @author  Good Marketing Group
 * @license GPL-2.0+
 * @link    http://goodgroupllc.com/
 */


remove_action( 'genesis_before', 'genesis_header_markup_open', 5 );
remove_action( 'genesis_before', 'genesis_do_header', 10 );
remove_action( 'genesis_before', 'genesis_header_markup_close', 15 );

remove_action( 'genesis_before', 'gmg_before_header_widget_area', 8 );

//* Remove navigation
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_after_header', 'genesis_do_subnav' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove sub-footer widgets
remove_action( 'genesis_before_footer', 'outreach_sub_footer', 5 );

//* Remove site footer widgets
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );

//* Remove site footer elements
remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action( 'genesis_footer', 'genesis_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_close', 15 );

remove_action( 'genesis_footer', 'genesis_do_subnav', 7 );

add_action('genesis_after_entry_content', 'gmg_add_interest_form');

function gmg_add_interest_form(){
    ?>

<div class="lead-sign-up">
    <form name="gmg_lead_form_entry" method="post" action="" id="gmg_lead_form_entry">

        <div class="form-names">
            <label for="CaptureName">Your Name *</label>
            <input id="CaptureFName" name="CaptureFName" placeholder="First" type="text" class="names"  required/><input id="CaptureLName" name="CaptureLName" placeholder="Last" type="text" class="names"  required/>
        </div>
        <br />

        <label for="CaptureEmail"> Your Email *</label>
        <input id="CaptureEmail" name="CaptureEmail" type="email" required />
        <br />
        <br />

        <label for="CaptureCompanyName">Company Name *</label>
        <input id="CaptureCompanyName" name="CaptureCompanyName" type="text" required/>
        <br />
        <br />
        
        <label for="CaptureMobilePhone">Mobile Phone</label>
        <input id="CaptureMobilePhone" name="CaptureMobilePhone" type="text" />
        <br />
        <br />


        <?php $states_full_key = array(
                "Alabama" => "Alabama",
                "Alaska" => "Alaska",
                "Arizona" => "Arizona",
                "Arkansas" => "Arkansas",
                "California" => "California",
                "Colorado" => "Colorado",
                "Connecticut" => "Connecticut",
                "Delaware" => "Delaware",
                "District of Columbia" => "District of Columbia",
                "Florida" => "Florida",
                "Georgia" => "Georgia",
                "Hawaii" => "Hawaii",
                "Idaho" => "Idaho",
                "Illinois" => "Illinois",
                "Indiana" => "Indiana",
                "Iowa" => "Iowa",
                "Kansas" => "Kansas",
                "Kentucky" => "Kentucky",
                "Louisiana" => "Louisiana",
                "Maine" => "Maine",
                "Maryland" => "Maryland",
                "Massachusetts" => "Massachusetts",
                "Michigan" => "Michigan",
                "Minnesota" => "Minnesota",
                "Mississippi" => "Mississippi",
                "Missouri" => "Missouri",
                "Montana" => "Montana",
                "Nebraska" => "Nebraska",
                "Nevada" => "Nevada",
                "New Hampshire" => "New Hampshire",
                "New Jersey" => "New Jersey",
                "New Mexico" => "New Mexico",
                "New York" => "New York",
                "North Carolina" => "North Carolina",
                "North Dakota" => "North Dakota",
                "Ohio" => "Ohio",
                "Oklahoma" => "Oklahoma",
                "Oregon" => "Oregon",
                "Pennsylvania" => "Pennsylvania",
                "Rhode Island" => "Rhode Island",
                "South Carolina" => "South Carolina",
                "South Dakota" => "South Dakota",
                "Tennessee" => "Tennessee",
                "Texas" => "Texas",
                "Utah" => "Utah",
                "Vermont" => "Vermont",
                "Virginia" => "Virginia",
                "Washington" => "Washington",
                "West Virginia" => "West Virginia",
                "Wisconsin" => "Wisconsin",
                "Wyoming" => "Wyoming"
            ) ?>

        <label for="CaptureState" class="fb-text-label">Company State *<br /></label>
        <select name="CaptureState" id="CaptureState" required>

            <?php foreach( $states_full_key as $state ): ?>
            <option value="<?php echo $state ?>">
                <?php echo $state ?>
            </option>
            <?php endforeach ?>

        </select>
        <br />
        <br />

        <div class="form-tags">
            <strong>Interested In: * </strong>

            <?php $fields = get_fields('overall_settings'); ?>
            <?php if( is_array( $fields ) AND count( $fields ) != 0): ?>
            <?php $term_array = $fields['new_tags']; ?>
            <?php if( is_array( $term_array ) AND count( $term_array ) != 0): ?>
            <?php foreach( $term_array as $each_term ): ?>
            <?php $term = $each_term['tag_name']; ?>
            <div class="checkbox-unit">
                <input id="CaptureInterests" name="<?php echo $term->name; ?>" type="checkbox" value="<?php echo $term->name; ?>">
                <label for="<?php echo $term->name; ?>"><?php echo $term->name; ?></label>
            </div>
            <?php endforeach; ?>
            
            <div class="checkbox-unit other">
                <input id="CaptureInterestsOther" name="Other" type="text" class="other-input" value="">
                <label for="Other">Other</label>
            </div>
            
            <?php endif; ?>
            <?php endif; ?>
        </div>

        <input type="text" name="CaptureFormList" id="CaptureFormList" value="<?php echo get_field('class', get_the_ID() ); ?>" style="visibility: hidden" />
        <div class="sign-up-submit">
            <button id="class_form_submit" class="btn btn-primary" type="submit" value="submit">Sign Up</button>
            <img id="sign-up-wait" src="https://goodmarketinggroup.com/wp-content/uploads/2018/03/Loading_icon.gif" width="100" style="display: none;" />
        </div>

        <h3 class="form-control" name="webdev_form_response" id="webdev_form_response" style="visibility: hidden"></h3>
    </form>
</div>
<?php
}

// This file handles pages, but only exists for the sake of child theme forward compatibility.
genesis();
