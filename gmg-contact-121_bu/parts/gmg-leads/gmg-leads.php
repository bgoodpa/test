<?php

//error_log('Inside GMG-leads');

$fields = get_fields('overall_settings');

if( $fields['lead_chaser'] != '' ){
    if( $fields['lead_chaser'][0] == 'yes' ){
        
        $quo = plugin_dir_path( __FILE__ );
        
        //Let's bring in the custom post type leads, which is where we will store the info.
        require_once($quo . '/includes/gmg-leads-cpt.php');

        //Let's also bring in the code to save the ACF fields.
        require_once($quo . '/includes/gmg-leads-acf.php');

        //Let's bring in Class Review.
        require_once($quo . '/includes/gmg-class-leads.php');
        
        add_action( 'admin_menu', 'register_gmg_leads_menu_page', 11 );
        
        //Let's also bring in options to be show in admin
        require_once($quo . '/includes/gmg-leads-admin.php');

        //Let's also bring in the dashboard widget to Update leads.
        require_once($quo . '/includes/gmg-leads-widget.php');

        //Let's bring in the code to handle returning leads info to Update a lead.
        require_once($quo . '/includes/gmg-leads-return.php');
        
    }
}