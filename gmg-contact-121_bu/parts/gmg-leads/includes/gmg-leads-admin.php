<?php

function register_gmg_leads_menu_page(){
    
//    error_log( 'Inside Leads menu page ' );
    
    $plugin = new Gmg_Contact_121();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Leads Chaser Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Leads Chaser Settings',
            'post_id'       => 'lead_options',
            'capability'    => 'manage_options',
        )
    );
    
    $fields = get_fields('lead_options');
    $capabilities = 'manage_options';
    
    if( is_array( $fields ) ){
        
        if( isset( $fields['let_users_update_leads'] ) ){
            
            if( $fields['let_users_update_leads'] == true ){
                
                $capabilities = 'publish_posts';
            }
            
            
        }
    }
    
//    error_log( 'Plugin Name is ' . $options_name );
    
    add_submenu_page(
        $options_name,                                     // parent slug
        __( 'Leads Dashboard', $options_name ),              // page title
        __( 'Leads Dashboard', $options_name ),                  // menu title
        $capabilities,                                              // capability
        $options_name .'-leads-dashboard',                                             // menu_slug
        'display_gmg_leads_dashboard'      // callable function
    );
    
    add_role( 'lc-leader', 'Leads Leader' );
    
}

function display_gmg_leads_dashboard(){
//    error_log( 'Update page displayed' );

    //Let's also bring in the dashboard widget to Update leads.
    require_once( plugin_dir_path( __FILE__ ) . '/gmg-leads-dashboard.php');
    
}

add_filter('acf/load_field/key=field_5b88055017b4b', 'gmg_lc_update_copyright', 10, 3);
function gmg_lc_update_copyright( $field ){
    
//    var_dump( $field );
    $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
    
    return $field;
}

add_action( 'admin_enqueue_scripts', 'gmg_leads_script_loader' );
function gmg_leads_script_loader(){
    
    wp_register_script('lead', plugin_dir_url( __FILE__ ) . 'js/gmg-contact-121-lead-admin.js', array(
        'jquery'), null, true );
    wp_enqueue_script('lead');
    wp_localize_script( 'lead', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    
    wp_register_script("charts", "https://www.gstatic.com/charts/loader.js");
	wp_enqueue_script("charts");
    
    wp_register_script('draw', plugin_dir_url( __FILE__ ) . 'js/gmg-lead-admin-visual.js', array(
        'jquery'), null, true );
    wp_enqueue_script('draw');
    wp_localize_script( 'draw', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    
    wp_register_script('backup', plugin_dir_url( __FILE__ ) . 'js/gmg-lead-dashboard-backup.js', array(
        'jquery'), null, true );
    wp_enqueue_script('backup');
    wp_localize_script( 'backup', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    
    wp_register_style( 'leader', plugin_dir_url( __FILE__ ) . 'css/gmg-lead-admin.css', false );
    wp_enqueue_style( 'leader' );
    
}

add_filter('acf/load_field/key=field_5b7d8b128356f', 'gmg_cm_lead_lists_load', 10, 3);
function gmg_cm_lead_lists_load( $field ){
    
    $bm = new LeadCM();
    $list_array = $bm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}

add_filter('acf/load_field/key=field_5bbf72b6d48b3', 'gmg_cm_quote_lists_load', 10, 3);
function gmg_cm_quote_lists_load( $field ){
    
    $bm = new LeadCM();
    $list_array = $bm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}

add_filter('acf/load_field/key=field_5bf8696c10c65', 'gmg_cm_email_history_log', 10, 1);
function gmg_cm_email_history_log( $field ){
    
    global $post;
    
//    error_log( 'Value is ' . $value );
//    error_log( 'ID is ' . $post->ID );
//    error_log( 'Field is ' . $field );
    
    $bm = new LeadCM();
    if( $bm->check_lead_history( $post->ID ) != false ){
        
        $field['message'] = $bm->get_lead_history( $post->ID );
        
    }
    
    return $field;
    
}