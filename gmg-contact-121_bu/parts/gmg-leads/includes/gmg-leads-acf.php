<?php

add_filter('acf/save_post' , 'gmg_lead_backend_save', 10, 1 );
function gmg_lead_backend_save( $post_id ) {
    
//    error_log( 'Inside lead Page Save ACF!' );
    
    error_log( 'This post is ' . get_post_type( $post_id ) );
    
    if( get_post_type( $post_id ) == 'c121-leads'){
        
        $lead = new lead( $post_id );
        
        //If calling wp_update_post, unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'gmg_lead_backend_save');
        
        $lead->update_title();
            
        // re-hook this function
        add_action('save_post', 'gmg_lead_backend_save');
            
        }
    
}
