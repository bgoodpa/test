<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_c121_lead_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_lead_dashboard_widget',         // Widget slug.
        'Add Lead Chaser',         // Title.
        'gmg_lead_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_c121_lead_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_lead_dashboard_widget_function() {
    
    $leads = new Leads();
    $fields = get_fields('lead_options');

	// Display whatever it is you want to show.
	echo "<p><strong>Please enter all the following information:</strong></p>";
    
    ?>

    <form name="gmg_lead_form" method="post" action="" id="gmg_lead_form_dashboard" value="lead_form">
        
        <strong>Customer Information</strong>
        
        <?php if( !$leads->check_if_salesperson() ): ?>        
            <p>
                <label for="leadSales">Salesperson</label><br />
            <?php
                $salespeople_array = $leads->get_salespeople();

                if( $salespeople_array ){

                    echo '<select id="leadSales" name="leadSales" style="width: 100%">';
                    foreach( $salespeople_array as $sales ){                    
                        echo '<option value="' . $sales['sales_person_email'] . '">' . $sales['sales_person_name'] . '</option>'; 
                    }
                    echo '</select>';

                } else {
                    ?>

                <input id="leadSales" name="leadSales" type="text" required />

                <?php 
                }
                ?>    

            </p>
        <?php else: ?>
            <input id="leadSales" name="leadSales" type="text" value="<?php echo wp_get_current_user()->user_email; ?>" style="visibility: hidden; width: 100%" />        
        <?php endif; ?>
        <label for="leadFName">*Customer Name</label><br />
        <table style="width: 100%">
            <tr>
                <td style="width: 50%">
                    <input id="leadFName" name="leadFName" type="text" placeholder="First" required style="width: 100%" />
                </td>
                <td style="width: 50%">
                    <input id="leadLName" name="leadLName" type="text" placeholder="Last" required style="width: 100%" />
                </td>
            </tr>
        </table>
        <p>
            <label for="leadPhone">Phone Number</label><br />
            <input id="leadPhone" name="leadPhone" type="text" style="width: 100%" />
        </p>
        
        <strong>Lead Information</strong>
        <p>
            <label for="leadJName">*Product(s)</label><br />
            <input id="leadJName" name="leadJName" type="text" required style="width: 100%" />
        </p>
        
        <?php if( isset( $fields['need_custom_date'] ) && $fields['need_custom_date'] == true ): ?>
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label for="leadDate">Custom Date</label><br />
                        <input id="leadDate" name="leadDate" type="date" />
                    </td>
                    <td style="width: 50%">
                        <label for="todayDate">If today, check here:</label>
                        <input id="todayDate" name="todayDate" type="checkbox" />
                    </td>            
                </tr>        
            </table>        
        <?php endif; ?>
        <p>
            <label for="leadReferrer">Referrer</label><br />
        <?php
            $referrers_array = $leads->get_referrers();
            if( $referrers_array ){

                echo '<select id="leadReferrer" name="leadReferrer" style="width: 100%">';
                foreach( $referrers_array as $referrer ){                    
                    echo '<option value="' . $referrer['referrer'] . '">' . $referrer['referrer'] . '</option>'; 
                }
                echo '</select>';

            } else {
                ?>

            <input id="leadReferrer" name="leadReferrer" type="text" required />

            <?php } ?>       

        </p>
        
        <p>
            <label for="leadQuote">Did you give: a Quote? </label><input id="leadQuote" name="leadQuote" type="checkbox" value="a quote" /> <label for="leadQuotes"> Multiple Quotes? </label><input id="leadQuotes" name="leadQuotes" type="checkbox" value="quotes" />
        </p>
        
        <p>
            <label for="leadClose">If you closed the sale, check here:</label>
            <input id="leadClose" name="leadClose" type="checkbox" />
        </p>
        
        <?php if( isset( $fields['personalize_initial_email'] ) && $fields['personalize_initial_email'] == true ): ?>
            <strong>Email Program</strong>
            <p>If you want them to be added to the Lead Chaser Email Program:</p>
        
            <p>
                <label for="leadEmail">Email</label><br />
                <input id="leadEmail" name="leadEmail" type="email" style="width: 100%" />
            </p>
        
            <p>
                <label for="leadEmailStart">Check to send them to the Email Program:</label>
                <input id="leadEmailStart" name="leadEmailStart" type="checkbox" />
            </p>
        
            <?php if( isset( $fields['email_template'] ) ): ?>
        
                <p id="email-reveal">To see a sample of the intial email and where the customized introduct is automatically inserted, click here.</p>
                <div id="email-template" style="display: none;">
                    <?php echo $fields['email_template']; ?>        
                </div>     
        
            <?php endif; ?>
            
            <p>
                <label for="leadComments">Enter your customized introduction to the initial email:</label><br />
                <textarea id="leadComments" name="leadComments" style="width: 100%"></textarea>
            </p>
        
        <?php endif; ?>       

        <input type="hidden" name="gmg_lead" id="gmg_lead" value="gmg_lead_submit" />
        <p>
            <input type="submit" name="gmg_lead_dashboard_button" class="button-primary" value="Add Lead">
        </p>

        <?php $leads = new leads(); ?>

        <?php if( $leads->get_last_used() != false ): ?>
        <p>Last used:
            <?php echo $leads->get_last_used() ?>
        </p>
        <?php endif; ?>

    </form>
    <?php
}

add_action( 'init', 'func_gmg_leads_send_contact' );
function func_gmg_leads_send_contact() {
    
    //Let's make sure that we're pulling from the right form.    
	if(isset($_POST['gmg_lead'])) {
        
        //Grab the Lead Options
        $fields = get_fields('lead_options');
        
        //Call the Customers Class        
        $customers = new Customers();
        
        //Call the Leads Class
        $leads = new leads();
        
        //Let's grab the customer info.
        $cust_info = array(
            'fname'          => $_POST['leadFName'],
            'lname'          => $_POST['leadLName'],
            'email'         => $_POST['leadEmail']
        );        
        
        //First, check to see if Customer exists.
        if( !$customers->check_if_customer_exists( $_POST['leadEmail'] ) ){
            
            //It doesn't, so create new customer.
            $customers->create_new_customer( $cust_info );
            
            //Go ahead and get the customer.
            $c_id = $customers->get_customer( $_POST['leadEmail'] );
//            error_log( 'Customer is ' . $c_id );
        
        } else {
            
            //The customer must be older, so get it.
            $c_id = $customers->get_customer( $_POST['leadEmail'] );
        }
        
        //Make a new LeadCustomer Class
        $customer = new LeadCustomer( $c_id );
        
        //Save the Customer to a Lead Array.        
        $leads_array = array( 'lead_customer'  =>  $c_id);
        $leads_array['lead_email'] =  $_POST['leadEmail'];
        
        //First, let's check if this user is a salesperson.
        
        if( $leads->check_if_salesperson() ){
            
            //They are so we'll just tell the code.            
            $leads_array['salesperson'] =  wp_get_current_user()->user_email;
            
        } else {
            
            //They are not so save the salesperson. 
            $leads_array['salesperson'] =  $_POST['leadSales'];
            
        }
        
        //Check the fields to see if we're showing a custom date
        if( $fields['need_custom_date'] == true ){
            
            //If so, check if they clicked Today's Date            
            if( isset( $_POST['todayDate'] ) ){
            
                if( $_POST['todayDate'] ){
                    
                    //Make the Today's Date the Lead Date
                    $today_date = new DateTime();
                    $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                    $lead_date = $today_date->format('F j, Y');                
                }
            
            } else {
                
                if( $_POST['leadDate'] ){
                    
                    //Otherwise, grab the custom Lead Date
                    $lead_date = DateTime::createFromFormat('Y-m-d', $_POST['leadDate'] )->format('F j, Y');
                    
                } else {
                    
                    //Make the Today's Date the Lead Date
                    $today_date = new DateTime();
                    $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                    $lead_date = $today_date->format('F j, Y'); 
  
                }
                
            }      
            
        } else {
            
            //Make the Today's Date the Lead Date            
            $today_date = new DateTime();
            $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
            $lead_date = $today_date->format('F j, Y');
            
        }
        
        //Set the date.
        $leads_array['lead_date'] = $lead_date;
        
        //Set the Product Name.
        $leads_array['lead_name'] = $_POST['leadJName'];
        
        //Create Leads Class
        $leads = new Leads();
        
        //Create new Lead
        $q_id = $leads->create_new_lead( $leads_array );
        
        //Create Lead Class
        $lead = new Lead( $q_id );
        
        //Are we capturing the personalized email content?
        if( $fields['personalize_initial_email'] == true ){
            
            //If so, grab it.
            $lead->set_lead_comments( $_POST['leadComments'] );
                        
        }
        
        //Set new Lead project for Customer.
        $customer->set_lead_projects( $q_id );
        

        
        //If the referrer is set, then save it.     
        if( isset( $_POST['leadReferrer'] ) ){            
            $lead->set_lead_referrer( $_POST['leadReferrer'] );            
        }
        
        //If the the sales was closed was checked.
        if( isset( $_POST['leadClose'] ) ){
            
            if( $_POST['leadClose'] ){
                $lead->set_lead_status( 'sale' );
                $todays_date = new DateTime();
                $todays_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                $lead->set_lead_close_date( $todays_date->format('F j, Y' ) );
                
                //Create a new Campaign Monitor Class
                $campaign = new LeadCM();
                
                //Send down the Lead to Campaign Monitor but don't do anything with them.
                $result = $campaign->add_lead( $q_id );
                
            }
            
        } else {
            
            //Set Lead Status as Pending.
            $lead->set_lead_status( 'pending' );
            
            //Create a new Campaign Monitor Class
            $campaign = new LeadCM();
            
            //Was the Lead Quote checked?
            if( isset( $_POST['leadQuote'] ) ||
               isset( $_POST['leadQuotes'] ) ){
                
                //Was it a Quote?                
                if( $_POST['leadQuote'] != '' ){
                    
                    //Set Quote Quanity.
                    $lead->set_quote_quantity( $_POST['leadQuote'] );
                    
                    //Send down the Quote to Campaign Monitor
                    $result = $campaign->lc_add_quote_customer( $q_id );
                }
                
                //Then it was multiple Quotes.
                else {
                    
                    //Set Quote Quanity.
                    $lead->set_quote_quantity( $_POST['leadQuotes'] );
                    
                    //Send down the Quote to Campaign Monitor
                    $result = $campaign->lc_add_quote_customer( $q_id );
                }
                
                //It wasn't, so it's a Lead.
            } else {
                
                //Send down the Quote to Campaign Monitor
                $result = $campaign->lc_add_lead_customer( $q_id );
            }
            
            //Highlight the response from Campaign Monitor
            if($result->was_successful()) {
                error_log( "Subscribed with code ". $result->http_status_code);
            } else {
                error_log( 'Failed with code '. $result->http_status_code );
            }
            
        }
        
        //Redirect
        wp_redirect( $_SERVER['HTTP_REFERER'] );
        
        exit();
        
    }

}
