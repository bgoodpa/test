<?php

add_action('wp_ajax_admin_chosen_leads', 'admin_chosen_leads');
add_action('wp_ajax_nopriv_admin_chosen_leads', 'admin_chosen_leads');
function admin_chosen_leads() {
    
    
    $page_name = (isset($_POST['lead']) ) ? $_POST['lead'] : '';
    $page = get_page_by_title( $page_name, OBJECT, 'c121-leads');
    $lead_id = $page->ID;
    
    $leads = new Leads();
    $lead = new Lead( $lead_id );
    $customer = new Customer ( $lead->get_customer_id() );
    
    $quote_date = DateTime::createFromFormat( 'F j, Y' , $lead->get_lead_date() );
//    error_log('Quote Date saved format is ' . $lead->get_lead_date() );
//    error_log('Quote Date reformatted is ' . $quote_date->format( 'Y-m-d' ) );    
    
    $return_array = array( 'foo' => 'Saved Draft!');
    
    $return_array['id'] = $lead->get_id();
    $return_array['fname'] = $customer->get_fname();
    $return_array['lname'] = $customer->get_lname();
    $return_array['jname'] = $lead->get_lead_name();
    $return_array['email'] = $lead->get_lead_email();
    $return_array['leaddate'] = $quote_date->format( 'Y-m-d' );
    $return_array['quoteqty'] = $lead->get_quote_quantity();
    $return_array['leadnotes'] = $lead->get_lead_notes();
    $return_array['leadsales'] = $leads->get_salespersons_email( $lead->get_lead_salesperson() );
    $return_array['leadstatus'] = $lead->get_lead_status()['value'];
    $return_array['cancelnotes'] = $lead->get_cancel_reason();
    
//    error_log('Status is ' .  $lead->get_lead_status()['value'] );
    
//    $return = array ( 'foo' => 'Success!' );
    echo wp_send_json( $return_array );
    exit();
    
}

add_action('wp_ajax_get_staff_leads', 'get_staff_leads');
add_action('wp_ajax_nopriv_get_staff_leads', 'get_staff_leads');
function get_staff_leads() {
    
    $leads= new Leads();
    
    $return_array = array( 'foo' => 'Success!');
    
    $staff_array = $leads->get_salespeople();
    foreach( $staff_array as $staff ){
        
        $customer_choices = $leads->get_the_leads( $staff['sales_person_email'], 'all' );
        $return_array[ $staff['sales_person_name'] ] = count( $customer_choices );
        
    }
    
    echo wp_send_json( $return_array );
    exit();
    
}

add_action('wp_ajax_gmg_backup_leads', 'gmg_backup_leads');
add_action('wp_ajax_nopriv_gmg_backup_leads', 'gmg_backup_leads');
function gmg_backup_leads(){
    
    error_log( 'Backup Leads!');
    
    if( isset( $_POST['fName'] ) && $_POST['fName'] != ''
       && isset( $_POST['lName'] ) && $_POST['lName'] != ''
       && isset( $_POST['email'] )  && $_POST['email'] != '' ){
        
        error_log( 'Backup Leads with Names and Sales ' . $_POST['sales'] . ' !');
        
        //Call the Customers Class        
        $customers = new Customers();
        
        //Call the Leads Class
        $leads = new leads();
        
        $email = $_POST['email'];
        
        //Let's grab the customer info.
        $cust_info = array(
            'fname'          => $_POST['fName'],
            'lname'          => $_POST['lName'],
            'email'         => $email
        );        
        
        //First, check to see if Customer exists.
        if( !$customers->check_if_customer_exists( $email ) ){
            
            //It doesn't, so create new customer.
            $customers->create_new_customer( $cust_info );
            
            //Go ahead and get the customer.
            $c_id = $customers->get_customer( $email );
//            error_log( 'Customer is ' . $c_id );
        
        } else {
            
            //The customer must be older, so get it.
            $c_id = $customers->get_customer( $email );
        }
        
        //Make a new LeadCustomer Class
        $customer = new LeadCustomer( $c_id );
        
        //Save the Customer to a Lead Array.        
        $leads_array = array( 'lead_customer'  =>  $c_id );
        $leads_array['lead_email'] =  $email;
        $leads_array['salesperson'] =  $_POST['sales'];
        
        if( isset( $_POST['job'] ) ){
            $leads_array['lead_name'] =  $_POST['job'];
        }
        
        if( $leads->check_for_lead_drafts( $_POST['sales'] , $c_id ) != false ){
            $l_id = $leads->check_for_lead_drafts( $_POST['sales'] , $c_id );
        }else{
            $l_id = $leads->create_new_lead_draft( $leads_array );
        }
        
        //Make a new Lead Class
        $lead = new Lead( $l_id );
        $lead->set_customer_id( $c_id );
        $lead->set_lead_email( $email );
        $lead->set_lead_salesperson( $this->get_salespersons_name( $info_array['salesperson'] ) );
        
        error_log( 'Date looks like' . $_POST['date'] . '.' );
        if( $_POST['date'] != '' ){
            $lead_date = DateTime::createFromFormat( 'Y-m-d' , $_POST['date'] );
            $lead->set_lead_date( $lead_date->format('F j, Y' ) );
        } else {
            $todays_date = new DateTime();
            $todays_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
            $lead->set_lead_date( $todays_date->format('F j, Y' ) );
            error_log( 'Date is now' . $todays_date->format('F j, Y' ) );
        }
        
        if( isset( $_POST['referrer'] ) ){
            $lead->set_lead_referrer( $_POST['referrer'] );
        }
        
        if( isset( $_POST['comments'] ) ){
            $lead->set_lead_comments( $_POST['comments'] );
        }        
        
//        error_log( 'Quote looks like' . $_POST['quote'] . '.' );
//        error_log( 'Quotes looks like' . $_POST['quotes'] . '.' );
//        error_log( 'Close looks like' . $_POST['close'] . '.' );
        
        if( $_POST['quote'] == 'true' ){
            $lead->set_quote_quantity( 'a quote' );
        }
        
        if( $_POST['quotes'] == 'true' ){
            $lead->set_quote_quantity( 'quotes' );
        }
        
        if( $_POST['close'] == 'true' ){          
            $todays_date = new DateTime();
            $todays_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
            $lead->set_lead_close_date( $todays_date->format('F j, Y' ) );
            $lead->set_lead_status( 'sale' );
        }
        
        $return_array = array( 'foo' => 'Saved!');
        
    } else {
        
        $return_array = array( 'boo' => 'Not enough info!');
        
    }
    
    echo wp_send_json( $return_array );
    exit();
    
}

add_action( 'admin_init', 'func_gmg_leads_update' );
function func_gmg_leads_update() {
    
//    error_log( 'Called GMG Lead Updater' );
    
	if(isset($_POST['gmg_lead_updater'])) {
        
        $page_name = (isset( $_POST['webdev_form_lead_ids'] ) ) ? $_POST['webdev_form_lead_ids'] : '';
        $page = get_page_by_title( $page_name, OBJECT, 'c121-leads');
        $lead_id = $page->ID;
        
//        $lead_id = $_POST['webdev_form_lead_ids'];
//        get_page_by_title()
//        error_log( 'The ID is ' . $lead_id );
        
        if( isset( $_POST['gmg_lead_delete_button'] ) ){
//            error_log( 'Delete!' );
            
            wp_trash_post( $lead_id );
            
        } else {
            
            $lead = new Lead( $lead_id );
            
            //Let's see if the Job Name has changed.
            if( $lead->get_lead_name() != $_POST['leadJName']  ){
                $lead->set_lead_name( $_POST['leadJName'] );
                $lead->update_title();
            }
            
            //Let's see if the email address has changed.
            if( $lead->get_lead_email() != $_POST['leadEmail']  ){
                $lead->set_lead_email( $_POST['leadEmail'] );
            }
            
            //Let's see if the Lead Date has changed.
            if( isset( $_POST['leadDate'] ) ){
                $lead_date = DateTime::createFromFormat( 'Y-m-d' , $_POST['leadDate'] );                
                if( $lead->get_lead_date() != $lead_date->format('F j, Y' ) ){
                    $lead->set_lead_date( $lead_date->format('F j, Y' ) );
                }
            }
            
            //Let's see if the notes has changed.
            if( $lead->get_lead_notes() != $_POST['leadNotes']  ){
                $lead->set_lead_notes( $_POST['leadNotes'] );
            }
            
            //Let's see if the salesperson has changed. 
            if( $lead->get_lead_salesperson() != $_POST['leadSales']  ){
                $leads = new Leads();
                $lead->set_lead_salesperson( $leads->get_salespersons_name( $_POST['leadSales'] ) );
            }
            
            //Let's see if the project has cancelled notes.
            if( $_POST['project_cancelled'] ){
                if( $lead->get_cancel_reason() != $_POST['project_cancelled'] ){
                    $lead->set_cancel_reason( $_POST['project_cancelled'] );                    
                }
            }
            
            //Let's see if this lead has a quote.            
            if( isset( $_POST['leadQuote']) || isset( $_POST['leadQuotes'] ) ){
                
                //It does, but let's see if post is a draft that was autosaved.
                if( $page->post_status == 'draft' ){
                    error_log( 'In Quote, was a draft, now it aint!');
                    wp_publish_post( $lead_id );
                    $lead->update_title();
                }
                
                //Secondly, we need to see if it was a quote originally.
                if( $lead->get_quote_quantity() != '' ){
                    
                    //It was so we're going to update it.
                    
                    //Is it a single quote?
                    if( isset( $_POST['leadQuote'] ) ){
                        if( $_POST['leadQuote'] != $lead->get_quote_quantity() ){
                            error_log( 'Quote is ' . $_POST['leadQuote'] );
                            $lead->set_quote_quantity( $_POST['leadQuote'] );
                        }
                        
                        //Then it must be a multiple.
                    } if( $_POST['leadQuotes'] != $lead->get_quote_quantity() ){
                        $lead->set_quote_quantity( $_POST['leadQuotes'] );
                        error_log( 'Lead is ' . $_POST['leadQuotes'] );
                    }
                    
                } else {
                    
                    //Looks like it was a lead before so now we need to make it a quote.
                    
                    //However we will only do this for the leads marked 'pending'.
                    if( $_POST['leadStatus'] == 'pending' || $lead->get_lead_status()['value'] == 'pending' ){
                        
                        if( $_POST['leadQuote'] && $_POST['leadQuote'] != $lead->get_quote_quantity() ){
                            error_log( 'Lead is ' . $_POST['leadQuote'] );
                            $lead->set_quote_quantity( $_POST['leadQuote'] );
                        } elseif( $_POST['leadQuotes'] && $_POST['leadQuotes'] != $lead->get_quote_quantity() ){
                            $lead->set_quote_quantity( $_POST['leadQuotes'] );
                            error_log( 'Lead is ' . $_POST['leadQuotes'] );
                        }
                        
                        //We need to send it down to Quote Chaser Journey to start chasing the quote.
                        $campaign = new LeadCM();
                        $result = $campaign->lc_add_quote_customer( $lead_id );
                        
                        if($result->was_successful()) {
                            error_log( "Subscribed with code ". $result->http_status_code);
                        } else {
                            error_log( 'Failed with code '. $result->http_status_code );
                        }
                        
                        //And stop the Lead Chaser
                        $result = $campaign->lc_update_lead_customer( $lead_id, 'lead_stop' );
                        
                        if($result->was_successful()) {
                            error_log( "Subscribed with code ". $result->http_status_code);
                        } else {
                            error_log( 'Failed with code '. $result->http_status_code );
                        }
                    }
                }
            } else {
                
                //Its a lead but we want to see if post is a draft that was autosaved.
                if( $page->post_status == 'draft' ){
                    error_log( 'In Lead, was a draft, now it aint!');
                    wp_publish_post( $lead_id );
                    $lead->update_title();
                    if( $_POST['leadStatus'] == 'pending'){
                        $campaign = new LeadCM();
                        $result = $campaign->lc_add_lead_customer( $lead_id );
                        
                        if($result->was_successful()) {
                            error_log( "Subscribed with code ". $result->http_status_code);
                        } else {
                            error_log( 'Failed with code '. $result->http_status_code );
                        }
                    }
                }
            }
            
            //Let's see if the status changed.            
            if( $lead->get_lead_status()['value'] != $_POST['leadStatus']  ){
                $lead->set_lead_status( $_POST['leadStatus'] );
                
                //If it's anything other than pending, we need to stop the Chaser Journey.                
                if( $_POST['leadStatus'] != 'pending'){
                    $todays_date = new DateTime();
                    $todays_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                    $lead->set_lead_close_date( $todays_date->format('F j, Y' ) );
                    
                    $campaign = new LeadCM();
                    
                    if( !$_POST['leadQuote'] || !$_POST['leadQuotes'] ){
                        if( !$lead->get_quote_quantity() ){                        
                            error_log( 'No Quote so it is a lead so stop!' );
                            //Stop the Lead Chaser                        
                            $result = $campaign->lc_update_lead_customer( $lead_id, 'lead_stop' );
                        }
                    } else {                        
                        error_log( 'It is a quote so stop!' );
                        $result = $campaign->lc_update_quote_customer( $lead_id, 'quote_stop' );
                    }
                    
                    if($result->was_successful()) {
                        error_log( "Subscribed with code ". $result->http_status_code);
                    } else {
                        error_log( 'Failed with code '. $result->http_status_code );
                    }
                }
            }
        }
    }
}