jQuery(document).ready(function ($) {
	"use strict";
    
    var get_chart = $('#get_chart');
    if( get_chart.val() == 'get_chart'){
        
        var month;
    
        $.ajax({
                url : ajax_object.ajaxurl,
                type : 'post',
                data : {
                    action : 'get_staff_leads',
                    range : month              
                },
                success:function( response ){
                    if(response['foo']){

                        var top = [];
                        var subtop = ['Staff', 'Leads'];
                        top.push( subtop );

                        for( var key in response ){
                            if( key != 'foo' ){
                                var Nsubtop = [ key, response[key] ];
                                console.log( key + ': ' + response[key] );
                                top.push( Nsubtop );
                                console.log( top );
                            }
                        }

                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

    //                    var data = google.visualization.arrayToDataTable([
    //                      ['Staff', 'Leads'],
    //                      ['Jenna',     6],
    //                      ['Jay',      1],
    //                      ['Ray',  2],
    //                      ['Shannon', 10]
    //                    ]);

                        var data = google.visualization.arrayToDataTable(top);

                        var options = {
                          title: 'Staff Leads'
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                        chart.draw(data, options);
                        }
                    } else{
                        alert( 'Problems' + response['errors']);
                    }

                },
                error: function(errorThrown){
                    alert('Error' + errorThrown);
                }
            });
        
    }
});