var timeoutInMiliseconds = 180000;
var timeoutId;

function resetTimer() { 
    window.clearTimeout(timeoutId)
    startTimer();
}
  
function startTimer() { 
    // window.setTimeout returns an Id that can be used to start and stop a timer
    timeoutId = window.setTimeout(doInactive, timeoutInMiliseconds)
}
  
function doInactive() {
    // does whatever you need it to actually do - probably signs them out or stops polling the server for info
    
    var namer = document.getElementById("leadFName");   
    
    if( namer.value != '' || namer.value != ' ' ){
        
        var sales = document.getElementById('leadSales').value;
        console.log( 'Sales is ' + sales );
        var fName = document.getElementById('leadFName').value;
        var lName = document.getElementById('leadLName').value;
        var email = document.getElementById('leadEmail').value;
        var job = document.getElementById('leadJName').value;
        var date = document.getElementById('leadDate').value;
        var referrer = document.getElementById('leadReferrer').value;
        var comments = document.getElementById('leadComments').value;
        var quote_checked = document.getElementById('leadQuote').checked;
        var quotes_checked = document.getElementById('leadQuotes').checked;
        var close_checked = document.getElementById('leadClose').checked;
        
        jQuery.ajax({
                url : ajax_object.ajaxurl,
                type : 'post',
                data : {
                    action : 'gmg_backup_leads',
                    sales : sales,
                    fName : fName,
                    lName : lName,
                    email : email,
                    job : job,
                    date : date,
                    referrer : referrer,
                    comments : comments,
                    quote : quote_checked,
                    quotes : quotes_checked,
                    close : close_checked,
                },
                success:function( response ){
                    if(response['foo']){
                        alert( response['foo'] );
                        window.location.reload(true);
                    } else{
                        alert( response['boo'] );
                    }
                }
        });
                        
    }
}
 
function setupTimers () {
    
    console.log( 'Timers!');
    
    document.addEventListener("mousemove", resetTimer, false);
    document.addEventListener("mousedown", resetTimer, false);
    document.addEventListener("keypress", resetTimer, false);
    document.addEventListener("touchmove", resetTimer, false);
     
    startTimer();
}

jQuery(document).ready(function ($) {
	"use strict";
    
    console.log( 'Ready to rock!');
    
    var lead_form = $('#gmg_lead');
    console.log( 'Form is ' + lead_form.val() );
    if( lead_form.val() == 'gmg_lead_submit' ){
        
        console.log( 'On the right page!');
        
        setupTimers();
        
    }
    
});