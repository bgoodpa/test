jQuery(document).ready(function ($) {
	"use strict";
    
    var email_click = $('#email-reveal');
    email_click.click(function () {
        $('#email-template').slideToggle();
    });
    
    var update_c_select = $('#webdev_form_lead_ids');
    update_c_select.change( function (i) {
        i.preventDefault();
        
        $("#leadQuote").prop('checked', false);
        $("#leadQuotes").prop('checked', false);
        
        var form_lead = $("#webdev_form_lead_ids  option:selected").val();
//        alert( 'Lead is ' + form_lead );
        
        $.ajax({
            url : ajax_object.ajaxurl,
            type : 'post',
            data : {
                action : 'admin_chosen_leads',
                lead : form_lead              
            },
            success:function( response ){
                if(response['foo']){
                    console.log( 'Status is ' + response['leadstatus'] );
                    
                    $("#leadFName").val( response['fname'] );
                    $("#leadLName").val( response['lname'] );
                    $("#leadJName").val( response['jname'] );
                    $("#leadEmail").val( response['email'] );
                    $("#leadDate").val( response['leaddate'] );
                    $("#leadNotes").val( response['leadnotes'] );
                    $("#project_cancelled").val( response['cancelnotes'] );
                    $("#leadSales").val( response['leadsales'] ).prop('selected', true);
                    $("#leadStatus").val( response['leadstatus'] ).prop('selected', true);
                    
                    if( response['quoteqty'] ){
                        if( response['quoteqty'] == 'a quote' ){
                            $("#leadQuote").val( response['quoteqty'] ).prop('checked', true);
                        } else {
                           $("#leadQuotes").val( response['quoteqty'] ).prop('checked', true); 
                        }
                    }
//                    $("#leadStatus").option( response['leadstatus'] );
                    
                } else{
                    alert( 'Problems' + response['errors']);
            
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            }
        });       
	});
    
});