<?php

//Add Custom Post Type For Leads
function Leads_init() {
	$quo_labels = array(
		'name'                  => _x( 'Leads', 'Post Type General Name', 'wellness_pro' ),
		'singular_name'         => _x( 'Lead', 'Post Type Singular Name', 'wellness_pro' ),
		'menu_name'             => __( 'Leads', 'wellness_pro' ),
		'name_admin_bar'        => __( 'Leads', 'wellness_pro' ),
		'archives'              => __( 'Lead Archives', 'wellness_pro' ),
		'attributes'            => __( 'Lead Attributes', 'wellness_pro' ),
		'parent_Lead_colon'     => __( 'Parent Lead:', 'wellness_pro' ),
		'all_Leads'             => __( 'All Leads', 'wellness_pro' ),
		'add_new_Lead'          => __( 'Add New Lead', 'wellness_pro' ),
		'add_new'               => __( 'Add New', 'wellness_pro' ),
		'new_Lead'              => __( 'New Lead', 'wellness_pro' ),
		'edit_Lead'             => __( 'Edit Lead', 'wellness_pro' ),
		'update_Lead'           => __( 'Update Lead', 'wellness_pro' ),
		'view_Lead'             => __( 'View Lead', 'wellness_pro' ),
		'view_Leads'            => __( 'View Leads', 'wellness_pro' ),
		'search_Leads'          => __( 'Search Lead', 'wellness_pro' ),
		'not_found'             => __( 'Not found', 'wellness_pro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wellness_pro' ),
		'featured_image'        => __( 'Featured Image', 'wellness_pro' ),
		'set_featured_image'    => __( 'Set featured image', 'wellness_pro' ),
		'remove_featured_image' => __( 'Remove featured image', 'wellness_pro' ),
		'use_featured_image'    => __( 'Use as featured image', 'wellness_pro' ),
		'insert_into_Lead'      => __( 'Insert into Lead', 'wellness_pro' ),
		'uploaded_to_this_Lead' => __( 'Uploaded to this Lead', 'wellness_pro' ),
		'Leads_list'            => __( 'Leads list', 'wellness_pro' ),
		'Leads_list_navigation' => __( 'Leads list navigation', 'wellness_pro' ),
		'filter_Leads_list'     => __( 'Filter Leads list', 'wellness_pro' ),
	);
    
	$quo_args = array(
		'label'                 => __( 'Lead', 'wellness_pro' ),
		'description'           => __( 'Custom Post Type for Leads', 'wellness_pro' ),
		'labels'                => $quo_labels,
		'supports'              => array( 'title', 'author' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'post',
	);
    
    $current_user = wp_get_current_user();
    if ( user_can( $current_user, 'administrator' ) ) {
        $quo_args['show_ui'] = true;
        $quo_args['show_in_menu'] = 'gmg-contact-121';
        $quo_args['menu_position'] = 7;        
    }
    
	register_post_type( 'c121-leads', $quo_args );

}
add_action( 'init', 'leads_init' );


// add order column to admin table list of posts
function gmg_Leads_add_new_post_column($cpt_columns) {
	$cpt_columns['menu_order'] = "Order";
	return $cpt_columns;
}
add_action('manage_Leads_posts_columns', 'gmg_Leads_add_new_post_column');

//Show custom order column values for Leads
function gmg_Leads_posts_show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action('manage_Leads_posts_custom_column','gmg_Leads_posts_show_order_column');


//Make Column Sortable for Leads
function gmg_Leads_order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-Leads_sortable_columns','gmg_Leads_order_column_register_sortable');
