<?php

class Lead {
    
    private $id;
    private $customer_id;    
    private $name;    
    private $email;    
    private $date;    
    private $cost;
    private $close;
    
    
    public function __construct( $id ) {
        $this->id = $id;
        $this->customer_id = get_field( 'lead_customer' , $this->id );
        $this->name = get_field( 'lead_name' , $this->id );
        $this->email = get_field( 'lead_email' , $this->id );
        $this->date = get_field( 'lead_date' , $this->id );
    }
    
    
//    Create lead
    
    public function update_lead( $info_array ){
        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $this->id);
        }
    }
    
    public function update_title(){
        
        $customer = new Customer( $this->customer_id );
        
        if( $this->has_quote() ){
            
            $outro = 'Quote';
            
        } else {
            
            $outro = 'Lead';
            
        }
        
        $post_title = $customer->get_name() . ' ' . $this->name . ' ' . $outro;

        $update_post = array(
            'ID'     => $this->id,
            'post_title'  => $post_title,
            'post_name' => $post_title
        );

        wp_update_post( $update_post );        
    }
    
//    Get lead
    
    public function get_id(){
        return $this->id;
    }
    
    public function get_customer_id(){
        return get_field( 'lead_customer' , $this->id );
    }
    
    public function get_lead_name(){
        return get_field( 'lead_name' , $this->id );
    }
    
    public function have_email(){
        if( get_field( 'lead_email' , $this->id ) ){
            return true;
        } else{
            return false;
        }
    }
    
    public function get_lead_email(){
        return get_field( 'lead_email' , $this->id );
    }
    
    public function get_lead_date(){
        return get_field( 'lead_date' , $this->id );
    }
    
//    public function get_quote_cost(){
//        return get_field( 'quote_cost' , $this->id );
//    }
    
    public function has_quote(){
        if( get_field( 'has_quote' , $this->id ) != '' ){
            return true;
        } else {
            return false;
        }
    }
    
    public function get_quote_quantity(){
        return get_field( 'has_quote' , $this->id );
    }
           
    
    public function get_lead_salesperson(){
        return get_field( 'lead_salesperson' , $this->id );
    }
    
    public function get_lead_status(){
        return get_field( 'lead_status' , $this->id );
    }
    
    public function get_lead_close_date(){
        return get_field( 'close_date' , $this->id );
    }
    
    public function get_lead_comments(){
        return get_field( 'customer_comments' , $this->id );
    }
    
    public function get_lead_notes(){
        return get_field( 'lead_notes' , $this->id );
    }

    public function get_lead_referrer(){
        return get_field( 'lead_referrer' , $this->id );
    }
    
    public function get_cancel_reason(){
        return get_field( 'cancel_reason' , $this->id );
    }    
    
    //    Set lead    

    public function set_customer_id( $c_id ){
        update_field( 'lead_customer' , $c_id , $this->id );
        $this->customer_id = $c_id;
    }
    
    public function set_lead_name( $name ){
        update_field( 'lead_name' , $name , $this->id );
        $this->name = $name;
    }
    
    public function set_lead_email( $email ){
        update_field( 'lead_email' , $email , $this->id );
        $this->email = $email;
    }
    
    public function set_lead_date( $date ){
        update_field( 'lead_date' , $date , $this->id );
        $this->date = $date;
    }
    
    public function set_quote_quantity( $quote ){
        update_field( 'has_quote' , $quote, $this->id );
    }
    
//    public function set_quote_cost( $cost ){
////        error_log ( 'First character is ' . substr( $cost , 0 , 1 ) );
//        if( substr( $cost , 0 , 1 ) != '$' ){
//            $cost = '$' . $cost;
//        }
//        update_field( 'quote_cost' , $cost , $this->id );
//        $this->cost = $cost;
//    }
           
    public function set_lead_salesperson( $salesperson ){
        update_field( 'lead_salesperson' , $salesperson , $this->id );
    }
    
    public function set_lead_close_date( $date ){
        update_field( 'close_date' , $date , $this->id );
    }
    
    public function set_lead_status( $status ){
        update_field( 'lead_status' , $status , $this->id );
    }
    
    public function set_lead_referrer( $referrer ){
        update_field( 'lead_referrer' , $referrer , $this->id );
    }
    
    public function set_lead_comments( $comments ){
        
        if( strpos($comments , 'http' ) !== false || strpos($comments , 'www' ) ){
            
            $comments_array = explode( ' ', $comments );
            $index = 0;
            
            foreach( $comments_array as $comment_individual ){
                
                if( strpos( $comment_individual , 'www' ) !== false ||
                   strpos( $comment_individual , 'http' ) !== false ){
                    
                    $comment_individual = '<a href="' . $comment_individual . '" target="_blank">' . $comment_individual . '</a>';
                    $comments_array[$index] = $comment_individual;                    
                }
                
                $index++;                
            }
            
            $comments = implode( ' ' , $comments_array );            
        }
        
        update_field( 'customer_comments' , $comments , $this->id );
    }
    
    public function set_lead_notes( $notes ){
        update_field( 'lead_notes' , $notes, $this->id );
    }
    
    public function set_cancel_reason( $notes ){
        update_field( 'cancel_reason' , $notes, $this->id );
    }
}

class Leads {
    
        public function __construct( ) {
            
        }
    
    //    Check leads    
    public function check_if_exists( $c_id , $cost ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                'relation'      =>  'AND',
                'customer'      => array(
                    'key'     => 'lead_customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
                'cost'      => array(
                    'key'     => 'quote_cost',
                    'value'   => $cost,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            return true;            
        } else{ 
            return false;
        }
        
    }
    
    public function get_lead( $leads ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                'relation'      =>  'AND',
                'customer'      => array(
                    'key'     => 'lead_customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
                'cost'      => array(
                    'key'     => 'quote_cost',
                    'value'   => $cost,
                    'compare' => '=',
                ),
            ),
        );
        
        return get_posts( $args )[0];
        
    }
    
    public function create_new_lead( $info_array ){
        
//        error_log( 'Customer is ' . $info_array['lead_customer'] );
        
        //First, let's get the LeadCustomer Class so we can get some info.
        $customer = new LeadCustomer( $info_array['lead_customer'] );
        $name = $customer->get_customer_name();
        
        //Second, get user by email address.
        $user = get_user_by( 'email', $info_array['salesperson'] );
        if( $user ){
            
            $my_post = array(
                'post_title'        => $name . ' ' . $info_array['lead_name'],
                'post_type'         => 'c121-leads',
                'post_content'      => '',
                'post_status'       => 'publish',
                'post_author'       => $user->ID,
            );
            
        } else{
            
            $my_post = array(
                'post_title'        => $name . ' ' . $info_array['lead_name'],
                'post_type'         => 'c121-leads',
                'post_content'      => '',
                'post_status'       => 'publish',
            );
            
        }
        
        $new_id = wp_insert_post( $my_post );
//        error_log( 'Inserted!' );
        
        $salesperson = $this->get_salespersons_name( $info_array['salesperson'] );
        
        $lead = new Lead( $new_id );
        $lead->set_lead_salesperson( $salesperson );
        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $new_id );
//            error_log( 'Updated!' );
        }
        
        return $new_id;
        
    }
    
    public function create_new_lead_draft( $info_array ){
        
        //First, let's get the LeadCustomer Class so we can get some info.
        $customer = new LeadCustomer( $info_array['lead_customer'] );
        $name = $customer->get_customer_name();
        
        if( $info_array['lead_name'] ){
            $post_title = $name . ' ' . $info_array['lead_name'] . ' (draft)';
        } else {
            $post_title = $name . ' (draft)';
        }
        
        //Second, get user by email address.
        $user = get_user_by( 'email', $info_array['salesperson'] );
        
        if( $user ){
            
            $my_post = array(
                'post_title'        => $post_title,
                'post_type'         => 'c121-leads',
                'post_content'      => '',
                'post_status'       => 'draft',
                'post_author'       => $user->ID,
            );
            
        } else{
            
            $my_post = array(
                'post_title'        => $post_title,
                'post_type'         => 'c121-leads',
                'post_content'      => '',
                'post_status'       => 'draft',
            );
            
        }
        
        $new_id = wp_insert_post( $my_post );
        
        $lead = new Lead( $new_id );
        $lead->set_lead_salesperson( $this->get_salespersons_name( $info_array['salesperson'] ) );
        $lead->set_lead_name($info_array['lead_name'] );
        $lead->set_lead_status( 'pending' );
        
        
        return $new_id;       
    }
    
    
    public function check_for_lead_drafts( $sales, $customer ){
        
        $user = get_user_by( 'email', $sales );
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'post_status'            => array( 'draft' ),
            'author'                 => $user->ID,
            'meta_query'             => array(
                array(
                    'key'     => 'lead_customer',
                    'value'   => $customer,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            return get_posts( $args )[0];            
        } else{
            return false;
        }        
    }    
    
    public function get_all_quotes(){
        
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC',
            'meta_query'             => array(
                array(
                    'key'     => 'lead_status',
                    'value'   => 'pending',
                    'compare' => '=',
                ),
                'relation' => 'AND',
                array(
                    'key'     => 'has_quote',
                    'compare'   => 'EXISTS',
                ),
            ),
        );
        
        $leads = array();
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $leads, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $leads;
        
    }
    
    public function get_the_quotes( $email, $status ){
        
        //Get user so we're only pulling those leads.        
        $user = get_user_by( 'email', $email );
            
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC',
            'author'                 => $user->ID,
            'meta_query'             => array(
                array(
                    'key'     => 'lead_status',
                    'value'   => $status,
                    'compare' => '=',
                ),
                'relation' => 'AND',
                array(
                    'key'     => 'has_quote',
                    'compare'   => 'EXISTS',
                ),
            ),
        );
         
        $leads = array();
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $leads, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $leads;
        
    }
    
    public function get_only_all_leads(){
        
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC',
            'meta_query'             => array(
                array(
                    'key'     => 'lead_status',
                    'value'   => 'pending',
                    'compare' => '=',
                ),
                'relation' => 'AND',
                array(
                    'key'     => 'has_quote',
                    'compare'   => 'NOT EXISTS',
                ),
            ),
        );
        
        $leads = array();
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $leads, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $leads;
        
    }
    
    public function get_only_the_leads( $email ){
        
        //Get user so we're only pulling those leads.        
        $user = get_user_by( 'email', $email );
//        error_log( 'Author will be ' . $user->display_name );
        
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC',
            'author'                 => $user->ID,
            'meta_query'             => array(
                array(
                    'key'     => 'lead_status',
                    'value'   => 'pending',
                    'compare' => '=',
                ),
                'relation' => 'AND',
                array(
                    'key'     => 'has_quote',
                    'compare'   => 'NOT EXISTS',
                ),
            ),
        );
         
        $leads = array();
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $leads, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $leads;
        
    }
    
    
    public function get_all_leads( $status ){
        
        if( $status != 'all'){
            $args = array(
                'post_type'              => array( 'c121-leads' ),
                'posts_per_page'         => '-1',
                'orderby'                => 'title',
                'order'                  => 'ASC',
                'post_status'            => array( 'publish', 'draft'),
                'meta_query'             => array(
                    array(
                        'key'     => 'lead_status',
                        'value'   => $status,
                        'compare' => '=',
                    ),
                ),
            );
            
        } else {
            
            $args = array(
                'post_type'              => array( 'c121-leads' ),
                'posts_per_page'         => '-1',
                'orderby'                => 'title',
                'post_status'            => array( 'publish', 'draft'),
                'order'                  => 'ASC' );            
        }
        
        $leads = array();
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
//                error_log( "All Leads $status: We got " . get_the_title() . ' and the id ' . get_the_ID() );
                
//                array_push( $leads, 
//                           array( $id  => $title )
//                           );
                $leads[] = get_the_title();
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $leads;
        
    }
    
    public function get_the_leads( $email, $status ){
        
        //Get user so we're only pulling those leads.        
        $user = get_user_by( 'email', $email );
//        error_log( 'Author will be ' . $user->display_name );
        
        if( $status != 'all'){
            
            $args = array(
                'post_type'              => array( 'c121-leads' ),
                'posts_per_page'         => '-1',
                'orderby'                => 'title',
                'order'                  => 'ASC',
                'author'                 => $user->ID,
                'post_status'            => array( 'publish', 'draft', ),
                'meta_query'             => array(
                    array(
                        'key'     => 'lead_status',
                        'value'   => $status,
                        'compare' => '=',
                    ),
                ),
            );
        
        } else {
            
            $args = array(
                'post_type'              => array( 'c121-leads' ),
                'posts_per_page'         => '-1',
                'orderby'                => 'title',
                'order'                  => 'ASC',
                'author'                 => $user->ID,
                'post_status'            => array( 'publish',
                                                 'draft', ),
            );            
        }
         
        $leads = array();
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
//                error_log( "We got " . get_the_title() . 'and the id ' . get_the_ID() );
                
//                array_push( $leads, 
//                           array( $id  => $title )
//                           );
                
//                array_push( $leads, get_the_title() );
                $leads[] = get_the_title();
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $leads;
        
    }
    
    public function get_last_used(){

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'c121-leads' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'        => 1,
            'order'                 => 'DESC',
            'orderby'               => 'date'
        );

        $arr_post = get_posts($args);

        if( $arr_post ){
            $post = $arr_post[0];
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $post->post_date);
            return $date->format('M d, Y');
        } else {
            return false;
        }
        
    }
    
    public function check_if_salesperson(){
        
        $current_user = wp_get_current_user();
//        error_log( 'Current user: ' . $current_user->user_email );
        if( $this->get_salespersons_name( $current_user->user_email ) ){
            return true;
        } else {
            return false;
        }
        
    }
    
    public function get_salespeople(){
        
        $fields = get_fields('lead_options');
        $salespeople = $fields['sales_people'];
        if( is_array( $salespeople ) && count( $salespeople ) > 0 ){
            return $salespeople;
        } else{
            return false;
        }
    }
    
    public function get_referrers(){
        
        $fields = get_fields('lead_options');
        $referrers = $fields['referral_list'];
        if( is_array( $referrers ) && count( $referrers ) > 0 ){
            return $referrers;
        } else{
            return false;
        }
    }    
    
    //    This function takes the email of the salesperson and returns the name of the Salesperson.
    
    public function get_salespersons_name( $email ){
        
        $fields = get_fields('lead_options');
        $salespeople = $fields['sales_people'];
        if( is_array( $salespeople ) && count( $salespeople ) > 0 ){
            foreach( $salespeople as $sales ){
                if ( $email == $sales['sales_person_email'] ){
                    return $sales['sales_person_name'];
                }
            }
        } else {
            if( $salespeople ){
                if ( $email == $salespeople['sales_person_email'] ){
                    return $salespeople['sales_person_name'];
                }                
            }
        }        
    }
    
    public function get_salespersons_email( $name ){
        
        $fields = get_fields('lead_options');
        $salespeople = $fields['sales_people'];
        if( is_array( $salespeople ) && count( $salespeople ) > 0 ){
            foreach( $salespeople as $sales ){
                if ( $name == $sales['sales_person_name'] ){
                    return $sales['sales_person_email'];
                }
            }
        } else {
            if( $salespeople ){
                if ( $name == $sales['sales_person_name'] ){
                    return $sales['sales_person_email'];
                }                
            }
        }        
    }
}


class LeadCustomer extends Customer{
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    public function get_customer_name(){
        return  get_field( 'fname', $this->id ) . ' ' . get_field( 'lname', $this->id );
    }
    
    public function set_lead_projects( $id ){
        $current_projects = $this->get_lead_projects();
        if( is_array( $current_projects ) ){
            if( !in_array( $id, $current_projects ) ){
                array_push( $current_projects , $id );
                update_field( 'quote_projects', $current_projects, $this->id );
            }
        } else {
            update_field( 'quote_projects', array( $id ), $this->id );
        }
    }
    
    public function get_lead_projects(){
        return get_field( 'quote_projects' , $this->id );
    }
    
}

class LeadCM extends CampaignMonitor{
        
    private $api_key;
    private $client_id;
    
    function __construct() {
        parent::__construct();
        
        $fields = get_fields('overall_settings');
        
        if( is_array( $fields ) AND count( $fields ) != 0) {
            $this->client_id = $fields['cm_client_id'];
            $this->api_key = $fields['cm_client_api_key'];
        }
    }
    
    public function get_clients_list(){
        
        $this->read_client_file();
        
//        error_log( 'API key is ' . $this->api_key );
        
        $wrap = new CS_REST_Clients(
            $this->client_id,
            array( 'api_key' => $this->api_key )
        );
        
        $result = $wrap->get_lists();
//        echo "Result of /api/v3.1/clients/{id}/lists\n<br />";
        if($result->was_successful()) {
            
            $list_array = array();
            $result_array = $result->response;
            foreach( $result_array as $result ){
                if( is_object( $result ) ){
                    array_push( $list_array, json_decode(json_encode($result), True) );
                }
                
            }
            
//            var_dump( $list_array );
            return $list_array;
            
//            echo "Got lists\n<br /><pre>";
//            var_dump($result->response);
            
        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
        }
//        echo '</pre>';
//        var_dump( $result );
//        return $result;
        }
    
    //Let's add a Lead to Campaign Monitor but not initiate any Journey (because they probably already made a sale).
    public function add_lead( $q_id ){
        error_log('Inside Add Lead!');
        
        $this->read_subscriber_file();
        
        $fields = get_fields('lead_options');
        $list_id = $fields['lead_chaser_list'];
        
        $lead = new Lead( $q_id );
        $customer = new Customer( $lead->get_customer_id() );
        $email = $lead->get_lead_email();
        
        error_log('Name is ' . $customer->get_name() . ', email is ' . $email . ', List ID is ' . $list_id );
        error_log('Client API Key is ' . $this->api_key ); 
        
        $wrap = new CS_REST_Subscribers( 
            $list_id,
            array( 'api_key' => $this->api_key )
        );
        
        if( $wrap->get( $email )->was_successful() ){
            
            //There's really no need to add the Lead because they are already there.
            
        } else {
            
            //Add them if they aren't.        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true,
//                    'ConsentToTrack' => 'yes',
                )
            );
        }
        
        return $result;
        
    }
    
    public function check_lead_history( $q_id ){
        
        $lead = new Lead( $q_id );
        if( $lead->have_email() ){
            
            $email = $lead->get_lead_email();
            
            $this->read_subscriber_file();
            $fields = get_fields('lead_options');            
            $list_id = $fields['lead_chaser_list'];
            
            $wrap = new CS_REST_Subscribers(
                $list_id,
                array( 'api_key' => $this->api_key )
            );
            
            if( $wrap->get( $email )->was_successful() ){
                
                return true;
                
            } else {
                return false;
            }
        } else{
            return false;
        }        
        
    }
    
    //Let's read a Leads emaill history.
    public function get_lead_history( $q_id ){
//        error_log('Inside Lead Email History!');
        
        $lead = new Lead( $q_id );
        $email = $lead->get_lead_email();
//            error_log('Email is ' . $email );

        $this->read_subscriber_file();
        $fields = get_fields('lead_options');            
        $list_id = $fields['lead_chaser_list'];
            
//            error_log('List ID is' . $list_id . ' and Key is ' . $this->api_key );
        
        $wrap = new CS_REST_Subscribers(
                $list_id,
                array( 'api_key' => $this->api_key )
            );
        
            
        $result = $wrap->get_history( $email );
//                echo "Result of GET /api/v3.1/subscribers/{list id}/history.{format}?email={email}\n<br />";
        echo var_dump( $result );

        $result_array = $this->process_lead_history( $result->response );

        return implode( '', $result_array );
        
    }
    
    public function process_lead_history( $result_array ){
        
        $html_output[] = '<table><tbody>';
        
        foreach( $result_array as $result ){
            
            if( is_array( $result->Actions ) && count( $result->Actions ) > 0 ){
                
                $actions_array = array();
                
                foreach( $result->Actions as $action ){
                    
                    $action_date = DateTime::createFromFormat('Y-m-d H:i:s', $action->Date);                         
                                        
                    array_push( $actions_array , $action->Event . ' on ' .$action_date->format('M d, Y') );                  
                }
                
                $actions = implode( ', ' , $actions_array );
                
            } else{
                
                $actions = 'No action';
                
            }
          
            array_push( $html_output , '<tr><td>Email: ' . $result->Name . '</td><td>Actions: ' . $actions . '</td></tr>' );            
            
        }
        
        array_push( $html_output, '</tbody></table>' );
        return $html_output;
        
    }
    
        //      Add Quote Customer
    public function lc_add_quote_customer( $q_id ){
        error_log('Inside Add Quote Customer!');
        
        $this->read_subscriber_file();
        
        $fields = get_fields('lead_options');
        $list_id = $fields['lead_chaser_list'];
        
        $lead = new Lead( $q_id );
        $customer = new Customer( $lead->get_customer_id() );
        $email = $lead->get_lead_email();
        
        error_log('Name is ' . $customer->get_name() . ', email is ' . $email . ', List ID is ' . $list_id );
        error_log('Client API Key is ' . $this->api_key ); 
        
        $wrap = new CS_REST_Subscribers( 
            $list_id,
            array( 'api_key' => $this->api_key )
        );
        
        if( $wrap->get( $email )->was_successful() ){
            
            $result = $wrap->update(  $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'QuoteQuantity',
                        'Value' => $lead->get_quote_quantity() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true,
//                    'ConsentToTrack' => true,
                )
            );
        } else {
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'QuoteQuantity',
                        'Value' => $lead->get_quote_quantity() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true,
//                    'ConsentToTrack' => 'yes',
                )
            );
        }
        
        return $result;
    }
    
        //      Update Quote Customer
    public function lc_update_quote_customer( $q_id, $gate ){
//        error_log('Inside CM!');
        
        $this->read_subscriber_file();
        
        $fields = get_fields('lead_options');
        $list_id = $fields['lead_chaser_list'];
        
//        require plugins_url() . '/gmg-contact-121/platforms/campaign-monitor/campaignmonitor/csrest_subscribers.php';
        
        $lead = new Lead( $q_id );
        $customer = new Customer( $lead->get_customer_id() );
        $email = $lead->get_lead_email();
        
//        error_log('Name is ' . $customer->get_name() . ', email is ' . $lead->get_lead_email() . ', List ID is ' . $list_id );
//        error_log('Client API Key is ' . $this->api_key ); 
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
//        $result = $wrap->get( $email );
//        
//        echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
//        if($result->was_successful()) {
//            echo "Got subscriber <pre>";
//            var_dump($result->response);
//        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
//        }
        
        if( $wrap->get( $email )->was_successful() ){
            
            error_log('Update!');
            
            $result = $wrap->update(  $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'QuoteQuantity',
                        'Value' => $lead->get_quote_quantity() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'JobGate',
                        'Value' => $gate ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
//                    'ConsentToTrack' => 'yes',
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        } else {
            
            error_log('New!');
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'QuoteQuantity',
                        'Value' => $lead->get_quote_quantity() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'JobGate',
                        'Value' => $gate ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
//                    'ConsentToTrack' => 'yes',
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true                   

                )
            );
        }
        
        return $result;
    }
    
    //      Add Lead Customer
    public function lc_add_lead_customer( $q_id ){
      error_log('Inside Add Lead Customer!');
        
        $this->read_subscriber_file();
        
        $fields = get_fields('lead_options');
        $list_id = $fields['lead_chaser_list'];
        
        $lead = new Lead( $q_id );
        $customer = new Customer( $lead->get_customer_id() );
        $email = $lead->get_lead_email();
        
        error_log('Name is ' . $customer->get_name() . ', email is ' . $email . ', List ID is ' . $list_id );
        error_log('Client API Key is ' . $this->api_key );
//        error_log('Customer Comments are ' . $lead->get_lead_comments() );
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
        if( $wrap->get( $email )->was_successful() ){
            
            error_log('update');
            
            $result = $wrap->update(  $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true,
//                    'ConsentToTrack' => true,
                )
            );
        } else {
            
            error_log('add');
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true,
//                    'ConsentToTrack' => 'yes',
                )
            );
        }
        
        return $result;
    }
    
            //      Update Lead Customer
    public function lc_update_lead_customer( $q_id, $gate ){
        error_log('Update Lead Customer!');
        
        $this->read_subscriber_file();
        
        $fields = get_fields('lead_options');
        $list_id = $fields['lead_chaser_list'];
        
//        require plugins_url() . '/gmg-contact-121/platforms/campaign-monitor/campaignmonitor/csrest_subscribers.php';
        
        $lead = new Lead( $q_id );
        $customer = new Customer( $lead->get_customer_id() );
        $email = $lead->get_lead_email();
        
//        error_log('Name is ' . $customer->get_name() . ', email is ' . $lead->get_lead_email() . ', List ID is ' . $list_id );
//        error_log('Client API Key is ' . $this->api_key ); 
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
//        $result = $wrap->get( $email );
//        
//        echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
//        if($result->was_successful()) {
//            echo "Got subscriber <pre>";
//            var_dump($result->response);
//        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
//        }
        
        if( $wrap->get( $email )->was_successful() ){
            
            error_log('Update!');
            
            $result = $wrap->update(  $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'JobGate',
                        'Value' => $gate ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
//                    'ConsentToTrack' => 'yes',
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        } else {
            
            error_log('New!');
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $customer->get_name(),
                'CustomFields' => array(
                    array(
                        'Key' => 'JobName',
                        'Value' => $lead->get_lead_name() ),
                    array(
                        'Key' => 'JobDate',
                        'Value' => $lead->get_lead_date() ),
                    array(
                        'Key' => 'JobSalesperson',
                        'Value' => $lead->get_lead_salesperson() ),
                    array(
                        'Key' => 'JobGate',
                        'Value' => $gate ),
                    array(
                        'Key' => 'Comments',
                        'Value' => $lead->get_lead_comments() ),
                ),
//                    'ConsentToTrack' => 'yes',
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true                   

                )
            );
        }
        
        return $result;
    }
    
    
}