<?php


//We need to know which are the right leads to get.
$current_user = wp_get_current_user();
$leads= new Leads();

//If this user is an admin or a designated person, then show them all the leads.
if ( user_can( $current_user, 'administrator' )  || user_can( $current_user, 'lc-leader' ) ) {
    
    $customer_choices = $leads->get_all_leads( 'all' );
    $lead_choices = $leads->get_only_all_leads();
    $quote_choices = $leads->get_all_quotes();
    $pending_choices = $leads->get_all_leads( 'pending' );
    $sale_choices = $leads->get_all_leads( 'sale' );
    $cancelled_choices = $leads->get_all_leads( 'cancel' );
    $postponed_choices = $leads->get_all_leads( 'post' );

} else{
    
    //If not, show them only their leads.    
    $lead_choices = $leads->get_only_the_leads( $current_user->user_email );
    $quote_choices = $leads->get_the_quotes(  $current_user->user_email, 'pending' );
    $pending_choices = $leads->get_the_leads( $current_user->user_email , 'pending' );
    $sale_choices = $leads->get_the_leads( $current_user->user_email , 'sale' );
    $cancelled_choices = $leads->get_the_leads( $current_user->user_email , 'cancel' );
    $postponed_choices = $leads->get_the_leads( $current_user->user_email , 'post' );
    
}

?>
<div class="leads-dashboard">

    <h1>Leads Dashboard</h1>

    <div class="leads-info">

        <h2>Leads Snapshot</h2>

        <div class="leads-snapshot">
            <h2>Everything Pending:
                <?php echo count( $pending_choices ); ?>
            </h2>
            <h2>Open Leads:
                <?php echo count( $lead_choices ); ?>
            </h2>
            <h2>Open Quotes:
                <?php echo count( $quote_choices ); ?>
            </h2>
            <h2>Closed Sales:
                <?php echo count( $sale_choices ); ?>
            </h2>
            <h2>Closed Postponed:
                <?php echo count( $cancelled_choices ); ?>
            </h2>
            <h2>Closed Cancelled:
                <?php echo count( $postponed_choices ); ?>
            </h2>
        </div>

        <div class="leads-graph">

            <?php if( user_can( $current_user, 'administrator' )  || user_can( $current_user, 'lc-leader' ) ): ?>
            <div id="piechart" name="piechart" style="width: 100%; height: 400px;"></div>
            <input id="get_chart" type="text" value="get_chart" style="visibility:hidden"/>

            <?php endif; ?>

        </div>

    </div>

    <div class="leads-action">

        <?php if( count( $pending_choices ) > 0 ): ?>

        <div class="leads-update-form">

            <h2>Update a Lead</h2>
            <p><strong>Pick a lead from below to update.</strong></p>

            <form name="gmg_lead_update_form" method="POST" action='' id="gmg_lead_update_form">

                <div name="gmg_lead_update" id="gmg_lead_update">

                    <div class="lead-search-box" id="lead-search-box">
                        
                        <p>                            
                            <label for="webdev_form_lead_ids">*Leads: </label>
                            <select name="webdev_form_lead_ids" id="webdev_form_lead_ids">

                                <?php foreach($pending_choices as $customer_array ): ?>
                                <option value="<?php echo $customer_array?>">
                                    <?php echo $customer_array; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </p>

                    </div>
                    <div class="update-lead-box" id="update-lead-box">
                        <p>
                            <label for="leadFName">Customer Name</label><br />
                            <input id="leadFName" name="leadFName" type="text" placeholder="First" required />
                            <input id="leadLName" name="leadLName" type="text" placeholder="Last" required />
                        </p>
                        <p>
                            <label for="leadEmail">Email</label><br />
                            <input id="leadEmail" name="leadEmail" type="email" required />
                        </p>
                        <p>
                            <label for="leadJName">Product(s)</label><br />
                            <input id="leadJName" name="leadJName" type="text" required />
                        </p>
                        <p>
                            <label for="leadDate">Lead Date</label><br />
                            <input id="leadDate" name="leadDate" type="date" required />
                        </p>
                        <p>
                            <label for="leadQuote">Did you give: a Quote? </label><input id="leadQuote" name="leadQuote" type="checkbox" value="a quote" /> <label for="leadQuotes"> Multiple Quotes? </label><input id="leadQuotes" name="leadQuotes" type="checkbox" value="quotes" />
                        </p>
                        <?php if( user_can( $current_user, 'administrator' )  || user_can( $current_user, 'lc-leader' ) ): ?>
                            <p>
                                <label for="leadSales">Salesperson</label><br />
                                <select id="leadSales" name="leadSales" required>
                                    <?php

                                    $salespeople_array = $leads->get_salespeople();

                                    if( $salespeople_array ){
                                        foreach( $salespeople_array as $sales ){
                                            echo '<option value="' . $sales['sales_person_email'] . '">' . $sales['sales_person_name'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </p>
                        <?php endif; ?>
                        <p>
                            <label for="leadNotes">Notes on lead. NOT VISIBLE TO PUBLIC!</label><br />
                            <textarea id="leadNotes" name="leadNotes"></textarea>
                        </p>
                        <p>
                            <label for="leadStatus">Status</label><br />
                            <select name="leadStatus" id="leadStatus" required>

                                <?php

                    $field_key = "field_5b7d9d969f08c";
                    $field = get_field_object( $field_key );

                    if( $field ) {
                        foreach( $field['choices'] as $k => $v ){
                            echo '<option value="' . $k . '">' . $v . '</option>';
                        }
                    }
                    ?>
                            </select>
                        </p>
                        <p>
                            <label for="project_cancelled">If cancelled, why?</label><br />
                            <textarea name="project_cancelled" id="project_cancelled"></textarea>
                        </p>
                    </div>

                    <input type="hidden" name="gmg_lead_updater" value="gmg_lead_submit" />
                    <p>
                        <input type="submit" name="gmg_lead_update_button" class="button-primary" value="Update">
<!--                        <input class="button-primary red" type="submit" name="gmg_lead_delete_button" value="Delete">-->
                    </p>

                </div>
            </form>

        </div>

        <?php else: ?>

        <p>You don't have any leads yet.</p>

        <?php endif; ?>

    </div>

</div>
