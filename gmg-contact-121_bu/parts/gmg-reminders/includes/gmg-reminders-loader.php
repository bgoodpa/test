<?php

add_action('genesis_entry_content', 'gmg_service_loader');
function gmg_service_loader() {
//    error_log( 'Page Loader!' );
    
    if ( is_page( 'appointment-sms' ) ) {
        
//        error_log( 'Review Page Loaded!' );
        echo '<div class="scheduler-form">';
        echo '<form id="gmg-sms-update-form" action="" method="post">';

        echo '<h2>Your Information</h2>';

    //    http://test.goodgroupllc.com/appointment-sms/?pid=[ReminderID,fallback=]
    //    http://test.goodgroupllc.com/appointment-sms/?pid=8590

        if( isset( $_GET['pid'] ) ){

    //        error_log( 'Get is ' . $_GET['pid'] );

            $reminder = new Reminder( $_GET['pid'] );
            $customer = new Customer( $reminder->get_customer() );

            ?>

            <p>
            <label for="webdev_form_name">*Name: </label>
            <input class="form-control" type="text" name="webdev_form_name"  placeholder="Full Name"  id="webdev_form_name" <?php if ( null !== $customer->get_name() ){ echo 'value="', strip_tags( $customer->get_name() ), '"'; } ?>>
            </p>

            <p>
            <label for="webdev_form_email">*Email: </label>
            <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address"  id="webdev_form_email" <?php if ( null !== $customer->get_email() ){ echo 'value="', strip_tags( $customer->get_email() ), '"'; } ?>>
            </p>

            <p>
            <em>If you want us to send you a text message about the appointment, give us your mobile number.</em>
            <label for="webdev_form_number">*Mobile Number: </label>
            <input class="form-control" type="text" name="webdev_form_number" placeholder="Mobile Phone Number"  id="webdev_form_number" <?php if ( null !== $customer->get_mobile() ){ echo 'value="', strip_tags( $customer->get_mobile() ), '"'; } ?>>
            </p>

            <input class="form-control" type="text" name="webdev_form_p_id" style="visibility: hidden" id="webdev_form_p_id" value="<?php echo $_GET['pid']; ?>">

            <?php

            } else {

                ?>
                <p>            
                <label for="webdev_form_name">*Name: </label>
                <input class="form-control" type="text" name="webdev_form_name"  placeholder="Full Name"  id="webdev_form_name">
            </p>

                <p>
                <label for="webdev_form_email">*Email: </label>
                <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address"  id="webdev_form_email" >
            </p>

                <p>
                    <em>If you want us to send you a text message about the appointment, give us your mobile number.</em>
                <label for="webdev_form_number">Mobile Number: </label>
                <input class="form-control" type="tel" name="webdev_form_number" placeholder="Mobile Phone Number" id="webdev_form_number" >
            </p>
    <?php
            
        }
    ?>

    <p class="form-control" name="webdev_form_response" id="webdev_form_response" style="visibility: hidden"></p>

    <button id="webdev_form_submit" class="btn btn-primary" type="submit" value="submit">Submit</button>

    <?php

    echo '</form>';
    echo '</div>';
        
    }
    
    if ( is_page( 'scheduler-form' ) ) {
        
//        error_log( 'Testimonials Loaded!' );
        
        if( isset( $_GET['pid'] ) ){
        
            error_log( 'Get is ' . $_GET['pid'] );

            $reminder = new Reminder( $_GET['pid'] );
            $customer = new Customer( $reminder->get_customer() );

        }  
    
        ?>

        <div class="scheduler-form">
            <form id="gmg-scheduler-form" action="" method="post">

                <?php if( $customer ): ?>

                    <label for="webdev_form_name">*Name: </label>
                    <input class="form-control" type="text" name="webdev_form_name"  placeholder="Full Name"  id="webdev_form_name" <?php if ( null !== $customer->get_name() ){ echo 'value="', strip_tags( $customer->get_name() ), '"'; } ?>>
                <?php else: ?>

                            <label for="webdev_form_name">*Name: </label>
                <input class="form-control" type="text" name="webdev_form_name"  placeholder="Full Name"  id="webdev_form_name" >

                <?php endif; ?>

                <br/>

                <?php if( $customer ): ?>

                    <label for="webdev_form_email">*Email: </label>
                <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address"  id="webdev_form_email" <?php if ( null !== $customer->get_email() ){ echo 'value="', strip_tags( $customer->get_email() ), '"'; } ?>>
                <?php else: ?>

                <label for="webdev_form_email">*Email: </label>
                <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address"  id="webdev_form_email" >

                <?php endif; ?>

                <br/>

                <p><strong>Contact You Via:</strong></p>
                <table name="webdev_form_call" class="call_checkboxes">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" class="mode" name="webdev_form_mode" id="webdev_form_mode_1" value="phone"> Phone </td>
                            <td><input type="checkbox" class="mode" name="webdev_form_mode" id="webdev_form_mode_2" value="email"> Email </td>
                            <td><input type="checkbox" class="mode" name="webdev_form_mode" id="webdev_form_mode_3" value="text"> Text </td>
                        </tr>
                    </tbody>
                </table>

                <label for="webdev_form_number">If Call, Best Phone Number: </label>
                <input class="form-control" type="tel" name="webdev_form_number" placeholder="Phone Number" id="webdev_form_number" <?php if (isset($_POST[ 'webdev_form_number'])){ echo 'value="', strip_tags($_POST[ 'webdev_form_number']), '"'; } ?>>

                <label for="webdev_form_question">Is this Phone Number a Mobile Number? </label>
                <input class="form-control" type="checkbox" name="webdev_form_question" id="webdev_form_question" value="Yes" >            
                <br/>
                <br/>

                <p><strong>Best Time To Call You Back:</strong></p>
                <table name="webdev_form_call" class="call_checkboxes">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" class="calls" name="webdev_form_call" id="webdev_form_call_1" value="10"> 10 AM - 12 PM </td>
                            <td><input type="checkbox" class="calls" name="webdev_form_call" id="webdev_form_call_2" value="12"> 12 PM - 2 PM </td>
                            <td><input type="checkbox" class="calls" name="webdev_form_call" id="webdev_form_call_3" value="2"> 2 PM - 4 PM </td>
                            <td><input type="checkbox" class="calls" name="webdev_form_call" id="webdev_form_call_4" value="4"> 4 PM - 5 PM </td>
                        </tr>
                    </tbody>
                </table>

                <p><strong>What days work best for you:</strong></p>
                <table class="day_checkboxes">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" class="days" name="webdev_form_day" id="webdev_form_day_1" value=" Mon"> Mon</td>
                            <td><input type="checkbox" class="days" name="webdev_form_day" id="webdev_form_day_2" value=" Tue"> Tue</td>
                            <td><input type="checkbox" class="days" name="webdev_form_day" id="webdev_form_day_3" value=" Wed"> Wed</td>
                            <td><input type="checkbox" class="days" name="webdev_form_day" id="webdev_form_day_4" value=" Thu"> Thu</td>
                            <td><input type="checkbox" class="days" name="webdev_form_day" id="webdev_form_day_5" value=" Fri"> Fri</td>
                        </tr>
                    </tbody>
                </table>            
                <p><strong>What time of the day:</strong></p>
                <table class="time_checkboxes">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" class="times" name="webdev_form_time" id="webdev_form_time_1" value="Morning"> Morning</td>
                            <td><input type="checkbox" class="times" name="webdev_form_time" id="webdev_form_time_2" value="Afternoon"> Afternoon</td>
                        </tr>
                    </tbody>
                </table>

                <?php if( $reminder ): ?>

                    <input class="form-control" type="text" name="webdev_form_p_id" style="visibility: hidden" id="webdev_form_p_id" value="<?php echo $reminder->get_id(); ?>">

                <?php endif; ?>

                <p class="form-control" name="webdev_form_response" id="webdev_form_response" style="visibility: hidden"></p>

                <button id="webdev_form_submit" class="btn btn-primary" type="submit" value="submit">Submit</button>

                </form>
        </div>
    <?php
        
    }
}