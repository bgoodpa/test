<?php

use Twilio\Rest\Client;

class Reminder {
    
    private $id;
        
//    private $product;
    
//    private $customer;
    
    public function __construct( $id ) {
        $this->id = $id;
//        $this->customer = get_field( 'customer', $id )->ID;
//        $this->product = get_field( 'product', $id );

    }
    
//    Check Reminders
    
    public function check_if_reminder( $email ){
        
    }
    
    public function can_text(){
        return get_field( 'upcoming_appointment', $this->id )['text'];
    }
    
    public function check_reminder_scheduler_dates( $days_out ){
        error_log('Check Reminder Scheduler Dates!' );
        
        $fields = get_fields('sr_options');
        
        //If we're doing this based on last service date, then do the following...
        if( $fields['show_last_service_date'] == true ){
            
            //Get the service date stored in Reminder
            $service_date = date_create( $this->get_last_service() );
            
            //Get the reminder frequency and see how many days in the future we should check.
            switch( strtolower( $this->get_frequency() ) ){
                case "1 year":
                    $future_date = $service_date->add( new DateInterval('P1Y') );
                    break;
                case "3 months":
                    $future_date = $service_date->add( new DateInterval('P3M') );
                    break;
                case "3 month":
                    $future_date = $service_date->add( new DateInterval('P3M') );
                    break;
                case "6 months":
                    $future_date = $service_date->add( new DateInterval('P6M') );
                    break;
                case "6 month":
                    $future_date = $service_date->add( new DateInterval('P6M') );
                    break;
            }
            
            //Otherwise, we're just doing this based on dates saved in the settings.
        } else {
            
            error_log( 'Count of Scheduler Dates ' . count( $fields['scheduler_dates_list'] ) );
            
            if( count( $fields['scheduler_dates_list'] ) > 0 ){
            
                $upcoming_dates = array();

                $today_date = new DateTime();
                $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                
                $scheduled_dates = $fields['scheduler_dates_list'];

                foreach( $scheduled_dates as $each_date ){
                    $sch_date = $each_date['scheduler_dates'];
                    error_log( 'Scheduler Date is ' . $sch_date );                    
                    $that_date = date_create( DateTime::createFromFormat('F j, Y', $sch_date )->format('Y-m-d') );
                    if( $that_date < $today_date ){
                        $diff = date_diff( $that_date , $today_date )->d;
//                        $upcoming_dates = array( $diff   => $that_date );
                        $upcoming_dates[ $diff ]   = $that_date;
                        error_log( 'Count of upcoming dates ' . count( $upcoming_dates ) );
                    }
                }

                ksort( $upcoming_dates );
                $future_date = date_create( array_shift( $upcoming_dates )->format('Y-m-d') );
                
            } else {
                
                exit();
            }
        }
        
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        
        $diff = date_diff( $today_date , $future_date );
        $diff_days = $diff->d;
//        error_log( 'Difference is ' . $diff->days . ' days.' );
        
        //If the customer has never been messaged and it's the right time, go ahead.
        if( !$this->have_service_messaged_date() && $diff->days <= $days_out ){
            
            error_log( 'Go ahead and message!' );

            $this->send_down_customer( 'scheduler' );             
            $this->set_service_messaged_date();
            
            //If they have been messaged before but it's in the past, then they might be ready to message again.
        } elseif( $this->have_service_messaged_date() ){
            
            error_log( 'Has service messaged date!' );
            
            $messaged_date = $this->get_service_messaged_date();
            //Let's make sure that, at least, messaged date was more than 30 days ago.
            if( date_diff( $messaged_date , $today_date )->d > $days_out & $diff->days <= $days_out  ){
                
                $this->send_down_customer( 'scheduler' );
                $this->set_service_messaged_date();
                
            }
            
        }
    }
    
    public function check_reminder_appointment_date(){
        
        if( !is_null( get_field( 'upcoming_appointment', $this->id ) ) ){
            
            error_log('Reminder Appointment Date is ' . get_field( 'upcoming_appointment', $this->id )['appointment_date'] );
        
//            error_log('Check Reminder Appointment Date!');

            $reminder = new Reminder( $this->id );
            $days_left = $reminder->get_days_until_appt();
            
            error_log( 'Days left are ' . $days_left);
            
//            if( ($days_left == '10') ||
//               ( $days_left == '5' ) ||
//               ( $days_left == '1' ) ){
              
            if( ($days_left == '7') ||
               ( $days_left == '1' ) ){
                error_log( 'One of those days!');
                
                $reminder->send_down_customer( 'appointment' );
                $this->set_appointment_messaged_date();
                
                if( $reminder->can_text() ){
                    
//                    error_log('We can text!');
                    
                    //gets service department phone number details from the database.
                    $part_details = get_option( 'gmg-contact-121-pval' );
                    $phone_number = $part_details['scheduler_phone'];
                    
                    $customer = new Customer( get_field( 'customer', $this->id ) );
                    $site_name = get_bloginfo( 'name' );
                    
                    $message_array = array();
                    
                    array_push( $message_array , 'FROM ' . $site_name . ': ' . $customer->get_name() . ', it is ');
                    array_push( $message_array , $days_left . ' day(s) until your appointment scheduled on ' . $reminder->get_appointment_date() . ' between ' . $reminder->get_appointment_time() . '. ' );
                    array_push( $message_array , 'Please call ' . $phone_number . ' if you need to reschedule.' );
                    
                    $customer->text_customer( implode( $message_array ) );
                    
                }
            } else {
                
                error_log( 'Not one of those days!');
                
                if( $days_left == '0' ){
                    $reminder->set_type_of_reminder('svc');
                    $today_date = new DateTime();
                    $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                    $reminder->set_service_date( $today_date->format('F j, Y') );                    
                }
                
            }          
        } else {
            error_log('No Appointment Date!');
        }
        
    }
    
    public function send_down_customer( $mode ){
        
        $customer = new Customer( get_field( 'customer', $this->id ) );
        
        $cm = new ReminderCM();
        
        switch( $mode ){
            case 'appointment':
                
                $appt_date = get_field( 'upcoming_appointment', $this->id )['appointment_date'];
                
                $reminder = new Reminder( $this->id );

                $info = array(
                    'name'          => $customer->get_name(),
                    'email'         => $customer->get_email(),
                    'project'       => $this->id,
                    'product'       => $this->get_product(),
                    'appointment'   => $appt_date,
                    'technician'    => $reminder->get_appointment_tech(),
                    'time'          => $reminder->get_appointment_time(),
                    'days'          => $reminder->get_days_until_appt(),
                );
                
                $result = $cm->add_appointment_customer( $info );
                break;
                
            case 'scheduler':
                
                $info = array(
                    'name'      => $customer->get_name(),
                    'email'     => $customer->get_email(),
                    'project'   => $this->id,
                    'product'       => $this->get_product(),
                );
                $result = $cm->add_scheduler_customer( $info );
                break;
                
        }

        if($result->was_successful()) {
            error_log( "Subscribed with code " . $result->http_status_code);
            
            switch( $mode ){
                case 'appointment':
                    $this->set_appointment_messaged_date();
                    break;
                case 'scheduler':
                    $this->set_service_messaged_date();                    
            }
                    
        } else {
            error_log( 'Failed with code ' . $result->http_status_code );
        }
    }
    
    
    public function restart_send_down_customer( $mode ){
        
        $customer = new Customer( get_field( 'customer', $this->id ) );
        
        $cm = new CampaignMonitor();
        
        switch( $mode ){
            case 'appointment':
                
                $result = $cm->unsubscribe_subscriber( $list_id , $customer->get_email() );
                
                if($result->was_successful()) {
                    
                    $appt_date = get_field( 'upcoming_appointment', $this->id )['appointment_date'];
                    
                    $reminder = new Reminder( $this->id );
                    
                    $info = array(
                        'name'          => $customer->get_name(),
                        'email'         => $customer->get_email(),
                        'product'       => $this->get_product(),
                        'project'       => $this->id,
                        'appointment'   => $appt_date,
                        'technician'    => $reminder->get_appointment_tech(),
                        'time'          => $reminder->get_appointment_time(),
                        'days'          => $reminder->get_days_until_appt(),
                    );
                    
                    $result = $cm->add_appointment_customer( $info , $list_id );
                    
                }
                break;
            case 'scheduler':
                
                $result = $cm->unsubscribe_subscriber( $list_id , $customer->get_email() );
                
                if($result->was_successful()) {
                
                    $info = array(
                        'name'      => $customer->get_name(),
                        'email'     => $customer->get_email(),
                        'project'   => $this->id,
                        'product'       => $this->get_product(),
                        
                    );
                    $result = $cm->add_scheduler_customer( $info );
                }
                break;
        }

        if($result->was_successful()) {
            error_log( "Subscribed with code " . $result->http_status_code);
            
            switch( $mode ){
                case 'appointment':
                    $this->set_appointment_messaged_date();
                    break;
                case 'scheduler':
                    $this->set_service_messaged_date();                    
            }
                    
        } else {
            error_log( 'Failed with code ' . $result->http_status_code );
        }
    }

    
//    Get Reminder
    
    public function get_id(){        
        return $this->id;
    }
    
    public function get_product(){        
        return get_field( 'product', $this->id );
    }
    
    public function get_customer(){        
        return get_field( 'customer', $this->id );
    }
    
    public function get_whole_reminder( ){
        $whole = array();
        
    }
    
    public function get_type_of_reminder(){
        return get_field( 'type_of_project' , $this->id );
    }
    
    public function get_reminder_id(){
        return get_field( 'project_id' , $this->id );        
    }

    // SERVICE SCHEDULER GET FUNCTIONS

    public function have_service_date(){
        if( NULL != get_field( 'last_serviced', $this->id )['last_serviced_date'] ){
            return false;
        } else { return true; }
    }

    public function have_service_messaged_date(){
        if( empty( get_field( 'last_serviced', $this->id )['scheduler_messaged_date'] ) ){
            return false;
        } else { return true; }
    }
        
    public function get_last_service(){
        $svc_date = get_field( 'last_serviced', $this->id )['last_serviced_date'];
        return DateTime::createFromFormat('F j, Y', $svc_date )->format('Y-m-d');
        
    }

    public function get_service_messaged_date(){        
        $svc_msg_date = get_field( 'last_serviced', $this->id )['scheduler_messaged_date'];
        return DateTime::createFromFormat('F j, Y', $svc_msg_date )->format('m/j/Y');
        
    }
    
    public function get_frequency(){
        return get_field( 'reminder_frequency', $this->id );
    }


    // SERVICE APPOINTMENT GET FUNCTIONS
    
    public function get_appointment_date(){
        if( $this->have_appointment() ){
            $appt_date = get_field( 'upcoming_appointment', $this->id )['appointment_date'];
            return DateTime::createFromFormat('F j, Y', $appt_date )->format('Y-m-d');
        } else {
            return ' ';
        } 
    }
    
    public function get_appointment_time(){
        
        if( $this->have_appointment_time() ){
            return get_field( 'upcoming_appointment', $this->id )['appointment_time'];
        }
    }
    
    public function get_days_until_appt(){
        
        $appt_date = date_create( get_field( 'upcoming_appointment', $this->id )['appointment_date'] );
        $appt_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        $appt_date = $appt_date->add( new DateInterval('P1D') );
        
        $today_date = date_create( );
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        
        $diff = date_diff( $appt_date, $today_date );
//        error_log( 'The days are ' . $diff->days );
        return $diff->days;

//        return diff( $today_date , $appt_date )->d;      
    }

    
    public function have_appointment(){
        if( empty( get_field( 'upcoming_appointment', $this->id )['appointment_date'] ) ){
            return false;
        } else { return true; }
    }
    
    public function have_appointment_time(){
        if( NULL != get_field( 'upcoming_appointment', $this->id )['appointment_time'] ){
            return true;
        } else { return false; }
    }
    
    public function have_appointment_messaged(){
        if( empty( get_field( 'upcoming_appointment', $this->id )['appointment_messaged_date'] ) ){
            return false;
        } else { return true; }
    }
    
    public function get_appointment_tech(){
        $appointment = get_field( 'upcoming_appointment', $this->id );
        if( isset( $appointment['appointment_technician'] ) ){
            return $appointment['appointment_technician'];
        } else {
            return false;
        }
    }
    
    public function have_appointment_address_same(){
        $appointment = get_field( 'upcoming_appointment', $this->id );
        if( isset( $appointment['same_address'] ) ){
            return true;
        } else {
            return false;
        }
    }
    
    public function get_reminder_street_address(){
        return get_field( 'project_state_address', $this->id);
    }
    
    public function get_reminder_town(){
        return get_field( 'project_town', $this->id);
    }
    
    public function get_reminder_state(){
        return get_field( 'project_state', $this->id);
    }
    
    public function get_reminder_zip(){
        return get_field( 'project_zip', $this->id);
    }
    
    public function get_notes(){
        return get_field( 'notes' , $this->id );      
    }
    
    
    
//    Create Reminder
    
    
    public function set_reminder_field( $field, $value ){
        
    }
    
    public function update_title(){
        
        $fields = get_fields('sr_options');        
        if( $fields['need_products'] == true ){
        
            $name = $this->set_reminder_title_with_product();
            
        } else {
            
            $name = $this->set_reminder_title();
            
        }

        $new_post = array(
            'ID'     => $this->id,
            'post_title'  => $name,
            'post_name' => $name
        );

        wp_update_post( $new_post );
        
    }
    
    public function update_reminder( $info_array ){
        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $this->id);
        }
        
        $this->update_title();
    }
    
    public function set_type_of_reminder( $type_array ){    
        update_field( 'type_of_project' , $type_array, $this->id );
    }
    
    public function set_product( $product ){        
        update_field( 'product' , $product , $this->id );
//        $this->product = $product;
    }
    
    public function set_customer( $c_id ){
//        $customer = get_post($c_id);
        update_field( 'customer' , $c_id , $this->id );
//        $this->customer = $c_id;
    }
    
    public function set_reminder_street_address( $street_address ){
        update_field( 'project_state_address', $street_address, $this->id);
        update_field( 'field_5ace0e9f81841', $street_address, $this->id);
    }
    
    public function set_reminder_town( $town ){
        update_field( 'project_town', $town, $this->id);
    }
    
    public function set_reminder_state( $state ){
        update_field( 'project_state', $state, $this->id);
    }
    
    public function set_reminder_zip( $zip ){
        update_field( 'project_zip', $zip, $this->id);
    }
    
    public function set_service_date( $date ){
        update_field( 'last_serviced' ,
                     array( 'last_serviced_date'   => $date ),
                           $this->id );
    }
    
    public function set_frequency( $frequency ){
        if( is_array( $frequency ) ){
            $number = $frequency['length_number'];
            $time = $frequency['length_choice'];
            update_field( 'reminder_frequency', $number . ' ' . $time, $this->id );
        } else {
            update_field( 'reminder_frequency', $frequency, $this->id );
        }
    }
    
    public function set_service_messaged_date(){
        $today_date = new DateTime();
        update_field( 'last_serviced' ,
                     array( 'scheduler_messaged_date'   => $today_date->format('F j, Y g:i a') ) ,
                     $this->id );
    }
    
    public function set_appointment_messaged_date(){        
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        
        update_field( 'upcoming_appointment' ,
                     array( 'appointment_messaged_date'   => $today_date->format('F j, Y') ) ,
                     $this->id );
    }
    
    public function set_appointment_date( $date ){
//        $new_date = DateTime::createFromFormat('F j, Y', $date );
//        $new_date = DateTime::createFromFormat('n/j/Y', $date );
        update_field( 'upcoming_appointment' ,
                     array( 'appointment_date'   => $date ),
                           $this->id ) ;        
    }
    
    public function set_appointment_time( $time ){
        update_field( 'field_5ac39a55e0bb3' ,
                     array( 'appointment_time'   => $time ),
                           $this->id ) ;        
    }
    
    
    public function set_custom_appointment_time( $time ){
        update_field( 'upcoming_appointment' ,
                     array( 'custom_appointment_time'   => $time ),
                           $this->id ) ;        
    }
                     
    
    public function set_appointment_tech( $technician ){        
        update_field( 'upcoming_appointment' ,
                     array( 'appointment_technician'   => $technician ),
                     $this->id );
    }
    
    public function set_text_option(){      
        update_field( 'upcoming_appointment' ,
                     array( 'text'   => 1 ) ,
                     $this->id );        
    }
    
    public function set_reminder_id(){
        $fields = get_fields('sr_options');
        $prefix = $fields['project_prefix'];
        
        update_field( 'project_id' , $prefix . $this->id , $this->id );        
    }
    
    public function set_reminder_title_with_product(){
        $fields = get_fields('sr_options');
        $prefix = $fields['project_prefix'];
        
        $customer = new Customer( $this->get_customer() );
        return $customer->get_name() . " - " . $this->get_product() . ' - ' . $this->get_frequency() . ' (' . $this->get_reminder_id() . ')';
    }
    
    public function set_reminder_title(){
        $fields = get_fields('sr_options');
        $prefix = $fields['project_prefix'];
        
        $customer = new Customer( $this->get_customer() );
        return $customer->get_name() . " - " . $this->get_frequency() . ' (' . $this->get_reminder_id() . ')';
    }
    
    public function set_notes( $notes ){
        update_field( 'notes' , $notes , $this->id );      
    }
    
    
    public function process_appointer( $post ){
    
        $this->set_type_of_reminder( 'appt' );

        //Set Appointment Date
        $appt_date = DateTime::createFromFormat('Y-m-d', $post['webdev_form_appt_date'] ); 
        $this->set_appointment_date( $appt_date->format('F j, Y') );

        //Set Appointment Time
        $this->set_appointment_time( $post['webdev_form_appt_time'] );

        error_log( 'Fields 1');

        //Let's get our saved options first.
        $fields = get_fields('sr_options');
        echo var_dump( $fields );

        error_log( 'Fields 2');

        //If we're showing Appointment Technicians and there is a technician.
        if( $fields['show_technicians'] == true && $post['webdev_form_technician'] != 'select' ){
            error_log( 'Technician is ' . $post['webdev_form_technician']  );        
            $this->set_appointment_technician( $post['webdev_form_technician'] );
        }

        error_log( 'Fields 3');

        //First, if we haven't messaged customer before, let's go ahead and send down the customer.

        if( !$this->have_appointment_messaged() ){
            $this->send_down_customer( 'appointment' );
        }

        error_log( 'Fields 4');

        //Also, if customer wants us to text them...                
        if( !empty( $post['webdev_form_text'] ) ){

            //error_log( 'Let us text the customer!' );
            $rcustomer = new ReminderCustomer( $this->get_customer() );
            $rcustomer->set_mobile( $post['webdev_form_number'] );
            $this->set_text_option();

            //gets service department phone number details from the database.
            $phone_number = $fields['scheduler_phone'];

            $site_name = get_bloginfo( 'name' );

            $message_array = array();
            array_push( $message_array , 'FROM ' . $site_name . ': ' . $customer->get_name() . ', you just scheduled an appointment on ' . DateTime::createFromFormat('Y-m-d', $this->get_appointment_date() )->format('F j, Y') . ' between ' . $this->get_appointment_time() . '. ' );
            array_push( $message_array , 'Please call ' . $phone_number . ' if you need to reschedule.' );

            $rcustomer->text_customer( implode( $message_array ) );
        }
    }
    
    public function process_scheduler( $post ){
                       
        //Let's get our saved options first.
        $fields = get_fields('sr_options');

        //Set the Type of Reminder as Service.
        $this->set_type_of_reminder( 'svc' );

        $error = false;

        //Check if client wants us to use Last Service Date.
        if( $fields['schedule_customers_based_on'] == 'service' ){

            //It was so make sure that Last Service Date was set in the form as well
            if( !empty( $post['webdev_form_svc_date'] ) ){
                
                $bare_date = $post['webdev_form_svc_date'];
                error_log( 'The bare date is ' . $bare_date );
                
                if( strpos( $bare_date , '-') != false ){
                    $date_pieces = explode('-', $bare_date);
                    if( strlen ( $date_pieces[0] ) != 4 ){
                        $new_date = $date_pieces[2] . '-' . $date_pieces[0] . '-' . $date_pieces[1];
                    } else {
                        $new_date = $date_pieces[0] . '-' . $date_pieces[1] . '-' . $date_pieces[2];
                    }
                } elseif( strpos( $bare_date , '/') != false ){
                    $date_pieces = explode('/', $bare_date);
                    if( strlen ( $date_pieces[0] ) != 4 ){
                        $new_date = $date_pieces[2] . '-' . $date_pieces[0] . '-' . $date_pieces[1];
                    } else {
                        $new_date = $date_pieces[0] . '-' . $date_pieces[1] . '-' . $date_pieces[2];
                    }
                }            

                //It was, so let's format it and set it.
                $service_date = DateTime::createFromFormat('Y-m-d', $new_date);
                $this->set_service_date( $service_date->format('F j, Y') );

            } else {

                //There was an error and we must record it.
                $this->set_reminder_error( 'svc' );
                $error = true;
            }
            
            //Check if customer wants to send down customer now.
        } elseif( $fields['schedule_customers_based_on'] == 'now'){
            
            $this->send_down_customer( 'scheduler' );
            $this->set_service_messaged_date();
            
            //Set error for true only so we don't go through stuff below.
            $error = true;
        
        } {

            //Otherwise there must be a set number of dates to check things against.

            //Do nothing.

        }

        if( !$error ){

            //Get Class Reminders
            $reminders = new Reminders();

            //We should also check to see if we need to send down this new service scheduler.  
            $days_out = $fields['scheduler_days'];            
            $this->check_reminder_scheduler_dates( $days_out );

            //Update the Reminder Title with new info.
            $this->update_title();
            $rcustomer = new ReminderCustomer( $this->get_customer() );
            $rcustomer->set_service_reminders( $this->get_id() );

            //Delete the error log if it exists.
            $reminders->delete_error_log( 'svc_add_error_log' );
        }
    }
    
    public function set_reminder_error( $type ){
    
        //Create the error array that we'll use.
        $error_array = array();

        //Get Customer info    
        $customer = new Customer( $this->get_customer() );

        //Get Class Reminders
        $reminders = new Reminders();

        //See which message we're showing.
        switch( $type ){
           case 'svc':
               $message = "didn't have a service date set.";
               break;
           case 'appt':
               $message = "didn't have either the appointment date or time set.";
               break;                           
        }

        $error_array[] = $customer->get_name() . ' ' . $message;
        $reminders->create_error_log( $error_array, 'svc_add_error_log' );

    }

}

class Reminders {
    
        public function __construct() {

    }
    
//    Get Reminders
    
    public function get_customer_service_reminders( $customer_id ){
        
    }
    
    public function get_all_service_reminders(){
        
//        error_log('Get all service reminders!');
        $service = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC'
        );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                array_push( $service, get_the_ID() );
                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $service;
        
    }
    
     public function get_all_service_reminders_ids(){
         
        $service = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC',
//            'meta_query'             => array(
//                array(
//                    'key'     => 'type_of_project',
//                    'value'   => 'appt',
//                    'compare' => '=',
//                ),
//                array(
//                    'key'     => 'type_of_project',
//                    'value'   => 'wmp',
//                    'compare' => '=',
//                ),
//                array(
//                    'key'     => 'type_of_project',
//                    'value'   => 'service',
//                    'compare' => '=',
//                ),                
//            ),
        );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                $proj_id = get_post_meta( $id, 'project_id', true );
                
                
                array_push( $service, 
                           array( $id  => $title  )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $service;
        
    }
    
    public function get_sales_reminders( $customer_id ){
        $sales = array();
        
    }
    
    public function check_if_reminder_exists_with_product( $c_id, $product, $frequency ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
                array(
                    'key'     => 'product',
                    'value'   => $product,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            
            $find = false;
            $proj_array = get_posts( $args );
            
            foreach( $proj_array as $proj ){
                $reminder = new Reminder( $proj->ID );
                if( $reminder->get_frequency() == $frequency ){
                    $find = true;
                }
            }
            return $find;
        } else{ return false; }
        
    }
    
    public function check_if_reminder_exists_with_frequency( $c_id, $frequency ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            
            $find = false;
            $proj_array = get_posts( $args );
            
            foreach( $proj_array as $proj ){
                $reminder = new Reminder( $proj->ID );
                if( $reminder->get_frequency() == $frequency ){
                    $find = true;
                }
            }
            return $find;
        } else{ return false; }
        
    }
    
    public function get_reminder_with_product( $c_id, $product, $frequency ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
                array(
                    'key'     => 'product',
                    'value'   => $product,
                    'compare' => '=',
                ),
            ),
        );
            
        $proj_array = get_posts( $args );

        foreach( $proj_array as $proj ){
            $reminder = new Reminder( $proj->ID );
            if( $reminder->get_frequency() == $frequency ){
                $find = $reminder->get_id();
            }
        }
        
        return $find;
    }
    
    public function get_reminder( $c_id, $frequency ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
            ),
        );
            
        $proj_array = get_posts( $args );

        foreach( $proj_array as $proj ){
            $reminder = new Reminder( $proj->ID );
            if( $reminder->get_frequency() == $frequency ){
                $find = $reminder->get_id();
            }
        }
        
        return $find;
    }
    
    public function get_appt_reminder( $c_id, $product ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
            ),
        );
            
        $proj_array = get_posts( $args );

        foreach( $proj_array as $proj ){
            $reminder = new Reminder( $proj->ID );
            if( $reminder->get_type_of_reminder()['value'] == 'appt' && $reminder->get_product() == $product ){
                $find = $reminder->get_id();
            }
        }
        
        return $find;
    }
    
    public function get_WMP_reminder( $c_id ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'customer',
                    'value'   => $c_id,
                    'compare' => '=',
                ),
            ),
        );
        
        $proj_array = get_posts( $args );
            
        foreach( $proj_array as $proj ){
            $reminder = new Reminder( $proj->ID );
            if( $reminder->get_type_of_reminder()['value'] == 'wmp' ){
                $find = $reminder->get_id();
            }
        }
        
        return $find;
        
//        $id = get_posts( $args )[0]->ID;
////        error_log( 'Reminder ID is ' . $id );
//        return $id;
        
    }
    
    public function get_reminder_by_proj_id( $proj_id ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'project_id',
                    'value'   => $proj_id,
                    'compare' => '=',
                ),
            ),
        );
        
        $id = get_posts( $args )[0]->ID;
//        error_log( 'Reminder ID is ' . $id );
        return $id;
        
    }
    
    public function check_if_reminder_exists_by_proj_id( $proj_id ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'project_id',
                    'value'   => $proj_id,
                    'compare' => '=',
                ),
            ),
        );
        
        $posts = get_posts( $args )[0];
        
        if( count( $posts ) > 0 ){
            return true;
        } else {
            return false;
        }
    }
    
    public function create_new_reminder( $info_array ){
        
        $my_post = array(
            'post_title'        => $info_array['name'], 
            'post_type'         => 'reminders',
            'post_status'       => 'publish'
        );
        
//        error_log( 'Before insert post' );
        
        $new_id = wp_insert_post( $my_post );
        
        $reminder = new Reminder( $new_id );
        $reminder->set_customer( $info_array['customer'] );
        
        if( isset( $info_array['project_street_address'] ) ) $reminder->set_reminder_street_address( $info_array['project_street_address'] );
        if( isset( $info_array['project_town'] ) ) $reminder->set_reminder_town( $info_array['project_town'] );
        if( isset( $info_array['project_state'] ) ) $reminder->set_reminder_state( $info_array['project_state'] );
        if( isset( $info_array['project_zip'] ) ) $reminder->set_reminder_zip( $info_array['project_zip'] );
        
        if( isset( $info_array['product'] ) ) $reminder->set_product( $info_array['product'] );
        
        if( isset( $info_array['reminder_frequency'] ) ) $reminder->set_frequency( $info_array['reminder_frequency'] );        
        $reminder->set_reminder_id();        
        $reminder->update_title();      
        
        return $new_id;
    }
    
    //This is a function to process Reminderss and see if they are new or exist and need to be updated.
    
    public function process_reminders( $reminder_info ){
        
        $fields = get_fields('sr_options');
        
        //First, let's see if we're using product to check for Reminder      
        if( $fields['need_products'] == true ){
            
            //Does this reminder exist?            
            if( !$this->check_if_reminder_exists_with_product( $reminder_info['customer'] ,  $reminder_info['product'] , $reminder_info['reminder_frequency'] ) ){
                
                //No, create a new one.            
                $p_id = $this->create_new_reminder( $reminder_info );
                error_log( 'New Project ID is ' . $p_id );
            
            } else {
            
                //If it does, update it.
                $p_id = $this->get_reminder_with_product( $reminder_info['customer'] ,
                                                         $reminder_info['product'] ,
                                                         $reminder_info['reminder_frequency'] );
                error_log( 'Old Project ID is ' . $p_id );
            }
        }
        
        //No product so we'll just do it another way.
        
        //Check if Reminder exists.        
        if( !$this->check_if_reminder_exists_with_frequency( $reminder_info['customer'] , $reminder_info['reminder_frequency'] ) ){
            
//            If it doesn't, create a new one.            
            $p_id = $this->create_new_reminder( $reminder_info );
            error_log( 'New Project ID is ' . $p_id );         
            
        } else {
            
//            If it does, update it.
            $p_id = $this->get_reminder( $reminder_info['customer'] ,
                                        $reminder_info['reminder_frequency'] );
            error_log( 'Old Project ID is ' . $p_id );
            
        }
        
        return $p_id;        
    }
    
    public function get_last_used(){
        
//        error_log( 'Get last used' );

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reminders' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'        => 1,
            'order'                 => 'DESC',
            'orderby'               => 'date'
        );

        $arr_post = get_posts($args);

        if( $arr_post ){
            $post = $arr_post[0];
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $post->post_date);
//            error_log( 'Date is' . $date->format('M d, Y') );
            return $date->format('M d, Y');
        } else {
            return false;
        }
        
    }
    
    public function create_error_log( $error_array, $log_name ){
        
        //error_log( 'We have errors so lets create the file' );
        array_unshift($error_array, "The service reminders for these customers were not created due to insufficient information:");
        
        error_log( 'Error Array count is ' . count( $error_array ) );
        $myfile = fopen(plugin_dir_path( __FILE__ ) . $log_name . '.txt', "w") or die("Unable to open file!");

        //error_log( 'Opened!' );
        fwrite($myfile, implode( "\r\n", $error_array ) );
        fclose($myfile);
        
    }
    
    public function delete_error_log( $log_name ){
        if( file_exists ( plugin_dir_path( __FILE__ ) . $log_name . '.txt' ) ) {                    
                    unlink( plugin_dir_path( __FILE__ ) . $log_name . '.txt' );
        } else{
            error_log( 'File does not exist' );
            
        }
    }
    
}


class ReminderCustomer extends Customer{
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    public function get_customer_name(){
        return  get_field( 'fname', $this->id ) . ' ' . get_field( 'lname', $this->id );
    }
    
    public function set_service_reminders( $p_id ){
        error_log( 'The ID is ' . $p_id );
        $current_projects = $this->get_service_reminders();
        $found = false;
        if( is_array( $current_projects ) ){
            foreach( $current_projects as $project){
                if( $p_id == $project->ID ){
                    $found = true;               
                }
            }
            if( !$found ){
                array_push( $current_projects , $p_id );
                update_field( 'service_projects', $current_projects, $this->id );
            }
        } else {
            update_field( 'service_projects', array( $p_id ), $this->id );
        }
    }
    
    public function get_service_reminders(){
        return get_field( 'service_projects' , $this->id );
    }
    
    public function text_customer( $message ){
        
        $customer = new Customer( $this->id );
        
        $phone = $customer->get_mobile();
//        error_log( "Text customer $this->id at phone number $phone with this message: $message" );
        
        if( $phone ){
//            error_log( "Text customer $this->id at phone number $phone with this message: $message" );
            
//            $to        = (isset($_POST['numbers']) ) ? $_POST['numbers'] : '';
            $sender_id = '610-569-9958';
//            $message   = (isset($_POST['message']) ) ? $_POST['message'] : '';

            //gets our api details from the database.
            $fields = get_fields('sr_options');
            
            $TWILIO_SID = $fields['twilio_api_sid'];
            $TWILIO_TOKEN = $fields['twilio_api_auth_token'];

            try{
                    $to = explode(',', $phone);

                    $client = new Client($TWILIO_SID, $TWILIO_TOKEN);

                      $response = $client->messages->create(
                            $to ,
                            array(
                              'from' => $sender_id,
                             'body' => $message
                            )
                        );

//                       self::DisplaySuccess();
                
                error_log( 'Success!' );
                
                return true;

                } catch (Exception $e) {

//                    self::DisplayError( $e->getMessage() );

                error_log( 'Failure!' . $e->getMessage() );
                
                return false;
                
                }
        }        
    }
    
    public function email_staff( $email_from, $message_array ) {
        
        $fields = get_fields('sr_options');
        $email_to = $fields['scheduler_email'];
        
        error_log( 'Ready to send Mail' );

        $site_name = get_bloginfo( 'name' );
        $site_email = get_bloginfo( 'admin_email' );

        $subject = "New Scheduler Request from $site_name";

        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type:text/html;charset=UTF-8";
        $headers[] = "From: $site_name <$site_email>";
        $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
        $headers[] = "Reply-To: {$email_from}";
        $headers[] = "Subject: {$subject}";
        $headers[] = "X-Mailer: PHP/".phpversion();

        $answer = wp_mail($email_to, $subject, implode("\n\n" , $message_array), implode("\r\n", $headers));

        error_log( 'Answer is ' . $answer );
        return $answer;        
        
    }    
}

class ReminderCustomers extends Customers{
    
    public function __construct() {
    }
    
    public function process_customers( $cust_info ){
        
        $email = $cust_info['email'];
//        error_log( 'Email is ' . $email );
        
        if( !$this->check_if_customer_exists( $email ) ){
            error_log( 'Customer does not exist!' );
            $c_id = $this->create_new_customer( $cust_info );
            $customer = new ReminderCustomer( $c_id );
            
        } else {
            
            $c_id = $this->get_customer( $email );
            error_log( 'Customer exists with ID ' . $c_id );
            $customer = new Customer( $c_id );
            $customer->update_customer( $cust_info );
            
        }
        
        //Check to see if a phone was sent down.
        if( isset( $cust_info['phone'] ) ){
            
            //If so, fix phone numbers.
            $customer->fix_phones();
        }
        
        return $c_id;
        
    }
    
}

class ReminderCM extends CampaignMonitor{
        
    private $api_key;
    private $client_id;
    
    function __construct() {
        parent::__construct();
        
        $fields = get_fields('overall_settings');
        
        if( is_array( $fields ) AND count( $fields ) != 0) {
            $this->client_id = $fields['cm_client_id'];
            $this->api_key = $fields['cm_client_api_key'];
        }
    }
    
    public function get_clients_list(){
        
        $this->read_client_file();
        
//        error_log( 'API key is ' . $this->api_key );
        
        $wrap = new CS_REST_Clients(
            $this->client_id,
            array( 'api_key' => $this->api_key )
        );
        
        $result = $wrap->get_lists();
//        echo "Result of /api/v3.1/clients/{id}/lists\n<br />";
        if($result->was_successful()) {
            
            $list_array = array();
            $result_array = $result->response;
            foreach( $result_array as $result ){
                if( is_object( $result ) ){
                    array_push( $list_array, json_decode(json_encode($result), True) );
                }
            }
            
//            var_dump( $list_array );
            return $list_array;
        }
    }
    
    
    //      Add Scheduler Customer
    public function add_scheduler_customer( $info ){
        
        $this->read_subscriber_file();
        
        $name = $info['name'];
        $email = $info['email'];
        $product = $info['product'];
        $project = $info['project'];
        
        $list_id = get_field( 'scheduler_list_id' , 'sr_options' );
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
//        $result = $wrap->get( $email );
//        
//        echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
//        if($result->was_successful()) {
//            echo "Got subscriber <pre>";
//            var_dump($result->response);
//        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
//        }
        
        if( $wrap->get( $email )->was_successful() ){
            
//            error_log('Update CM!');
            
            $result = $wrap->update(  $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'Product',
                        'Value' => $product
                    ),
                    array(
                        'Key' => 'ProjectID',
                        'Value' => $project
                    ),                
                ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true
                )
            );
            
        } else {
            
//            error_log('Add CM!');           
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'Product',
                        'Value' => $product
                    ),
                    array(
                        'Key' => 'ProjectID',
                        'Value' => $project
                    ),                
                ),
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;
    }
    
    //      Add Scheduler Customer
    public function add_appointment_customer( $info ){
        
        $this->read_subscriber_file();
        $list_id = get_field( 'appointer_list_id' , 'sr_options' );
        
        $name = $info['name'];
        $email = $info['email'];
        $product = $info['product'];
        $project = $info['project'];
        $appointment = $info['appointment'];
        $time = $info['time'];
        $days = $info['days'];
        $technician = $info['technician'];
        
//        error_log('Name is ' . $name . ', email is ' . $email . ', List ID is ' . $list_id );
//        error_log('Client API Key is ' . $this->api_key );    
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
        $result = $wrap->get( $email );
        
//        echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
//        if($result->was_successful()) {
//            echo "Got subscriber <pre>";
//            var_dump($result->response);
//        } else {
//            echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
//            var_dump($result->response);
//        }
        
        if( $wrap->get( $email )->was_successful() ){
            
//            error_log( 'Subscriber already found' );
            
            $result = $wrap->update( $email ,
                array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'Product',
                        'Value' => $product
                    ),
                    array(
                        'Key' => 'AppointmentDate',
                        'Value' => $appointment
                    ),
                    array(
                        'Key' => 'AppointmentTime',
                        'Value' => $time
                    ),
                    array(
                        'Key' => 'Technician',
                        'Value' => $technician
                    ),
                    array(
                        'Key' => 'DaystoAppointment',
                        'Value' => $days
                    ),
                    array(
                        'Key' => 'ProjectID',
                        'Value' => $project
                    ),
                ),
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
            
        } else {
            
//            error_log('Add CM!');
        
            $result = $wrap->add(
                array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'Product',
                        'Value' => $product
                    ),
                    array(
                        'Key' => 'AppointmentDate',
                        'Value' => $appointment
                    ),
                    array(
                        'Key' => 'AppointmentTime',
                        'Value' => $time
                    ),
                    array(
                        'Key' => 'Technician',
                        'Value' => $technician
                    ),
                    array(
                        'Key' => 'DaystoAppointment',
                        'Value' => $days
                    ),
                    array(
                        'Key' => 'ProjectID',
                        'Value' => $project
                    ),
                ),
                    'Resubscribe' => true,
                    'RestartSubscriptionBasedAutoresponders' => true
                )
            );
        }
        
        return $result;      
    }
    
    //Unsubscribe Subscriber    
    public function unsubscribe_subscriber( $list_id , $email ){
        
        $this->read_subscriber_file();   
        
        $wrap = new CS_REST_Subscribers( $list_id, 
                                        array( 'api_key' => $this->api_key )
                                       );
        
        return $wrap->unsubscribe( $email );
    }
    
}
