jQuery(document).ready(function ($) {
    "use strict";
    
    var formy = $('#gmg-scheduler-form');

    formy.on('submit', function (f) {
        f.preventDefault();

        var form_name = $("#webdev_form_name").val();
        var form_email = $("#webdev_form_email").val();
        var form_phone = $("#webdev_form_phone").val();
        var form_id = $("#webdev_form_p_id").val();

        var form_question = $("#webdev_form_question");
        var question;
        if (form_question.is(':checked ')) {
            question = 'true';
        } else {
            question = 'false';
        }
        
        var selectedcalls = [];
        var calls_selected;

        $(".calls:checked").each(function () {
            selectedcalls.push($(this).val());
        });

        if (selectedcalls) {
            calls_selected = selectedcalls.join(',');
        }
        
        var selectedmode = [];
        var mode_selected;
        
        $(".mode:checked").each(function () {
            selectedmode.push($(this).val());
        });

        if (selectedmode) {
            mode_selected = selectedmode.join(',');
        }

        var selecteddays = [];
        var selected;

        $(".days:checked").each(function () {
            selecteddays.push($(this).val());
        });

        if (selecteddays) {
            selected = selecteddays.join(',');
        }

        var pickedtimes = [];
        var times_selected;

        $(".times:checked").each(function () {
            pickedtimes.push($(this).val());
        });

        if (pickedtimes) {
            times_selected = pickedtimes.join(',');
        }

        
        console.log('Email is ' + form_email);

        $.ajax({
            url: ajax_object.ajaxurl,
            type: 'post',
            data: {
                action: 'scheduler_form_send_down',
                name: form_name,
                email: form_email,
                phone: form_phone,
                question: question,
                days: selected,
                times: times_selected,
                calls: calls_selected,
                mode: mode_selected,
                p_id: form_id
            },
            success: function (response) {
                if (response['foo']) {
                    //                    alert( 'Success' + response['foo']);
                    $('#webdev_form_response').html(response['foo']);
                    $('#webdev_form_response').css("visibility", "visible");
                    window.location.href = "/thank-you/";
                } else {
                    //                    alert( 'Problems' + response['errors']);
                    $('#webdev_form_response').html(response['errors']);
                    $('#webdev_form_response').css("visibility", "visible");
                }

            },
            error: function (errorThrown) {
                alert('Error' + errorThrown);
            }
        });
    });

    var sms_form = $("#gmg-sms-update-form");

    sms_form.on('submit', function (g) {
        g.preventDefault();

        var form_name = $("#webdev_form_name").val();
        var form_email = $("#webdev_form_email").val();
        var form_number = $("#webdev_form_number").val();
        var form_id = $("#webdev_form_p_id").val();

        $.ajax({
            url: ajax_object.ajaxurl,
            type: 'post',
            data: {
                action: 'appointment_sms_update',
                name: form_name,
                email: form_email,
                number: form_number,
                p_id: form_id
            },
            success: function (response) {
                if (response['foo']) {
                    //                    alert( 'Success' + response['foo']);
                    $('#webdev_form_response').html(response['foo']);
                    $('#webdev_form_response').css("visibility", "visible");
                    //                    window.location.href = "/thank-you/";
                } else {
                    //                    alert( 'Problems' + response['errors']);
                    $('#webdev_form_response').html(response['errors']);
                    $('#webdev_form_response').css("visibility", "visible");
                }

            },
            error: function (errorThrown) {
                alert('Error' + errorThrown);
            }
        });
    });
});
