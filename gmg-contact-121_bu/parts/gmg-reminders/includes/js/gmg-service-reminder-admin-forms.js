jQuery(document).ready(function ($) {
    "use strict";
    
    var update_select = $('#webdev_form_reminder_ids');
    update_select.change( function (h) {
        h.preventDefault();
        
        var form_project = $("#webdev_form_reminder_ids option:selected").val();
//        console.log( 'The project is ' + form_project );
        $('#webdev_form_svc_frequency').trigger("reset");
        $('#webdev_form_svc_date').trigger("reset");
        $('#webdev_form_appt_date').trigger("reset");
        $('#webdev_form_appt_time').trigger("reset");
        $('#webdev_form_product').trigger("reset");
        $('#webdev_form_reminder_state').trigger("reset");
        $('#webdev_form_technician').trigger("reset");        
        
//        $("#update_customer_box").css("display","none");
        
        $.ajax({
            url : ajax_object.ajaxurl,
            type : 'post',
            data : {
                action : 'admin_chosen_projects',
                project : form_project
            },
            success:function( response ){
                if(response['foo']){
//                    alert( 'Success' + response['foo']);
                    
//                    $("#project_id").val( response['id'] );                    
                    $("#webdev_form_fname").val( response['fname'] );
                    $("#webdev_form_lname").val( response['lname'] );
                    $("#webdev_form_email").val( response['email'] );
                    $("#webdev_form_number").val( response['phone'] );
                    $("#webdev_form_reminder_address").val( response['street_address'] );
                    $("#webdev_form_reminder_town").val( response['town'] );
//                    console.log( 'The state is ' + response['state'] );
                    $("#webdev_form_reminder_state").val( response['state'] );
                    $("#webdev_form_reminder_zip").val( response['zip'] );
                    $('#webdev_form_product').val( response['product'] );
                    var freq = response['freq'];
//                    console.log( 'The freq value is ' + $("#webdev_form_svc_frequency[value='" + freq + "']").val() );
                    $("#webdev_form_svc_frequency[value='" + freq + "']").prop( "checked" , true );
                    $("#webdev_form_notes").val( response['notes'] );
                    
                    if( response['type'] == 'svc' ){
                        $("#webdev_form_svc_date").val( response['last_service'] );
                        $("#webdev_form_appt_date").val('');
                        $("#webdev_form_appt_time").val('');                      
                        
                    }
                    
                    if( response['type'] == 'appt' ){                        
                        $("#webdev_form_appt_date").val( response['appt_date'] );
                        $("#webdev_form_appt_time").val( response['appt_time'] );
                        $("#webdev_form_svc_date").val('');
                    }
                } else{
                    alert( 'Problems' + response['errors']);
//                    $('#webdev_form_response').html( response['errors'] );
                    $('#webdev_form_response').css("visibility","visible");
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            }
        });       
	});
    
});
