<?php


/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_reminders_import_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_revminders_import_dashboard_widget',         // Widget slug.
        'Import Service Schedule Reminders',         // Title.
        'gmg_revminders_import_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_reminders_import_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_revminders_import_dashboard_widget_function() {
?>
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="import-box">
            
            <?php $index = 1; ?>
            <?php $fields = get_fields('sr_options'); ?>
            
            <p><strong>This is only to import Service Schedule Reminders using a .csv. This platform notifies the customer immeadiatly to schedule thier appointment.</strong></p>

            <p>If you would like to import new customers, please choose which column in your .csv file the information is in:</p>

            <table>
                <tbody>
                    <tr>
                        <td>
                            <label for="SkipHeader">First line is header *</label>
                            <input id="SkipHeader" name="SkipHeader" type="checkbox" value="yes" />
                        </td>
                    </tr>
<tr>
                        <td>
                            <label for="CustFName">First Name *</label><br />
                            <input id="CustFName" name="CustFName" type="number" value="<?php echo $index; ?>" />
                        </td>
                        <?php $index++; ?>    
                        <td>
                            <label for="CustLName">Last Name *</label><br />
                            <input id="CustLName" name="CustLName" type="number" value="<?php echo $index; ?>" />
                        </td>
                        <?php $index++; ?>
                    </tr>
                    <tr>
                        <td>
                            <label for="CustEmail">Email *</label><br />
                            <input id="CustEmail" name="CustEmail" type="number" value="<?php echo $index; ?>" />
                        </td>
                        <?php $index++; ?>
                        <?php if( $fields['need_phone_number'] == true ): ?>                        
                            <td>
                                <label for="CustPhone">Phone *</label><br />
                                <input id="CustPhone" name="CustPhone" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        <?php endif; ?>
                    </tr>
                    <?php if( $fields['need_address'] == true ): ?>
                        <tr>
                            <td>
                                <label for="ProjectStreetAddress">Project Street *</label><br />
                                <input id="ProjectStreetAddress" name="ProjectStreetAddress" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                            <td>
                                <label for="ProjectTown">Project Town *</label><br />
                                <input id="ProjectTown" name="ProjectTown" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        </tr>
                        <tr>
                            <td>
                                <label for="ProjectState">Project State *</label><br />
                                <input id="ProjectState" name="ProjectState" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                            <td>
                                <label for="ProjectZip">Project Zip *</label><br />
                                <input id="ProjectZip" name="ProjectZip" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        </tr>
                    <?php endif; ?>
                    <?php if( $fields['need_products'] == true || count( $fields['service_project_lengths'] ) > 1 ): ?>
                        <tr>
                            <?php if( $fields['need_products'] == true): ?>
                                <td>
                                    <label for="Product">Product</label><br />
                                    <input id="Product" name="Product" type="number" value="<?php echo $index; ?>" />
                                </td>
                                <?php $index++; ?>
                            <?php endif; ?>
                            <?php if( count( $fields['service_project_lengths'] ) > 1 ): ?>
                                <td>
                                    <label for="Frequency">Reminder Frequency</label><br />
                                    <input id="Frequency" name="Frequency" type="number" value="<?php echo $index; ?>" />
                                </td>
                                <?php $index++; ?>
                            <?php endif; ?>
                        </tr>
                    <?php endif; ?>
                </tbody>

            </table>

            <input type="file" name="import_csv">

            <input type="submit" name="import_sr_form" id="import_sr_form" class="button button-primary" value="Import">
            
            <?php if( file_exists ( plugin_dir_path( __FILE__ ) . 'svc_import_error_log.txt' ) ) : ?>
                <a href="<?php echo plugins_url( 'svc_import_error_log.txt', __FILE__ ); ?>" target="_blank">Error Log</a>
            <?php endif; ?>

        </div>

    </form>

<?php
    
}

/****
*
* This is the function to import new projects from the Import Service Reminder Project dashboard widget.
*
****/

add_action( 'admin_init', 'func_gmg_import_all_projects' );
function func_gmg_import_all_projects() {
    
    if( isset( $_POST[ 'import_sr_form' ] ) ) {
        
//        error_log( 'Inside Import Projects Post!' );
        
//        error_log( 'Files are ' . $_FILES['import_csv']['name'] );
        
        if( $_FILES['import_csv'] ) {            
            $filename = explode('.', $_FILES['import_csv']['name']);
//            error_log( 'Filename is ' . $filename[0] . '!' );
            if( $filename[1]=='csv' ){
                $csv_array = array_map('str_getcsv', file( $_FILES['import_csv']['tmp_name'] ));
//                echo var_dump( $csv_array );
//                error_log( 'Array is ' . count( $csv_array ) . ' big!' );
                if( $_POST['SkipHeader'] == 'yes' ){
                    $start = 1;
                } else {
                    $start = 0;
                }
                
                $fname_column = intval($_POST['CustFName']) - 1;
                $lname_column = intval($_POST['CustLName']) - 1;
                $email_column = intval($_POST['CustEmail']) - 1;
                $phone_column = intval($_POST['CustPhone']) - 1;
                $street_address_column = intval($_POST['ProjectStreetAddress']) - 1;
                $town_column = intval($_POST['ProjectTown']) - 1;
                $state_column = intval($_POST['ProjectState']) - 1;
                $zip_column = intval($_POST['ProjectZip']) - 1;
                $frequency_column = intval($_POST['Frequency']) - 1;
//                $LSvc_column = intval($_POST['LServiceDate']) - 1;
                $product_column = intval($_POST['Product']) - 1;
                
                //gets our api details from the database.
                $fields = get_fields('sr_options');
                
                $index = 0;
                
                foreach( $csv_array as $csv_line ){
                    
                    error_log( 'Index at top is ' . $index );
                    
//                    error_log( 'Customer FName is ' . $csv_line[$fname_column] );
                    
//                    echo var_dump( $_POST );
                    
//                    error_log( 'Customer FName is ' . $_POST['CustFName'] . ' index.' );
                    if( $csv_line[$fname_column] != '' && $csv_line[$fname_column] != ' ' && $csv_line[$lname_column] != '' && $csv_line[$lname_column] != ' ' ){
                        
//                        error_log( 'Inside!');
                        
                        if( $index >= $start ){
//                            error_log( 'The index is good!' );
                            $email = ltrim( rtrim( $csv_line[$email_column] ) );
                            
//                            Put together the Customer Info
                            
                            error_log('Customer with name ' . $csv_line[$fname_column] . ' email ' . $email . ' and Frequency ' . $csv_line[$frequency_column]  );

                            $cust_info = array(
                                'fname'          => $csv_line[$fname_column],
                                'lname'          => $csv_line[$lname_column],
                                'email'         => $email,
                                'phone_number'  => $csv_line[$phone_column]
                            );
                            
                            $rcustomers = new ReminderCustomers();
                            
                            //Send down the customer info to either create new customer or update exisiting one. Either way, it returns customer id.
                            
                            $c_id = $rcustomers->process_customers( $cust_info );
                            //error_log( 'Established Customer ID is ' . $c_id );
                            
                            $rcustomer = new ReminderCustomer( $c_id );
                            
                            //Set Project info.
                            $proj_info = array(
                                'name'          => $csv_line[$fname_column] . ' ' . $csv_line[$lname_column],
                                'customer'                  => $c_id,
                                'project_street_address'    => $csv_line[$street_address_column],
                                'project_town'          => $csv_line[$town_column],
                                'project_state'         => $csv_line[$state_column],
                                'project_zip'           => $csv_line[$zip_column],
                                'reminder_frequency'    => $csv_line[$frequency_column],
                                'product'               => $csv_line[$product_column]
                            );
                            
                            $reminders = new Reminders();
                            //Send down the Reminder info to either create new reminder or update exisiting one. Either way, it returns reminder id.
                            
                            $p_id = $reminders->process_reminders( $proj_info );
                            //error_log( 'Established Reminder ID is ' . $p_id );
                            $reminder = new Reminder( $p_id );
                            
                            //You can only import Service Reminder Scheduler Projects...            
                            $reminder->set_type_of_reminder( 'svc' );

                            //Now we're going to set service date as today.
                            $today_date = new DateTime();
                            $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
                            $reminder->set_service_date( $today_date->format('F j, Y') );
                            
                            //This customer is immeadiatly processed.                            
                            $reminder->send_down_customer( 'scheduler' ); 

                            //Update the Reminder Title with new info.
                            $reminder->update_title();
                            $rcustomer->set_service_reminders( $reminder->get_id() );

                            //Delete the error log if it exists.
                            $reminders->delete_error_log( 'svc_import_error_log' );                          
                            
                        }
                    } else {
                        
                        $error_array = array();
                        array_push( $error_array , $csv_line[$fname_column] . ' ' . $csv_line[$lname_column] );
                        
                        $reminders = new Reminders();
                        $reminders->create_error_log( $error_array, 'svc_import_error_log' );
                    }
                    
                    $index++;
//                    error_log( 'Index at bottom is ' . $index );
                }
                
                
            }
        }
    }
}