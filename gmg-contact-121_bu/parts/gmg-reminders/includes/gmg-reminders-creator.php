<?php

//This function was created to autocreate specific pages for the platform if they do not exist

add_action( 'wp_loaded', 'gmg_service_check_if_pages_exist' );
function gmg_service_check_if_pages_exist(){
    
    $fields = get_fields('sr_options');
    
    //Only autocreate if client wants Scheduler and Scheduler Form
    if( $fields['do_scheduler'] == true && $fields['show_scheduler_form'] == true ){
        
        if ( !gmg_service_page_exists_by_slug('scheduler-form') && get_page_by_title( 'Scheduler Form') == NULL ) {

            $args = array(
                'post_type'     => 'page',
                'post_title'    => 'Scheduler Form',
                'post_name'     => 'scheduler-form',
                'post_content'      => '',
                'post_status'        => 'publish'
            );

            wp_insert_post( $args );        

        } else {
    //        error_log('Review Page already exists!' );
        }
    }
    
    //Only autocreate if client wants Appointer
    if( $fields['do_appointer'] == true ){
        
        if ( !gmg_service_page_exists_by_slug('appointment-sms') && get_page_by_title( 'Appointment SMS') == NULL ) {

            $args = array(
                'post_type'     => 'page',
                'post_title'    => 'Appointment SMS',
                'post_name'     => 'appointment-sms',
                'post_content'      => '',
                'post_status'        => 'publish'
            );

            wp_insert_post( $args ); 

        } else {
    //        error_log('Scheduler Page already exists!' );
        }
    }
    
    //Only autocreate if client wants Scheduler Form or Appointer
    if( $fields['show_scheduler_form'] == true || $fields['do_appointer'] == true ){
    
        if ( !gmg_service_page_exists_by_slug('thank-you') && get_page_by_title( 'Thank You') == NULL ) {

            $args = array(
                'post_type'     => 'page',
                'post_title'    => 'Thank You',
                'post_name'     => 'thank-you',
                'post_content'      => "<h2>Thanks for reaching out to us</h2><p>We'll reach out to you soon.</p>",
                'post_status'        => 'publish'
            );

            wp_insert_post( $args ); 

        } else {
    //        error_log('Scheduler Page already exists!' );
        }
        
    }
    

}

function gmg_service_page_exists_by_slug( $post_slug ) {
    $args_posts = array(
        'post_type'      => array ( 'page' ),
        'post_status'    => array ( 'publish' ),
        'name'           => $post_slug,
        'posts_per_page' => 1,
    );
    
    if( get_posts( $args_posts ) ){
        return true;
    } else {
        return false;
    }
}
