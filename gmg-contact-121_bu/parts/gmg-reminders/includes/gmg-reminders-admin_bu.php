<?php

function register_gmg_reminders_menu_page(){
    
//    error_log( 'Inside Boomer menu page ' );
    
    $plugin = new Gmg_Contact_121();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Service Reminders Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Service Reminders Settings',
            'post_id'       => 'sr_options',
            'capability'    => 'manage_options',
	));
    
    add_submenu_page(
        $options_name,                                     // parent slug
        __( 'Update a Reminder', $options_name ),              // page title
        __( 'Update a Reminder', $options_name ),                  // menu title
        'publish_posts',                                               // capability
        $options_name . '-reminders-update',                                             // menu_slug
        'display_gmg_contact_121_reminder_update_page'      // callable function
    );
    
}

add_filter('acf/load_field/key=field_5b880537733db', 'gmg_sr_update_copyright', 10, 3);
function gmg_sr_update_copyright( $field ){
    
//    var_dump( $field );
    $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
    
    return $field;
    
//    return $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
}

add_action( 'admin_enqueue_scripts', 'gmg_reminders_admin_script_loader' );
function gmg_reminders_admin_script_loader(){
    
    $bool = wp_register_script('reminder', plugin_dir_url( __FILE__ ) . 'js/gmg-service-reminder-admin-forms.js', array(
        'jquery'), null, true );
//    error_log( 'Register: ' . $bool );
    wp_enqueue_script('reminder');
    wp_localize_script( 'reminder', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'gmg_reminders_script_loader' );
function gmg_reminders_script_loader(){
    
    $bool = wp_register_script('client', plugin_dir_url( __FILE__ ) . 'js/gmg-service-reminder-forms.js', array(
        'jquery'), null, true );
//    error_log( 'Register: ' . $bool );
    wp_enqueue_script('client');
    wp_localize_script( 'client', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

function display_gmg_contact_121_reminder_update_page(){
//    error_log( 'Update page displayed ' );
    
    $fields = get_fields('sr_options');
    
    ?>

    <form name="gmg_sr_update_form" method="post" action="" id="gmg_sr_update_form">
        <h2>Update a Reminder</h2>
        
        <div class="search-box">
            <!--            <p><strong>Search by Reminder</strong></p>-->

            <?php $reminders = new Reminders(); ?>
            <?php $reminder_numbers = $reminders->get_all_service_reminders_ids(); ?>

            <p>
                <label for="webdev_form_reminder_ids">*Reminders: </label>
                <select name="webdev_form_reminder_ids" id="webdev_form_reminder_ids">
                    
                    <?php $index = 0; ?>

                    <?php foreach($reminder_numbers as $reminder_array ): ?>                    
                        <?php $key = key($reminder_array); ?>
                        <?php if( $index != 0 ): ?>
                            <option value="<?php echo $key ?>"><?php echo $reminder_array[$key]; ?></option>
                            <?php $index++ ?>
                        <?php else: ?>
                            <option value="<?php echo $key ?>" selected="selected"><?php echo $reminder_array[$key]; ?></option>
                        <?php endif ?>
                    
                    <?php endforeach; ?>
                </select>
            </p>

        </div>
        
        <div class="customer-box">

            <p><strong>Customer Information</strong></p>
            <label for="webdev_form_fname">*Name: </label>
            <table>
                <tr>
                    <td>
                        <input class="form-control" type="text" name="webdev_form_fname" placeholder="First" id="webdev_form_fname" required>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="webdev_form_lname" placeholder="Last" id="webdev_form_lname" required>
                    </td>
                </tr>
            </table>
            <p></p>

            <p>
                <label for="webdev_form_email">*Email: </label>
                <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address" id="webdev_form_email" required>
            </p>

            <p>
                <label for="webdev_form_number">*Phone Number: </label>
                <input class="form-control" type="tel" name="webdev_form_number" placeholder="Phone Number" id="webdev_form_number" required>
            </p>
            
            <hr />
            
        </div>

        <div class="project-box">

            <p><strong>Project Information</strong></p>
            <p>
                <label for="webdev_form_svc_frequency">Service Frequency * <br />
                    <?php

                    $field_key = "field_5b912ad4a11bc";
                    $field = get_field_object( $field_key );
//                            echo var_dump( $field );

                    if( $field ) {
                        foreach( $field['choices'] as $k => $v ){
                            if( $field['default_value'][0] != $k ){
                                echo '<td><input type="radio" name="webdev_form_svc_frequency" id="webdev_form_svc_frequency" value="' . $k . '"> ' . $v . ' </td>';
                            } else {
                                echo '<td><input type="radio" name="webdev_form_svc_frequency" id="webdev_form_svc_frequency" value="' . $k . '" checked> ' . $v . ' </td>';                                 
                            }
                        }
                    }
                ?>
                </label>
            </p>
            <p>
                <label for="webdev_form_product">*Product: </label><br />
                <select name="webdev_form_product" id="webdev_form_product" required>
                    
                    <?php
    
                    $products_array = $fields['service_products'];
                    foreach( $products_array as $product ){
                        echo '<option value="' . $product['product_name'] . '">' . $product['product_name'] . '</option>';
                    }
                ?>
                    
                </select>            
            
            </p>
            
                <label for="webdev_form_reminder_address">*Project Address: </label><br />
                <input class="form-control" name="webdev_form_reminder_address" type="text" placeholder="Street Address" id="webdev_form_reminder_address"   required />
                <table>
                    <tr>
                        <td style="width: 40%;">
                            <label for="webdev_form_reminder_town">Town *</label>
                            <input id="webdev_form_reminder_town" name="webdev_form_reminder_town" type="text" placeholder="City"   required />
                        </td>
                        <td style="width: 20%;">
                            <label for="webdev_form_reminder_state">State *</label>
                            <select name="webdev_form_reminder_state" id="webdev_form_reminder_state"   required>

                                <?php

                                $field_key = "field_5b7220a2e974f";
                                $field = get_field_object( $field_key );

                                if( $field ) {
                                    foreach( $field['choices'] as $k => $v ){
                                        echo '<option value="' . $k . '">' . $v . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width: 40%;">
                            <label for="webdev_form_reminder_zip"> Zip *</label>
                            <input id="webdev_form_reminder_zip" name="webdev_form_reminder_zip" type="text" placeholder="Zip"  required />
                        </td>
                    </tr>
                </table>
            
            <hr />

            <?php if( $fields['do_scheduler'] == true ): ?>

                <div class="scheduler-box">
                    <p><strong>Schedule Reminder</strong></p>
                    <p>If you want to set up a reminder to be sent to remind customers to make a service appointment, enter their last service date.</p>
                    <p>
                        <label for="webdev_form_svc_date">Last Service Date: </label>
                        <input class="form-control" type="date" value="05-01-2017" name="webdev_form_svc_date" id="webdev_form_svc_date">
                    </p>

                </div>
            
<!--            <hr />-->

            <?php endif; ?>

            <?php if( $fields['do_appointer'] == true ): ?>
            <hr />
            <div class="appointment-box">

                <p><strong>Appointment Reminder</strong></p>
                <!--                <h3>Appointment</h3>-->
                <p>If you are reminding the customer of an upcoming appointment, enter all the information below.</p>

                <p>
                    <label for="webdev_form_appt_date">Appointment Date: </label>
                    <input class="form-control" type="date" value="05-01-2017" name="webdev_form_appt_date" id="webdev_form_appt_date">
                </p>

                <p>
                    <label for="webdev_form_appt_time">Appointment Time: </label>
                    <select name="webdev_form_appt_time" id="webdev_form_appt_time">

                        <?php
    
                        $times_array = $fields['appointment_time_options'];    
                        foreach( $times_array as $time ){
                            echo '<option value="' . $time['time_option'] . '">' . $time['time_option'] . '</option>';
                        }
                        ?>
                    </select>
                </p>
                <p>
                    <label for="webdev_form_technician">Technician: </label>
                    <select name="webdev_form_technician" id="webdev_form_technician">

                        <?php
    
                        $techs_array = $fields['technicians'];    
                        foreach( $techs_array as $tech ){
                            echo '<option value="' . $tech['tech_name'] . '">' . $tech['tech_name'] . '</option>';
                        }
                        ?>
                    </select>
                </p>
                <p>
                    <label for="webdev_form_text">Send a text message?
                        <input id="webdev_form_text" type="checkbox" name="webdev_form_text" value="yes" />
                    </label>
                </p>

            </div>

        <?php endif; ?>

        
        
        <p>
            <label for="webdev_form_notes">Reminder Notes:</label><br />
            <textarea class="form-control" name="webdev_form_notes" rows="6" cols="50" placeholder="Leave any notes about this reminder" id="webdev_form_notes"></textarea>
        </p>
        
        </div>
        

        <input type="hidden" name="gmg_update_reminder" value="gmg_update_reminder" />
        <p>
            <input class="button-primary" type="submit" name="gmg_update_reminder_button" value="Update">
            <input class="button-primary red" type="submit" name="gmg_delete_reminder_button" value="Delete">
        </p>
    </form>

<?php
    
}

add_filter('acf/load_field/key=field_5b8017eabdd8a', 'gmg_sr_scheduler_lists_load', 10, 3);
function gmg_sr_scheduler_lists_load( $field ){
    
    $rm = new ReminderCM();
    $list_array = $rm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}

add_filter('acf/load_field/key=field_5b801802bdd8b', 'gmg_sr_appointer_lists_load', 10, 3);
function gmg_sr_appointer_lists_load( $field ){
    
    $bm = new ReminderCM();
    $list_array = $bm->get_clients_list();
    if( is_array( $list_array ) AND count( $list_array ) != 0) {
        foreach( $list_array as $list ) {
            
            $field['choices'][ $list['ListID'] ] = $list['Name'];
        }
        
        return $field;
        
    } else { return $field; }    

}

add_action('wp_ajax_admin_chosen_projects', 'admin_chosen_projects');
add_action('wp_ajax_nopriv_admin_chosen_projects', 'admin_chosen_projects');
function admin_chosen_projects(){
    
//    error_log( 'Chosen Products!' );
    
    $reminder_id = (isset($_POST['project']) ) ? $_POST['project'] : '';
    
    $reminder = new Reminder( $reminder_id );
    $customer = new Customer ( $reminder->get_customer() );
    $return_array = array( 'foo' => 'Success!');
    
    $return_array['id'] = $reminder->get_id();
    $return_array['fname'] = $customer->get_fname();
    $return_array['lname'] = $customer->get_lname();
    $return_array['email'] = $customer->get_email();
    $return_array['phone'] = $customer->get_phone();
    $return_array['street_address'] = $reminder->get_reminder_street_address();
    $return_array['town'] = $reminder->get_reminder_town();
    $return_array['state'] = strtolower( $reminder->get_reminder_state()['value'] );
    $return_array['zip'] = $reminder->get_reminder_zip();
    $return_array['freq'] = $reminder->get_frequency()['value'];
    $return_array['type'] = $reminder->get_type_of_reminder()['value'];
    $return_array['notes'] = $reminder->get_notes();
    $return_array['product'] = $reminder->get_product();
    
//    error_log( 'Type of project: ' . $reminder->get_type_of_reminder() );
    
    if( $reminder->get_type_of_reminder()['value'] == 'svc' ){
//        error_log( 'SVC!' );
        $return_array['last_service'] = $reminder->get_last_service();
        
    } elseif( $reminder->get_type_of_reminder()['value'] == 'appt' ){
//        error_log( 'APPT!' );
        $return_array['appt_date'] = $reminder->get_appointment_date();
        $return_array['appt_time'] = $reminder->get_appointment_time();
    }
    
//    $return = array ( 'foo' => 'Success!' );
    echo wp_send_json( $return_array );
    exit();
    
}

add_action( 'init', 'func_gmg_service_reminder_update' );
function func_gmg_service_reminder_update() {
    
//    error_log( 'Update Projects?' );
    
	if( isset( $_POST['gmg_update_reminder'] ) ) {
        
        if( isset( $_POST['gmg_delete_reminder_button'] ) ){
            
            $p_id = $_POST['webdev_form_reminder_ids'];
            wp_trash_post( $p_id );
            
        } else {
            
            $fields = get_fields('sr_options');
        
//            error_log( 'Inside Project Update!' );

            $p_id = $_POST['webdev_form_reminder_ids'];       
            $fname = $_POST['webdev_form_fname'];
            $lname = $_POST['webdev_form_lname'];
            $email = $_POST['webdev_form_email'];
            $phone = $_POST['webdev_form_number'];
            $reminder_street = $_POST['webdev_form_reminder_address'];
            $reminder_town = $_POST['webdev_form_reminder_town'];
            $reminder_state = $_POST['webdev_form_reminder_state'];
            $reminder_zip = $_POST['webdev_form_reminder_zip'];

//            error_log( 'Name is ' . $fname );

            $reminder = new Reminder( $p_id );
            $customer = new Customer( $reminder->get_customer() );

            if( $customer->get_fname() != $_POST['webdev_form_fname']  ){
                $customer->set_fname( $_POST['webdev_form_fname'] );
            }

            if( $customer->get_lname() != $_POST['webdev_form_lname']  ){
                $customer->set_lname( $_POST['webdev_form_lname'] );
            }

            if( $customer->get_email() != $_POST['webdev_form_email']  ){
                $customer->set_email( $_POST['webdev_form_email'] );
            }

            if( $customer->get_phone() != $_POST['webdev_form_number']  ){
                $customer->set_phone( $_POST['webdev_form_number'] );
            }

            if( $reminder->get_reminder_street_address() != $_POST['webdev_form_reminder_address']  ){
                $reminder->set_reminder_street_address( $_POST['webdev_form_reminder_address'] );
            }
            
            if( $reminder->get_reminder_town() != $_POST['webdev_form_reminder_town']  ){
                $reminder->set_reminder_town( $_POST['webdev_form_reminder_town'] );
            }
            
            if( $reminder->get_reminder_state() != $_POST['webdev_form_reminder_state']  ){
                $reminder->set_reminder_state( $_POST['webdev_form_reminder_state'] );
            }
            
            if( $reminder->get_reminder_zip() != $_POST['webdev_form_reminder_zip']  ){
                $reminder->set_reminder_zip( $_POST['webdev_form_reminder_zip'] );
            }
            
            if( $reminder->get_frequency()['value'] != $_POST['webdev_form_svc_frequency']  ){
                $reminder->set_frequency( $_POST['webdev_form_svc_frequency'] );
            }
            
            if( $reminder->get_appointment_tech() != $_POST['webdev_form_technician']  ){
                $reminder->set_appointment_tech( $_POST['webdev_form_technician'] );
            }           
            
            if( !empty( $_POST['webdev_form_notes'] ) ){
//                error_log( 'Notes are ' . $_POST['webdev_form_notes'] );
                if( $reminder->get_notes() != $_POST['webdev_form_notes']  ){
                    $reminder->set_notes( $_POST['webdev_form_notes'] );
                }
            }
        
            if( !empty( $_POST['webdev_form_svc_date'] ) ){
                
                //If it is, set the project type and all pertinant information.
                $reminder->set_type_of_reminder( 'svc' );
                $service_date = DateTime::createFromFormat('Y-m-d', $_POST['webdev_form_svc_date'] );
                $reminder->set_service_date( $service_date->format('F j, Y') );

                //We should also check to see if we need to send down this new service scheduler.  
                $days_out = $fields['scheduler_days'];            
                $reminder->check_reminder_scheduler_dates( $days_out );

            } else {

                if( !empty( $_POST['webdev_form_appt_date']) ){
                    
                    //If there is an Appointment Date, then it must be an appointment is.
                    
                    $prev_type = $reminder->get_type_of_reminder()['value'];
                    
                    //Set as Appointment.
                    $reminder->set_type_of_reminder( 'appt' );
                        
                    //Also, set Appointment Time
                    if( !empty( $_POST['webdev_form_appt_time']) ){
                        $reminder->set_appointment_time( $_POST['webdev_form_appt_time'] );
                    }
                        
                    //Also, set Appointment Date
                    if( $reminder->get_appointment_date() != $_POST['webdev_form_appt_date']  ){
                        $appt_date = DateTime::createFromFormat('Y-m-d', $_POST['webdev_form_appt_date'] );
                        $reminder->set_appointment_date( $appt_date->format('F j, Y') );
                        
                        //Check to see if it used to be a Scheduler
                        if( $prev_type == 'svc' ){
                            
                            //Restart the Campaign Monitor Journey again but this time for appointment.
                            $reminder->restart_send_down_customer( 'appointment' );
                        } else {
                            
                            //Just send down the customer.                            
                            $reminder->send_down_customer( 'appointment' );
                        }
                    }
                }
                    
                if( !empty( $_POST['webdev_form_text'] ) ){

    //            error_log( 'Let us text the customer!' );

                    $customer->set_mobile( $customer->get_phone() );
                    $reminder->set_text_option();

                    //gets service department phone number details from the database.
                    $phone_number = $fields['scheduler_phone'];
                    $site_name = get_bloginfo( 'name' );

                    $message_array = array();
                    array_push( $message_array , 'FROM ' . $site_name . ': ' . $customer->get_name() . ', you just scheduled an appointment on ' . DateTime::createFromFormat('Y-m-d', $reminder->get_appointment_date() )->format('F j, Y') . ' between ' . $reminder->get_appointment_time() . '. ' );
                    array_push( $message_array , 'Please call ' . $phone_number . ' if you need to reschedule.' );
                    
                    $rcustomer = new ReminderCustomer( $reminder->get_customer() );
                    $rcustomer->text_customer( implode( $message_array ) );
                }
            }
            
            $reminder->update_title();
        }
    }
}

add_action('wp_ajax_scheduler_form_send_down', 'scheduler_form_send_down');
add_action('wp_ajax_nopriv_scheduler_form_send_down', 'scheduler_form_send_down');
function scheduler_form_send_down(){
    
    error_log( 'Inside Scheduler Form Receiver!' );

    $name = (isset($_POST['name']) ) ? $_POST['name'] : '';
    $email = (isset($_POST['email']) ) ? $_POST['email'] : '';
    $phone = (isset($_POST['name']) ) ? $_POST['name'] : '';
    
    $message_array = array();
    
    array_push(
        $message_array ,
        "<p>FROM: " . $name . '</p>' .
        "<p>EMAIL: " . $email . '</p>' .
        "<p>PHONE NUMBER: " . $phone . '</p>'
    );
    
    if( isset($_POST['question'] ) ){
        array_push( $message_array , "<p>MOBILE?: " . $_POST['question'] . "</p>" );
    }
    
    if( isset( $_POST['p_id'] ) ){
        
        $reminder = new Reminder( $_POST['p_id'] );
        
        array_push(
            $message_array ,
            "<p>PROJECT TYPE: " . $reminder->get_type_of_reminder()['label'] . "</p>" );
        
    }
    
    if( isset($_POST['calls'] ) ){
        $calls = $_POST['calls'];
        
        switch( $calls ){
            case '10':
                array_push( $message_array , "<p>Best Time to Call: 10 AM - 12 PM</p>" );
                break;
            case '12':
                array_push( $message_array , "<p>Best Time to Call: 12 PM - 2 PM</p>" );
                break;
            case '2':
                array_push( $message_array , "<p>Best Time to Call: 2 PM - 4 PM</p>" );
                break;
            case '4':
                array_push( $message_array , "<p>Best Time to Call: 4 PM - 5 PM</p>" );
                break;                
        }
    }
    
    if( isset($_POST['days'] ) ){
        array_push( $message_array , "<p>Good Days to Schedule: " . $_POST['days'] . "</p>" );
    }  

    if( isset($_POST['times'] ) ){
        array_push( $message_array , "<p>Good Times to Schedule: " . $_POST['times'] . "</p>" );
    }

    $success_message = '<p>Your message has been sent</p>';
    
    $reminder = new Reminder( $_POST['p_id'] );
    $rcustomer = new ReminderCustomer( $reminder->get_customer() );
    $answer = $rcustomer->email_staff( $email , $message_array );
    
    if ($answer){
        $return = array ( 'foo' => $success_message );
        error_log( 'Success' );
    } else {
        $return = array ( 'foo' => 'There was an error sending the message.' );
        error_log( 'Failure' );
    }
    
    echo wp_send_json( $return );
    exit();
    
    
}

add_action('wp_ajax_appointment_sms_update', 'appointment_sms_update');
add_action('wp_ajax_nopriv_appointment_sms_update', 'appointment_sms_update');
function appointment_sms_update(){
    
    error_log( 'Inside Appointment SMS Udpate!' );

    $name = (isset($_POST['name']) ) ? $_POST['name'] : '';
    $email = (isset($_POST['email']) ) ? $_POST['email'] : '';
    $phone = (isset($_POST['number']) ) ? $_POST['number'] : '';
    $p_id = (isset($_POST['p_id']) ) ? $_POST['p_id'] : '';
    
    $success_message = '<p>Your info has been been updated. You will receive a notification soon.</p>';
    
    if( $p_id ){
        $reminder = new Reminder( $p_id );
        $customer = new Customer( $reminder->get_customer() );
        $customer->set_mobile( $phone );
        $reminder->set_text_option();
        
        $fields = get_fields('sr_options');
        
        //gets service department phone number details from the database
        $phone_number = $fields['scheduler_phone'];
        $site_name = get_bloginfo( 'name' );
        
        $message_array = array();
        array_push( $message_array , 'FROM ' . $site_name . ': Appointment scheduled for ');
        array_push( $message_array , $customer->get_name() );
        array_push( $message_array , ' scheduled on ' . DateTime::createFromFormat('Y-m-d', $reminder->get_appointment_date() )->format('F j, Y') . ' between ' . $reminder->get_appointment_time() . '. ' );
        array_push( $message_array , 'Any questions or you need to reschedule, please call ' . $phone_number . '.' );
        
        $rcustomer = new ReminderCustomer( $reminder->get_customer() );
        $success = $rcustomer->text_customer( implode( $message_array ) );
        
        if ( $success ){
            $return = array ( 'foo' => $success_message );
            
            $recipient = $fields['reviewer_email'];
            
            $email_message = $customer->get_name() . 'just filled out the text form for their appointment scheduled on ' . DateTime::createFromFormat('Y-m-d', $reminder->get_appointment_date() )->format('F j, Y') . '. Thanks.';
            
            $site_name = get_bloginfo( 'name' );
            $site_email = get_bloginfo( 'admin_email' );
            $customer_email = $customer->get_email();

            $subject = "Appointment Texting Form Filled Out on $site_name";
            $headers   = array();
            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-type: text/plain; charset=iso-8859-1";
            $headers[] = "From: $site_name <$site_email>";
            $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
            $headers[] = "Reply-To: { $customer_email }";
            $headers[] = "Subject: { $subject }";
            $headers[] = "X-Mailer: PHP/".phpversion();

            wp_mail( $recipient, $subject, $email_message , implode("\r\n", $headers) );
            
        } else {
            $return = array ( 'foo' => 'There was an error sending the message.' );
            error_log( 'Failure' );
        }
        
    } else {
        
        $return = array ( 'foo' => 'There was an error sending the message.' );
        error_log( 'Failure' );
        
    }
    
    echo wp_send_json( $return );
    exit();    
    
}