<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_reminders_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_revminders_dashboard_widget',         // Widget slug.
        'Add Service Reminder',         // Title.
        'gmg_revminders_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_reminders_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_revminders_dashboard_widget_function() {
    
    $fields = get_fields('sr_options');
    
    ?>

    <form name="gmg_sr_add_form" method="post" action="" id="gmg_sr_update_form">
        <div class="customer-box">

            <p><strong>Customer Information</strong></p>
            <label for="webdev_form_fname">*Name: </label>
            <table>
                <tr>
                    <td>
                        <input class="form-control" type="text" name="webdev_form_fname" placeholder="First" id="webdev_form_fname">
                    </td>
                    <td>
                        <input class="form-control" type="text" name="webdev_form_lname" placeholder="Last" id="webdev_form_lname">
                    </td>
                </tr>
            </table>

            <p>
                <label for="webdev_form_email">*Email: </label>
                <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address" id="webdev_form_email">
            </p>

            <p>
                <label for="webdev_form_number">*Phone Number: </label>
                <input class="form-control" type="tel" name="webdev_form_number" placeholder="Phone Number" id="webdev_form_number">
            </p>
            
            <hr />
            
        </div>

        <div class="project-box">

            <p><strong>Project Information</strong></p>
            <p>
                <label for="webdev_form_svc_frequency">Service Frequency * <br />
                    <?php

                    $field_key = "field_5b912ad4a11bc";
                    $field = get_field_object( $field_key );
//                            echo var_dump( $field );

                    if( $field ) {
                        foreach( $field['choices'] as $k => $v ){
                            if( $field['default_value'][0] != $k ){
                                echo '<td><input type="radio" name="webdev_form_svc_frequency" id="webdev_form_svc_frequency" value="' . $k . '">' . $v . '</td>';
                            } else {
                                echo '<td><input type="radio" name="webdev_form_svc_frequency" id="webdev_form_svc_frequency" value="' . $k . '" checked>' . $v . '</td>';                                 
                            }
                        }
                    }
                ?>
                </label>
            </p>
                <label for="webdev_form_reminder_address">*Project Address: </label><br />
                <input class="form-control" name="webdev_form_reminder_address" type="text" placeholder="Street Address" id="webdev_form_reminder_address" style="width: 100%;" />
                <table>
                    <tr>
                        <td style="width: 40%;">
                            <label for="webdev_form_reminder_town">Town *</label>
                            <input id="webdev_form_reminder_town" name="webdev_form_reminder_town" type="text" placeholder="City" style="width: 100%;" />
                        </td>
                        <td style="width: 20%;">
                            <label for="webdev_form_reminder_state">State *</label>
                            <select name="webdev_form_reminder_state" id="webdev_form_reminder_state" style="width: 100%;">

                                <?php

                                $field_key = "field_5b7220a2e974f";
                                $field = get_field_object( $field_key );

                                if( $field ) {
                                    foreach( $field['choices'] as $k => $v ){
                                        echo '<option value="' . $k . '">' . $v . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width: 40%;">
                            <label for="webdev_form_reminder_zip"> Zip *</label>
                            <input id="webdev_form_reminder_zip" name="webdev_form_reminder_zip" type="text" placeholder="Zip" style="width: 100%;" />
                        </td>
                    </tr>
                </table>
            
            <hr />

            <?php if( $fields['do_scheduler'] == true ): ?>

                <div class="scheduler-box">
                    <p><strong>Schedule Reminder</strong></p>
                    <p>If you want to set up a reminder to be sent to remind customers to make a service appointment, enter their last service date.</p>
                    <p>
                        <label for="webdev_form_svc_date">Last Service Date: </label>
                        <input class="form-control" type="date" value="05-01-2017" name="webdev_form_svc_date" id="webdev_form_svc_date">
                    </p>

                </div>
            
<!--            <hr />-->

            <?php endif; ?>

            <?php if( $fields['do_appointer'] == true ): ?>
            <hr />
            <div class="appointment-box">

                <p><strong>Appointment Reminder</strong></p>
                <!--                <h3>Appointment</h3>-->
                <p>If you are reminding the customer of an upcoming appointment, enter all the information below.</p>

                <p>
                    <label for="webdev_form_appt_date">Appointment Date: </label>
                    <input class="form-control" type="date" value="05-01-2017" name="webdev_form_appt_date" id="webdev_form_appt_date">
                </p>

                <p>
                    <label for="webdev_form_appt_time">Appointment Time: </label>
                    <select name="webdev_form_appt_time" id="webdev_form_appt_time">

                        <?php
                        $field_key = "field_5ad5114952941";
                        $field = get_field_object( $field_key );

                        if( $field ) {
                            foreach( $field['choices'] as $k => $v ){
                                echo '<option value="' . $k . '">' . $v . '</option>';
                            }
                        }
                        ?>
                    </select>
                </p>
                <p>
                    <label for="webdev_form_text">Send a text message?
                        <input id="webdev_form_text" type="checkbox" name="webdev_form_text" value="yes" />
                    </label>
                </p>

            </div>

        <?php endif; ?>

        </div>

            <input type="hidden" name="gmg_add_sr_reminder" value="gmg_add_sr_reminder" />
            <p>
                <input class="button-primary" type="submit" name="gmg_add_reminder" value="Submit">
            </p>
        </form>

    <?php
}

add_action( 'init', 'func_gmg_service_reminder_add' );
function func_gmg_service_reminder_add() {
    
    error_log( 'Called GMG Add SR!' );
    
	if( isset( $_POST['gmg_add_sr_reminder'] ) ) {
        
        if( !empty( $_POST['webdev_form_fname'] ) || !empty( $_POST['webdev_form_lname'] ) ){
            
        } else {
        
        //Get fields for whatever options we need to check.
        $fields = get_fields('sr_options');
        
        if( $fields['do_scheduler'] == true && !empty( $_POST['webdev_form_svc_date'] ) || $fields['do_appointer'] == true && !empty( $_POST['webdev_form_appt_date'] ) && !empty( $_POST['webdev_form_appt_time'] ) ) {        
        
            error_log( 'Inside!' );

            //Let's pass down the info into an array.

            $cust_info = array(
                    'fname'         => $_POST['webdev_form_fname'],
                    'lname'         => $_POST['webdev_form_lname'],
                    'email'         => $_POST['webdev_form_email'],
                    'phone_number'  => $_POST['webdev_form_number'],
                    'address'       => $_POST['webdev_form_customer_address']
                );

            $rcustomers = new ReminderCustomers();

            //Send down the customer info to either create new customer or update exisiting one. Either way, it returns customer id.

            $c_id = $rcustomers->process_customers( $cust_info );
            error_log( 'Established Customer ID is ' . $c_id );

            $proj_info = array(
                'name'                  => $_POST['webdev_form_fname'] . ' ' . $_POST['webdev_form_lname'],
                'customer'              => $c_id,
                'product'               => $_POST['webdev_form_product'],
                'reminder_frequency'    => $_POST['webdev_form_svc_frequency']
            );

            $reminders = new Reminders();

            //Send down the Reminder info to either create new reminder or update exisiting one. Either way, it returns reminder id.

            $p_id = $reminders->process_reminders( $proj_info );
            error_log( 'Established Reminder ID is ' . $p_id );

            $reminder = new Reminder( $p_id );

    //        Let's go ahead and see if this is a Service Reminder Scheduler.        
            if( !empty( $_POST['webdev_form_svc_date'] ) ){

    //            If it is, set the project type and all pertinant information.            
                $reminder->set_type_of_reminder( 'svc' );

                //Now let's set the last service date.            
    //            error_log( 'SVC Date is ' . $_POST['webdev_form_svc_date'] );
                $service_date = DateTime::createFromFormat('Y-m-d', $_POST['webdev_form_svc_date'] );     
    //            error_log( 'The to-be-saved Service date is ' . $service_date->format('F j, Y') );
                $reminder->set_service_date( $service_date->format('F j, Y') );

    //            We should also check to see if we need to send down this new service scheduler.           
                $days_out = $fields['sr_scheduler_days'];            
                $reminder->check_reminder_scheduler_dates( $days_out );

            } else {

        //        It must be a Service Reminder Appointment Project.

                    $reminder->set_type_of_reminder( 'appt' );

                    //Set Appointment Date
                    $appt_date = DateTime::createFromFormat('Y-m-d', $_POST['webdev_form_appt_date'] ); 
                    $reminder->set_appointment_date( $appt_date->format('F j, Y') );

                    //Set Appointment Time
                    $reminder->set_appointment_time( $_POST['webdev_form_appt_time'] );            

                    //If there is an Appointment Tech, set it.
                    if( !empty( $_POST['webdev_appt_technician']) ){
                        $reminder->set_appointment_technician( $_POST['webdev_appt_technician'] );
                    }

                    //First, if we haven't messaged customer before, let's go ahead and send down the customer.

                    if( !$reminder->have_appointment_messaged() ){
                        $reminder->send_down_customer( 'appointment' );
                    }

    //                Also, if customer wants us to text them...

                    if( !empty( $_POST['webdev_form_text'] ) ){

            //            error_log( 'Let us text the customer!' );

                        $customer->set_mobile( $customer->get_phone() );
                        $reminder->set_text_option();

                        //gets service department phone number details from the database.
                        $phone_number = $fields['sr_scheduler_phone'];

                        $site_name = get_bloginfo( 'name' );

                        $message_array = array();
                        array_push( $message_array , 'FROM ' . $site_name . ': ' . $customer->get_name() . ', you just scheduled an appointment on ' . DateTime::createFromFormat('Y-m-d', $reminder->get_appointment_date() )->format('F j, Y') . ' between ' . $reminder->get_appointment_time()['label'] . '. ' );
                        array_push( $message_array , 'Please call ' . $phone_number . ' if you need to reschedule.' );

                        $customer->text_customer( implode( $message_array ) );
                    }
            }
            
            $reminder->update_title();
            $customer->set_service_reminders( $reminder->get_id() );

            wp_redirect( $_SERVER['HTTP_REFERER'] );
            exit();
            
        } else {
            error_log( 'Error in the form!' );
        }
    }
}