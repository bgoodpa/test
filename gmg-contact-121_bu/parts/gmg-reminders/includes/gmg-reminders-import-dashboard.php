<?php


/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_reminders_import_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_revminders_import_dashboard_widget',         // Widget slug.
        'Import Service Schedule Reminders',         // Title.
        'gmg_revminders_import_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_reminders_import_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_revminders_import_dashboard_widget_function() {
?>
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="import-box">
            
            <?php $index = 1; ?>
            <?php $fields = get_fields('sr_options'); ?>
            
            <p><strong>This is only to import Service Schedule Reminders using a .csv.</strong></p>
            <?php if( $fields['schedule_customers_based_on'] == 'now' ): ?>            
                <p><strong>This platform notifies the customer immeadiatly to schedule thier appointment.</strong></p>
            <?php elseif( $fields['schedule_customers_based_on'] == 'service' ): ?>
                <p><strong>This platform will put the customer into a queue until it's time to notify them to schedule thier appointment.</strong></p>
                <p><strong>When entering the date, please put it in the mm/dd/yyyy or mm-dd-yyyy format.</strong></p>
            <?php else: ?>
                <p><strong>This platform will put the customer into a queue until it's time to notify them to schedule thier appointment.</strong></p>
            <?php endif; ?>

            <p>If you would like to import new customers, please choose which column in your .csv file the information is in:</p>

            <table>
                <tbody>
                    <tr>
                        <td>
                            <label for="SkipHeader">First line is header *</label>
                            <input id="SkipHeader" name="SkipHeader" type="checkbox" value="yes" />
                        </td>
                    </tr>
<tr>
                        <td>
                            <label for="CustFName">First Name *</label><br />
                            <input id="CustFName" name="CustFName" type="number" value="<?php echo $index; ?>" />
                        </td>
                        <?php $index++; ?>    
                        <td>
                            <label for="CustLName">Last Name *</label><br />
                            <input id="CustLName" name="CustLName" type="number" value="<?php echo $index; ?>" />
                        </td>
                        <?php $index++; ?>
                    </tr>
                    <tr>
                        <td>
                            <label for="CustEmail">Email *</label><br />
                            <input id="CustEmail" name="CustEmail" type="number" value="<?php echo $index; ?>" />
                        </td>
                        <?php $index++; ?>
                        <?php if( $fields['need_phone_number'] == true ): ?>                        
                            <td>
                                <label for="CustPhone">Phone *</label><br />
                                <input id="CustPhone" name="CustPhone" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        <?php endif; ?>
                    </tr>
                    <?php if( $fields['need_address'] == true ): ?>
                        <tr>
                            <td>
                                <label for="ProjectStreetAddress">Project Street *</label><br />
                                <input id="ProjectStreetAddress" name="ProjectStreetAddress" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                            <td>
                                <label for="ProjectTown">Project Town *</label><br />
                                <input id="ProjectTown" name="ProjectTown" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        </tr>
                        <tr>
                            <td>
                                <label for="ProjectState">Project State *</label><br />
                                <input id="ProjectState" name="ProjectState" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                            <td>
                                <label for="ProjectZip">Project Zip *</label><br />
                                <input id="ProjectZip" name="ProjectZip" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        </tr>
                    <?php endif; ?>
                    <?php if( $fields['need_products'] == true || count( $fields['service_project_lengths'] ) > 1 ): ?>
                        <tr>
                            <?php if( $fields['need_products'] == true): ?>
                                <td>
                                    <label for="Product">Product</label><br />
                                    <input id="Product" name="Product" type="number" value="<?php echo $index; ?>" />
                                </td>
                                <?php $index++; ?>
                            <?php endif; ?>
                            <?php if( count( $fields['service_project_lengths'] ) > 1 ): ?>
                                <td>
                                    <label for="Frequency">Reminder Frequency</label><br />
                                    <input id="Frequency" name="Frequency" type="number" value="<?php echo $index; ?>" />
                                </td>
                                <?php $index++; ?>
                            <?php endif; ?>
                        </tr>
                    <?php endif; ?>
                    <?php if( $fields['schedule_customers_based_on'] == 'service' ): ?>
                        <tr>
                            <td>
                                <label for="LServiceDate">Last Service Date</label><br />
                                <input id="LServiceDate" name="LServiceDate" type="number" value="<?php echo $index; ?>" />
                            </td>
                            <?php $index++; ?>
                        </tr>
                    <?php endif; ?>
                </tbody>

            </table>

            <input type="file" name="import_csv">

            <input type="submit" name="import_sr_form" id="import_sr_form" class="button button-primary" value="Import">
            
            <?php if( file_exists ( plugin_dir_path( __FILE__ ) . 'svc_import_error_log.txt' ) ) : ?>
                <a href="<?php echo plugins_url( 'svc_import_error_log.txt', __FILE__ ); ?>" target="_blank">Error Log</a>
            <?php endif; ?>

        </div>

    </form>

<?php
    
}

/****
*
* This is the function to import new projects from the Import Service Reminder Project dashboard widget.
*
****/

add_action( 'admin_init', 'func_gmg_import_all_projects' );
function func_gmg_import_all_projects() {
    
    //If the Import Form button was pressed.
    if( isset( $_POST[ 'import_sr_form' ] ) ) {        
                
        //error_log( 'Files are ' . $_FILES['import_csv']['name'] );
        
        //If there was an import file.
        if( $_FILES['import_csv'] ) {
            
            //Get the filename.
            $filename = explode('.', $_FILES['import_csv']['name']);
            //error_log( 'Filename is ' . $filename[0] . '!' );
            
            //Make sure it's a csv.
            if( $filename[1]=='csv' ){
                
                //Let's go ahead and map all the date to an array.
                $csv_array = array_map('str_getcsv', file( $_FILES['import_csv']['tmp_name'] ));
                
                //echo var_dump( $csv_array );
                //error_log( 'Array is ' . count( $csv_array ) . ' big!' );
                
                //Check to see if the user marked whether the first line was a header...
                if( isset( $_POST['SkipHeader']) ) {
                     if( $_POST['SkipHeader'] == 'yes' ){
                        $start = 1;
                    } else { $start = 0; }
                } else {
                    $start = 0;
                }
                
                $fname_column = intval($_POST['CustFName']) - 1;
                $lname_column = intval($_POST['CustLName']) - 1;
                $email_column = intval($_POST['CustEmail']) - 1;
                $phone_column = intval($_POST['CustPhone']) - 1;
                $street_address_column = intval($_POST['ProjectStreetAddress']) - 1;
                $town_column = intval($_POST['ProjectTown']) - 1;
                $state_column = intval($_POST['ProjectState']) - 1;
                $zip_column = intval($_POST['ProjectZip']) - 1;
                $frequency_column = intval($_POST['Frequency']) - 1;
                $product_column = intval($_POST['Product']) - 1;
                $LSvc_column = intval($_POST['LServiceDate']) - 1;
        
                //Let's get our saved options.        
                $fields = get_fields('sr_options');
                
                $index = 0;
                
                foreach( $csv_array as $csv_line ){
                    
                    if( $index >= $start ){
            
                        $cust_info = array(
                            'fname'         => $csv_line[$fname_column],
                            'lname'         => $csv_line[$lname_column],
                            'email'         => $csv_line[$email_column]
                        );

                        //Set Project Name for later.        
                        $proj_info['name'] = $cust_info['fname'] . ' ' . $cust_info['lname'];

                        //Do we need to capture phone number?
                        if( $fields['need_phone_number'] == true ){
                            //If so, grab it.
                            $cust_info[ 'phone_number' ] = $csv_line[$phone_column];
                        }

                        //User the Reminder Customers Class
                        $rcustomers = new ReminderCustomers();

                        //Send down the customer info to either create new customer or update exisiting one. Either way, it returns customer id.

                        $c_id = $rcustomers->process_customers( $cust_info );
                        //error_log( 'Established Customer ID is ' . $c_id );

                        //Create a new ReminderCustomer Class
                        $rcustomer = new ReminderCustomer( $c_id );

                        //Let's set customer right off the bat.
                        $proj_info['customer'] = $c_id;       

                        //Do we need to capture an address?
                        if( $fields['need_address'] == true ){

                            //If so, capture all that info.
                            $proj_info['project_street_address'] =  $csv_line[$street_address_column];
                            $proj_info['project_town'] = $csv_line[$town_column];
                            $proj_info['project_state'] = $csv_line[$state_column];
                            $proj_info['project_zip'] = $csv_line[$zip_column];
                        }

                        //Do we need to capture chosen product?
                        if( $fields['need_products'] == true ){

                            //If so, do it!
                            $proj_info['product'] = $csv_line[$product_column];

                        }

                        //Lastly, are there any Reminder Frequencies to grab?
                        if( count( $fields['service_project_lengths'] ) > 1 ){

                            //If so, do it!
                            $proj_info['reminder_frequency'] = $csv_line[$frequency_column];

                        } else {

                            //if not, just set the one.
                            $proj_info['reminder_frequency'] = $fields['service_project_lengths'][0];
                        }

                        //With all of that, let's create a Reminder and then we can add the rest.
                        $reminders = new Reminders();

                        //Send down the Reminder info to either create new reminder or update exisiting one. Either way, it returns reminder id.
                        $p_id = $reminders->process_reminders( $proj_info );
                        //error_log( 'Established Reminder ID is ' . $p_id );

                        //Now we're ready to capture the rest.
                        $reminder = new Reminder( $p_id );

                        //Let's also set the Error Array just in case we need to record errors.
                        $error_array = array();

                        //Since the Importer only creates Service Reminder Schedulers, we can only do one thing...

                        //Set the Type of Reminder as Service.

                        if( $fields['schedule_customers_based_on'] == 'service' ){

                            //If so, do it!
                            $proj_info['webdev_form_svc_date'] = $csv_line[$LSvc_column];

                        }

                        $reminder->process_scheduler( $proj_info );

                        }
                    
                    $index++;
                }
            }
        }
    }
}