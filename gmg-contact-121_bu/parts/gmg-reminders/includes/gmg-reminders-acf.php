<?php

add_filter('acf/save_post' , 'gmg_sr_project_meta_save', 10, 1 );
function gmg_sr_project_meta_save( $post_id ) {
    
    if( get_post_type( $post_id ) == 'sr_projects'){
    
//        error_log('Inside Project Meta Save!');

        //Check it's not an auto save routine
         if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
              return;

        //Perform permission checks! For example:
        if ( !current_user_can('edit_post', $post_id) ) 
              return;

        // Check if it's not a revision.
        if ( wp_is_post_revision( $post_id ) )
            return;

    //    error_log( 'Save Meta for Project ' . $post_id );
        $fields = get_fields( $post_id );
        
        $options_name = 'gmg-contact-121-pval';
        $options = get_option( $options_name );

        //If calling wp_update_post, unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'gmg_sr_project_meta_save');
        
        $project = new Project( $post_id );
        
//        if( get_field( 'customer', $post_id ) ){
//            $customer = get_field( 'customer', $post_id );
//            $project->update_title();            
//        } else {
//            $project->update_title( get_field( 'type_of_service', $post_id ) . ' Service' );
//        }
        
        $project->update_title(); 
        
        // re-hook this function
        add_action('save_post', 'gmg_sr_project_meta_save');
        
        $project_id = $options['sr_prefix'] . $post_id;
        
        $project->set_project_id( $project_id );
        $c_id = $project->get_customer();
//        error_log( 'Customer ID is ' . $c_id );
        $customer = new Customer( $c_id );
        
//        $customer->set_projects( $project_id , $project->set_project_title() );
        
//        if( $project->have_appointment() ) {
//            
//            error_log( 'Have appointment!' );
//            
//            if ( !$project->have_appointment_messaged() ){
//                
//                error_log( 'Does NOT have appointment messaged date!' );
//                error_log( 'Let us send down appointment!' );
//                
//                $project->send_down_customer( 'appointment' );
//                $project->set_appointment_messaged_date();
//            }
//            
//        }
    }
    
}
