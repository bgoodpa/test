<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_reminders_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_revminders_dashboard_widget',         // Widget slug.
        'Add Service Reminder',         // Title.
        'gmg_revminders_dashboard_widget_function' // Display function.   
        );	
}
add_action( 'wp_dashboard_setup', 'gmg_reminders_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_revminders_dashboard_widget_function() {
    
    $fields = get_fields('sr_options');
    
    ?>

    <form name="gmg_sr_add_form" method="post" action="" id="gmg_sr_add_form">
        <div class="customer-box">

            <p><strong>Customer Information</strong></p>
            <label for="webdev_form_fname">*Name: </label>
            <table>
                <tr>
                    <td>
                        <input class="form-control" type="text" name="webdev_form_fname" placeholder="First" id="webdev_form_fname" required>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="webdev_form_lname" placeholder="Last" id="webdev_form_lname" required>
                    </td>
                </tr>
            </table>
            <p></p>

            <p>
                <label for="webdev_form_email">*Email: </label>
                <input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address" id="webdev_form_email" required>
            </p>
            
            <?php if( $fields['need_phone_number'] == true ): ?>

            <p>
                <label for="webdev_form_number">*Phone Number: </label>
                <input class="form-control" type="tel" name="webdev_form_number" placeholder="Phone Number" id="webdev_form_number" required>
            </p>
            
            <?php endif; ?>
            
            <hr />
            
        </div>

        <div class="project-box">
            
            <?php if( count( $fields['service_project_lengths'] ) > 1 || $fields['need_products'] == true || $fields['need_address'] == true  ): ?>

                <p><strong>Project Information</strong></p>

                <?php if( count( $fields['service_project_lengths'] ) > 1 ): ?>
                <p>
                    <label for="webdev_form_svc_frequency">Service Frequency * <br />
                        <?php

    //                    $field_key = "field_5b912ad4a11bc";
    //                    $field = get_field_object( $field_key );
    //                            echo var_dump( $field );

                        $lengths = $fields['service_project_lengths'];

                        if( $lengths ) {
                            foreach( $lengths as $length ){
                                echo '<td><input type="radio" name="webdev_form_svc_frequency" id="webdev_form_svc_frequency" value="' . $length['length_number'] . ' ' . $length['length_choice'] . '"> ' . $length['length_number'] . ' ' . $length['length_choice'] . ' </td>';
                            }
                        }
                    ?>
                    </label>
                </p>
                <?php endif; ?>

                <?php if( $fields['need_products'] == true ): ?>
                <p>
                    <label for="webdev_form_product">*Product: </label><br />
                    <select name="webdev_form_product" id="webdev_form_product" style="width: 100%;"  required>

                        <?php

                        $products_array = $fields['service_products'];
                        foreach( $products_array as $product ){
                            echo '<option value="' . $product['product_name'] . '">' . $product['product_name'] . '</option>';
                        }
                    ?>

                    </select>            

                </p>
                <?php endif; ?>

                <?php if( $fields['need_address'] == true ): ?>

                    <label for="webdev_form_reminder_address">*Project Address: </label><br />
                    <input class="form-control" name="webdev_form_reminder_address" type="text" placeholder="Street Address" id="webdev_form_reminder_address" style="width: 100%;"  required />
                    <table>
                        <tr>
                            <td style="width: 40%;">
                                <label for="webdev_form_reminder_town">Town *</label>
                                <input id="webdev_form_reminder_town" name="webdev_form_reminder_town" type="text" placeholder="City" style="width: 100%;"  required />
                            </td>
                            <td style="width: 20%;">
                                <label for="webdev_form_reminder_state">State *</label>
                                <select name="webdev_form_reminder_state" id="webdev_form_reminder_state" style="width: 100%;"  required>

                                    <?php

                                    $field_key = "field_5b7220a2e974f";
                                    $field = get_field_object( $field_key );

                                    if( $field ) {
                                        foreach( $field['choices'] as $k => $v ){
                                            echo '<option value="' . $k . '">' . $v . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td style="width: 40%;">
                                <label for="webdev_form_reminder_zip"> Zip *</label>
                                <input id="webdev_form_reminder_zip" name="webdev_form_reminder_zip" type="text" placeholder="Zip" style="width: 100%;" required />
                            </td>
                        </tr>
                    </table>

                <?php endif; ?>

                <hr />
            
            <?php endif; ?>

            <?php if( $fields['do_scheduler'] == true && $fields['show_last_service_date'] == true ): ?>

                <div class="scheduler-box">
                    <p><strong>Schedule Reminder</strong></p>
                    <p>If you want to set up a reminder to be sent to remind customers to make a service appointment, enter their last service date.</p>
                    <p>
                        <label for="webdev_form_svc_date">Last Service Date: </label>
                        <input class="form-control" type="date" value="05-01-2017" name="webdev_form_svc_date" id="webdev_form_svc_date">
                    </p>

                </div>
            
<!--            <hr />-->

            <?php endif; ?>

            <?php if( $fields['do_appointer'] == true ): ?>
            <hr />
            <div class="appointment-box">

                <p><strong>Appointment Reminder</strong></p>
                <!--                <h3>Appointment</h3>-->
                <p>If you are reminding the customer of an upcoming appointment, enter all the information below.</p>

                <p>
                    <label for="webdev_form_appt_date">Appointment Date: </label>
                    <input class="form-control" type="date" value="05-01-2017" name="webdev_form_appt_date" id="webdev_form_appt_date">
                </p>

                <p>
                    <label for="webdev_form_appt_time">Appointment Time: </label>
                    <select name="webdev_form_appt_time" id="webdev_form_appt_time">

                        <?php
    
                        $index = 0;
    
                        $times_array = $fields['appointment_time_options'];    
                        foreach( $times_array as $time ){
                            if( $index == 0 ){
                                echo '<option value="select" selected="yes">[Select]</option>';
                                echo '<option value="' . $time['time_option'] . '">' . $time['time_option'] . '</option>';
                            } else {
                                echo '<option value="' . $time['time_option'] . '">' . $time['time_option'] . '</option>';
                            }
                            $index++;
                        }
                        ?>
                    </select>
                </p>
                <?php if( $fields['show_technicians'] == true ): ?>
                    <?php if( count( $fields['technicians'] ) > 0 ): ?>
                        <p>
                            <label for="webdev_form_technician">Technician: </label>
                            <select name="webdev_form_technician" id="webdev_form_technician">

                                <?php
    
                                $index = 0;

                                $techs_array = $fields['technicians'];    
                                foreach( $techs_array as $tech ){
                                    if( $index == 0 ){
                                        echo '<option value="select" selected="yes">[Select]</option>';
                                        echo '<option value="' . $tech['tech_name'] . '">' . $tech['tech_name'] . '</option>';
                                    } else {
                                        echo '<option value="' . $tech['tech_name'] . '">' . $tech['tech_name'] . '</option>';
                                    }
                                    $index++;
                                }
                                ?>
                            </select>
                        </p>
                    <?php endif; ?>
                <?php endif; ?>
                <p>
                    <label for="webdev_form_text">Send a text message?
                        <input id="webdev_form_text" type="checkbox" name="webdev_form_text" value="yes" />
                    </label>
                </p>

            </div>

        <?php endif; ?>

        </div>
        
        <input type="hidden" name="gmg_add_sr_reminder" value="gmg_add_sr_reminder" />
        <p>
            <input class="button-primary" type="submit" name="gmg_add_reminder" value="Submit">
        </p>
        
        <?php $reminders = new Reminders(); ?>
        <?php if( $reminders->get_last_used() != false ): ?>
        <p>Last used:
            <?php echo $reminders->get_last_used() ?>
        </p>
        <?php endif; ?>
        
        <?php if( file_exists ( plugin_dir_path( __FILE__ ) . 'svc_add_error_log.txt' ) ) : ?>
            <a href="<?php echo plugins_url( 'svc_add_error_log.txt', __FILE__ ); ?>" target="_blank">Error Log</a>
        <?php endif; ?>
        
        </form>

    <?php
}

add_action( 'init', 'func_gmg_service_reminder_add' );
function func_gmg_service_reminder_add() {
    
	if( isset( $_POST['gmg_add_sr_reminder'] ) ) {
        
        //The Add Dashboard Submit button was pressed.
        
        //Let's get our saved options first.        
        $fields = get_fields('sr_options');
            
        $cust_info = array(
            'fname'         => $_POST['webdev_form_fname'],
            'lname'         => $_POST['webdev_form_lname'],
            'email'         => $_POST['webdev_form_email']
        );
        
        $proj_info['name'] = $cust_info['fname'] . ' ' . $cust_info['lname'];
        
        //Do we need to capture phone number?
        if( $fields['need_phone_number'] == true ){
            
            //If so, grab it.
            $cust_info[ 'phone_number' ] = $_POST['webdev_form_number'];
        }
        
        $rcustomers = new ReminderCustomers();

        //Send down the customer info to either create new customer or update exisiting one. Either way, it returns customer id.

        $c_id = $rcustomers->process_customers( $cust_info );
//        error_log( 'Established Customer ID is ' . $c_id );
        
        $rcustomer = new ReminderCustomer( $c_id );
        
        //Let's set customer right off the bat.
        $proj_info['customer'] = $c_id;       
        
        //Do we need to capture an address?
        if( $fields['need_address'] == true ){
            
            //If so, capture all that info.
            $proj_info['project_street_address'] =  $_POST['webdev_form_reminder_address'];
            $proj_info['project_town'] = $_POST['webdev_form_reminder_town'];
            $proj_info['project_state'] = $_POST['webdev_form_reminder_state'];
            $proj_info['project_zip'] = $_POST['webdev_form_reminder_zip'];
            
        }

        //Do we need to capture chosen product?
        if( $fields['need_products'] == true ){
            
            //If so, do it!
            $proj_info['product'] = $_POST['webdev_form_product'];
            
        }
        
        //Lastly, are there any Reminder Frequencies to grab?
        if( count( $fields['service_project_lengths'] ) > 1 ){
            
            //If so, do it!
            $proj_info['reminder_frequency'] = $_POST['webdev_form_svc_frequency'];
            
        } else {
            
            //if not, just set the one.
            $proj_info['reminder_frequency'] = $fields['service_project_lengths'][0];
        }
        
        //With all of that, let's create a Reminder and then we can add the rest.
        $reminders = new Reminders();
        
        //Send down the Reminder info to either create new reminder or update exisiting one. Either way, it returns reminder id.
        $p_id = $reminders->process_reminders( $proj_info );
        //error_log( 'Established Reminder ID is ' . $p_id );
        
        //Now we're ready to capture the rest.
        $reminder = new Reminder( $p_id );
        
        //Let's also set the Error Array just in case we need to record errors.
        $error_array = array();
        
        //First we need to know right off the bat is if this is going to be a strictly Scheduler Service Reminder, Appointment SR, or could be both.        
        
        //First, is Do Scheduler chosen but not Do Appointer?
        if( $fields['do_scheduler'] == true && $fields['do_appointer'] == false ){
            
            //Pass down to the function that handles processing Scheduler.            
            process_scheduler( $reminder, $_POST );            
            
            //Next, is Do Appointer chosen but not Do Scheduler?
        } elseif( $fields['do_scheduler'] == false && $fields['do_appointer'] == true ){
                        
            //Pass down to the function that handles processing Appointer.            
            process_appointer( $reminder, $_POST );            
            
        } else{
            
            //Both must be activated and we'll just figure things out.
            
            //It might be an Appointer...
            
            if( $fields['do_appointer'] == true && !empty( $_POST['webdev_form_appt_date'] ) && $_POST['webdev_form_appt_time'] != 'select' ){
                
                //It is an Appointer and has all the neccessary parts.
               
               process_appointer( $reminder, $_POST );          
                       
                
            //It might be an Appointer but there were errors.
                
            } elseif( $fields['do_appointer'] == true && !empty( $_POST['webdev_form_appt_date'] ) && $_POST['webdev_form_appt_time'] == 'select' ) {
                
                //There might have been an error so let's record it.
                set_reminder_error( $cust_info, 'appt' );
                
            } else{
                
                //Or it might be a Scheduler
                
                if( $fields['do_scheduler'] == true ) {
                    
                    //Pass down to the function that handles processing Scheduler.
                    process_scheduler( $reminder, $_POST );
                
                }
            }
        }
    }
}

           
function process_appointer( $reminder , $post ){
    
    $reminder->set_type_of_reminder( 'appt' );

    //Set Appointment Date
    $appt_date = DateTime::createFromFormat('Y-m-d', $post['webdev_form_appt_date'] ); 
    $reminder->set_appointment_date( $appt_date->format('F j, Y') );

    //Set Appointment Time
    $reminder->set_appointment_time( $post['webdev_form_appt_time'] );
    
    error_log( 'Fields 1');
    
    //Let's get our saved options first.
    $fields = get_fields('sr_options');
    echo var_dump( $fields );
    
    error_log( 'Fields 2');

    //If we're showing Appointment Technicians and there is a technician.
    if( $fields['show_technicians'] == true && $post['webdev_form_technician'] != 'select' ){
        error_log( 'Technician is ' . $post['webdev_form_technician']  );        
        $reminder->set_appointment_technician( $post['webdev_form_technician'] );
    }
    
    error_log( 'Fields 3');

    //First, if we haven't messaged customer before, let's go ahead and send down the customer.

    if( !$reminder->have_appointment_messaged() ){
        $reminder->send_down_customer( 'appointment' );
    }
    
    error_log( 'Fields 4');

    //Also, if customer wants us to text them...                
    if( !empty( $post['webdev_form_text'] ) ){

        //error_log( 'Let us text the customer!' );
        $rcustomer = new ReminderCustomer( $reminder->get_customer() );
        $rcustomer->set_mobile( $post['webdev_form_number'] );
        $reminder->set_text_option();

        //gets service department phone number details from the database.
        $phone_number = $fields['scheduler_phone'];

        $site_name = get_bloginfo( 'name' );

        $message_array = array();
        array_push( $message_array , 'FROM ' . $site_name . ': ' . $customer->get_name() . ', you just scheduled an appointment on ' . DateTime::createFromFormat('Y-m-d', $reminder->get_appointment_date() )->format('F j, Y') . ' between ' . $reminder->get_appointment_time() . '. ' );
        array_push( $message_array , 'Please call ' . $phone_number . ' if you need to reschedule.' );

        $rcustomer->text_customer( implode( $message_array ) );
    }
}
                   
function process_scheduler( $reminder, $post ){
                       
    //Let's get our saved options first.
    $fields = get_fields('sr_options');

    //Set the Type of Reminder as Service.
    $reminder->set_type_of_reminder( 'svc' );
    
    $error = false;

    //Check if Show Last Service Date was set in settings.
    if( $fields['show_last_service_date'] == true ){

        //It was so make sure that Last Service Date was set in the form as well
        if( !empty( $post['webdev_form_svc_date'] ) ){

            //It was, so let's format it and set it.
            $service_date = DateTime::createFromFormat('Y-m-d', $post['webdev_form_svc_date'] );
            $reminder->set_service_date( $service_date->format('F j, Y') );

        } else {
            
            //There was an error and we must record it.
            set_reminder_error( $reminder, 'svc' );
            $error = true;
        }

    } else {

        //There must be a set of dates already set in the settings and we'll use them.

        //Do nothing.

    }

    if( !$error ){
        
        //Get Class Reminders
        $reminders = new Reminders();

        //We should also check to see if we need to send down this new service scheduler.  
        $days_out = $fields['scheduler_days'];            
        $reminder->check_reminder_scheduler_dates( $days_out );

        //Update the Reminder Title with new info.
        $reminder->update_title();
        $rcustomer = new ReminderCustomer( $reminder->get_customer() );
        $rcustomer->set_service_reminders( $reminder->get_id() );

        //Delete the error log if it exists.
        $reminders->delete_error_log( 'svc_add_error_log' );
    }
}

function set_reminder_error( $reminder, $type ){
    
    //Create the error array that we'll use.
    $error_array = array();
    
    //Get Customer info    
    $customer = new Customer( $reminder->get_customer() );
    
    //Get Class Reminders
    $reminders = new Reminders();

    //See which message we're showing.
    switch( $type ){
       case 'svc':
           $message = "didn't have a service date set.";
           break;
       case 'appt':
           $message = "didn't have either the appointment date or time set.";
           break;                           
    }

    $error_array[] = $customer->get_name() . ' ' . $message;
    $reminders->create_error_log( $error_array, 'svc_add_error_log' );

}