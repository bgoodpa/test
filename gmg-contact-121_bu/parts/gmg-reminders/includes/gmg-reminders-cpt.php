<?php

    //Add Custom Post Type For projects
    function reminders_init() {
        $proj_labels = array(
            'name'                  => _x( 'Service Reminders', 'Post Type General Name', 'wellness_pro' ),
            'singular_name'         => _x( 'Service Reminder', 'Post Type Singular Name', 'wellness_pro' ),
            'menu_name'             => __( 'Service Reminders', 'wellness_pro' ),
            'name_admin_bar'        => __( 'Service Reminders', 'wellness_pro' ),
            'archives'              => __( 'Service Reminder Archives', 'wellness_pro' ),
            'attributes'            => __( 'Service Reminder Attributes', 'wellness_pro' ),
            'parent_Project_colon'     => __( 'Parent Service Reminder:', 'wellness_pro' ),
            'all_Projects'             => __( 'All Service Reminders', 'wellness_pro' ),
            'add_new_Project'          => __( 'Add New Service Reminder', 'wellness_pro' ),
            'add_new'               => __( 'Add New', 'wellness_pro' ),
            'new_Project'              => __( 'New Service Reminder', 'wellness_pro' ),
            'edit_Project'             => __( 'Edit Service Reminder', 'wellness_pro' ),
            'update_Project'           => __( 'Update Service Reminder', 'wellness_pro' ),
            'view_Project'             => __( 'View Service Reminder', 'wellness_pro' ),
            'view_Projects'            => __( 'View Service Reminders', 'wellness_pro' ),
            'search_Projects'          => __( 'Search Service Reminder', 'wellness_pro' ),
            'not_found'             => __( 'Not found', 'wellness_pro' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'wellness_pro' ),
            'featured_image'        => __( 'Featured Image', 'wellness_pro' ),
            'set_featured_image'    => __( 'Set featured image', 'wellness_pro' ),
            'remove_featured_image' => __( 'Remove featured image', 'wellness_pro' ),
            'use_featured_image'    => __( 'Use as featured image', 'wellness_pro' ),
            'insert_into_Project'      => __( 'Insert into Service Reminder', 'wellness_pro' ),
            'uploaded_to_this_Project' => __( 'Uploaded to this Service Reminder', 'wellness_pro' ),
            'Projects_list'            => __( 'Service Reminders list', 'wellness_pro' ),
            'Projects_list_navigation' => __( 'Service Reminders list navigation', 'wellness_pro' ),
            'filter_Projects_list'     => __( 'Filter Service Reminders list', 'wellness_pro' ),
        );

        $proj_args = array(
            'label'                 => __( 'Service Reminder', 'wellness_pro' ),
            'description'           => __( 'Custom Post Type for Service Reminders', 'wellness_pro' ),
            'labels'                => $proj_labels,
            'supports'              => array( 'title' ),
            'taxonomies'            => array( 'category', 'post_tag' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
//            'show_in_menu'          => 'gmg-contact-121',
//            'menu_position'         => 2,
//            'menu_icon'             => 'dashicons-index-card',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => false,
            'has_archive'           => false,		
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'capability_type'       => 'page',
        );
        
        $current_user = wp_get_current_user();
        if ( user_can( $current_user, 'administrator' ) ) {
            $proj_args['show_in_menu'] = 'gmg-contact-121';
            $proj_args['menu_position'] = 6;        
    }
        
        register_post_type( 'reminders', $proj_args );
    }
add_action( 'init', 'reminders_init' );


// add order column to admin table list of posts
function gmg_reminders_add_new_post_column($cpt_columns) {
	$cpt_columns['customer'] = "Customer";
	return $cpt_columns;
}
add_action('manage_projects_posts_columns', 'gmg_reminders_add_new_post_column');

//Show custom order column values for projects
function gmg_reminders_posts_show_order_column($name){
	global $post;

	switch ($name) {
		case 'customer':
            $reminder = new Reminder( $post->id );
            $customer = new Customer ( $reminder->get_customer() );
            echo $customer->get_name();
			break;
		default:
			break;
	}
}
add_action('manage_projects_posts_custom_column','gmg_reminders_posts_show_order_column');


//Make Column Sortable for projects
function gmg_reminders_order_column_register_sortable($columns){
	$columns['customer'] = 'Customer';
	return $columns;
}
add_filter('manage_edit-projects_sortable_columns','gmg_reminders_order_column_register_sortable');
