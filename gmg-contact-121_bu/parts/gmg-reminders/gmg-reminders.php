<?php

$fields = get_fields('overall_settings');

if( $fields['service_reminders'] != '' ){
    if( $fields['service_reminders'][0] == 'yes' ){

        $pros = plugin_dir_path( __FILE__ );
        
        //Let's also bring in options to be show in admin
        require_once( $pros . '/includes/gmg-reminders-admin.php' );

        //Let's bring in the custom post type Reminders
        require_once( $pros . '/includes/gmg-reminders-cpt.php' );

        //Let's also bring in the Class Projects
        require_once( $pros . '/includes/gmg-Class-reminders.php' );

        //Let's also bring in the code to save the ACF fields.
        require_once( $pros . '/includes/gmg-reminders-acf.php' );
        
        //Let's also bring in code to load the right page content.
        require_once( $pros . '/includes/gmg-reminders-loader.php');

        //Let's also bring in code to create the right pages if they don't exist.
        require_once( $pros . '/includes/gmg-reminders-creator.php');

        //Let's also bring in the Add Reminders Dashboard widget
        require_once($pros . '/includes/gmg-reminders-add-dashboard.php');

        //Let's also bring in the Import Reminders Dashboard widget
        require_once($pros . '/includes/gmg-reminders-import-dashboard.php');
        
        add_action( 'admin_menu', 'register_gmg_reminders_menu_page', 11 );
        
    }
}