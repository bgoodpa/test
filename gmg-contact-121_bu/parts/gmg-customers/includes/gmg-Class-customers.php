<?php

use Twilio\Rest\Client;

class Customer {
    
    private $fname;
    private $lname;
    
//    private $email;
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
        $this->fname = get_field( 'fname' , $this->id );
        $this->lname = get_field( 'lname' , $this->id );
//        $this->email = get_field( 'email' , $this->id );
    }
    
    public function check_mobile( $id ){
        
    }
    
    
//    Set Customers
    
    public function set_customer_field( $field, $value ){
        
    }
    
    public function update_customer( $info_array ){        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $this->id);
        }
//        error_log( 'Let us fix phones!' );
        $this->fix_phones();        
    }
    
    public function set_fname( $fname ){
        update_field( 'fname', $fname, $this->id);
        $this->fname = $fname;
    }
    
    public function set_lname( $lname ){
        update_field( 'lname', $lname, $this->id);
        $this->fname = $lname;
    }
    
    public function set_email( $email ){
        update_field( 'email', $email, $this->id);
        $this->fname = $email;
    }
    
    public function set_street_address( $street_address ){
        update_field( 'billing_street_address', $street_address, $this->id);
    }
    
    public function set_street_town( $town ){
        update_field( 'billing_town', $town, $this->id);
    }
    
    public function set_address( $state ){
        update_field( 'billing_state', $state, $this->id);
    }
    
    public function set_street_zip( $zip ){
        update_field( 'billing_zip', $zip, $this->id);
    }
    
    public function set_phone( $phone ){
        update_field( 'phone_number', $phone, $this->id);
        $this->fix_phones();
    }
    
    public function update_title(){

        $new_post = array(
            'ID'     => $this->id,
            'post_title'  => $this->fname . ' ' . $this->lname,
            'post_name' => $this->fname . '-' . $this->lname
        );

        wp_update_post( $new_post );        
    }
    
    public function fix_phones(){
        
//        error_log('Fixing phone');
        
        if( get_field( 'phone_number', $this->id ) ){
            
            update_field( 'phone_number' , fix_phone( get_field( 'phone_number', $this->id ) ) , $this->id );
            
        }
        
        if( get_field( 'mobile_number', $this->id ) ){
            
            update_field( 'mobile_number' , fix_phone( get_field( 'mobile_number', $this->id ) ) , $this->id );
            
        }
        
    }
    
    public function set_mobile( $phone ){
//        error_log( 'Set Mobile with ' . $phone );
        $updated_phone = fix_phone( $phone );
//        error_log( 'Update Mobile with ' . $updated_phone );
        update_field( 'mobile_number' , $updated_phone , $this->id );
    }
    
    public function set_service_projects( $id ){
//        error_log( 'Set Service Projects' );
        $current_projects = $this->get_service_projects();
        if( $current_projects ){
            if( is_array( $current_projects ) ){
                $go = false;
                foreach( $current_projects as $current ){
                    if( $current->ID == $id ){
                        $go = true;
                    }
                }
                
                if( $go ){                    
                    $post = get_post( $id );
                    $current_projects[] = $post;
                    update_field( 'field_5acf9f13bab87', $current_projects, $this->id );
                }
            } else {
//                echo 'Not array and it looks like this: ' . $current_projects ;
                if( $current_projects != $id ){
                    update_field( 'field_5acf9f13bab87', array( array ( $id ) ), $this->id );
                }
            }
        } else {
            update_field( 'field_5acf9f13bab87', array( get_post( $id) ), $this->id );
        }
    }
    
    public function set_quote_projects( $id ){
        $current_projects = $this->get_service_projects();
        if( is_array( $current_projects ) ){
            if( !in_array( $id, $current_projects ) ){
                array_push( $current_projects , $id );
                update_field( 'quote_projects', $current_projects, $this->id );
            }
        } else {
            update_field( 'quote_projects', array( $id ), $this->id );
        }
    }
    
    
//    Get Customers
    
    public function get_id(){
        return $this->id;
    }
    
    public function get_email(){
        return get_field( 'email' , $this->id );
    }
    
    public function get_name(){
        return $this->fname . ' ' . $this->lname;
    }
    
    public function get_fname(){
        return $this->fname;
    }
    
    public function get_lname(){
        return $this->lname;
    }
    
    public function get_phone(){
        return get_field( 'phone_number', $this->id );
    }
    
    public function get_street_address(){
        return get_field( 'billing_street_address', $this->id);
    }
    
    public function get_street_town(){
        return get_field( 'billing_town', $this->id);
    }
    
    public function get_address(){
        return get_field( 'billing_state', $this->id);
    }
    
    public function get_street_zip(){
        return get_field( 'billing_zip', $this->id);
    }
    
//    public function get_address(){
//        return get_field( 'address', $this->id );
//    }
    
    public function get_mobile(){
//        error_log('ID is ' . $this->id );
//        error_log('Mobile is ' . get_post_meta( $this->id, 'mobile_number', true ) );
        return get_post_meta( $this->id, 'mobile_number', true );
    }
    
    public function get_service_projects(){
//        return get_field( 'field_5b5b52bc85b1a' , $this->id );
        return get_field( 'field_5acf9f13bab87' , $this->id );
    }
    
    public function get_quote_projects(){
        return get_field( 'quote_projects' , $this->id );
    }
    
    public function get_overall_customer_tags(){
        return get_field( 'overall_tags', $this->id );
    }
    
    public function process_overall_tags( $new_tags ){
        
        $customer_tags = $this->get_overall_customer_tags();
        error_log( 'Process overall customer tags is this big: ' . count( $customer_tags ) );
        
        if( is_array( $customer_tags ) ){
            
            error_log( 'Process overall tags is array this big: ' . count( $customer_tags ) );
            
            foreach ($new_tags as $tag ){
                array_push( $customer_tags , $tag );
                wp_set_post_tags( $this->id, $tag, true );
            }
            
            update_field( 'overall_tags', $customer_tags , $this->id );
            
        } else{
            
            error_log( 'Process overall tags is not array');
            
            update_field( 'overall_tags', $new_tags , $this->id );
            wp_set_post_tags( $this->id, $new_tags, true );
            $customer_tags = $new_tags;
        }
        
        
    }
    
}

    function fix_phone( $phone ){        
//        error_log('Fix phone is ' . $phone );       
        if( strpos( $phone , '-') != false ){
            $phone_numbs = preg_replace("/[^0-9]/", "", $phone);
        } else {
            $phone_numbs = $phone;
        }
        
        if ( strlen($phone_numbs) == 10 ){
//            error_log('Phone is ' . $phone_numbs );
            preg_match( "/([0-9]{3})([0-9]{3})([0-9]{4})/", $phone_numbs, $phone_pieces );
            $phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3];
        } else {
            preg_match( "/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", $phone_numbs, $phone_pieces );
            $phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3] . '-' . $phone_pieces[4];
        }
//        error_log('Fix Phone is now ' . $phone );
        return $phone;
        
    }

class Customers {
    
        public function __construct( ) {
        }
    
    //    Check Customers
    
    public function check_if_customer_exists_by_email( $email ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'customers' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'email',
                    'value'   => $email,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            return true;            
        } else{ return false; }
        
    }
    
    public function check_if_customer_exists_by_phone( $phone_number ){
        
//        error_log( 'Sent down, phone is ' . $phone_number );

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'customers' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'phone_number',
                    'value'   => $phone_number,
                    'compare' => '=',
                ),
            ),
        );

        if( get_posts( $args ) ){
            $post = get_posts( $args )[0];
//            error_log( 'Post is ' . get_the_title( $post ) );
            return true;            
        } else{
//            error_log( 'No Exists!');
            return false;
        }
    }
    
    public function get_customer_by_email( $email ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'customers' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'email',
                    'value'   => $email,
                    'compare' => '=',
                ),
            ),
        );
        
        return get_posts( $args )[0]->ID;
        
    }
    
    public function get_customer_by_phone( $phone_number ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'customers' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'phone_number',
                    'value'   => $phone_number,
                    'compare' => '=',
                ),
            ),
        );
        
        if ( get_posts( $args ) ){
            $post = get_posts( $args )[0];
//            error_log( 'Customer is ' . get_the_title( $post ) . ' and Post is Object is ' . is_object( $post ) );
            return $post->ID;
            
        } else{
//            error_log( 'No post found!');
            return false;
        }       
        
    }
    
    public function create_new_customer( $info_array ){
        
        $my_post = array(
            'post_title'        => $info_array['fname'] . ' ' . $info_array['lname'],
            'post_type'         => 'customers',
            'post_status'       => 'publish',
        );
        
        $new_id = wp_insert_post( $my_post );
        
        foreach( $info_array as $key => $value ){
            add_post_meta( $new_id , $key , $value );
        }
        
        return $new_id;
        
    }
    
    public function get_all_customers(){
         
        $customers = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'customers' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $customers, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $customers;
        
    }
    
    public function get_customers_by_tags( $tags ){
        
//        error_log( 'Inside Promos and Tags with ' . count( $tags ) );
        
        $customers = array();
        
        $args = array(
            'post_type'             => array( 'customers' ),
            'posts_per_page'        => '-1',
            'orderby'               => 'title',
            'order'                 => 'ASC',
            'post_status'           => 'publish',
            );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                
                $go = false;
                
                $query->the_post();
                $id = get_the_ID();
                $customer = new Customer($id);
                $customer_tags = $customer->get_overall_customer_tags();
                
                if( $customer_tags != null ){
                    
                    //It might be an array of tags.
                
                    if( is_array( $customer_tags ) ){
                
                        foreach( $tags as $tag ){

                            if( in_array( $tag , $customer_tags ) ){

                                error_log('Tag ID ' . $tag . ' is found!' );

                                if( !in_array( $customer->get_email() , $customers ) ){

                                    array_push( $customers, $customer->get_email() );
                                }
                            }
                        }
                    } else {
                        
                        //Or it might just be a single
                        foreach( $tags as $tag ){
                        
                            if( $customer_tags === $tag ){

                                error_log('Tag ID ' . $tag . ' is found!' );

                                if( !in_array( $customer->get_email() , $customers ) ){

                                    array_push( $customers, $customer->get_email() );
                                }
                            }
                        }
                    }
                }
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( count( $customers ) > 0 ){
            
            return $customers;
            
        } else {
            
            return false;
        }
        
    }
    
    public function get_states(){
        
        return array('AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY', 'AE', 'AA', 'AP');
    }
    
}
