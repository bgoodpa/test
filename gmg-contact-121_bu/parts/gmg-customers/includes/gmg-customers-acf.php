<?php

add_filter('acf/save_post' , 'gmg_customer_meta_save', 10, 1 );
function gmg_customer_meta_save( $post_id ) {
    
    if( get_post_type( $post_id ) == 'customers'){
    
        error_log('Inside Customer Meta Save!');

        //Check it's not an auto save routine
         if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
              return;

        //Perform permission checks! For example:
        if ( !current_user_can('edit_post', $post_id) ) 
              return;

        // Check if it's not a revision.
        if ( wp_is_post_revision( $post_id ) )
            return;
        
        $customer = new Customer( $post_id );     
        $customer->fix_phones();
        
//        echo var_dump( $customer->get_service_projects() );
        
        //If calling wp_update_post, unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'gmg_customer_meta_save');
        
        $customer->update_title();

        // re-hook this function
        add_action('save_post', 'gmg_customer_meta_save');
    }
    
}