<?php

$gst = plugin_dir_path( __FILE__ );

//Let's bring in the Customer Class.
require_once($gst . '/includes/gmg-Class-customers.php');

//Let's also bring in the code to create the Customer CPT.
require_once($gst . '/includes/gmg-customers-cpt.php');

//Let's also bring in a way for you to export all customers posts.
require_once($gst . '/includes/gmg-customers-export.php');

//Let's also bring in the code to save the ACF fields.
require_once($gst . '/includes/gmg-customers-acf.php');

//Let's also bring in the code to create Customer ACF fields.
//require_once($gst . '/includes/gmg-customers-admin.php');

//Let's also bring in the dashboard widget
//require_once($revs . '/includes/gmg-customers-info-dashboard.php');
