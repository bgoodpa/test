jQuery(document).ready(function ($) {
    "use strict";
    var form = $('#export_guests_posts');
    form.on('submit', function (e) {
        e.preventDefault();
        alert('Success');
        $.ajax({
            url: ajax_objecty.ajaxurl,
            type: 'post',
            data: {
                action: 'gmg_export_all_guests',
                form: form,
            },
            success: function (response) {
                if (response['foo']) {
                    alert('Success' + response['foo']);
                } else {
                    alert('Problems' + response['errors']);
                }

            },
            error: function (errorThrown) {
                alert('Error' + errorThrown);
            }
        });
    });
});
