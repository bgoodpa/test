<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://goodmarketinggroup.com/
 * @since      1.0.0
 *
 * @package    Gmg_Contact_121
 * @subpackage Gmg_Contact_121/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gmg_Contact_121
 * @subpackage Gmg_Contact_121/admin
 * @author     Good Group LLC <info@goodmarketinggroup.com>
 */
class Gmg_Contact_121_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Contact_121_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Contact_121_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gmg-contact-121-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Contact_121_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Contact_121_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gmg-contact-121-admin.js', array( 'jquery' ), $this->version, false );
        wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	}  
    
    
    /**
     * Registers and Defines the necessary fields we need.
     *
     */
    public function register_gmg_contact_121_menu_pages(){
        
        add_menu_page(
            __( 'GMG Contact 1:1', $this->plugin_name ),              // page title
            __( 'Contact 1:1', $this->plugin_name ),                  // menu title
            'publish_posts',                                               // capability
            $this->plugin_name,                                             // menu_slug
            array( $this, 'display_gmg_contact_121_plugin_intro_page' ),       // callable function
            'dashicons-email-alt',
            8       // Menu Position
        );
        
        add_submenu_page(
            $this->plugin_name,                                     // parent slug
            __( 'Contact 1:1 Programs', $this->plugin_name ),              // page title
            __( 'Programs', $this->plugin_name ),                  // menu title
            'publish_posts',                                               // capability
            $this->plugin_name .'-programs',                                             // menu_slug
            array( $this, 'display_gmg_contact_121_plugin_intro_page' )       // callable function
        );
        
        acf_add_options_sub_page(array(
            'parent_slug' 	=> $this->plugin_name,
            'page_title' 	=> 'GMG Contact 1:1 Overall Settings ',
            'menu_title' 	=> 'Overall Settings ',
            'post_id'            => 'overall_settings',
            'capability' 	=> 'manage_options'
        ));
        
    }
    
    public function gmg_contact_121_overall_plugin_settings_save(){
        error_log( 'Overall Plugin Settings Save' );
    }
    
    public function gmg_contact_121_plugin_settings_save(){

        register_setting( $this->plugin_name, $this->plugin_name, array($this, 'plugin_overall_options_validate') );
        
    }
    
            /**
     * Sanitises all input fields.
     *
     */
    public function plugin_overall_options_validate($input) {
        
        $newinput = array();

        return $newinput;
    }
    
    public function display_gmg_contact_121_plugin_intro_page(){
        
        ?>

<h2>Welcome to GMG Contact 1:1 Plugin</h2>
<p>This plugin was created by Good Marketing Group staff to implement programs that are specific to the Trade & Services Industries.</p>
<p>These are the following platforms that fall under Contact 1:1:</p>
<table>
    <tr>
        <?php        
            $fields = get_fields('overall_settings');
            if( isset( $fields['reputation_management'] )){
                if( $fields['reputation_management'][0] == 'yes' ){
                    echo '<td><p class="subscribed">Subscribed</p></td>';
                } else {
                    echo '<td><p class="not-subscribed">Not Subscribed</p></td>';
                }
            }

                ?>

        <td><strong>Reputation Management</strong>: A platform designed to encourage satisfied customers to leave reviews not only the website but also on social media. Using a custom-build Email Marketing journey program, we can nudge customers to leave reviews and spread word-of-mouth.
            <a href="https://goodmarketinggroup.com/reputation-management/" target="_blank" class="button-primary">Learn More</a></td>

    </tr>

    <tr>
        <?php
            if( isset( $fields['service_reminders'] ) && $fields['service_reminders'][0] == 'yes' ){
                echo '<td><p class="subscribed">Subscribed</p></td>';
            } else {
                echo '<td><p class="not-subscribed">Not Subscribed</p></td>';
            }

                ?>

        <td><strong>Service Reminders</strong>: A platform that reminds customers about scheduling annual appointments as well as upcoming schedule appointments. Utilizes texting to reach customers on thier phones.<a href="https://goodmarketinggroup.com/service-reminders/" target="_blank" class="button-primary">Learn More</a></td>

    </tr>


    <tr>
        <?php
            if( isset( $fields['capture_&_connect'] ) && $fields['capture_&_connect'][0] == 'yes' ){
                echo '<td><p class="subscribed">Subscribed</p></td>';
            } else {
                echo '<td><p class="not-subscribed">Not Subscribed</p></td>';
            }

                ?>
        <td><strong>Capture and Connect</strong>: A platform that captures every customer that fills out forms and sends them down to specific Email Marketing journies. For companies with many product lines, this platform allows companies to individually market to customers.</td>
    </tr>

    <tr>
        <?php
            if( isset( $fields['lead_chaser'] ) && $fields['lead_chaser'][0] == 'yes' ){
                echo '<td><p class="subscribed">Subscribed</p></td>';
            } else {
                echo '<td><p class="not-subscribed">Not Subscribed</p></td>';
            }
                ?>
        <td><strong>Lead Chaser</strong>: A platform that allows sales staff to enter customer leads and follow up with them automatically. Using a custom-build Email Marketing journey program, we can nudge customers to respond to the lead inquiries and close sales.</td>
    </tr>

    <tr>
        <?php
            if( isset( $fields['boomerang'] ) && $fields['boomerang'][0] == 'yes' ){
                echo '<td><p class="subscribed">Subscribed</p></td>';
            } else {
                echo '<td><p class="not-subscribed">Not Subscribed</p></td>';
            }

                ?>
        <td><strong>Boomerang</strong>: A platform that allows clients to send down lists of customers to our email marketing platform and cross-market those customers with products and services that they are already primed for.</td>
    </tr>

    <tr>
        <td></td>
        <td><strong>Build a Project</strong>: Coming Soon</td>
    </tr>
</table>

<p>If you would like to learn more about any of these programs that you are not using, please contact us at 484-902-8914.</p>

<?php
        
    }
}