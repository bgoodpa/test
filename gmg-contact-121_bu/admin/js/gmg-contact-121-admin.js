jQuery(document).ready(function ($) {
	"use strict";
    
    var update_select = $('#webdev_form_project_ids');
    update_select.change( function (h) {
        h.preventDefault();
        
        var form_project = $("#webdev_form_project_ids  option:selected").val();
        $('#webdev_form_svc_frequency').trigger("reset");
        
//        $("#update_customer_box").css("display","none");
        
        $.ajax({
            url : ajax_object.ajaxurl,
            type : 'post',
            data : {
                action : 'admin_chosen_projects',
                project : form_project              
            },
            success:function( response ){
                if(response['foo']){
//                    alert( 'Success' + response['foo']);
                    
//                    $("#project_id").val( response['id'] );                    
                    $("#webdev_form_fname").val( response['fname'] );
                    $("#webdev_form_lname").val( response['lname'] );
                    $("#webdev_form_email").val( response['email'] );
                    $("#webdev_form_number").val( response['phone'] );
                    $("#webdev_form_project_address").val( response['street_address'] );
                    $("#webdev_form_project_town").val( response['town'] );
                    $("#webdev_form_project_state").val( response['state'] );
                    $("#webdev_form_project_zip").val( response['zip'] );
                    var freq = response['freq'];
                    console.log( 'The freq value is ' + $("#webdev_form_svc_frequency[value='" + freq + "']").val() );
                    $("#webdev_form_svc_frequency[value='" + freq + "']").prop( "checked" , true );
                    $("#webdev_form_notes").val( response['notes'] );
                    
                    if( response['type'] == 'svc' ){
                        $("#webdev_form_svc_date").val( response['last_service'] );
                        $("#webdev_form_appt_date").val('');
                        $("#webdev_form_appt_time").val('');                      
                        
                    }
                    
                    if( response['type'] == 'appt' ){                        
                        $("#webdev_form_appt_date").val( response['appt_date'] );
                        $("#webdev_form_appt_time").val( response['appt_time'] );
                        $("#webdev_form_svc_date").val('');
                    }
                } else{
                    alert( 'Problems' + response['errors']);
//                    $('#webdev_form_response').html( response['errors'] );
                    $('#webdev_form_response').css("visibility","visible");
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            }
        });       
	});
    
    var update_c_select = $('#webdev_form_customer_ids');
    update_c_select.change( function (i) {
        i.preventDefault();
        
        $("#update_project_box").css("display","none");
        
        var form_customer = $("#webdev_form_customer_ids  option:selected").val();
//        $("#update_customer_box").css("visibility","hidden");
        
        $.ajax({
            url : ajax_object.ajaxurl,
            type : 'post',
            data : {
                action : 'admin_chosen_customers',
                customer : form_customer              
            },
            success:function( response ){
                if(response['foo']){
                    
                    $("#hidden-box").css("visibility","visible");
//                    alert( 'Success' + response['foo']);
                    
                    var $i = 0;
                    console.log( response['project-' + $i ] );
                    if( response['project-' + $i ] ){
                        console.log( ' Inside projects!');
                        var cust_proj_select = $('#webdev_form_customer_proj_ids');
                        cust_proj_select.empty();
                        while( response['project-' + $i ] ){
                            console.log( ' Try to set objects' + response['project-' + $i ] );
                            
                            cust_proj_select.append( $('<option>', {
                                value: response['project-' + $i ],
                                text : response['project-' + $i ]
                            }));
                            $i++;
                        
                        }
                        
                        $("#webdev_form_name").val( response['name'] );
                        $("#webdev_form_email").val( response['email'] );
                        $("#webdev_form_number").val( response['phone'] );
                        $("#webdev_form_customer_address").val( response['address'] );  
                    }
                    
                } else{
                    alert( 'Problems' + response['errors']);
//                    $('#webdev_form_response').html( response['errors'] );
                    $('#webdev_form_response').css("visibility","visible");
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            }
        });       
	});
    
    var update_q_select = $('#webdev_form_quote_ids');
    update_q_select.change( function (j) {
        j.preventDefault();
        
//        alert( 'Got it!' );
        
        var form_quote = $("#webdev_form_quote_ids  option:selected").val();
        
        $.ajax({
            url : ajax_object.ajaxurl,
            type : 'post',
            data : {
                action : 'admin_chosen_quotes',
                quote : form_quote              
            },
            success:function( response ){
                if(response['foo']){
                        
                    $("#quoteFName").val( response['fname'] );
                    $("#quoteLName").val( response['lname'] );
                    $("#quoteJName").val( response['jname'] );
                    $("#quoteEmail").val( response['email'] );
                    $("#quoteDate").val( response['quotedate'] );
                    $("#quoteCost").val( response['quotecost'] );
                    $("#quoteSales").val( response['quotesales'] );
                    
                } else{
                    alert( 'Problems' + response['errors']);
            
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            }
        });       
	});
    
});